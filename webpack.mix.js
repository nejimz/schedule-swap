let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js([
	'resources/assets/js/app.js'
	], 'public/js');

mix.sass(
	'resources/assets/sass/app.scss'
	, 'public/css');

mix.copyDirectory(
	'node_modules/font-awesome/fonts', 
	'public/fonts');

mix.sass(
	'resources/assets/sass/admin.scss'
	, 'public/css');

mix.styles([
	'node_modules/jquery-ui-bundle/jquery-ui.min.css',
	'node_modules/jquery-ui-bundle/jquery-ui.structure.min.css',
	'node_modules/jquery-ui-bundle/jquery-ui.theme.min.css'
	], 'public/css/jqueryui.css');

mix.scripts([
	'resources/assets/js/password-meter.js'
	], 'public/js/password-meter.js');

mix.babel([
	'resources/assets/js/bulma-extensions/bulma-calendar.js'
	], 'public/js/bulma-calendar.js');

mix.scripts([
	'node_modules/moment/moment.js'
], 'public/js/moment.js');

mix.scripts([
	'resources/assets/js/idle-timer.js'
	], 'public/js/idle-timer.js');

// mix.scripts([
// 	'node_modules/jquery/dist/jquery.min.js',
// ], 'public/js/jquery.min.js');