@extends('layouts.app')

@section('content')

    <section class="hero is-info">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">
                  Forgot Password (Security Questions)
                </h1>
            </div>
        </div>
    </section>
    
    @if(count($user->questions) == 0)
    <div class="columns is-marginless is-centered">
        <div class="column is-5">
            <div class="notification is-danger has-text-centered">
                No security question has been setup on this account.
                <br>
                <a href="{{ URL::previous() }}" class="is-right">Back</a>
            </div>
        </div>
    </div>
    @else
    <div class="columns is-marginless is-centered">
        <div class="column is-5">
            <div class="card">
                <header class="card-header">
                    <p class="card-header-title">Answer either of the follwing questions.</p>
                </header>

                <div class="card-content">
                    <form class="forgot-password-form" method="POST" action="{{ route('password.security.reset') }}">

                        <input type="hidden" readonly="true" name="id" value="{{ $user->id }}" >
                        
                        {{ csrf_field() }}

                        <div class="field">
                            <p class="control">
                                <div class="control">
                                    <div class="select is-fullwidth">
                                        <select name="question" id="question">
                                            <option value="">-- Select Security Questions --</option>
                                            @foreach($user->questions as $question)
                                                <option value="{{ $question['pivot']['id'] }}">{{ $question['question'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>                                
                            </p>
                             @if ($errors->has('question'))
                                <p class="help is-danger">
                                    {{ $errors->first('question') }}
                                </p>
                            @endif
                        </div>
                        <div class="field">
                            <label class="label">Your answer.</label>
                        </div>
                        <div class="field">
                            <p class="control">
                                <div class="control">
                                   <input class="input is-fullwidth" id="answer" type="text" name="answer"
                                       value="" required autofocus>
                                    <input class="input is-fullwidth" id="user" type="hidden" name="user"
                                       value="{{ Crypt::encryptString(json_encode($user)) }}" required autofocus>
                                </div>                                
                            </p>
                            @if ($errors->has('answer'))
                                <p class="help is-danger">
                                    {{ $errors->first('answer') }}
                                </p>
                            @endif
                        </div>                        
                        <div class="field">
                            <div class="control has-text-centered">
                                <button type="submit" class="button is-info is-fullwidth">Next</button>
                            </div>
                        </div>

                        <div class="field" style="margin-top:40px;">
                            <div class="control">
                               <a href="{{ route('password.security') }}" class="button is-danger is-fullwidth">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
