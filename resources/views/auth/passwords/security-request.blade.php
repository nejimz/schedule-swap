@extends('layouts.app')

@section('content')

    <section class="hero is-info">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">
                  Forgot Password (Security Questions)
                </h1>
            </div>
        </div>
    </section>

    <div class="columns is-marginless is-centered">
        <div class="column is-5">
            <div class="card">
                <header class="card-header">
                    <p class="card-header-title">Did you forgot your password?</p>
                </header>

                <div class="card-content">
                    @if (session('status'))
                        <div class="notification">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="forgot-password-form" method="POST" action="{{ route('password.security.request') }}">

                        {{ csrf_field() }}
                        <div class="field">
                            <label class="label">Enter your username.</label>
                        </div>
                        <div class="field">
                            <p class="control">
                                <div class="control has-icons-left has-icons-right">
                                    <input class="input is-fullwidth" id="username" type="username" name="username"
                                       value="{{ old('username') }}" required autofocus>
                                    <span class="icon is-small is-left">
                                      <i class="fa fa-user"></i>
                                    </span>
                                </div>                                
                            </p>

                            @if ($errors->has('username'))
                                <p class="help is-danger">
                                    {{ $errors->first('username') }}
                                </p>
                            @endif
                        </div>

                        <div class="field">
                            <div class="control has-text-centered">
                                <button type="submit" class="button is-info is-fullwidth">Next</button>
                            </div>
                        </div>
                        <div class="field" style="margin-top:40px;">
                            <div class="control">
                               <a href="{{ route('password.request') }}" class="button is-danger is-fullwidth">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
