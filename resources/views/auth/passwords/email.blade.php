@extends('layouts.app')

@section('content')

    <section class="hero is-info">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">
                   Forgot Password
                </h1>
            </div>
        </div>
    </section>

    <div class="columns is-marginless is-centered">
        <div class="column is-5">
            <div class="card">
                <header class="card-header">
                    <p class="card-header-title">Did you forgot your password?</p>
                </header>

                <div class="card-content">
                    @if (session('status'))
                        <div class="notification">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="forgot-password-form" method="POST" action="{{ route('password.email', [$center = Request::segment(2)]) }}">

                        {{ csrf_field() }}
                        <div class="field">
                            <label class="label">E-Mail Address</label>
                        </div>
                        <div class="field">
                            <p class="control">
                                <div class="control has-icons-left has-icons-right">
                                    <input class="input is-fullwidth" id="email" type="email" name="email"
                                       value="{{ old('email') }}" required autofocus>
                                    <input type='hidden' name="center" value="{{ Request::segment(2) }}">
                                    <span class="icon is-small is-left">
                                      <i class="fa fa-envelope"></i>
                                    </span>
                                </div>
                            </p>

                            @if ($errors->has('email'))
                                <p class="help is-danger">
                                    {{ $errors->first('email') }}
                                </p>
                            @endif
                        </div>

                        <div class="field">
                            <div class="control has-text-centered">
                                <button type="submit" class="button is-info is-fullwidth">Send Password Reset Link</button>
                                 <a href="{{ route('password.security') }}" class="has-text-link">I don't have access to my e-email right now.</a>
                            </div>
                        </div>
                        <div class="field" style="margin-top:40px;">
                            <div class="control">
                                @if(Request::segment(3) == "")
                                    <a href="{{ route('landing') }}" class="button is-danger is-fullwidth">Back</a>
                                @else
                                    <a href="{{ route('center.login', Request::segment(3)) }}" class="button is-danger is-fullwidth">Back</a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
