@extends('layouts.app')

@section('content')

    <section class="hero is-info">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">
                    {{ config('app.name') }} 
                </h1>
            </div>
        </div>
    </section>

    <div class="columns is-marginless is-centered">
        <div class="column is-4">
            <div class="card">
                <header class="card-header">
                    <p class="card-header-title">Login</p>
                </header>

                <div class="card-content">
                    <form class="login-form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="center" value="{{ $center or 'bac' }}">
                        <div class="field">
                            <label class="label">Username</label>
                        </div>
                        <div class="field">
                            <p class="control">
                                <div class="control has-icons-left has-icons-right">
                                    <input class="input" id="username" type="text" name="username" placeholder="Username" value="{{ old('username') }}" required autofocus>
                                    <span class="icon is-small is-left">
                                      <i class="fa fa-user"></i>
                                    </span>
                                </div>                                
                            </p>

                             @if ($errors->has('username'))
                                <p class="help is-danger">
                                    {{ $errors->first('username') }}
                                </p>
                            @endif
                        </div>

                        <div class="field">
                            <label class="label">Password</label>
                        </div>
                        <div class="field">
                            <p class="control">
                                <div class="control has-icons-left has-icons-right">
                                    <input class="input" id="password" type="password" name="password" placeholder="Password" required>
                                    <span class="icon is-small is-left">
                                      <i class="fa fa-key"></i>
                                    </span>
                                </div>                                
                            </p>

                            @if ($errors->has('password'))
                                <p class="help is-danger">
                                    {{ $errors->first('password') }}
                                </p>
                            @endif
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        <label class="checkbox">
                                            <input type="checkbox" name="policy"> I agree to the <a href="{{ route('policy') }}" target="_blank"> Terms and Conditions and Privacy Policy</a> of Panasiatic Enterprise. 
                                        </label>
                                    </p>    
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        <label class="checkbox">
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                        </label>
                                    </p>
                                </div>
                            </div>                            
                        </div>
                        <div class="field is-horizontal">
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        <button type="submit" class="button is-info">Login</button>
                                        @if(Route::getCurrentRoute()->getName() == "landing")
                                            <a href="{{ route('password.request') }}">Forgot Your Password?</a>
                                        @else
                                            <a href="{{ route('password.request', $center) }}">Forgot Your Password?</a>
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                                                        
                            
                        @if ($errors->has('policy'))                            
                            <p class="help is-danger">
                                {{ $errors->first('policy') }}
                            </p>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
