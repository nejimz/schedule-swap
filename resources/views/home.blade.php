@extends('layouts.master')

@section('content')
<?php
$un_list = ['cebtestwf', 'cebtestagent01', 'cebtestagent02', 'cebtestagent03', 'jaasong', 'dgquijote', 'jctillaflor', 'nbazaragoza', 'mmsapalo'];
?>

  <div class="container">
    @if(env('FORECAST', false))
      <div class="columns">
        <div class="column is-12">
          <div class="notification is-warning has-text-centered">
            <h3 class="title">VOT/VTO SLOTS OFFERED</h3>
          </div>
        </div>
      </div>
    @endif
    <div class="columns">
      <div class="column">
      @if(Auth::user()->isAgent())
          <a class="button is-link is-large" style="width:100%; margin:5px; height: 75px;" href="{{ route('shift-swap.index') }}">
            <span>Shift Swap</span>
          </a>
          <a class="button is-link is-large" style="width:100%; margin:5px; height: 75px;" href="{{ route('vot.index') }}">
            <span>Voluntary Overtime</span>
          </a>
          <a class="button is-link is-large" style="width:100%; margin:5px; height: 75px;" href="{{ route('vto.index') }}">
            <span>Voluntary Time-Off</span>
          </a>
      @endif
      @if(Auth::user()->isAdmin())
          <a class="button is-link is-large" style="width:100%; margin:5px; height: 75px;" href="{{ route('forecast.index') }}">
            <span>Forecast Voluntary Overtime / Time-Off</span>
          </a>
      @endif
      </div>
      <div class="column">
        <div class="card">
          <div class="card-header">
            <p class="card-header-title">My Information</p>
          </div>
          <div class="card-content is-paddingless">
            <table class="table is-narrow is-fullwidth no-border">
              <tbody>
                <tr>
                  <td width="40%">Avaya</td>
                  <td id="info_avaya" width="60%"></td>
                </tr>
                <tr>
                  <td>Name</td>
                  <td id="info_name">{{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}</td>
                </tr>
                <tr>
                  <td>Tier</td>
                  <td id="info_tier"></td>
                </tr>
                <tr>
                  <td>Work Pattern</td>
                  <td id="info_work_pattern"></td>
                </tr>
                <tr>
                  <td>Skill</td>
                  <td id="info_skill"></td>
                </tr>
              </tbody>
            </table>

          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <p class="card-header-title">My Schedule</p>
          </div>
          <div class="card-content is-paddingless">
            <table id="my_schedule_table" class="table is-narrow is-fullwidth">
              <thead>
                <tr>
                  <th width="30%">Shift Date</th>
                  <th width="40%">Shift Schedule (EST)</th>
                  <th width="30%"></th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
    var xhr_information = null;
    information();

    function information()
    {
      xhr_information = $.ajax({
        type : 'get',
        url : '{{ route("schedule_json", Auth::user()->id) }}',
        cache : false,
        dataType : "json",
        beforeSend: function(xhr) {
          $('#my_schedule_table tbody').html('<tr><td colspan="3" class="has-text-centered"><img src="{{ asset("img/loading/2.gif") }}" title="Loading..." /></td></tr>');
          if (xhr_information != null){ xhr_information.abort(); }
        }
      }).done(function(result_set) {
        schedule(result_set['schedules']);
        $('#info_avaya').html(result_set['information']['avaya']);
        $('#info_tier').html(result_set['information']['tier']);
        $('#info_work_pattern').html(result_set['information']['wp']);
        $('#info_skill').html(result_set['information']['skill']);
      }).fail(function(jqXHR, textStatus) {
        var errors = error_message(jqXHR['responseJSON']['errors']);
      });
    }

    function schedule(rows)
    {
      var table = '';
      $.each(rows, function(key, value){
        table += '<tr><th colspan="3" class="has-text-centered">Week Ending ' + key + '</th></tr>';
        $.each(value, function(k, v){

          var icon = '';
          var voluntary_request = '';
          var schedule = v['schedule'];
          var shift_date_format = v['shift_date_format'];
          if(v['restrictions'] != null)
          {
            icon = '<i class="fa fa-ban" title="' + v['restrictions'] + '"></i>';
          }

          if(v['new_shift_date'] == '' && v['new_schedule'] != '')
          {
            //icon = '<i class="fa fa-star-o" title="New Schedule ' + v['new_schedule'] + '"></i>';
            schedule = v['new_schedule'];
            shift_date_format = v['shift_date_format'];
          }
          else if(v['new_shift_date'] != '' && v['new_schedule'] != '')
          {
            //icon = '<i class="fa fa-star-o" title="New Schedule ' + v['new_schedule'] + '"></i>';
            schedule = v['new_schedule'];
            shift_date_format = v['shift_date_format'];
          }

          if(v['vots'].length > 0)
          {
            voluntary_request += '' + voluntary_list('Overtime', v['vots']);
          }

          if(v['vtos'].length > 0)
          {
            voluntary_request += '' + voluntary_list('Timeoff', v['vtos']);
          }
          
          table += '<tr><td>' + shift_date_format + '</td><td>' + schedule + '</td><td>' + voluntary_request + '</td></tr>';
        });

      });

      $('#my_schedule_table tbody').html(table);
    }

    function error_message(errors)
    {
      var messages = '';
      $.each(errors, function(key, value){
        messages += '' + value[0] + '<br />';
      });
      return messages;
    }

    function voluntary_list(type, rows)
    {
      var list = '<span class="tag is-dark" title="' + type + '">' + type + '</span><br>' ;
      $.each(rows, function(key, value){
        list += '<span class="tag is-info" title="' + value + '">' + value + '</span><br>' ;
      });

      return list;
    }
    </script>
@endsection
