@extends('layouts.master')

@section('content')

<style type="text/css">
  table.no-border td{
    border: none!important;
  }
</style>

<div class="container">
  <!--div class="columns">
    <div class="column is-12">
      <h4 class="title is-4"><i class="fas fa-asterisk fa-lg"></i>&nbsp;Shift Swap</h4>
    </div>
  </div-->
  <div class="columns">
    <div class="column is-7">
      <div class="card">
        <div class="card-header">
          <label class="card-header-title label" for="search_avaya">Search Avaya</label>
        </div>
        <div id="my-shift-swap-request-details" class="card-content">

          <form id="search_avaya_form" method="get" action="{{ route('ss_search_json') }}">

            <div class="field has-addons">
              <div class="control has-icons-left is-expanded">
                <input class="input" type="text" id="search_avaya" name="search_avaya" autocomplete="off" />
                <span class="icon is-small is-left">
                  <i class="fa fa-search"></i>
                </span>
              </div>
              <div class="control">
                <button class="button is-info" type="submit">Search</button>
              </div>
            </div>

          </form>
        </div>
      </div>

      <div id="ss_message"></div>
      <br />
      <div id="shift_swap_card" class="card" style="display: none;">
        <div class="card-header">
          <p class="card-header-title">Shift Swap</p>
        </div>
        <div class="card-content">

          <form id="shift_swap_form" method="post" action="{{ route('shift-swap.store') }}" class="">

            <div class="columns field is-horizontal">
              <div class="column is-4">
                <div class="field-label is-normal">
                  <label class="label" for="swap_type">Swap Type</label>
                </div>
              </div>
              <div class="column is-4 field-body">
                <div class="field is-narrow">
                  <div class="control">
                    <div class="select is-fullwidth">
                      <select id="swap_type" name="swap_type">
                        <option value="">-- Select --</option>
                        @foreach($swap_types as $row)
                        <option value="{{ $row->abbr }}">{{ $row->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="columns field is-horizontal">
              <div class="column is-4">
                <div class="field-label is-normal">
                  <label class="label" for="week_ending">Week Ending</label>
                </div>
              </div>
              <div class="column is-4 field-body">
                <div class="field is-narrow">
                  <div class="control">
                    <div class="select is-fullwidth">
                      <select id="week_ending" name="week_ending">
                        <option value="">-- Select --</option>
                        @foreach($week_endings as $row)
                        <option value="{{ $row->week_ending }}">{{ $row->week_ending }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="columns field is-horizontal field-not-2rd">
              <div class="column is-4">
                <div class="field-label is-normal">
                  <label class="label" for="my_shift_date">My Shift Date</label>
                </div>
              </div>
              <div class="column is-4 field-body">
                <div class="field is-narrow">
                  <div class="control">
                    <div class="select is-fullwidth">
                      <select id="my_shift_date" name="my_shift_date">
                        <option value="">-- Select --</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="columns field is-horizontal field-not-2rd">
              <div class="column is-4">
                <div class="field-label is-normal">
                  <label class="label" for="my_schedule">My Schedule</label>
                </div>
              </div>
              <div class="column is-4 field-body">
                <div class="field">
                  <div class="control">
                    <input class="input" type="text" id="my_schedule" name="my_schedule" placeholder="" readonly="">
                  </div>
                </div>
              </div>
            </div>

            <div class="columns field is-horizontal">
              <div class="column is-4">
                <div class="field-label is-normal">
                  <label class="label" for="buddy_avaya">Buddy Avaya</label>
                </div>
              </div>
              <div class="column is-4 field-body">
                <div class="field">
                  <div class="control">
                    <input class="input" type="text" id="buddy_avaya" name="buddy_avaya" placeholder="" readonly="" />
                  </div>
                </div>
              </div>
            </div>

            <div class="columns field is-horizontal">
              <div class="column is-4">
                <div class="field-label is-normal">
                  <label class="label" for="buddy_name">Buddy Name</label>
                </div>
              </div>
              <div class="column is-6 field-body">
                <div class="field">
                  <div class="control">
                    <input class="input" type="text" id="buddy_name" name="buddy_name" placeholder="" readonly="" />
                  </div>
                </div>
              </div>
            </div>

            <div class="columns field is-horizontal field-not-2rd">
              <div class="column is-4">
                <div class="field-label is-normal">
                  <label class="label" for="buddy_shift_date">Buddy Shift Date</label>
                </div>
              </div>
              <div class="column is-4 field-body">
                <div class="field is-narrow">
                  <div class="control">
                    <div class="select is-fullwidth">
                      <select id="buddy_shift_date" name="buddy_shift_date">
                        <option value="">-- Select --</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="columns field is-horizontal field-not-2rd">
              <div class="column is-4">
                <div class="field-label is-normal">
                  <label class="label" for="buddy_schedule">Buddy Schedule</label>
                </div>
              </div>
              <div class="column is-4 field-body">
                <div class="field">
                  <div class="control">
                    <input class="input" type="text" id="buddy_schedule" name="buddy_schedule" placeholder="" readonly="">
                  </div>
                </div>
              </div>
            </div>

            <div class="columns field is-horizontal">
              <div class="column is-4">
                <div class="field-label is-normal"></div>
              </div>
              <div class="column is-5 field-body">
                <div class="field">
                  <div class="control">
                    {!! csrf_field() !!}
                    <button class="button is-info" type="submit">Submit</button>
                  </div>
                </div>
              </div>
            </div>

          </form>

        </div>
      </div>

      <div id="shift_swap_details_card" class="card" style="display: none;">
        <table class="table is-bordered is-narrow is-fullwidth">
          <thead>
            <tr>
              <th width="50%">My Shift Swap Request Details</th>
              <td width="50%" class="has-text-right" id="shift_swap_type"></td>
            </tr>
            <tr>
              <td><span class="tag">Tracking Number</span></td>
              <td class="has-text-right" id="shift_swap_tracking_number"></td>
            </tr>
            <tr>
              <td><span class="tag">Created At</span></td>
              <td class="has-text-right" id="shift_swap_created_at"></td>
            </tr>
            <tr>
              <td><span class="tag">Expired At</span></td>
              <td class="has-text-right" id="shift_swap_expired_at"></td>
            </tr>
            <tr>
              <td><span class="tag">Buddy Name</span></td>
              <td class="has-text-right" id="shift_swap_buddy_name"></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td colspan="2">&nbsp;</td>
            <tr>
            <tr>
              <th class="has-text-centered">My Old Schedule</th>
              <th class="has-text-centered">My New Schedule</th>
            </tr>
            <tr>
              <td id="shift_swap_details_my_old_schedule" class="has-text-centered"></td>
              <td id="shift_swap_details_my_new_schedule" class="has-text-centered"></td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            <tr>
            <tr>
              <th class="has-text-centered">Buddy Old Schedule</th>
              <th class="has-text-centered">Buddy New Schedule</th>
            </tr>
            <tr>
              <td id="shift_swap_details_buddy_old_schedule" class="has-text-centered"></td>
              <td id="shift_swap_details_buddy_new_schedule" class="has-text-centered"></td>
            </tr>
            <tr id="shift_swap_accept_row_form" style="display: none;">
              <td colspan="2">
                <br />
                <form id="shift_swap_confirmation_form" method="post" action="{{ route('ss_confirmation') }}">
                  <div class="notification is-info">
                    <label class="checkbox">
                      <input type="hidden" name="id" id="request_id" value="">
                      <input type="hidden" name="accepted" value="0">
                      <input type="checkbox" id="accepted" name="accepted" value="1"> I confirm the new schedule. I commit to adhere to the new schedule stated above. I understand that once the new schedule is accepted, it is final. Failure to comply shall be subject to Attendance Policy.</label>
                  </div>
                  <p class="has-text-centered">
                    {!! csrf_field() !!}
                    <button type="submit" class="button is-success" id="shift_swap_accept_button" name="confirmation" value="accept" disabled="">I Accept</button>
                    <button type="submit" class="button is-danger" id="shift_swap_decline_button" name="confirmation" value="denie">Decline</button>
                  </p><br />
                </form>
              </td>
            <tr>
          </tbody>
        </table>
      </div>
    </div>

    <div class="column is-5">
      <div class="card">
        <div class="card-header">
          <p class="card-header-title">My Information</p>
        </div>
        <div class="card-content is-paddingless">
          <table class="table is-narrow is-fullwidth no-border">
            <tbody>
              <tr>
                <td width="40%">Avaya</td>
                <td id="info_avaya" width="60%"></td>
              </tr>
              <tr>
                <td>Name</td>
                <td id="info_name">{{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}</td>
              </tr>
              <tr>
                <td>Tier</td>
                <td id="info_tier"></td>
              </tr>
              <tr>
                <td>Work Pattern</td>
                <td id="info_work_pattern"></td>
              </tr>
              <tr>
                <td>Skill</td>
                <td id="info_skill"></td>
              </tr>
            </tbody>
          </table>

        </div>
      </div>

      <div class="card">
        <div class="card-header">
          <p class="card-header-title">My Schedule</p>
        </div>
        <div class="card-content is-paddingless">
          <table id="my_schedule_table" class="table is-narrow is-fullwidth">
            <thead>
              <tr>
                <th>Shift Date</th>
                <th>Shift Schedule (EST)</th>
                <th></th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>

      <div class="card">
        <div class="card-header">
          <p class="card-header-title">
            My Request&nbsp;<a href="{{ route('ss_history', 'request') }}"><i class="fa fa-history"></i></a>
          </p>
        </div>
        <div class="card-content is-paddingless">
          <table id="my_request_table" class="table is-bordered is-narrow is-fullwidth">
            <thead>
              <tr>
                <th width="30%" class="has-text-centered">Request Id</th>
                <th width="40%" class="has-text-centered">Swap Type</th>
                <th width="30%" class="has-text-centered">Status</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>

      <div class="card">
        <div class="card-header">
          <p class="card-header-title">
            Received Request&nbsp;<a href="{{ route('ss_history', 'received') }}"><i class="fa fa-history"></i></a>
          </p>
        </div>
        <div class="card-content is-paddingless">
          <table id="my_received_request_table" class="table is-bordered is-narrow is-fullwidth">
            <thead>
              <tr>
                <th width="30%" class="has-text-centered">Request Id</th>
                <th width="40%" class="has-text-centered">Swap Type</th>
                <th width="30%" class="has-text-centered">Status</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>

    </div>
  </div>
</div>
@endsection


@section('scripts')
<script type="text/javascript">
  //
  var xhr_search_form = null;
  var xhr_information = null;
  var xhr_week_ending_and_my_shift_date = null;
  var xhr_shift_swap_form = null;
  var xhr_shift_swap_confirmation_form = null;

  // On-load
  information();
  // Hide Shift Swap Details
  $('#shift_swap_card').hide();
  $('#shift_swap_details_card').hide();

  // Functions
  $(function(){
    /*
    Search Avaya
    */
    $('#search_avaya_form').submit(function(){
      $('#shift_swap_details_card').hide();
      xhr_search_form = $.ajax({
        type : $(this).attr('method'),
        url : $(this).attr('action'),
        data : $(this).serialize(),
        cache : false,
        dataType : "json",
        async : false, 
        beforeSend: function(xhr){
          $('#ss_message').html('<br /><h4 class="has-text-centered"><img src="{{ asset("img/loading/2.gif") }}" title="Loading..." /></h4>');
          if (xhr_search_form != null)
          {
            xhr_search_form.abort();
          }
        }
      }).done(function(data) {
        // Reset Form
        $('#shift_swap_form input, #shift_swap_form select').not('input[type=hidden]').val('');
        $('#my_shift_date, #buddy_shift_date').html('<option value="">-- Select --</option>');
        // End Reset Form
        if(data['avaya'] === undefined)
        {
          $('#ss_message').html('<br><div class="notification is-warning">Avaya does not exists!</div>');
          $('#shift_swap_card').hide();
        }
        else
        {
          $('#ss_message').html('');
          // Populate fields
          $('#buddy_avaya').val(data['avaya']);
          $('#buddy_name').val(data['name']);
          $('#shift_swap_card').show();
        }
      }).fail(function(jqXHR, textStatus){
        var errors = error_message(jqXHR['responseJSON']['errors']);
        $('#ss_message').html('<br /><div class="notification is-warning">' + errors + '</div>');
        $('#shift_swap_card').hide();
        $('#shift_swap_details_card').hide();
      });
      return false;
    });

    /*
    Select Swap Type
    */

    $('#swap_type').change(function(){
      var value = $(this).val();
      $('.field-not-2rd').show();
      $('#my_schedule, #buddy_shift_date, #buddy_schedule').val('');
      $('#week_ending, #my_shift_date, #buddy_shift_date').attr('disabled', false);


      if(value == 'sd')
      {
        week_ending_and_my_shift_date(value);
        $('#week_ending').attr('disabled', true);
        $('#my_shift_date, #buddy_shift_date').attr('disabled', false);
      }
      else if(value == 'srd')
      {
        week_ending_and_my_shift_date(value);
        $('#week_ending').attr('disabled', true);
        $('#my_shift_date, #buddy_shift_date').attr('disabled', false);
      }
      else if(value == '2rd')
      {
        $('#my_shift_date, #buddy_shift_date').val('').attr('disabled', true);
        $('.field-not-2rd').hide();
      }
      else if(value == 'shrd')
      {
        week_ending_and_my_shift_date(value);
        $('#week_ending').attr('disabled', true);
        $('#my_shift_date, #buddy_shift_date').attr('disabled', false);
      }
      return false;
    });

    /*
    Select My Shift Date
    */

    $('#my_shift_date').change(function(){
      var value = $(this).val();
      var my_shift = value.split('_');

      $('#my_schedule').val(my_shift[1]);
      $('#ss_message').html('');
      $('#buddy_shift_date, #buddy_schedule').val('');

      return false;
    });

    /*
    Select Buddy Shift Date
    */

    $('#buddy_shift_date').change(function(){
      var value = $(this).val();
      var swap_type = $('#swap_type').val();
      var my = $('#my_shift_date').val().split('_');
      var buddy = value.split('_');

      var my_shift_date = my[0];
      var my_shift = my[1];
      var buddy_shift_date = buddy[0];
      var buddy_shift = buddy[1];

      //console.log(my_shift_date);console.log(my_shift);
      //console.log(buddy_shift_date);console.log(buddy_shift);
      // Populate Buddy Schedule
      $('#buddy_schedule').val(buddy_shift);
      // Swap Single RD Swap Filter
      if((my_shift == 'Off' && buddy_shift != 'Off') || (my_shift != 'Off' && buddy_shift == 'Off'))
      {
        $('#ss_message').html('<br /><div class="notification is-warning">Sorry, swap not allowed. Schedules must be <strong>Rest Day / Off</strong></div>');
        return false;
      }
      // Swap Same Work Pattern Filter
      //  my_shift != 'Off' && buddy_shift != 'Off' && my_shift == buddy_shift)
      if(swap_type == 'sd' && my_shift_date != buddy_shift_date)
      {
        $('#ss_message').html('<br /><div class="notification is-warning">Sorry, swap not allowed. Shift Date are not the same.</div>');
        return false;
      }
      else if(swap_type == 'sd' && my_shift == buddy_shift)
      {
        $('#ss_message').html('<br /><div class="notification is-warning">Sorry, swap not allowed. You have the same Work Pattern</div>');
        return false;
      }
      else if(swap_type == 'srd' && my_shift_date == buddy_shift_date)
      {
        $('#ss_message').html('<br /><div class="notification is-warning">Sorry, swap not allowed. You have the same Shift Date and Schedule.</div>');
        return false;
      }
      else if(swap_type == 'srd' && my_shift != buddy_shift)
      {
        $('#ss_message').html('<br /><div class="notification is-warning">Sorry, Swap not allowed. Your Work Pattern are not the same.</div>');
        return false;
      }

      $('#ss_message').html('');

      return false;
    });

    /*
    Select Shift Swap Form
    */

    $('#shift_swap_form').submit(function(){
      xhr_shift_swap_form = $.ajax({
        type : $(this).attr('method'),
        url : $(this).attr('action'),
        data : $(this).serialize(),
        cache : false,
        dataType : "json",
        async : false, 
        beforeSend: function(xhr){
          $('#ss_message').html('<br /><br /><h4 class="has-text-centered"><img src="{{ asset("img/loading/2.gif") }}" title="Loading..." /></h4>');
          if (xhr_shift_swap_form != null)
          {
            xhr_shift_swap_form.abort();
          }
        }
      }).done(function(result) {
        information();
        $('#shift_swap_card').hide();
        $('#ss_message').html('<br /><div class="notification is-success">Shift Swap Successfully Submitted!</div>');
      }).fail(function(jqXHR, textStatus) {
        var errors = error_message(jqXHR['responseJSON']['errors']);
        $('#ss_message').html('<br /><div class="notification is-warning">' + errors + '</div>');
      });
      return false;
    });

    /*
    Select Shift Swap Form
    */

    $('#accepted').change(function(){
      if($(this).is(":checked"))
      {
        $('#shift_swap_accept_button').attr('disabled', false);
        $('#shift_swap_decline_button').attr('disabled', true);
      }
      else
      {
        $('#shift_swap_accept_button').attr('disabled', true);
        $('#shift_swap_decline_button').attr('disabled', false);
      }
    });

    $('#shift_swap_confirmation_form').submit(function(){
      $('#shift_swap_decline_button').attr('disabled', true);
      xhr_shift_swap_confirmation_form = $.ajax({
        type : $(this).attr('method'),
        url : $(this).attr('action'),
        data : $(this).serialize(),
        cache : false,
        dataType : "json",
        async : false, 
        beforeSend: function(xhr){
          $('#ss_message').html('<br /><h4 class="has-text-centered"><img src="{{ asset("img/loading/2.gif") }}" title="Loading..." /></h4>');
          if (xhr_shift_swap_confirmation_form != null)
          {
            xhr_shift_swap_confirmation_form.abort();
          }
        }
      }).done(function(result) {
        var message = 'Shift Swap Successfully Declined!';

        $('#shift_swap_decline_button').attr('disabled', true);

        if(result['status_exists'] == 1)
        {
          if(result['status'] == 1)
          {
            message = 'Shift Swap Already Approved!';
          }
          else if(result['status'] == 0)
          {
            message = 'Shift Swap Already Declined!';
          }
          $('#ss_message').html('<br /><div class="notification is-warning">' + message + '</div>');
        }
        else
        {
          $('#shift_swap_accept_row_form').hide();
          information();

          if(result['status'] == 1)
          {
            message = 'Shift Swap Successfully Approved!';
          }
          $('#ss_message').html('<br /><div class="notification is-success">' + message + '</div>');
        }


      }).fail(function(jqXHR, textStatus) {
        var errors = error_message(jqXHR['responseJSON']['errors']);
        $('#ss_message').html('<br /><div class="notification is-warning">' + errors + '</div>');
      });
      return false;
    });
  });

  function information()
  {
    $('#my_schedule_table tbody, #my_request_table tbody, #my_received_request_table tbody').html('<tr><td colspan="3" class="has-text-centered"><img src="{{ asset("img/loading/2.gif") }}" title="Loading..." /></td></tr>');
    xhr_information = $.ajax({
      type : 'get',
      url : '{{ route("ss_details_json", Auth::user()->id) }}',
      cache : false,
      dataType : "json",
        async : false, 
      beforeSend: function(xhr){
        if (xhr_information != null)
        {
          xhr_information.abort();
        }
      }
    }).done(function(result_set) {
      $('#info_avaya').html(result_set['information']['avaya']);
      $('#info_tier').html(result_set['information']['tier']);
      $('#info_work_pattern').html(result_set['information']['wp']);
      $('#info_skill').html(result_set['information']['skill']);
      schedule(result_set['schedules']);
      var my_request = shift_swap_request('request', result_set['my_request']);
      var received_request = shift_swap_request('received_request', result_set['received_request']);

      $('#my_request_table tbody').html(my_request);
      $('#my_received_request_table tbody').html(received_request);
    }).fail(function(jqXHR, textStatus) {
      var errors = error_message(jqXHR['responseJSON']['errors']);
      $('#ss_message').html('<br /><div class="notification is-warning">' + errors + '</div>');
    });
  }

  function schedule(rows)
  {
    var table = '';
    $.each(rows, function(key, value){
      table += '<tr><th colspan="3" class="has-text-centered">Week Ending ' + key + '</th></tr>';
      $.each(value, function(k, v){

        var icon = '';
        var schedule = v['schedule'];
        var shift_date_format = v['shift_date_format'];
        if(v['restrictions'] != null)
        {
          icon = '<a href="javascript:void(0)" class="tooltip is-tooltip-left" data-tooltip="' + v['restrictions'] + '"><i class="fa fa-ban"></i></a>';
        }

        if(v['new_shift_date'] == '' && v['new_schedule'] != '')
        {
          icon = '<a href="javascript:void(0)" class="tooltip is-tooltip-left" data-tooltip="New Schedule is ' + v['new_schedule'] + '"><i class="fa fa-star-o"></i></a>';
          //schedule = v['new_schedule'];
          shift_date_format = v['shift_date_format'];
        }
        else if(v['new_shift_date'] != '' && v['new_schedule'] != '')
        {
          icon = '<a href="javascript:void(0)" class="tooltip is-tooltip-left" data-tooltip="New Schedule is ' + v['new_schedule'] + '"><i class="fa fa-star-o"></i></a>';
          //schedule = v['new_schedule'];
          shift_date_format = v['shift_date_format'];
        }
         
        table += '<tr><td>' + shift_date_format + '</td><td>' + schedule + '</td><td>' + icon + '</td></tr>';
      });

    });

    $('#my_schedule_table tbody').html(table);
  }

  function shift_swap_request(type, rows)
  {
    var table = '';
    var icon = '';
    var data = '';

    $.each(rows, function(key, value){

      if(value['swap_type'] == '2rd')
      {
        /*var employee_shift_date = Object.keys(value['employee_shift_date']).map(function(key) {
          return [value['employee_shift_date'][key]];
        });

        var employee_schedule = Object.keys(value['employee_schedule']).map(function(key) {
          return [value['employee_schedule'][key]];
        });

        var buddy_shift_date = Object.keys(value['buddy_shift_date']).map(function(key) {
          return [value['buddy_shift_date'][key]];
        });

        var buddy_schedule = Object.keys(value['buddy_schedule']).map(function(key) {
          return [value['buddy_schedule'][key]];
        });*/

        var employee_shift_date = '';
        var employee_schedule = '';
        var buddy_shift_date = '';
        var buddy_schedule = '';

        $.each(value['employee_shift_date'], function(i, date){
          employee_shift_date += '' + date + ',';
        });

        $.each(value['buddy_shift_date'], function(i, date){
          buddy_shift_date += '' + date + ',';
        });

        employee_shift_date = employee_shift_date.substring(0, employee_shift_date.length - 1);
        buddy_shift_date = buddy_shift_date.substring(0, buddy_shift_date.length - 1);

        value['employee_shift_date'] = employee_shift_date;
        value['buddy_shift_date'] = buddy_shift_date;
      }
      else if(value['swap_type'] == 'shrd')
      {
        /*var employee_shift_date = Object.keys(value['employee_shift_date']).map(function(key) {
          return [value['employee_shift_date'][key]];
        });

        var buddy_shift_date = Object.keys(value['buddy_shift_date']).map(function(key) {
          return [value['buddy_shift_date'][key]];
        });*/

        var employee_shift_date = '';
        var buddy_shift_date = '';

        $.each(value['employee_shift_date'], function(i, date){
          employee_shift_date += '' + date + ',';
        });

        $.each(value['buddy_shift_date'], function(i, date){
          buddy_shift_date += '' + date + ',';
        });

        employee_shift_date = employee_shift_date.substring(0, employee_shift_date.length - 1);
        buddy_shift_date = buddy_shift_date.substring(0, buddy_shift_date.length - 1);

        value['employee_shift_date'] = employee_shift_date;
        value['buddy_shift_date'] = buddy_shift_date;
      }

      data =  "'" + type + "', '" + value['status'] + "', '" + value['tracking_number'] + "', '" + 
              value['swap_type'] + "', '" + value['swap_type_name'] + "', '" + 
              value['created_at'] + "', '" + 
              value['expired_at'] + "', '" + 
              value['buddy_name'] + "', '', '" + 
              value['employee_shift_date'] + "', '" + value['employee_schedule'] + "', '" + 
              value['buddy_shift_date'] + "', '" + value['buddy_schedule'] + "' ";

      icon = '<span onclick="shift_swap_details(' + data + ')" class="tag is-dark is-uppercase">' + value['swap_type_name'] + '</span>';

      table += '<tr><td class="has-text-centered">' + 
                value['tracking_number'] + 
                '</td><td class="has-text-centered"><a href="#my-shift-swap-request-details">' + icon + 
                '</a></td><td class="has-text-centered"><a href="#my-shift-swap-request-details" onclick="shift_swap_details(' + data + ')" >' + swap_type_status_icon(value['status']) + '</a></td></tr>';
    });

    return table;
  }

  function shift_swap_details(type, status, tracking_number, swap_type, swap_type_name, created_at, expired_at, buddy_name, week_ending, employee_shift_date, employee_schedule, buddy_shift_date, buddy_schedule)
  {
    $('#shift_swap_card').hide();

    if(swap_type == '2rd' || swap_type == 'shrd')
    {
      console.log(employee_shift_date);
      employee_shift_date = employee_shift_date.split(',');
      buddy_shift_date = buddy_shift_date.split(',');

      var my_old_rd_shift_dates = '';
      var my_new_rd_shift_dates = '';

      var buddy_old_rd_shift_dates = '';
      var buddy_new_rd_shift_dates = '';

      $.each(employee_shift_date, function(key, value){
        my_old_rd_shift_dates += '<span class="tag is-dark">' + value + '</span> ';
        buddy_new_rd_shift_dates += '<span class="tag is-success">' + value + '</span> ';
      });

      $.each(buddy_shift_date, function(key, value){
        buddy_old_rd_shift_dates += '<span class="tag is-dark">' + value + '</span> ';
        my_new_rd_shift_dates += '<span class="tag is-success">' + value + '</span> ';
      });

      if(type == 'request')
      {
        $('#shift_swap_details_my_old_schedule').html(buddy_old_rd_shift_dates);
        $('#shift_swap_details_my_new_schedule').html(buddy_new_rd_shift_dates);

        $('#shift_swap_details_buddy_old_schedule').html(my_old_rd_shift_dates);
        $('#shift_swap_details_buddy_new_schedule').html(my_new_rd_shift_dates);
      }
      else
      {
        $('#shift_swap_details_my_old_schedule').html(my_old_rd_shift_dates);
        $('#shift_swap_details_my_new_schedule').html(my_new_rd_shift_dates);

        $('#shift_swap_details_buddy_old_schedule').html(buddy_old_rd_shift_dates);
        $('#shift_swap_details_buddy_new_schedule').html(buddy_new_rd_shift_dates);
      }
      
    }
    else
    {

      var my_old_schedule = '<span class="tag is-dark">' + employee_shift_date + 
                            '</span> <span class="tag is-dark">' + employee_schedule + '</span>';
      var my_new_schedule = '<span class="tag is-success">' + buddy_shift_date + 
                            '</span> <span class="tag is-success">' + buddy_schedule + '</span>';

      var buddy_old_schedule =  '<span class="tag is-dark">' + buddy_shift_date + 
                                '</span> <span class="tag is-dark">' + buddy_schedule + '</span>';
      var buddy_new_schedule =  '<span class="tag is-success">' + employee_shift_date + 
                                '</span> <span class="tag is-success">' + employee_schedule + '</span>';
      if(type == 'request')
      {
        $('#shift_swap_details_my_old_schedule').html(my_old_schedule);
        $('#shift_swap_details_my_new_schedule').html(my_new_schedule);

        $('#shift_swap_details_buddy_old_schedule').html(buddy_old_schedule);
        $('#shift_swap_details_buddy_new_schedule').html(buddy_new_schedule);
      }
      else
      {
        $('#shift_swap_details_my_old_schedule').html(buddy_old_schedule);
        $('#shift_swap_details_my_new_schedule').html(buddy_new_schedule);

        $('#shift_swap_details_buddy_old_schedule').html(my_old_schedule);
        $('#shift_swap_details_buddy_new_schedule').html(my_new_schedule);
      }

    }
    
    if(type == 'request')
    {
      $('#shift_swap_accept_row_form').hide();
    }
    else if(type == 'received_request')
    {
      $('#request_id').val(tracking_number);
      $('#shift_swap_accept_row_form').show();

      if(status != 'null')
      {
        $('#shift_swap_accept_row_form').hide();
      }
    }

    $('#shift_swap_type').html(swap_type_name);
    $('#shift_swap_tracking_number').html('<span class="tag">' + tracking_number + '</span>');
    $('#shift_swap_created_at').html('<span class="tag">' + created_at + '</span>');
    $('#shift_swap_expired_at').html('<span class="tag">' + expired_at + '</span>');
    $('#shift_swap_buddy_name').html('<span class="tag">' + buddy_name + '</span>');

    $('#shift_swap_details_card').show();
    $('#ss_message').html('');
    $('#shift_swap_confirmation_form input[name=accepted]').prop('checked',($('#shift_swap_confirmation_form input[name=accepted]').is('checked') == 1));
  }

  function swap_type_status_icon(status)
  {
    if(status == 0)
    {
      return '<span class="tag is-warning">DENIED</span>';
    }
    else if(status == 1)
    {
      return '<span class="tag is-success">APPROVED</span>';
    }
    else if(status == 2)
    {
      return '<span class="tag is-danger">EXPIRED</span>';
    }
    return '<span class="tag is-info">PENDING</span>';
  }

  function week_ending_and_my_shift_date(swap_type)
  {
    var datas = 'buddy_avaya=' + $('#buddy_avaya').val() + '&swap_type=' + swap_type;

    xhr_week_ending_and_my_shift_date = $.ajax({
      type : 'get',
      url : '{{ route("ss_we_and_my_sd", Auth::user()->id) }}',
      data : datas,
      cache : false,
      dataType : "json",
        async : false, 
      beforeSend: function(xhr){
        $('#ss_message').html('<br /><h4 class="has-text-centered"><img src="{{ asset("img/loading/2.gif") }}" title="Loading..." /></h4>');
        if (xhr_week_ending_and_my_shift_date != null)
        {
          xhr_week_ending_and_my_shift_date.abort();
        }
      }
    }).done(function(result_set) {

      var week_endings = '<option value="">-- Select --</option>';
      var shift_dates = '<option value="">-- Select --</option>';
      var buddy_shift_dates = '<option value="">-- Select --</option>';

      $.each(result_set['week_endings'], function(key, value){
        week_endings += '<option value="' + value['week_ending'] + '">' + value['week_ending'] + '</option>';
      });

      $.each(result_set['shift_dates'], function(key, value){
        shift_dates += '<option value="' + value['shift_date'] + '_' + value['schedule'] + '">' + value['shift_date'] + '</option>';
      });

      $.each(result_set['buddy_shift_dates'], function(key, value){
        buddy_shift_dates += '<option value="' + value['shift_date'] + '_' + value['schedule'] + '">' + value['shift_date'] + '</option>';
      });

      $('#swap_type_loading').hide();
      $('#week_ending').html(week_endings);
      $('#my_shift_date').html(shift_dates);
      $('#buddy_shift_date').html(buddy_shift_dates);
      $('#ss_message').html('');
    }).fail(function(jqXHR, textStatus) {
      var errors = error_message(jqXHR['responseJSON']['errors']);
      $('#ss_message').html('<br /><div class="notification is-warning">' + errors + '</div>');
    });
  }

  function error_message(errors)
  {
    var messages = '';
    $.each(errors, function(key, value){
      messages += '' + value[0] + '<br />';
    });

    return messages;
  }
</script>
@endsection
