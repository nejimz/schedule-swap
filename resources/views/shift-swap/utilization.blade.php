@extends('layouts.master')

@section('content')

<h4 class="title is-4"><i class="fa fa-building"></i> Utilization</h4>

<div class="columns">
	<div class="column is-5"></div>
	<div class="column is-7">
		<form id="ss-download" method="get" action="">
			<div class="field has-addons">
				<div class="control">
					<div class="select">
						<select id="type" name="type">
							<option value="">Pending/Expired</option>
							<option value="1">Approved</option>
							<option value="0">Denied</option>
						</select>
					</div>
				</div>
				<div class="control has-icons-left is-expanded">
					<input class="input" type="text" id="start_date" name="start_date" readonly="" autocomplete="off" value="{{ $start_date }}" />
					<span class="icon is-small is-left">
						<i class="fa fa-calendar"></i>
					</span>
				</div>
				<div class="control has-icons-left is-expanded">
					<input class="input" type="text" id="end_date" name="end_date" readonly="" autocomplete="off" value="{{ $end_date }}" />
					<span class="icon is-small is-left">
						<i class="fa fa-calendar"></i>
					</span>
				</div>
				<div class="control">
					<button class="button is-info" type="submit">Generate</button>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="columns">
	<div class="column is-12">
		<table class="table is-bordered is-narrow">
			<thead>
				<tr>
					<th class="has-text-centered" rowspan="2" style="padding-top: 20px;">Center</th>
					<th class="has-text-centered" rowspan="2" style="padding-top: 20px;">Type</th>
					<th class="has-text-centered" colspan="{{ $week_endings_count }}">Week Ending</th>
				</tr>
				<tr>
					@foreach($week_endings as $week_ending)
					<th class="has-text-centered">{{ \Carbon\Carbon::parse($week_ending)->format('m-d') }}</th>
					@endforeach
					<th class="has-text-centered">TOT</th>
				</tr>
				<tbody>
				@foreach($centers as  $center)
					<tr>
						<td class="has-text-centered" rowspan="{{ $swap_types_count }}" style="padding-top: 40px;">{{ $center->code }}</td>
					</tr>
					<?php $we_totals = [];?>
					@foreach($swap_types as $swap_type)
					<tr>
						<td class="has-text-centered">{{ strtoupper($swap_type->abbr) }}</td>
						<?php $total = 0; ?>
						@foreach($week_endings as $week_ending)
						<?php
						$status = ($type == '')? NULL : $type;
						$count = App\SwapAgentReqeust::whereSwapType($swap_type->abbr)->whereCenterId($center->id)
								->whereWeekEnding($week_ending)->where('status', $status)->count();
						
						if(array_key_exists($week_ending, $we_totals)) {
							$we_totals[$week_ending] += $count;
						} else {
							$we_totals[$week_ending] = $count;
						}
						$total += $count;
						?>
						<td class="has-text-centered">{{ $count }}</td>
						@endforeach
						<th class="has-text-centered">{{ $total }}</th>
					</tr>
					@endforeach
					<tr>
						<th class="has-text-centered">TOT</th>
						<?php $grand_total = 0;?>
						@foreach($we_totals as $total)
						<th class="has-text-centered">{{ $total }}</th>
						<?php $grand_total += $total;?>
						@endforeach
						<th class="has-text-centered">{{ $grand_total }}</th>
					</tr>
				@endforeach
				</tbody>
			</thead>
		</table>
	</div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
	$(function(){
		$('#type').val('<?php echo $type ?>');
    	$('#start_date, #end_date').datepicker({
    		beforeShowDay: function(date) {
    			return [date.getDay() === 0,''];
    		}
    	});
	});
	var xhr_center_week_endings = null;
	//center_week_endings(1);
	function center_week_endings(center_id)
	{
		$('.panel-block').removeClass('is-active');
		$('#center' + center_id).addClass('is-active');
		$('#week_dates').html('<a class="panel-block"><img src="{{ asset("img/loading/2.gif") }}" title="Loading..." /></a>');
		xhr_week_endings = $.ajax({
		  type : 'get',
		  url : '{{ route("ss_center_we") }}/' + center_id,
		  cache : false,
		  dataType : "json",
		    async : false, 
		  beforeSend: function(xhr){
		    if (xhr_center_week_endings != null)
		    {
		      xhr_center_week_endings.abort();
		    }
		  }
		}).done(function(json) {
			var we_items = '';
			$.each(json['week_endings'], function(key, value){
				//we_items += '<a class="panel-block" href="{{ route("ss_center_schedule") }}/' + center_id + '/' + value['date'] + '"><u>' + value['date'] + '</u>&nbsp;has&nbsp;<strong>' + value['count'] + '</strong>&nbsp;uploaded schedule and&nbsp;<strong>' + value['restriction'] + '</strong>&nbsp;restrictions.</a>';
				we_items += '<a class="panel-block" href="{{ route("ss_center_schedule") }}/' + center_id + '/' + value['date'] + '">' + value['date'] + '</a>';
			});
			$('#week_dates').html(we_items);
		}).fail(function(jqXHR, textStatus) {
			var we_items = '';
			we_items += '<a class="panel-block">Please reload your browser.</a>';
			$('#week_dates').html(we_items);
		});
	}
</script>
@endsection



