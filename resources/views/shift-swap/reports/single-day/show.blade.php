<table border="1" width="100%">
	<tr>
		<th colspan="4"></th>
		<th colspan="5">Agent A</th>
		<th colspan="5">Agent B</th>
	</tr>
	<tr>
		<th>Request Id</th>
		<th>Shift Swap Date</th>
		<th>Organization</th>
		<th>Shift Duration</th>
		<th>Name</th>
		<th>Avaya</th>
		<th>Work Pattern</th>
		<th>Day Off</th>
		<th>Requested Start Time</th>
		<th>Name</th>
		<th>Avaya</th>
		<th>Work Pattern</th>
		<th>Day Off</th>
		<th>Requested Start Time</th>
	</tr>
	@foreach($rows as $row)
		<?php
		$week_ending = $row->week_ending->toDateString();

		$employee_schedule = explode('-', $row->employee_schedule);
		$employee_schedule_start = \Carbon\Carbon::parse($employee_schedule[0]);
		$employee_rd = $row->get_rest_date($week_ending, $row->user_id);

		$employee_schedule_end = \Carbon\Carbon::parse($employee_schedule[1]);
		#$shift_duration->diffInHours($employee_schedule_start);
		$shift_duration = $row->buddy_user_information->shift_duration;


		$buddy_schedule = explode('-', $row->buddy_schedule);
		$buddy_schedule_start = trim($buddy_schedule[0]);
		$buddy_rd = $row->get_rest_date($week_ending, $row->buddy_user_id);
		?>
		<tr>
			<td>{{ $row->id }}</td>
			<td>{{ $row->employee_shift_date->toDateString() }}</td>
			<td>{{ $row->user_information->skill }}</td>
			<td>{{ $shift_duration }}</td>
			<td>{{ $row->user->name }}</td>
			<td>{{ $row->user_information->avaya }}</td>
			<td>{{ $row->buddy_user_information->wp }}</td>
			<td>{{ $employee_rd }}</td>
			<td>{{ $buddy_schedule_start }}</td>
			<td>{{ $row->buddy_user->name }}</td>
			<td>{{ $row->buddy_user_information->avaya }}</td>
			<td>{{ $row->user_information->wp }}</td>
			<td>{{ $buddy_rd }}</td>
			<td>{{ $employee_schedule_start->format('h:i A') }}</td>
		</tr>
	@endforeach
</table>