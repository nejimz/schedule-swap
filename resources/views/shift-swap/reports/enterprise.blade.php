@extends('layouts.master')

@section('content')

<div class="container">
  <div class="columns">
    <div class="column is-3"></div>
    <div class="column is-6">
      <div class="card">
        <div class="card-header">
          <label class="card-header-title label" for="search_avaya">Download Shift Swap</label>
        </div>
        <div class="card-content">
          @include('partials.error-message')
          <form id="ss-download" method="get" action="{{ route('enterprise-reports.show', str_random(10)) }}">

            <div class="field is-horizontal">
				<div class="field-label is-normal">
					<label class="label" for="organization" title="Organization">Org</label>
				</div>
  				<div class="field-body">
	  				<div class="field">
		            	<div class="control">
			                <div class="select is-fullwidth">
			                	<select id="organization" name="organization">
				                    <option value="">-- Select --</option>
				                    @foreach($skills as $row)
				                    <option value="{{ $row->skill }}">{{ $row->skill }}</option>
				                    @endforeach
			                	</select>
			                </div>
		            	</div>
	  				</div>
  				</div>
            </div>

            <div class="field is-horizontal">
				<div class="field-label is-normal">
					<label class="label" for="tier">Tier</label>
				</div>
  				<div class="field-body">
	  				<div class="field">
		            	<div class="control">
			                <div class="select is-fullwidth">
			                	<select id="tier" name="tier">
				                    <option value="">-- Select --</option>
				                    @foreach($tiers as $row)
				                    <option value="{{ $row->tier }}">{{ $row->tier }}</option>
				                    @endforeach
			                	</select>
			                </div>
		            	</div>
	  				</div>
  				</div>
            </div>

            <div class="field is-horizontal">
				<div class="field-label is-normal">
					<label class="label" for="shift_date">Shift Date</label>
				</div>
  				<div class="field-body">
	  				<div class="field">
						<div class="control has-icons-left is-expanded">
							<input class="input" type="text" id="shift_date" name="shift_date" autocomplete="off" />
							<span class="icon is-small is-left">
								<i class="fa fa-calendar"></i>
							</span>
						</div>
	  				</div>
  				</div>
            </div>

            <div class="field is-horizontal">
				<div class="field-label is-normal">
					<label class="label" for="center_id">Center</label>
				</div>
  				<div class="field-body">
	  				<div class="field">
		            	<div class="control">
			                <div class="select is-fullwidth">
			                	<select id="center_id" name="center_id">
				                    <option value="">-- Select --</option>
				                    @foreach($centers as $row)
				                    <option value="{{ $row->id }}">{{ $row->name }}</option>
				                    @endforeach
			                	</select>
			                </div>
		            	</div>
	  				</div>
  				</div>
            </div>

            <div class="field is-horizontal">
				<div class="field-label is-normal">
					<label class="label" for="swap_type">Swap Type</label>
				</div>
  				<div class="field-body">
	  				<div class="field">
		            	<div class="control">
			                <div class="select is-fullwidth">
			                	<select id="swap_type" name="swap_type">
				                    <option value="">-- Select --</option>
				                    @foreach($swap_types as $row)
				                    <option value="{{ $row->abbr }}">{{ $row->name }}</option>
				                    @endforeach
			                	</select>
			                </div>
		            	</div>
	  				</div>
  				</div>
            </div>

            <div class="field is-horizontal">
				<div class="field-label is-normal">
					<label class="label" for="shift_date">&nbsp;</label>
				</div>
  				<div class="field-body">
	  				<div class="field">
						<div class="control has-icons-left is-expanded">
							{!! csrf_field() !!}
                			<button class="button is-info" type="submit">Download</button>
						</div>
	  				</div>
  				</div>
            </div>

            <div class="field is-horizontal">
            	<div class="control">
            	</div>
            </div>

          </form>
        </div>
      </div>
    </div>
    <div class="column is-3"></div>
  </div>
</div>

@endsection

@section('scripts')
 {{--  <script src="{{ asset('js/bulma-calendar.js') }}"></script> --}}
  <script>
    // bulmaCalendar.attach('#shift_date');
     $('#shift_date').datepicker();
  </script>
@endsection



