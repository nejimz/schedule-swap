@extends('layouts.master')

@section('content')

<div class="container">
  <div class="columns">
    <div class="column is-3"></div>
    <div class="column is-6">
      <div class="card">
        <div class="card-header">
          <label class="card-header-title label" for="search_avaya">Download Shift Swap</label>
        </div>
        <div class="card-content">
          @include('partials.error-message')
          <form id="ss-download" method="get" action="{{ route('ss-reports.show', str_random(10)) }}">

            <div class="field has-addons">
              <div class="control">
                <div class="select">
                  <select id="swap_type" name="swap_type">
                    <option value="">-- Select --</option>
                    @foreach($swap_types as $row)
                    <option value="{{ $row->abbr }}">{{ $row->name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="control has-icons-left is-expanded">
                <input class="input" type="text" id="date_confirmed" name="date_confirmed" autocomplete="off" />
                <span class="icon is-small is-left">
                  <i class="fa fa-calendar"></i>
                </span>
              </div>
              <div class="control">
                <button class="button is-info" type="submit">Download</button>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
    <div class="column is-3"></div>
  </div>
</div>

@endsection

@section('scripts')
  {{-- <script src="{{ asset('js/bulma-calendar.js') }}"></script> --}}
  <script>
    // bulmaCalendar.attach('#date_confirmed', {
    //   closeOnOverlayClick: true,
    //   closeOnSelect: true
    // });

    $('#date_confirmed').datepicker();
  </script>
@endsection



