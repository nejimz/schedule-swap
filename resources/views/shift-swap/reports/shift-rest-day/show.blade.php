<table border="1" width="100%">
	<tr>
		<th colspan="3"></th><th colspan="4">Agent A</th><th colspan="4">Agent B</th>
	</tr>
	<tr>
		<th>Request Id</th>
		<th>Organization</th>
		<th>Shift Duration</th>
		<th>Name</th>
		<th>Avaya</th>
		<th>Work Pattern</th>
		<th>Requested Day</th>
		<th>Name</th>
		<th>Avaya</th>
		<th>Work Pattern</th>
		<th>Requested Day</th>
	</tr>
	@foreach($rows as $row)
		<?php
		$employee_schedule = explode('-', $row->employee_schedule);
		$shift_duration = $row->buddy_user_information->shift_duration;
		?>
		<tr>
			<td>{{ $row->id }}</td>
			<td>{{ $row->user_information->skill }}</td>
			<td>{{ $shift_duration }}</td>
			<td>{{ $row->user->name }}</td>
			<td>{{ $row->user_information->avaya }}</td>
			<td>{{ $row->buddy_user_information->wp }}</td>
			<td>{{ $row->buddy_shift_date->format('Y-F-d (D)') }}</td>
			<td>{{ $row->buddy_user->name }}</td>
			<td>{{ $row->buddy_user_information->avaya }}</td>
			<td>{{ $row->user_information->wp }}</td>
			<td>{{ $row->employee_shift_date->format('Y-F-d (D)') }}</td>
		</tr>
	@endforeach
</table>