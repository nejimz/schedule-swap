@extends('layouts.master')

@section('content')
<div class="container">
  <div class="columns">
    <div class="column is-3"></div>
    <div class="column is-6">
      <div class="card">
        <div class="card-header">
          <label class="card-header-title label" for="search_avaya">Upload Schedule Swap</label>
        </div>
        <div class="card-content">
          <form id="schedule-upload" method="post" action="{{ route('schedule-upload.store') }}">

            <div class="notification is-info">
              CSV file format is required.<br/>
              Always remove the Header.<br/>
              Always follow the column arrangement.<br/> 
              Download template <a href="{{ asset('templates/schedule-template.csv') }}">here</a>.
            </div>
            <div class="field has-addons">
              <div class="control has-icons-left is-expanded">
                <div class="file">
                  <label class="file-label">
                    <input class="file-input" type="file" id="file" name="schedule">
                    <span class="file-cta">
                      <span class="file-icon">
                        <i class="fa fa-upload"></i>
                      </span>
                      <span class="file-label">
                        Choose a file…
                      </span>
                    </span>
                  </label>
                </div>
              </div>
              <div class="control">
                {!! csrf_field() !!}
                <button class="button is-info" type="submit" id="schedule-upload-btn" disabled="">Upload</button>
              </div>
            </div>

          </form>
          <div class="error-message"></div>
        </div>
      </div>
    </div>
    <div class="column is-3"></div>
  </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
  var xhr_file = null;
  var file_arr = [];
  var file_chunk = [];

  $(function(){
    $('#file').change(function(e){
      var file = e.target.files[0];
      if(file['type'] != 'application/vnd.ms-excel')
      {
        $('.error-message').html('<br /><div class="notification is-warning">Only CSV file extension is allowed!</div><br />');
      }
      else
      {
        Papa.parse(file, {
          header: false,
          dynamicTyping: true,
          skipEmptyLines: true,
          complete: function(results, file) {
            file_arr = results.data;
            file_chunk = results.data;
            //console.log(file_arr);
            $('#schedule-upload-btn').attr('disabled', false);
          }// End complete: function(results, file)
        });
      }

    });

    $('#schedule-upload').submit(function(e){
      if($('#file').val() == '')
      {
        $('.error-message').html('<div class="notification is-warning">File is required!</div><br />');
        return false;
      }

      var method = $(this).attr('method');
      var url = $(this).attr('action');
      var token = $('input[name=_token').val();
      //var center = $('#center').val();
      var parameters = 'row=' + JSON.stringify(file_chunk) + '&_token=' + token;
      xhr_file = $.ajax({
          type : method,
          url : url,
          data : parameters,
          cache : false,
          async: false,
          dataType : "json",
          beforeSend: function(xhr){
            $('#schedule-upload-btn').attr('disabled', true);
            $('.error-message').html('<br /><div class="notification is-info has-text-centered"><img src="{{ asset("img/loading/2.gif") }}" title="Uploading" /></div>');
            if (xhr_file != null)
            {
                xhr_file.abort();
            }
          }
      }).done(function(data){
        xhr_file = null;
        var messages = 'Done uploading Schedules!<hr />';
        $.each(data['errors'], function(key, value){
          messages += '' + value + '<br />';
        });

        $('.error-message').html('<br /><div class="notification is-success">' + messages + '</div>');
        //console.log(data);
      }).fail(function(jqXHR, textStatus){
          //console.log('Request failed: ' + textStatus);
      });


      return false;
      e.preventDefault();
    });
  });
</script>
@endsection