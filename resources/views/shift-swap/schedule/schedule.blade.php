@extends('layouts.master')

@section('content')

<h4 class="title is-4"><i class="fa fa-calendar"></i> {{ $center->name }} Agents Schedules (WE {{ $week_ending }}) {{ number_format($week_ending_count) }}</h4>
<div class="columns">
	<div class="column is-4">
		<form action="" method="get">
			<div class="field has-addons">
				<div class="control">
					<input class="input" type="text" name="search" placeholder="e.g Avaya" value="{{ $search }}">
				</div>
				<div class="control">
					<button class="button is-info"><i class="fa fa-search"></i></button>
				</div>
			</div>
		</form>
	</div>
	<div class="column is-8">&nbsp;</div>
</div>
<table class=" table is-bordered is-striped is-narrow is-hoverable is-fullwidth ">
	<thead>
		<tr>
			<th width="10%">Avaya</th>
			<th width="15%">Username</th>
			<th width="12%">Shift Date</th>
			<th width="19%">Schedule</th>
			<th width="19%">Expiration Date</th>
			<th width="25%">Restrictions</th>
		</tr>
	</thead>
	<tbody>
		@foreach($rows as $row)
		<tr>
			<td>{{ $row->user->information->avaya or '' }}</td>
			<td>{{ $row->user->username or '' }}</td>
			<td>{{ $row->shift_date }}</td>
			<td>{{ $row->schedule }}</td>
			<td>{{ $row->expiration_date }}</td>
			<td>{{ $row->restrictions }}</td>
		</tr>
		@endforeach
	</tbody>
</table>
{!! $rows->links('vendor.pagination.default') !!}

@endsection

@section('scripts')
<script type="text/javascript">
	var xhr_center_week_endings = null;
	//center_week_endings(1);
	function center_week_endings(center_id)
	{
		$('.panel-block').removeClass('is-active');
		$('#center' + center_id).addClass('is-active');
		$('#week_dates').html('<a class="panel-block"><img src="{{ asset("img/loading/2.gif") }}" title="Loading..." /></a>');
		xhr_week_endings = $.ajax({
		  type : 'get',
		  url : '{{ route("ss_center_we") }}/' + center_id,
		  cache : false,
		  dataType : "json",
		    async : false, 
		  beforeSend: function(xhr){
		    if (xhr_center_week_endings != null)
		    {
		      xhr_center_week_endings.abort();
		    }
		  }
		}).done(function(json) {
			console.log(json);
			var we_items = '';
			$.each(json['week_endings'], function(key, value){
				we_items += '<a class="panel-block" href="{{ route("ss_center_schedule") }}/' + center_id + '/' + value['date'] + '"><u>' + value['date'] + '</u>&nbsp;has&nbsp;<strong>' + value['count'] + '</strong>&nbsp;uploaded schedule and&nbsp;<strong>' + value['restriction'] + '</strong>&nbsp;restrictions.</a>';
			});
			$('#week_dates').html(we_items);
		}).fail(function(jqXHR, textStatus) {

		});
	}
</script>
@endsection



