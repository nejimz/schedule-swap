@extends('layouts.master')

@section('content')
<?php
$user = Auth::user();
?>
	<h4 class="title is-4"><i class="fa fa-building"></i> Centers</h4>

	<div class="columns">
	  	<div class="column is-3">
	  		<nav class="panel">
	  			<p class="panel-block"><strong>Centers</strong></p>
	  			@foreach($rows as $row)
	  				@if($user->center_id == $row->id || $user->isRoot())
					<a id="center{{ $row->id }}" class="panel-block" onclick="center_week_endings({{ $row->id }})">
						<span class="panel-icon">
							<i class="fa fa-building" aria-hidden="true"></i>
						</span>
						{{ $row->name . ' (' . $row->code . ') ' }}
					</a>
	  				@endif
				@endforeach
	  		</nav>
	  	</div>
  		<div class="column is-9">
	  		<nav class="panel">
	  			<p class="panel-block"><strong>Week Endings</strong></p>
	  			<div id="week_dates" style="margin-top: -1px;"></div>
	  		</nav>
  		</div>
	</div>

@endsection

@section('scripts')
<script type="text/javascript">
	var xhr_center_week_endings = null;
	//center_week_endings(1);
	function center_week_endings(center_id)
	{
		$('.panel-block').removeClass('is-active');
		$('#center' + center_id).addClass('is-active');
		$('#week_dates').html('<a class="panel-block"><img src="{{ asset("img/loading/2.gif") }}" title="Loading..." /></a>');
		xhr_week_endings = $.ajax({
		  type : 'get',
		  url : '{{ route("ss_center_we") }}/' + center_id,
		  cache : false,
		  dataType : "json",
		    async : false, 
		  beforeSend: function(xhr){
		    if (xhr_center_week_endings != null)
		    {
		      xhr_center_week_endings.abort();
		    }
		  }
		}).done(function(json) {
			var we_items = '';
			$.each(json['week_endings'], function(key, value){
				//we_items += '<a class="panel-block" href="{{ route("ss_center_schedule") }}/' + center_id + '/' + value['date'] + '"><u>' + value['date'] + '</u>&nbsp;has&nbsp;<strong>' + value['count'] + '</strong>&nbsp;uploaded schedule and&nbsp;<strong>' + value['restriction'] + '</strong>&nbsp;restrictions.</a>';
				we_items += '<a class="panel-block" href="{{ route("ss_center_schedule") }}/' + center_id + '/' + value['date'] + '">' + value['date'] + '</a>';
			});
			$('#week_dates').html(we_items);
		}).fail(function(jqXHR, textStatus) {
			var we_items = '';
			we_items += '<a class="panel-block">Please reload your browser.</a>';
			$('#week_dates').html(we_items);
		});
	}
</script>
@endsection



