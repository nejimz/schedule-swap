@extends('layouts.master')

@section('content')

<div class="container">
  <div class="columns">
    <div class="column is-12">
      <h3 class="title is-4">{{ $type }}</h3>
      <table class="table is-bordered is-narrow is-fullwidth">
        <thead>
          <tr>
            <th width="4%"></th>
            <th width="15%" class="has-text-centered">Request Id</th>
            <th width="15%">Swap Type</th>
            <th width="15%" class="has-text-centered">Week Ending</th>
            <th width="25%">Buddy Name</th>
            <th width="16%">Created At</th>
            <th width="10%" class="has-text-centered">Status</th>
          </tr>
        </thead>
        <tbody>
        @foreach($rows as $row)
          <tr>
            <td class="has-text-centered">
              <a href="#" onclick="details({{ $row->id }})"><i class="fa fa-list"></i></a>
            </td>
            <td class="has-text-centered">{{ $row->id }}</td>
            <td class="is-uppercase">{{ $row->swap_type_name }}</td>
            <td class="has-text-centered">{{ $row->week_ending->toDateString() }}</td>
            <td>{{ $row->buddy_user->name }}</td>
            <td>{{ $row->created_at }}</td>
            <td class="has-text-centered is-uppercase">
              @if(is_null($row->status))
                @if($row->status_type == 'expired')
                <span class="tag is-danger">{{ $row->status_type }}</span>
                @else
                <span class="tag is-info">{{ $row->status_type }}</span>
                @endif
              @elseif($row->status == 0)
              <span class="tag is-warning">{{ $row->status_type }}</span>
              @elseif($row->status == 1)
              <span class="tag is-success">{{ $row->status_type }}</span>
              @elseif($row->status == 3)
              <span class="tag is-warning">{{ $row->status_type }}</span>
              @endif
              
            </td>
          </tr>
        @endforeach
        </tbody>
      </table>
      {!! $rows->links('vendor.pagination.default') !!}
    </div>
  </div>
</div>

<div id="modal-details" class="modal">
  <div class="modal-background" onclick="modal_hide()"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Request Details</p>
      <button class="delete" aria-label="close" onclick="modal_hide()"></button>
    </header>
    <section class="modal-card-body">

      <table class="table is-fullwidth is-narrow is-bordered">
        <tr>
          <th class="has-text-centered">My Old Schedule</th>
          <th class="has-text-centered">My New Schedule</th>
        </tr>
        <tr>
          <td class="has-text-centered" id="my_old_schedule"><span class="tag is-dark">2018-06-10 4:00 PM - 12:00 AM</span></td>
          <td class="has-text-centered" id="my_new_schedule"><span class="tag is-success">2018-06-10 4:00 PM - 12:00 AM</span></td>
        </tr>
        <tr>
          <th colspan="2">&nbsp;</th>
        </tr>
        <tr>
          <th class="has-text-centered">Buddy Old Schedule</th>
          <th class="has-text-centered">Buddy New Schedule</th>
        </tr>
        <tr>
          <td class="has-text-centered" id="buddy_old_schedule"><span class="tag is-dark">2018-06-10 4:00 PM - 12:00 AM</span></td>
          <td class="has-text-centered" id="buddy_new_schedule"><span class="tag is-success">2018-06-10 4:00 PM - 12:00 AM</span></td>
        </tr>
      </table>

    </section>
  </div>
</div>

<script type="text/javascript">
  var xhr_records = null;

  function details(id)
  {
    xhr_records = $.ajax({
      type : 'get',
      url : '{{ route("ss_request_history_json") }}/' + id,
      cache : false,
      dataType : "json",
      beforeSend: function(xhr){
        if (xhr_records != null)
        {
          xhr_records.abort();
        }
      }
    }).done(function(result) {


      var my_old = '';
      var my_new = '';

      var buddy_old = '';
      var buddy_new = '';

      var swap_type = result[0]['swap_type'];

      var employee_shift_date = result[0]['employee_shift_date'];
      var employee_schedule = result[0]['employee_schedule'];
      var buddy_shift_date = result[0]['buddy_shift_date'];
      var buddy_schedule = result[0]['buddy_schedule'];

      if(swap_type == '2rd' || swap_type == 'shrd')
      {

        $.each(employee_shift_date, function(key, value){
          my_new += '<span class="tag is-dark">' + value + '</span> ';
          buddy_old += '<span class="tag is-success">' + value + '</span> ';
        });

        $.each(buddy_shift_date, function(key, value){
          buddy_new += '<span class="tag is-dark">' + value + '</span> ';
          my_old += '<span class="tag is-success">' + value + '</span> ';
        });
      }
      else
      {
        var my_old = '<span class="tag is-dark">' + employee_shift_date + ' ' + employee_schedule + '</span>';
        var my_new = '<span class="tag is-success">' + buddy_shift_date + ' ' + buddy_schedule + '</span>';

        var buddy_old =  '<span class="tag is-dark">' + buddy_shift_date + ' ' + buddy_schedule + '</span>';
        var buddy_new =  '<span class="tag is-success">' + employee_shift_date + ' ' + employee_schedule + '</span>';
      }

      $('#my_old_schedule').html(my_old);
      $('#my_new_schedule').html(my_new);

      $('#buddy_old_schedule').html(buddy_old);
      $('#buddy_new_schedule').html(buddy_new);

      $('#modal-details').addClass('is-active');

    }).fail(function(jqXHR, textStatus) {
    });
  }

  function modal_hide()
  {
    $('#modal-details').removeClass('is-active');
  }
</script>
@endsection



