@extends('layouts.master')

@section('content')
<!-- Content ... -->
<div class="container">
    <div class="columns">
        <div class="column is-5">
            <div class="card" style="margin-bottom:10px;">
                <div class="card-header">
                    <label class="card-header-title label">My Profile</label>
                </div>
                <div class="card-content is-paddingless" style="margin:-1px;">
                    <table class="table is-bordered is-marginless is-fullwidth">
                        <tbody>
                            <tr>
                                <td><strong>Name</strong></td>
                                <td>{{ auth()->user()->last_name.', '.auth()->user()->first_name }}</td>
                            </tr>
                            <tr>
                                <td><strong>Username</strong></td>
                                <td>{{ auth()->user()->username }}</td>
                            </tr>
                            <tr>
                                <td><strong>Center</strong></td>
                                <td>{{ auth()->user()->center->name }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
            /*@php
                // BAQ Test Users
                $betaTestUsers = [
                    'baq1ktilano',
                    'baq1hefontalvo',
                    'baq1ysoto',
                    'baq1jumelendez',
                    'baq1arojas',
                    'baq1vihiggins',
                    'baq1aaguilar',
                    'baq1acoll',
                    'baq1mvinachi',
                    'baq1agomez1',
                    'dgquijote',
                    'jctillaflor',
                    'cebtestagent01',
                    'cebtestagent02',
                    'cebtestagent03',
                    
                    'baq1crossell',
                    'baq1anosorio',
                    'baq1ffonseca',
                    'baq1jacuenca',
                    'baq1dcardona',
                    'baq1zzabala',
                    'baq1kisilva',
                    'baq1korcasita',
                    'baq1rurodriguez',
                    'baq1lnunez2',
                    'baq1ricalvarez',
                    'baq1ireyes',
                    'baq1dapertuz',
                    'baq1amvalverde',
                    'baq1alcabrera',
                    'baq1ocardenas',
                    'baq1imeza',
                    'baq1lalarcon2',
                    'baq1rosvergara',
                    'baq1mjesus',
                    'baq1jteran',
                    'baq1nfortich',
                    'baq1mmontes',
                    'baq1cargonzalez',
                    'baq1sgutierrez',
                    'baq1aoliveros',
                    'baq1rcolonna',
                    'baq1albarrios',
                    'baq1rohernandez',
                    'baq1clizeth',

                    'dmg1marjlozada',
                    'dmg1judomen',
                    'dmg1micdy',
                    'dmg1acelocia',
                    'dmg1avillaflores',
                    'dmg1rubcalumpang',
                    'dmg1abato',
                    'dmg1mteramoto',
                    'dmg1jotilos',
                    'dmg1sferolin',
                    'dmg1elmramirez',
                    'dmg1jacruz',
                    'dmg1deamiguel',
                    'dmg1jjamarolin',
                    'dmg1kperojon',
                    'dmg1drayoso',
                    'dmg1jaligato',
                    'dmg1nallosada',
                    'dmg1jparong',
                    'dmg1anortega',
                    'dmg1cabubo',
                    'dmg1romapula',
                    'dmg1malama',
                    'dmg1jalayaay',
                    'dmg1mestorco',
                    'dmg1nizerna',
                    'dmg1gtuangco',
                    'dmg1jgirasol',
                    'dmg1aabol',
                    'dmg1ndelapena'

                ];
            @endphp*/
            ?>

            <?php #@if(in_array(auth()->user()->username, $betaTestUsers)) ?>
                <table class="table is-bordered is-marginless is-fullwidth">
                    <tbody>
                        <tr>
                            <td><strong>Application Links</strong></td>
                            <td width="10">
                                <a href="{{ asset('downloads/beta-release.apk') }}" title="Android" onclick="logged('APK')">
                                    <i class="fa fa-android"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            <?php #@endif ?>
            
            @include('partials.error-message')
        </div>
        <div class="column is-7">
            <div class="columns">
                <div class="column">
                    <div class="card">
                        <div class="card-header">
                            <label class="card-header-title label">My Account</label>
                        </div>
                        <div class="card-content">
                            <div class="field email-field">
                                <label class="label" for="email">
                                  <i class="fa fa-envelope"></i>  Email
                                </label>
                                <p class="help" style="margin-bottom:10px;">This e-mail will receive updates and news regarding {{ config('app.name') }}. Also, this will be used for password retrieval.</p>
                                <div class="control view-control view-control-email">
                                    <div class="control" style="margin-bottom: 10px;">
                                        <p>
                                            {{-- <a href="#" class="is-right view-control-button" data-value="email">
                                                <i class="fa fa-edit"></i>
                                            </a> --}}
                                            {{ auth()->user()->email }}
                                        </p>
                                    </div>
                                </div>
                                <div class="control form-control form-control-email">
                                    <div class="control" style="margin-bottom: 10px;">
                                        <p>
                                            <form action="{{ route('account.update', auth()->user()->id) }}" method="POST">
                                                @csrf
                                                @method('PATCH')
                                                <input class="input" style="margin-bottom:10px;" type="text" name="email" id="email" value="{{ auth()->user()->email }}">
                                                <a href="#" class="button is-danger form-control-button" data-value="email">Cancel</a>
                                                <input class="button is-link" type="submit" value="Save">
                                            </form>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            
                            <div class="field">
                                <label class="label" for="sq">
                                    <i class="fa fa-question-circle"></i> Security Questions
                                </label>
                                <p class="help" style="margin-bottom:10px;">If you forgot both your password and email or you have no access to your email, security questions are secondary means to verify and retrieve your account.</p>
                               
                                @php $sqCount = 1; @endphp
                                @foreach(auth()->user()->questions as $sq)
                                    <div class="control view-control view-control-sq{{$sqCount}}" style="margin-bottom: 10px;">
                                        <p>
                                            {{-- <a href="#" class="is-right view-control-button" data-value="sq{{$sqCount}}">
                                                <i class="fa fa-edit"></i>
                                            </a> --}}
                                            {{ $sq->question }}
                                        </p>
                                    </div>
                                    <div class="control form-control form-control-sq{{$sqCount}}">
                                        <div class="control" style="margin-bottom: 10px;">
                                            <p>
                                                <form action="{{ route('account.update', auth()->user()->id) }}" method="POST">
                                                    @csrf
                                                    @method('PATCH')
                                                    <div class="select is-fullwidth" style="margin-bottom: 10px;">
                                                        <select name="security_challenge">
                                                            @foreach(App\SecurityChallenge::all() as $challenge)
                                                                @if($sq->pivot->security_challenge_id == $challenge->id)
                                                                    <option value="{{ $challenge->id }}" selected>{{ $challenge->question }}</option>
                                                                @else
                                                                    <option value="{{ $challenge->id }}">{{ $challenge->question }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <p class="help" style="margin-bottom:5px;">Answer</p>
                                                    <input class="input" type="hidden" name="security_question_id" value="{{ $sq->pivot->id }}">
                                                    <input class="input" type="text" name="answer" placeholder="Answer" id="answer{{$sqCount}}" style="margin-bottom: 10px;">
                                                    <a href="#" class="button is-danger form-control-button" data-value="sq{{$sqCount}}">Cancel</a>
                                                    <input class="button is-link" type="submit" value="Save">
                                                </form>
                                            </p>
                                        </div>
                                    </div>
                                    @php $sqCount++; @endphp
                                @endforeach
                            </div>
                            <hr>
                            <div class="field">
                                <label class="label" for="password">
                                    <i class="fa fa-key"></i> Password
                                </label>
                                <p class="help" style="margin-bottom:10px;">Password must meet complexity requirement policy.</p>
                               
                                <div class="control view-control view-control-password" style="margin-bottom: 10px;">
                                    <p>
                                        <a href="#" class="is-right view-control-button" data-value="password">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        Change Password
                                    </p>
                                </div>
                                <div class="control form-control form-control-password">
                                    <div class="control" style="margin-bottom: 10px;">
                                        <p>
                                            <form action="{{ route('account.update', auth()->user()->id) }}" method="POST">
                                                @csrf
                                                @method('PATCH')
                                                <p class="help" style="margin-bottom:5px;">Old Password</p>
                                                <input class="input" style="margin-bottom:10px;" type="password" placeholder="Old Password" name="old_password" id="old_password" value="">
                                                <p class="help" style="margin-bottom:5px;">New Password</p>
                                                <input class="input" style="margin-bottom:10px;" type="password" placeholder="New Password" name="new_password" id="new_password" value="">
                                                <p class="help" style="margin-bottom:5px;">Confirm Password</p>
                                                <input class="input" style="margin-bottom:10px;" type="password" placeholder="Confirm Password" name="new_password_confirmation" id="new_password_confirmation" value="">
                                                <a href="#" class="button is-danger form-control-button" data-value="password">Cancel</a>
                                                <input class="button is-link" type="submit" value="Save">
                                            </form>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script>
        var xhr_logged = null;
        resetControl();

        $('.view-control-button').on('click', function(){
            resetControl();
            $('.view-control-'+$(this).data('value')).addClass('is-hidden');
            $('.form-control-'+$(this).data('value')).removeClass('is-hidden');
        });

        $('.form-control-button').on('click', function(){
            resetControl();
            $('.form-control-'+$(this).data('value')).addClass('is-hidden');
            $('.view-control-'+$(this).data('value')).removeClass('is-hidden');
        });

        function resetControl(){
            $('.view-control').removeClass('is-hidden');
            $('.form-control').addClass('is-hidden');
        }

        function logged(type)
        {
            xhr_logged = $.ajax({
                type : 'get',
                url : '{{ route("account.download") }}',
                data: 'type=' + type,
                cache : false,
                dataType : "json",
                async : false, 
                beforeSend: function(xhr){
                    if (xhr_logged != null)
                    {
                        xhr_logged.abort();
                    }
                }
            }).done(function(json) {

            }).fail(function(jqXHR, textStatus) {

            });
        }
    </script>
@endsection




