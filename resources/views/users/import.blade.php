 <div class="modal" id="importUser">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Import Users</p>
            <button class="delete" aria-label="close" onclick="modals.importUser.close()"></button>
        </header>
        <section class="modal-card-body is-paddingless" style="height: 280px;">
            <section class="hero is-info">
                <div class="hero-body" style="padding:12px 24px;">
                    <p class="title is-6 is-marginless">Note!</p> 
                    <ul>
                        <li>Only CSV file is allowed.</li> 
                        <li>Always remove header before uploading.</li>
                    </ul>
                </div>
            </section>
            <div class="file has-name is-boxed is-success is-centered" style="margin-top:25px;">
                <label class="file-label">
                    <input class="file-input" type="file" name="resume">
                    <span class="file-cta">
                        <span class="file-icon">
                            <i class="fa fa-upload"></i>
                        </span>
                        <span class="file-label">
                            Choose a file…
                        </span>
                    </span>
                    <span class="file-name">
                        Screen Shot 2017-07-29 at 15.54.25.png
                    </span>
                </label>
            </div>
        </section>
        <footer class="modal-card-foot">
            <button class="button is-success" disabled="disabled">
                <span class="icon is-small is-pulled-right">
                    <i class="fa fa-upload"></i> 
                </span>
                &nbsp;Upload
            </button>
            <button class="button is-danger" onclick="modals.importUser.close()">Cancel</button>
            
            <!-- <span class="icon is-small is-pulled-right">
                <i class="fa fa-exclamation-circle"></i> 
            </span> -->
            <a class="button is-link" href="#" style="position:absolute; right:20px;">
                <span class="icon is-small is-pulled-right">
                    <i class="fa fa-download"></i> 
                </span>
                &nbsp;Template
            </a>
        </footer>
    </div>        
</div>