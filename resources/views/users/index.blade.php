<!-- 
    Extend Layout 
-->
@extends('layouts.master')

<!-- 
    Content Section 
-->
@section('content')
    <div class="columns">
        <div class="column is-9">
            <h1 class="title is-4 is-left"><i class="fa fa-users"></i> Users</h1>
        </div>
        <div class="column is-3">
           <div class="buttons">
                <a href="#" class="button is-warning" onclick="modals.importUser.open()">
                    <span class="icon is-small">
                        <i class="fa fa-upload"></i> 
                    </span>
                    <span>Import</span>
                </a>
                <a href="#" class="button is-primary" onclick="modals.exportUser.open()">
                    <span class="icon is-small">
                        <i class="fa fa-download"></i> 
                    </span>
                    <span>Export</span>
                </a>
                <a href="#" class="button is-success" onclick="modals.formUser.open()">
                    <span class="icon is-small">
                        <i class="fa fa-plus"></i> 
                    </span>
                    <span>Add</span>
                </a>
            </div>
        </div>
    </div>

	<table class="table is-bordered is-fullwidth">
        <thead>
            <tr>
                <th width="20%">Username</th>
                <th width="70%">Name</th>
                <th width="5%">Avaya</th>
                <th width="5%"></th>
            </tr>
        </thead>
        <tbody>
            @forelse($users as $user)
                <tr>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->name }}</td>
                    <td></td>
                    <td></td>
                </tr>
            @empty
                <tr>
                    <td colspan="5"><center>No Users Found</center></td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection

<!-- 
    Modal Section 
-->
@section('modals')
    <!--    For Modal    -->
   @include('users.form')
    <!--    Import Modal    -->
   @include('users.import')
    <!--    Script Section    -->
    @include('users.export')
@endsection

<!-- 
    Script Section 
-->
@section('scripts')
    <script>
        var requestSent = false;
        var modals = {
            formUser: 
            {
                open: function() { 
                    document.getElementById('formUser').classList.add('is-active');
                    forms.clear.notifications();
                },
                close:function() { 
                    document.getElementById('formUser').classList.remove('is-active');
                    forms.clear.all(document.getElementById('userForm'));
                },
                submit:function(){
                    if(requestSent == false)
                    {
                        requestSent = true;
                        forms.clear.notifications();
                        axios.post("{{ route('users.store') }}", {
                            'username': document.querySelector('#username').value,
                            'first_name': document.querySelector('#first_name').value,
                            'middle_name': document.querySelector('#middle_name').value,
                            'last_name': document.querySelector('#last_name').value
                            }).then(function(response) {
                                forms.success(response, document.getElementById('userForm'));
                            }).catch(function(error){
                                if(error.response !== undefined)
                                {   
                                    console.log('here agian');
                                    forms.error(error);
                                }
                            });
                    }
                }
            },
            importUser: 
            {
                open: function() { 
                    document.getElementById('importUser').classList.add('is-active');
                },
                close:function() { 
                    document.getElementById('importUser').classList.remove('is-active');
                }
            },
            exportUser: 
            {
                open: function() { 
                    document.getElementById('exportUser').classList.add('is-active');
                },
                close:function() { 
                    document.getElementById('exportUser').classList.remove('is-active');
                }
            }
        };
        
        var forms = {
            success: function(response, thisForm) {
                thisForm.insertAdjacentHTML('beforebegin', '<div class="notification is-success" id="notification-'+thisForm.id+'">'+response.data.success+'</div>');
                forms.clear.all(thisForm);
                document.getElementById('success').scrollIntoView();
            },
            error: function(error){
                const errors = error.response.data.errors;
                const firstItem = Object.keys(errors)[0];
                const firstItemDOM = document.getElementById(firstItem);
                const firstErrorMessage = errors[firstItem][0];
                firstItemDOM.scrollIntoView();
                requestSent = false;
                firstItemDOM.insertAdjacentHTML('afterend', `<div class="help is-danger">${firstErrorMessage}</div>`);
            },
            clear: {
                errors: function() {
                    const errorMessages = document.querySelectorAll('.help')
                    errorMessages.forEach((element) => element.textContent = '')
                },
                notifications: function() {
                    const notificationMessages = document.querySelectorAll('.notification')
                    notificationMessages.forEach((element) => element.remove());
                },
                all: function(thisForm) { 
                    requestSent = false;
                    forms.clear.errors();
                    thisForm.reset();
                }
            }
        }
    </script>
@endsection

<!-- 
    Style Section 
-->
@section('styles')
    <style lang="">
        .modal {
            flex-direction: column;
        }
    </style>
@endsection
