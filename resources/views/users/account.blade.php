@extends('layouts.master')

@section('content')
<!-- Content ... -->
<div class="container">
    <div class="columns">
        <div class="column is-3"></div>
        <div class="column is-6">
            <div class="card">
                <div class="card-header">
                    <label class="card-header-title label">My Account</label>
                </div>
                <div class="card-content">
                    @include('partials.error-message')
                    <form class="is-horizontal" id="userForm" method="post" action="{{ $route }}">
                        <div class="field">
                            <label class="label" for="email">Email</label>
                            <div class="control">
                                <input class="input" type="email" placeholder="Email" name="email" id="email" readonly="" value="{{ $row->email}}">
                            </div>
                        </div>
                        <!--div class="field">
                            <div class="control">
                                {!! csrf_field() !!}
                                <input type="hidden" name="_method" value="put">
                                <button class="button is-info" type="submit" disabled="">Submit</button>
                            </div>
                        </div-->
                    </form>
                </div>
            </div>
        </div>
        <div class="column is-3"></div>
    </div>
</div>

@endsection




