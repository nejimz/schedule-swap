<div class="modal" id="formUser">
    <div class="modal-background"></div>
    <div class="modal-card">
       
        <header class="modal-card-head">
            <p class="modal-card-title">Add User</p>
            <a class="delete" aria-label="close" onclick="modals.formUser.close()"></a>
        </header>
        <section class="modal-card-body">
        <!-- Content ... -->
        <form class="is-horizontal" id="userForm" onsubmit="event.preventDefault(); return modals.formUser.submit()">
            <div class="field">
                <label class="label">Username</label>
                <div class="control">
                    <input class="input" type="text" placeholder="Username" name="username" id="username">
                </div>
            </div>
            <div class="field">
                <label class="label">First Name</label>
                <div class="control">
                    <input class="input" type="text" placeholder="First Name" name="first_name" id="first_name">
                </div>
            </div>
                <div class="field">
                <label class="label">Middle Name</label>
                <div class="control">
                    <input class="input" type="text" placeholder="Middle Name" name="middle_name" id="middle_name">
                </div>
            </div>
            <div class="field">
                <label class="label">Last Name</label>
                <div class="control">
                    <input class="input" type="text" placeholder="Last Name" name="last_name" id="last_name">
                </div>
            </div>
        </section>
        <footer class="modal-card-foot">
            <button class="button is-success" id="saveUserForm">Save changes</button>
            <a class="button" onclick="modals.formUser.close()">Cancel</a>
        </footer>        
        </form>
    </div>  
</div>