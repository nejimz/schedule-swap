@extends('layouts.master')

@section('content')
<div class="container">
  <div class="columns">
    <div class="column is-3"></div>
    <div class="column is-6">
      <div class="card">
        <div class="card-header">
          <label class="card-header-title label" for="search_avaya">Upload Forecasted VTO/VOT</label>
        </div>
        <div class="card-content">
          <form id="file-upload" method="post" action="{{ route('forecast.upload.store') }}">

            <div class="notification is-info">
              CSV file format is required.<br/>
              Always remove the Header.<br/>
              Always follow the column arrangement.<br/> 
              Download template <a href="{{ asset('templates/forecast-template.csv') }}">here</a>.
            </div>

            <div class="field is-horizontal">
              <div class="field-label is-normal">
                <label class="label" for="type">Type</label>
              </div>
              <div class="field-body">
                <div class="field">
                  <div class="control">
                    <div class="select">
                      <select id="type" name="type">
                        <option value="">-- All --</option>
                        <option value="vto">VTO</option>
                        <option value="vot">VOT</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="field is-horizontal">
              <div class="field-label is-normal">
                <label class="label" for="center_id">Center</label>
              </div>
              <div class="field-body">
                <div class="field">
                  <div class="control">
                    <div class="select">
                      <select id="center_id" name="center_id">
                        <option value="">-- Select --</option>
                        @foreach($centers as $center)
                        <option value="{{ $center->id }}">{{ $center->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="field is-horizontal">
              <div class="field-label is-normal"></div>
              <div class="field-body">
                <div class="field has-addons">
                  <div class="control has-icons-left is-expanded">
                    <div class="file">
                      <label class="file-label">
                        <input class="file-input" type="file" id="file" name="file">
                        <span class="file-cta">
                          <span class="file-icon">
                            <i class="fa fa-upload"></i>
                          </span>
                          <span class="file-label">
                            Choose a file…
                          </span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="field is-horizontal">
              <div class="field-label is-normal"></div>
              <div class="field-body">
                <div class="field">
                  <div class="control">
                    {!! csrf_field() !!}
                    <button class="button is-info" type="submit" id="file-upload-btn" disabled="">Upload</button>
                  </div>
                </div>
              </div>
            </div>

          </form>
          <div class="error-message"></div>
        </div>
      </div>
    </div>
    <div class="column is-3"></div>
  </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
  var xhr_file = null;
  var file_arr = [];
  var file_chunk = [];

  $(function(){
    $('#file').change(function(e){
      var file = e.target.files[0];
      if(file['type'] != 'application/vnd.ms-excel')
      {
        $('.error-message').html('<br /><div class="notification is-warning">Only CSV file extension is allowed!</div><br />');
      }
      else
      {
        Papa.parse(file, {
          header: false,
          dynamicTyping: true,
          skipEmptyLines: true,
          complete: function(results, file) {
            file_arr = results.data;
            file_chunk = results.data;
            //console.log(file_arr);
            $('#file-upload-btn').attr('disabled', false);
          }// End complete: function(results, file)
        });
      }

    });

    $('#file-upload').submit(function(e){

      var method = $(this).attr('method');
      var url = $(this).attr('action');
      var type = $('#type').val();
      var center_id = $('#center_id').val();
      var token = $('input[name=_token]').val();

      if (type == '') {
        $('.error-message').html('<br /><div class="notification is-warning">Type is required!</div>');
        return false;
      } else if(center_id == '') {
        $('.error-message').html('<br /><div class="notification is-warning">Center is required!</div>');
        return false;
      } else if($('#file').val() == '') {
        $('.error-message').html('<br /><div class="notification is-warning">File is required!</div>');
        return false;
      }
      //var center = $('#center').val();
      var parameters = 'row=' + JSON.stringify(file_chunk) + '&_token=' + token + '&type=' + type + '&center_id=' + center_id;
      xhr_file = $.ajax({
          type : method,
          url : url,
          data : parameters,
          cache : false,
          async: false,
          dataType : "json",
          beforeSend: function(xhr){
            $('#file-upload-btn').attr('disabled', true);
            $('.error-message').html('<br /><div class="notification is-info has-text-centered"><img src="{{ asset("img/loading/2.gif") }}" title="Uploading" /></div>');
            if (xhr_file != null)
            {
                xhr_file.abort();
            }
          }
      }).done(function(data){
        xhr_file = null;
        var messages = 'Done uploading Forecast!';
        $('.error-message').html('<br /><div class="notification is-success">' + messages + '</div>');
        //console.log(data);
      }).fail(function(jqXHR, textStatus){
          //console.log('Request failed: ' + textStatus);
      });


      return false;
      e.preventDefault();
    });
  });
</script>
@endsection