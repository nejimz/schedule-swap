@extends('layouts.master')

@section('content')

<div class="container">
  	<div class="columns">
    	<div class="column is-3"></div>
    	<div class="column is-6">
      		<div class="card">
		        <div class="card-header">
		          <label class="card-header-title label">Forecast</label>
		        </div>
       			<div class="card-content">
					@include('partials.error-message')
		          	<form method="post" action="{{ $route }}">
			          	@if($enterprise)
			            <div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label" for="center_id">Center</label>
							</div>
			  				<div class="field-body">
				  				<div class="field">
					            	<div class="control">
						                <div class="select is-fullwidth">
						                	<select id="center_id" name="center_id">
							                    <option value="0">-- Select All --</option>
							                    @foreach($centers as $row)
							                    <option value="{{ $row->id }}">{{ $row->name }}</option>
							                    @endforeach
						                	</select>
						                </div>
					            	</div>
				  				</div>
			  				</div>
			            </div>
			            @else
			            <input type="hidden" name="center_id" value="{{ $user->center_id }}">
			            @endif
			            <div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label" for="organization" title="Type">Type</label>
							</div>
			  				<div class="field-body">
				  				<div class="field">
					            	<div class="control">
						                <div class="select is-fullwidth">
						                	<select id="type" name="type">
							                    <option value="">-- Select --</option>
							                    <option value="ot">Overtime</option>
							                    <option value="to">Time-Off</option>
						                	</select>
						                </div>
					            	</div>
				  				</div>
			  				</div>
			            </div>
			            <div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label" for="organization" title="Organization">Org.</label>
							</div>
			  				<div class="field-body">
				  				<div class="field">
					            	<div class="control">
						                <div class="select is-fullwidth">
						                	<select id="organization" name="organization">
							                    <option value="">-- Select --</option>
						                	</select>
						                </div>
					            	</div>
				  				</div>
			  				</div>
			            </div>

			            <div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label" for="shift_date">Shift Date</label>
							</div>
			  				<div class="field-body">
				  				<div class="field">
									<div class="control has-icons-left is-expanded">
										<input class="input" type="text" id="shift_date" name="shift_date" autocomplete="off" />
										<span class="icon is-small is-left">
											<i class="fa fa-calendar"></i>
										</span>
									</div>
				  				</div>
			  				</div>
			            </div>

			            <div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label" for="slot">Slot/s</label>
							</div>
			  				<div class="field-body">
				  				<div class="field">
									<div class="control">
										<input class="input" type="text" id="slot" name="slot" autocomplete="off" />
									</div>
				  				</div>
			  				</div>
			            </div>
			            
			            <div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label" for="timeType" title="Type">Forecast Type</label>
							</div>
			  				<div class="field-body">
				  				<div class="field">
					            	<div class="control">
						                <div class="select is-fullwidth">
						                	<select id="timeType" name="timeType">
							                    <option value="">-- Select --</option>
							                    <option value="wp">WP</option>
							                    <option value="ti">Time Interval</option>
						                	</select>
						                </div>
					            	</div>
				  				</div>
			  				</div>
			            </div>
			            
			            <div class="field is-horizontal" id="wp_field" style="display: none;">
							<div class="field-label is-normal">
								<label class="label" for="wp" title="Work Pattern">WP</label>
							</div>
			  				<div class="field-body">
				  				<div class="field">
					            	<div class="control">
						                <div class="select is-fullwidth">
						                	<select id="wp" name="wp">
							                    <option value="">-- Select --</option>
						                	</select>
						                </div>
					            	</div>
				  				</div>
			  				</div>
			            </div>
			            
			            <div class="field is-horizontal" id="ti_field" style="display: none;">
			            <!-- <div class="field is-horizontal" id="ti_field"> -->
							<div class="field-label is-normal">
								<label class="label" for="wp" title=""></label>
							</div>
			  				<div class="field-body">
				  				<div class="field">
					            	<div class="control">
					            		@foreach($hours as $key => $hour)
					            		<label class="checkbox">
					            			<input class="cb" type="checkbox" name="hours[{{ $key }}]" value="{{ $hour }}"> {{ $hour }}
					            		</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					            		@endforeach
					            	</div>
				  				</div>
			  				</div>
			            </div>

			            <div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label" for="shift_date">&nbsp;</label>
							</div>
			  				<div class="field-body">
				  				<div class="field">
									<div class="control has-icons-left is-expanded">
										{!! csrf_field() !!}
			                			<button class="button is-info" type="submit">Submit</button>
									</div>
				  				</div>
			  				</div>
			            </div>
		          	</form>
        		</div>
      		</div>
    	</div>
    	<div class="column is-3"></div>
  	</div>
</div>

@endsection

@section('scripts')
	<script>
	var option_select_default = '<option value="0">-- Select --</option>';
	var xhr_skills_wps = null;

    $('#shift_date').datepicker();

	<?php 
	if(!$enterprise)
	{
		echo 'skills_wps(' . $user->center_id . ');';
	}
	else
	{
		echo 'skills_wps(0);'; 
	}
	?>

	$(function(){
		$('#timeType').change(function(){
			console.log($(this).val());
			if($(this).val() == 'wp')
			{
				$('#it_field').hide();
				$('#wp_field').show();
			}
			else
			{
				$('#wp_field').hide();
				$('#ti_field').show();
			}
		});

		$('#center_id').change(function(){
			var center = $(this).val();

			if(center != '')
			{
				skills_wps(center);
			}
			else
			{
	    		$('#organization, #wp').html(option_select_default);
			}

		});

		/*$('input.cb').change(function(){
			//var allCB = $('input:checkbox').length;
			var allCheckedCB = $('input:checkbox:checked').length;
			var index = $(':input').index(this);
			var next_element = $(':input:eq(' + (index + 1) + ')');

			if (allCheckedCB == 1) {
				next_element.prop('checked', true);
				next_element.attr('readonly', true);
			}
		});*/
	});

	function skills_wps(center)
	{
	    xhr_skills_wps = $.ajax({
			type : 'get',
			url : '{{ route("skills_wps_json") }}/' + center,
			cache : false,
			dataType : "json",
	      	beforeSend: function(xhr){
		        if (xhr_skills_wps != null)
		        {
		          xhr_skills_wps.abort();
		        }
	      	}
	    }).done(function(result) {
	    	var skills = option_select_default;
	    	var wps = option_select_default;
			$.each(result['skills'], function(key, value){
				skills += '<option value="' + value + '">' + value + '</option>';
			});
			$.each(result['wps'], function(key, value){
				wps += '<option value="' + value + '">' + value + '</option>';
			});
	    	$('#organization').html(skills);
	    	$('#wp').html(wps);
	    }).fail(function(jqXHR, textStatus){
	    	//
	    });
	}
	</script>
@endsection



