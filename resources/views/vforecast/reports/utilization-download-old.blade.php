<?php
$second_col = '<tr>';
?>
<table border="1" width="100%">
	<tr>
		<th rowspan="2" align="left">Center</th>
		@foreach($dates as $date)
			<th colspan="6">{{ $date }}</th>
			<?php
			$second_col .= '<th align="center">Number of Agents</th><th align="center">VTO Availed</th><th align="center">VTO Offered</th><th align="center">Number of Agents</th><th align="center">VOT Availed</th><th align="center">VOT Offered</th>';
			?>
		@endforeach
	</tr>
	{!! $second_col . '</tr>' !!}
	@foreach($centers as $center)
	<tr>
		<th align="left">{{ $center->name }}</th>
		@foreach($dates as $date)
		<?php
		$center_id = $center->id;

		//Timeoff
		$vto_agent_count = App\ForecastVTORequest::whereHas('items.forecast', function($query) use ($center_id, $date) {
			$query->whereCenterId($center_id)->whereShiftDate($date);
		})->count('id');
		$vto_count = App\ForecastVTORequestItem::whereHas('forecast', function($query) use ($center_id, $date) {
			$query->whereCenterId($center_id)->whereShiftDate($date);
		})->count('id');
		$vto_forecasted_total = App\ForecastVTO::whereCenterId($center_id)->whereShiftDate($date)->sum('slots');

		// Overtime
		$vot_agent_count = App\ForecastVOTRequest::whereHas('items.forecast', function($query) use ($center_id, $date) {
			$query->whereCenterId($center_id)->whereShiftDate($date);
		})->count('id');
		$vot_count = App\ForecastVOTRequestItem::whereHas('forecast', function($query) use ($center_id, $date) {
			$query->whereCenterId($center_id)->whereShiftDate($date);
		})->count('id');
		$vot_forecasted_total = App\ForecastVOT::whereCenterId($center_id)->whereShiftDate($date)->sum('slots');
		?>
			<td align="center">{{ $vto_agent_count }}</td>
			<td align="center">{{ $vto_count }}</td>
			<th align="center">{{ $vto_forecasted_total }}</th>

			<td align="center">{{ $vot_agent_count }}</td>
			<td align="center">{{ $vot_count }}</td>
			<th align="center">{{ $vot_forecasted_total }}</th>
		@endforeach
	</tr>
	@endforeach
</table>