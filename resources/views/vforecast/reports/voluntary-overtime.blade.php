<table border="1">
	<tr>
		<th colspan="9">{{ $file }}</th>
	</tr>
	<tr>
		<th>Request ID</th>
		<th>Avaya</th>
		<th>TAS Login</th>
		<th>Name</th>
		<th>Work Pattern</th>
		<th>Organization</th>
		<th>Shift Date</th>
		<th>Schedule Start</th>
		<th>Schedule End</th>
	</tr>
	@foreach($rows as $row)
		<tr>
			<td>{{ $row->id }}</td>
			<td>{{ $row->request->user->information->avaya }}</td>
			<td>{{ $row->request->user->username }}</td>
			<td>{{ $row->request->user->name }}</td>
			<td>{{ $row->request->user->information->wp }}</td>
			<td>{{ $row->request->user->information->skill }}</td>
			<td>{{ $row->forecast->shift_date }}</td>
			<td>{{ date('h:i A' ,strtotime($row->forecast->time_start)) }} </td>
			<td>{{ date('h:i A' ,strtotime($row->forecast->time_end)) }}</td>
		</tr>
	@endforeach
</table>