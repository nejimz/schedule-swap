@extends('layouts.master')

@section('content')

<div class="container">
  <div class="columns">
    <div class="column is-3"></div>
    <div class="column is-6">
      <div class="card">
        <div class="card-header">
          <label class="card-header-title label" for="search_avaya">Download Forecasted VTO/VOT</label>
        </div>
        <div class="card-content">
          @include('partials.error-message')
          <form id="ss-download" method="get" action="{{ route('vot-enterprise-reports.show', str_random(10)) }}">
			<div class="field is-horizontal">
				<div class="field-label is-normal">
					<label class="label" for="type">Forecast Type</label>
				</div>
				<div class="field-body">
					<div class="field">
						<div class="control">
							<div class="select is-fullwidth">
								<select id="type" name="type">
									<option value="">-- Select --</option>
									<option value="ot">Overtime</option>
									<option value="to">Time-Off</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="field is-horizontal">
				<div class="field-label is-normal">
					<label class="label" for="center_id">Center</label>
				</div>
				<div class="field-body">
					<div class="field">
						<div class="control">
							<div class="select is-fullwidth">
								<select id="center_id" name="center_id">
									<option value="0">-- Select All --</option>
									@foreach($centers as $row)
										<option value="{{ $row->id }}">{{ $row->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="field is-horizontal">
				<div class="field-label is-normal">
				<label class="label" for="organization" title="Organization">Org</label>
				</div>
				<div class="field-body">
					<div class="field">
						<div class="control">
							<div class="select is-fullwidth">
							<select id="organization" name="organization">
								<option value="">-- Select All --</option>
							</select>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="field is-horizontal">
				<div class="field-label is-normal">
					<label class="label" for="shift_date">Shift Date</label>
				</div>
				<div class="field-body">
					<div class="field">
						<div class="control has-icons-left is-expanded">
							<input class="input" type="text" id="shift_date" name="shift_date" autocomplete="off" />
							<span class="icon is-small is-left">
								<i class="fa fa-calendar"></i>
							</span>
						</div>
					</div>
				</div>
			</div>

			<div class="field is-horizontal">
				<div class="field-label is-normal">
					<label class="label" for="shift_date">&nbsp;</label>
				</div>
				<div class="field-body">
					<div class="field">
						<div class="control has-icons-left is-expanded">
							{!! csrf_field() !!}
							<button class="button is-info" type="submit">Download</button>
						</div>
					</div>
				</div>
			</div>
          </form>
        </div>
      </div>
    </div>
    <div class="column is-3"></div>
  </div>
</div>

@endsection

@section('scripts')
  <script>
    // bulmaCalendar.attach('#shift_date');
    	skills_wps(0);
    	$('#shift_date').datepicker();
				var option_select_default = '<option value="">-- Select All --</option>';
				var xhr_skills_wps = null;

				$('#center_id').change(function(){
					var center = $(this).val();
					if(center != '')
					{
						skills_wps(center);
					}
					else
					{
							$('#organization, #wp').html(option_select_default);
					}
			});

			function skills_wps(center)
			{
					xhr_skills_wps = $.ajax({
					type : 'get',
					url : '{{ route("skills_wps_json") }}/' + center,
					cache : false,
					dataType : "json",
							beforeSend: function(xhr){
								if (xhr_skills_wps != null)
								{
									xhr_skills_wps.abort();
								}
							}
					}).done(function(result) {
						var skills = option_select_default;
						var wps = option_select_default;
					$.each(result['skills'], function(key, value){
						skills += '<option value="' + value + '">' + value + '</option>';
					});
					$.each(result['wps'], function(key, value){
						wps += '<option value="' + value + '">' + value + '</option>';
					});
						$('#organization').html(skills);
						$('#wp').html(wps);
					}).fail(function(jqXHR, textStatus){
						//
					});
			}
  </script>
@endsection

@section('styles')
	<style>
		.field-label label {
			width: 105px;
		}
	</style>
@endsection



