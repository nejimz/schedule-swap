@foreach($dates as $date)
<table border="1" width="100%">
	<tr>
		<th colspan="4">{{ $date }}</th>
	</tr>
	@foreach($centers as $center)
	<tr>
		<th colspan="4" align="left">{{ $center->name }}</th>
	</tr>
	<tr>
		<th align="left">Organization</th>
		<th align="center">Type</th>
		<th align="center">Slot/s</th>
		<th align="center">Availed</th>
	</tr>
	<?php
	$center_id = $center->id;
	$organizations_first = App\ForecastVTO::whereCenterId($center_id)->whereShiftDate($date)->groupBy('organization')->orderBy('organization', 'ASC')->select([DB::raw('\'VTO\' AS forecast_type'), 'organization', DB::raw('SUM(slots) AS total_slots')]);

	$organizations = App\ForecastVOT::whereCenterId($center_id)->whereShiftDate($date)->groupBy('organization')->orderBy('organization', 'ASC')->union($organizations_first)->get([DB::raw('\'VOT\' AS forecast_type'), 'organization', DB::raw('SUM(slots) AS total_slots')]);
	?>
		@foreach($organizations as $row)
		<?php
		$organization = $row->organization;

		if ($row->forecast_type == 'VTO') {
			$availed = App\ForecastVTORequestItem::whereHas('forecast', function($query) use ($center_id, $date, $organization) {
				$query->whereCenterId($center_id)->whereShiftDate($date)->whereOrganization($organization);
			})->count('id');
		} else {
			$availed = App\ForecastVOTRequestItem::whereHas('forecast', function($query) use ($center_id, $date, $organization) {
				$query->whereCenterId($center_id)->whereShiftDate($date)->whereOrganization($organization);
			})->count('id');
		}
		?>
		<tr>
			<td align="left">{{ $organization }}</td>
			<td align="center">{{ $row->forecast_type }}</td>
			<td align="center">{{ $row->total_slots }}</td>
			<td align="center">{{ $availed }}</td>
		</tr>
		@endforeach
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	@endforeach
</table>
<br>
@endforeach