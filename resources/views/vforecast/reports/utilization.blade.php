@extends('layouts.master')

@section('content')

<div class="container">
  	<div class="columns">
    	<div class="column is-3"></div>
    	<div class="column is-6">

      		<div class="card">
		        <div class="card-header">
		          	<label class="card-header-title label" for="search_avaya">VOT/VTO Utilization</label>
		        </div>
		        <div class="card-content">
		          	@include('partials.error-message')
		          	<form id="ss-download" method="get" action="{{ route('forecast.utilization.create') }}">
						<div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label" for="center_id">Forecast Type</label>
							</div>
							<div class="field-body">
								<div class="field">
									<div class="control">
										<div class="select is-fullwidth">
											<select id="center_id" name="center_id">
							                    <option value="">-- All --</option>
							                    @foreach($centers as $center)
							                    <option value="{{ $center->id }}">{{ $center->name }}</option>
							                    @endforeach
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label" for="detailed_date_from">From</label>
							</div>
							<div class="field-body">
								<div class="field">
									<div class="control has-icons-left is-expanded">
										<input class="input" type="text" id="detailed_date_from" name="date_from" autocomplete="off" />
										<span class="icon is-small is-left">
											<i class="fa fa-calendar"></i>
										</span>
									</div>
								</div>
							</div>
						</div>

						<div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label" for="detailed_date_to">To</label>
							</div>
							<div class="field-body">
								<div class="field">
									<div class="control has-icons-left is-expanded">
										<input class="input" type="text" id="detailed_date_to" name="date_to" autocomplete="off" />
										<span class="icon is-small is-left">
											<i class="fa fa-calendar"></i>
										</span>
									</div>
								</div>
							</div>
						</div>

						<div class="field is-horizontal">
							<div class="field-label is-normal"></div>
							<div class="field-body">
								<div class="field">
			                		<button class="button is-info" type="submit">Download</button>
								</div>
							</div>
						</div>
		          	</form>
		        </div>
		    </div>
		    <br>
      		<div class="card">
		        <div class="card-header">
		          	<label class="card-header-title label" for="search_avaya">VOT/VTO Utilization Detailed</label>
		        </div>
		        <div class="card-content">
		          	@include('partials.error-message')
		          	<form id="ss-download" method="get" action="{{ route('forecast.utilization-detailed.create') }}">
						<div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label" for="center_id">Forecast Type</label>
							</div>
							<div class="field-body">
								<div class="field">
									<div class="control">
										<div class="select is-fullwidth">
											<select id="center_id" name="center_id">
							                    <option value="">-- All --</option>
							                    @foreach($centers as $center)
							                    <option value="{{ $center->id }}">{{ $center->name }}</option>
							                    @endforeach
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label" for="date_from">From</label>
							</div>
							<div class="field-body">
								<div class="field">
									<div class="control has-icons-left is-expanded">
										<input class="input" type="text" id="date_from" name="date_from" autocomplete="off" />
										<span class="icon is-small is-left">
											<i class="fa fa-calendar"></i>
										</span>
									</div>
								</div>
							</div>
						</div>

						<div class="field is-horizontal">
							<div class="field-label is-normal">
								<label class="label" for="date_to">To</label>
							</div>
							<div class="field-body">
								<div class="field">
									<div class="control has-icons-left is-expanded">
										<input class="input" type="text" id="date_to" name="date_to" autocomplete="off" />
										<span class="icon is-small is-left">
											<i class="fa fa-calendar"></i>
										</span>
									</div>
								</div>
							</div>
						</div>

						<div class="field is-horizontal">
							<div class="field-label is-normal"></div>
							<div class="field-body">
								<div class="field">
			                		<button class="button is-info" type="submit">Download</button>
								</div>
							</div>
						</div>
		          	</form>
		        </div>
		    </div>

    	</div>
    	<div class="column is-3"></div>
  	</div>
</div>

@endsection

@section('scripts')
  <script>
    $('#date_from, #date_to, #detailed_date_from, #detailed_date_to').datepicker();
  </script>
@endsection



