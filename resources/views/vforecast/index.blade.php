@extends('layouts.master')

@section('content')
<style>

.calendar-date {
	font-size: 18px !important;
}
.date-item { 
	font-size: 18px !important;
}
.calendar-body .calendar-date {
	height: 70px !important;
	/*border: 1px solid #ccc !important;*/
}
</style>
<h4 class="title is-4">Forecasted</h4>
<div class="columns">
	<div class="column is-6">
		@include('vendor.calendar.bulma')
	</div>
	<div class="column is-6">
		<table id="forecast-list" class="table is-narrow is-fullwidth" style="display: none;">
			<thead>
					<tr class="is-selected">
						<td colspan="4" id="shift_schedule" class=" has-text-centered">&nbsp;</td>
					</tr>
				<tr>
					<th width="40%">Organization</th>
					<th width="30%">Schedule</th>
					<th width="20%" class="has-text-centered">Type</th>
					<th width="10%" class="has-text-centered">Slot/s</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

<div class="columns">
	<div class="column is-4">&nbsp;</div>
	<div class="column is-4">&nbsp;</div>
	<div class="column is-4">&nbsp;</div>
</div>

@endsection

@section('scripts')
	<script src="{{ asset('js/moment.js') }}"></script>
	<script>
		var xhr_vot_request = null;
		var xhr_vot_submit = null;
		var calendar_month = moment().format('MMMM YYYY');
		var calendar = {
			month:
			{
				next: function() {
					calendar_month = moment(calendar_month, 'MMMM YYYY').add(1, 'month').format('MMMM YYYY');
					calendar.month.update(calendar_month);
				},
				previous: function() {
					calendar_month = moment(calendar_month, 'MMMM YYYY').subtract(1, 'month').format('MMMM YYYY');
					calendar.month.update(calendar_month);
				},
				update: function(calendar_month) {
					document.getElementById('calendar-month').innerHTML = calendar_month;
					$.ajax({
						url: "{{ route('forecast.calendar') }}", 
						data: { calendar_month: calendar_month },
						dataType: "html",
						beforeSend: function(xhr) {
							$('.calendar-body').html('<img src="{{ asset("img/loading/2.gif") }}" title="Loading..." />');
						}
					}).done(function(data){
						$('.calendar-body').html(data);
					});
				}
			}
		};

		calendar.month.update(calendar_month);

		function forecasted(shift_date)
		{
			$('#forecast-list').show();
			var xhr_vot_request = $.ajax({
		        type : "get",
		        url : "{{ route('forecast.list') }}/" + shift_date,
		        cache : false,
		        dataType : "json",
		        beforeSend: function(xhr){
		        	$('#forecast-list tbody').html('<tr><td colspan="3" class="has-text-centered"><img src="{{ asset("img/loading/2.gif") }}" title="Loading..." /></td></tr>');
			        if (xhr_vot_request != null)
			        {
			        	xhr_vot_request.abort();
			        }
		        }
			}).done(function(json){
				//console.log(json);
				var tbody = '';
				$.each(json, function(key, value){

					tbody += '<tr><td><label class="checkbox">' + value['skill'] + '</label></td><td><label class="checkbox">' + value['schedule'] + '</label></td><td class="has-text-centered">' + value['type'] + '</td><td class="has-text-centered">' + value['slots'] + '</td></tr>';

				});
				$('#shift_schedule').html('' + shift_date);
				$('#forecast-list tbody').html(tbody);
				window.location = "#forecast-list";
      		}).fail(function(jqXHR, textStatus){

			});
		}
	</script>
@endsection



