@extends('layouts.master')

@section('content')<style>

.calendar-date {
	font-size: 18px !important;
}
.date-item { 
	font-size: 18px !important;
}
.calendar-body .calendar-date {
	height: 70px !important;
	/*border: 1px solid #ccc !important;*/
}
</style>
<h4 class="title is-4">Forecasted Voluntary Overtime</h4>
<div class="columns">
	<div class="column is-7">
		@include('vendor.calendar.bulma')
	</div>
	<div class="column is-5">
		<div id="messages"></div>
		<form id="vot-form" method="post" action="{{ route('vot.submit') }}" style="display: none;">
			<table id="vot-list" class="table is-narrow is-fullwidth">
				<thead>
					<tr class="is-selected">
						<td colspan="3" id="shift_schedule" class=" has-text-centered">&nbsp;</td>
					</tr>
					<tr>
						<th width="10%"></th>
						<th width="60%">Schedule</th>
						<th width="30%" class="has-text-centered">Slot/s</th>
					</tr>
				</thead>
				<tbody></tbody>
				@if(Auth::user()->isAgent())
				<tfoot>
					<tr>
						<td colspan="3" class="has-text-centered">
							<div class="notification is-info">
								<label class="checkbox">
									<input type="checkbox" id="confirmation" name="confirmation">
									I confirm the new schedule. I commit to adhere to the new schedule stated above. I understand that once the VOLUNTARY OVERTIME is accepted, it is final. Failure to comply shall be subject to schedule adherance. 
								</label>
							</div>
							{!! csrf_field() !!}
							<input type="hidden" id="vot_shift_date" name="vot_shift_date" value="">
							<input type="hidden" id="vot_schedule" name="vot_schedule" value="">
			    			<button id="submit-button" class="button is-info" type="submit" disabled="">Submit</button>
						</td>
					</tr>
				</tfoot>
				@endif
			</table>

		</form>
	</div>
</div>

<div class="columns">
	<div class="column is-4">&nbsp;</div>
	<div class="column is-4">
	</div>
	<div class="column is-4">&nbsp;</div>
</div>
@endsection

@section('scripts')
	<script src="{{ asset('js/moment.js') }}"></script>
	<script type="text/javascript">

		var xhr_vot_request = null;
		var xhr_vot_submit = null;
		var calendar_month = moment().format('MMMM YYYY');
		var calendar = {
			month:
			{
				next: function() {
					calendar_month = moment(calendar_month, 'MMMM YYYY').add(1, 'month').format('MMMM YYYY');
					calendar.month.update(calendar_month);
				},
				previous: function() {
					calendar_month = moment(calendar_month, 'MMMM YYYY').subtract(1, 'month').format('MMMM YYYY');
					calendar.month.update(calendar_month);
				},
				update: function(calendar_month) {
					document.getElementById('calendar-month').innerHTML = calendar_month;
					$.ajax({
						url: "{{ route('vot.calendar') }}", 
						data: { calendar_month: calendar_month },
						dataType: "html",
						beforeSend: function(xhr) {
							$('.calendar-body').html('<img src="{{ asset("img/loading/2.gif") }}" title="Loading..." />');
						}
					}).done(function(data){
						$('.calendar-body').html(data);
					});
				}
			}
		};

		calendar.month.update(calendar_month);

		function vot_request(shift_date, schedule)
		{
			$('#vot-form').show();
		    $('table#vot-list tbody').html('<tr><td colspan="3" class="has-text-centered"><img src="{{ asset("img/loading/2.gif") }}" title="Loading..." /></td></tr>');
			var xhr_vot_request = $.ajax({
		        type : "get",
		        url : "{{ route('vot.list') }}/" + shift_date + "/" + schedule + "",
		        cache : false,
		        dataType : "json",
		        beforeSend: function(xhr){
			        if (xhr_vot_request != null)
			        {
			        	xhr_vot_request.abort();
			        }
		        }
			}).done(function(json){
				//console.log(json);
				var tbody = '';
				var counter = 0;
				$.each(json, function(key, value){
					var checkbox = '';
					var disable_label = 'data-tooltip="Available"';
					var disable_check = 'data-tooltip="Available"';
					
					if(value['status'] == 1)
					{
						var disable_label = 'disabled="disabled" data-tooltip="You`re on schedule!"';
						var disable_check = 'disabled="disabled" data-tooltip="You`re on schedule!"';
						checkbox += '<label class="checkbox tooltip" for="id' + value['id'] + '" ' + disable_check + '><input id="id' + value['id'] + '" name="items[' + value['id'] + ']" ' + disable_check + ' class="cb is-readonly" type="checkbox" value="' + value['id'] + '" /></label><input type="hidden" name="schedule_list[' + counter + ']" value="' + value['schedule'] + '"/>';
						counter++;
					}
					else if(value['status'] == 2)
					{
						var disable_label = 'disabled="disabled" data-tooltip="Availed!"';
						var disable_check = 'disabled="disabled" data-tooltip="Availed!"';
						checkbox += '<label class="checkbox tooltip" for="id' + value['id'] + '" ' + disable_check + '><input id="id' + value['id'] + '" name="prev_items[' + value['id'] + ']" type="hidden" value="' + value['id'] + '"><input id="id' + value['id'] + '" name="items[' + value['id'] + ']" type="hidden" value="' + value['id'] + '"><input id="id' + value['id'] + '" name="items[' + value['id'] + ']" ' + disable_check + ' class="cb" type="checkbox" value="' + value['id'] + '" checked /></label>';
					}
					else if(value['status'] == 3)
					{
						var disable_label = 'disabled="disabled" data-tooltip="Expired!"';
						var disable_check = 'disabled="disabled" data-tooltip="Expired!"';
						checkbox += '<label class="checkbox tooltip" for="id' + value['id'] + '" ' + disable_check + '><input id="id' + value['id'] + '" name="expr_items[' + value['id'] + ']" type="hidden" value="' + value['id'] + '"><input id="id' + value['id'] + '" name="items[' + value['id'] + ']" type="hidden" value="' + value['id'] + '"><input id="id' + value['id'] + '" name="items[' + value['id'] + ']" ' + disable_check + ' class="cb" type="checkbox" value="' + value['id'] + '" /></label>';
					}
					else
					{
						checkbox += '<label class="checkbox tooltip" for="id' + value['id'] + '" ' + disable_check + '><input id="id' + value['id'] + '" name="items[' + value['id'] + ']" ' + disable_check + ' class="cb" type="checkbox" value="' + value['id'] + '" /></label>';
					}
					<?php if(!Auth::user()->isAgent()){ echo'checkbox = \'\';'; } ?>
					tbody += '<tr><td>' + checkbox + '</td><td><label class="checkbox tooltip" for="id' + value['id'] + '" ' + disable_label + '>' + value['schedule'] + '</label></td><td class="has-text-centered">' + value['slots'] + '</td></tr>';
				});
				$('#shift_schedule').html('' + shift_date + ' ' + schedule + '<input type="hidden" id="my_schedule" value="' + schedule + '">');
				$('#vot_shift_date').val(shift_date);
				$('#vot_schedule').val(schedule);
				$('table#vot-list tbody').html(tbody);
				window.location = "#messages";
      		}).fail(function(jqXHR, textStatus){

			});
		}

		function error_message(errors)
		{
			var messages = '';
			$.each(errors, function(key, value){
				messages += '' + value[0] + '<br />';
			});

			return messages;
		}

		function auto_checked_next_cb(next_element)
		{
			if (!next_element.attr('disabled')) {
				next_element.attr('checked', true).attr('onclick', 'return false;').bind('click');
			}
		}

		$(function(){
			$(document).on('change', 'input.cb', function(){
				var my_schedule = $('#my_schedule').val();
				var allCheckedCB = $('input:checkbox:checked').length;
				var index = $(':input').index(this);
				var prev_element = $(':input:eq(' + (index - 1) + ')');
				var next_element = $(':input:eq(' + (index + 1) + ')');
				var status = this.checked;
				$(this).attr('checked', true);

				//if (allCheckedCB > 2 && my_schedule == 'Off' && !prev_element.is(':checked')) {
				//	$(this).prop('checked', false);
				//	$(this).removeAttr('checked');
			    //	$('#messages').html('<div class="notification is-warning">Gaps on Rest Day are not allowed!<br />Please plot your overtime hours consecutively.</div>');
				//	return false;
				//}

				/*if (next_element.attr('onclick') != '' && !$(this).is(':checked')) {
					$(this).attr('checked', false);
					//next_element.removeAttr('checked').removeAttr('onclick').unbind('click');
					next_element.attr('checked', false).removeAttr('onclick').unbind('click');

					var first_checked_cb_index = $(':input').index($('input:checkbox:checked:first'));
					var first_checked_cb_next_element = $(':input:eq(' + (first_checked_cb_index + 1) + ')');

					auto_checked_next_cb(first_checked_cb_next_element);

				} else if (allCheckedCB == 1) {
					auto_checked_next_cb(next_element);
				}*/
			});

			$('#vot-form').submit(function(e){

				var xhr_vot_submit = $.ajax({
			        type : "post",
			        url : "{{ route('vot.store') }}",
			        cache : false,
			        data: $(this).serialize(),
			        dataType : "json",
			        beforeSend: function(xhr){
			        	$(this).attr('disabled', true);
				        if (xhr_vot_submit != null)
				        {
				        	xhr_vot_submit.abort();
				        }
			        }
				}).done(function(json){
					//console.log(json);
					//var tbody = '';
			        vot_request(json['shift_date'], json['schedule']);
			      	$('#messages').html('<div class="notification is-success">' + json['success'] + '</div><br />');
					window.location = "#messages";
	      		}).fail(function(jqXHR, textStatus){
			    	var errors = error_message(jqXHR['responseJSON']['errors']);
			    	$('#messages').html('<div class="notification is-warning">' + errors + '</div><br />');
					window.location = "#messages";
				});

				e.preventDefault();
				return false;
			});

			$('#confirmation').change(function(){
				$("#submit-button").attr("disabled", !this.checked);
			});
		});
	</script>
@endsection



