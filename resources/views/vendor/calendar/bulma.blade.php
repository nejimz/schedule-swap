<div class="calendar is-large" id="forcasted-calendar">
  <div class="calendar-nav">
    <div class="calendar-nav-previous-month">
      <button class="button is-small is-text"  onclick="calendar.month.previous()">
        <svg viewBox="0 0 50 80" xml:space="preserve">
          <polyline fill="none" stroke-width=".5em" stroke-linecap="round" stroke-linejoin="round" points="45.63,75.8 0.375,38.087 45.63,0.375 "></polyline>
        </svg>
      </button>
    </div>
    <div class="calendar-month" id="calendar-month"></div>
    <div class="calendar-nav-next-month">
      <button class="button is-small is-text" onclick="calendar.month.next()">
        <svg viewBox="0 0 50 80" xml:space="preserve">
          <polyline fill="none" stroke-width=".5em" stroke-linecap="round" stroke-linejoin="round" points="0.375,0.375 45.63,38.087 0.375,75.8 "></polyline>
        </svg>
      </button>
    </div>
  </div>
  <div class="calendar-container">
    <div class="calendar-header">
      <div class="calendar-date">Sun</div>
      <div class="calendar-date">Mon</div>
      <div class="calendar-date">Tue</div>
      <div class="calendar-date">Wed</div>
      <div class="calendar-date">Thu</div>
      <div class="calendar-date">Fri</div>
      <div class="calendar-date">Sat</div>
    </div>
    <div class="calendar-body">Loading...</div>
  </div>
</div>