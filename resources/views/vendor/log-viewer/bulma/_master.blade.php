<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Admin Portal</title>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
        <link href="{{ asset('css/jqueryui.css') }}" rel="stylesheet">
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        <style>
        /*
         * Boxes
         */

        .box {
            display: block;
            padding: 0;
            min-height: 70px;
            background: #fff;
            width: 100%;
            box-shadow: 0 1px 1px rgba(0,0,0,0.1);
            border-radius: .25rem;
        }

        .box > .box-icon > i,
        .box .box-content .box-text,
        .box .box-content .box-number {
            color: #FFF;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
        }

        .box > .box-icon {
            border-radius: 2px 0 0 2px;
            display: block;
            float: left;
            height: 70px; width: 70px;
            text-align: center;
            font-size: 40px;
            line-height: 70px;
            background: rgba(0,0,0,0.2);
        }

        .box .box-content {
            padding: 5px 10px;
            margin-left: 70px;
        }

        .box .box-content .box-text {
            display: block;
            font-size: 1rem;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            font-weight: 600;
        }

        .box .box-content .box-number {
            display: block;
        }

        .box .box-content .progress {
            background: rgba(0,0,0,0.2);
            margin: 5px -10px 5px -10px;
        }

        .box .box-content .progress .progress-bar {
            background-color: #FFF;
        }

        /*
         * Log Menu
         */

        .log-menu .list-group-item.disabled {
            cursor: not-allowed;
        }

        .log-menu .list-group-item.disabled .level-name {
            color: #D1D1D1;
        }

        /*
         * Log Entry
         */

        .stack-content {
            color: #AE0E0E;
            font-family: consolas, Menlo, Courier, monospace;
            white-space: pre-line;
            font-size: 10pt;
        }

        /*
         * Colors: Badge & Infobox
         */

        .tag.badge-env,
        .tag.badge-level-all,
        .tag.badge-level-emergency,
        .tag.badge-level-alert,
        .tag.badge-level-critical,
        .tag.badge-level-error,
        .tag.badge-level-warning,
        .tag.badge-level-notice,
        .tag.badge-level-info,
        .tag.badge-level-debug,
        .tag.empty {
            color: #fff;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
        }

        .tag.badge-level-all,
        .box.level-all {
            background-color: {{ log_styler()->color('all') }};
        }

        .tag.badge-level-emergency,
        .box.level-emergency {
            background-color: {{ log_styler()->color('emergency') }};
        }

        .tag.badge-level-alert,
        .box.level-alert  {
            background-color: {{ log_styler()->color('alert') }};
        }

        .tag.badge-level-critical,
        .box.level-critical {
            background-color: {{ log_styler()->color('critical') }};
        }

        .tag.badge-level-error,
        .box.level-error {
            background-color: {{ log_styler()->color('error') }};
        }

        .tag.badge-level-warning,
        .box.level-warning {
            background-color: {{ log_styler()->color('warning') }};
        }

        .tag.badge-level-notice,
        .box.level-notice {
            background-color: {{ log_styler()->color('notice') }};
        }

        .tag.badge-level-info,
        .box.level-info {
            background-color: {{ log_styler()->color('info') }};
        }

        .tag.badge-level-debug,
        .box.level-debug {
            background-color: {{ log_styler()->color('debug') }};
        }

        .tag.empty,
        .box.empty {
            background-color: {{ log_styler()->color('empty') }};
        }

        .tag.badge-env {
            background-color: #6A1B9A;
        }
    </style>
    </head>
    <body>
        <div id="app">
            @include('admin.partials.navbar')
            <div class="columns is-fullheight is-marginless">
                <div class="column is-2 is-sidebar-menu is-hidden-mobile" style="padding:15px 0px;">
                  @include('admin.partials.menu')
                </div>
                <div class="column is-main-content">
                  @yield('content')
                </div>
             </div>
        </div>
        @yield('scripts')
        @yield('modals')
        <script type="text/javascript">
        
        function toggleBurger()
        {
            var burger = $('.burger');
            var menu = $('.navbar-menu');
            burger.toggleClass('is-active');
            menu.toggleClass('is-active');
        }
        </script>
    </body>
</html>
