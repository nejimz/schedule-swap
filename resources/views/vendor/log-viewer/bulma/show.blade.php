@extends('log-viewer::bulma._master')

@section('content')
    <div class="columns is-marginless">
        <div class="column">
            <h1 class="title is-3">Log [{{ $log->date }}]</h1>
        </div>
    </div>
    <div class="columns is-marginless">
        <div class="column is-3">
            <nav class="card">
                <header class="card-header">
                    <p class="card-header-title" style="border-bottom: 1px solid rgb(219, 219, 219);">
                       <i class="fa fa-fw fa-flag"></i> Levels
                    </p>
                </header>        
                <div class="card-content is-paddingless">
                    <table class="table is-narrow is-marginless is-stripped is-hoverable is-fullwidth">
                        <tbody>
                            @foreach($log->menu() as $levelKey => $item)
                                <tr>
                                    <td class="is-paddingless">
                                        @if ($item['count'] === 0)
                                            <a class="panel-block is-borderless has-text-grey-lighter" style="border:0px; background: #fff;">
                                                <span class="panel-icon has-text-grey-lighter">
                                                  {!! $item['icon'] !!} 
                                                </span>
                                                <p style="position: relative; width: 100%;">
                                                    {{ $item['name'] }}
                                                    <span class="tag empty" style="position: absolute; right:0px;">
                                                        {{ $item['count'] }}
                                                    </span>
                                                </p>
                                            </a>   
                                        @else
                                            <a class="panel-block" href="{{ $item['url'] }}" style="border:0px; background: #fff;">
                                                <span class="panel-icon has-text-link">
                                                  {!! $item['icon'] !!} 
                                                </span>
                                                <p style="position: relative; width: 100%;">
                                                    {{ $item['name'] }}
                                                    <span class="tag badge-level-{{ $levelKey }}" style="position: absolute; right:0px;">
                                                        {{ $item['count'] }}
                                                    </span>
                                                </p>
                                            </a>   
                                        @endif
                                    </td>
                                </tr>
                            @endforeach     
                        </tbody> 
                    </table>
                </div>            
            </nav>
        </div>
        <div class="column is-9">
            <nav class="card">
                <header class="card-header">
                    <p class="card-header-title">
                        Log Info:
                    </p>
                    <a href="{{ route('log-viewer::logs.download', [$log->date]) }}" class="button is-primary is-right" style="margin:10px 5px;">
                        <i class="fa fa-download" style="margin-right:5px;"></i> Donwload
                    </a>
                </header>
                <div class="card-content is-paddingless">
                    <table class="table is-stripped is-hoverable is-fullwidth">
                        <tr>
                            <td>File Path:</td>
                            <td colspan="3">{{ $log->getPath() }}</td>
                        </tr>
                        <tr>
                            <td>Log entires: <span class="tag is-link">{{ $entries->total() }}</span></td>
                            <td>Size: <span class="tag is-link">{{ $log->size() }}</span></td>
                            <td>Created at: <span class="tag is-link">{{ $log->createdAt() }}</span></td>
                            <td>Updated at: <span class="tag is-link">{{ $log->updatedAt() }}</span></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div class="field">
                                    <form action="{{ route('log-viewer::logs.search', [$log->date, $level]) }}" method="GET">
                                        <div class="control has-icons-left">
                                            <span class="icon is-left">
                                                <i class="fa fa-search"></i>
                                            </span>
                                            <input class="input" type="input" id="query" name="query"  value="{!! request('query') !!}"type="text" placeholder="Type here to search">
                                        </div>
                                     </form>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </nav>

             <nav class="card">
                @if ($entries->hasPages())
                     <header class="card-header" style="padding:10px; border-bottom: 1px solid #ccc;">
                        <span class="tag is-info is-right" style="margin-right:150px;"> 
                            Page {!! $entries->currentPage() !!} of {!! $entries->lastPage() !!}
                        </span>
                        {!! $entries->links('vendor.pagination.bulma', compact('query')) !!}
                    </header>
                @endif
                 <div class="card-content is-paddingless">
                    @forelse($entries as $key => $entry)
                        <section class="accordions">
                            <article class="accordion">
                                <div class="accordion-header toggle" style="background-color: #fff; color:inherit;">
                                    <table class="table is-narrow is-marginless is-paddingless is-fullwidth">
                                        @if($key == 0)
                                            <thead>
                                                <tr>
                                                    <th>ENV</th>
                                                    <th style="width: 120px;">Level</th>
                                                    <th style="width: 65px;">Time</th>
                                                    <th>Header</th>
                                                    <th class="text-right">Actions</th>
                                                </tr>
                                            </thead>
                                        @endif
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <span class="tag badge-env">{{ $entry->env }}</span>
                                                </td>
                                                <td>
                                                    <span class="tag badge-level-{{ $entry->level }}">
                                                        {!! $entry->level() !!}
                                                    </span>
                                                </td>
                                                <td>
                                                    <span class="tag is-secondary">
                                                        {{ $entry->datetime->format('H:i:s') }}
                                                    </span>
                                                </td>
                                                <td>
                                                    <small>{{ $entry->header }}</small>
                                                </td>
                                                <td class="text-right">
                                                    @if ($entry->hasStack())
                                                        <a class="button is-link is-small" role="button" aria-label="toggle">
                                                            <i class="fa fa-stack-overflow" style="margin-right:5px;"></i> Stack
                                                        </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="accordion-body">
                                    <div class="accordion-content stack-content">
                                        @if ($entry->hasStack())
                                            {!! $entry->stack() !!}
                                        @endif
                                    </div>
                                </div>
                            </article>
                        </section>
                    @empty
                        <span class="tag is-secondary">{{ trans('log-viewer::general.empty-logs') }}</span>
                    @endforelse
                </div>
            </nav>
        </div>
    </div>
@endsection

@section('modals')
    {{-- DELETE MODAL --}}
    <div id="delete-log-modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form id="delete-log-form" action="{{ route('log-viewer::logs.delete') }}" method="POST">
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="date" value="{{ $log->date }}">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">DELETE LOG FILE</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to <span class="badge badge-danger">DELETE</span> this log file <span class="badge badge-primary">{{ $log->date }}</span> ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary mr-auto" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-sm btn-danger" data-loading-text="Loading&hellip;">DELETE FILE</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            var deleteLogModal = $('div#delete-log-modal'),
                deleteLogForm  = $('form#delete-log-form'),
                submitBtn      = deleteLogForm.find('button[type=submit]');

            deleteLogForm.on('submit', function(event) {
                event.preventDefault();
                submitBtn.button('loading');

                $.ajax({
                    url:      $(this).attr('action'),
                    type:     $(this).attr('method'),
                    dataType: 'json',
                    data:     $(this).serialize(),
                    success: function(data) {
                        submitBtn.button('reset');
                        if (data.result === 'success') {
                            deleteLogModal.modal('hide');
                            location.replace("{{ route('log-viewer::logs.list') }}");
                        }
                        else {
                            alert('OOPS ! This is a lack of coffee exception !')
                        }
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        alert('AJAX ERROR ! Check the console !');
                        console.error(errorThrown);
                        submitBtn.button('reset');
                    }
                });

                return false;
            });

            @unless (empty(log_styler()->toHighlight()))
                $('.stack-content').each(function() {
                    var $this = $(this);
                    var html = $this.html().trim()
                        .replace(/({!! join(log_styler()->toHighlight(), '|') !!})/gm, '<strong>$1</strong>');

                    $this.html(html);
                });
            @endunless
        });
    </script>
@endsection
