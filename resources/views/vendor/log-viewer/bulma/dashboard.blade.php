@extends('log-viewer::bulma._master')

@section('content')
   <div class="columns is-marginless">
        <div class="column">
            <nav class="card">
                <header class="card-header">
                    <p class="card-header-title title is-4 is-marginless">
                        <i class="fa fa-pie-chart"></i>&nbsp;Dashboard
                    </p>
                </header>

                <div class="card-content">
                    <h4 class="title is-6">General</h4>
                    <div class="columns is-multiline">
                        <div class="column is-3">
                            <div class="box level-info">
                                <div class="box-icon" style="color:#fff;">
                                    <i class="fa fa-users"></i>
                                </div>

                                <div class="box-content">
                                    <span class="box-text">Users</span>
                                    <span class="box-number">
                                        <small>{{ \App\User::all()->count() }} entries</small>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="column is-3">
                            <div class="box level-notice">
                                <div class="box-icon" style="color:#fff;">
                                    <i class="fa fa-building"></i>
                                </div>

                                <div class="box-content">
                                    <span class="box-text">Centers</span>
                                    <span class="box-number">
                                        <small>{{ \App\Center::all()->count() }} entries</small>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h4 class="title is-6">Centers</h4>
                        <div class="columns is-multiline">
                            @foreach(\App\Center::all() as $center)
                                <div class="column is-3">
                                    <div class="box level-all">
                                        <div class="box-icon" style="color:#fff; font-size: 25px; font-weight: bold;">
                                            {{ $center->code }}
                                        </div>

                                        <div class="box-content">
                                            <span class="box-text">{{ $center->name }}</span>
                                            <span class="box-number" style="line-height: 17px;">
                                                <small>
                                                    {{ \App\User::where('center_id', $center->id)->where('role','=','agent')->count() }} agents <br>
                                                    {{ \App\User::where('center_id', $center->id)->where('role','=','admin')->count() }} staffs
                                                </small>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    <hr>
                    <h4 class="title is-6">System Logs</h4>
                    <div class="columns">
                        <div class="column is-3">
                            <canvas id="stats-doughnut-chart" height="300" class="mb-3"></canvas>
                        </div>

                        <div class="column is-9">
                            <div class="columns is-multiline">
                                @foreach($percents as $level => $item)
                                    <div class="column is-4">
                                        <div class="box level-{{ $level }} {{ $item['count'] === 0 ? 'empty' : '' }}">
                                            <div class="box-icon">
                                                {!! log_styler()->icon($level) !!}
                                            </div>

                                            <div class="box-content">
                                                <span class="box-text">{{ $item['name'] }}</span>
                                                <span class="box-number">
                                                    <small>{{ $item['count'] }} entries - {!! $item['percent'] !!} %</small>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
@endsection

@section('scripts')
    <script>
        $(function() {
            new Chart(document.getElementById("stats-doughnut-chart"), {
                type: 'doughnut',
                data: {!! $chartData !!},
                options: {
                    legend: {
                        position: 'bottom'
                    }
                }
            });
        });
    </script>
@endsection