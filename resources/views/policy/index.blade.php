@extends('layouts.app')

@section('content')

    <div class="container">
        <h3 class="title has-text-centered">Privacy Policy</h3>

        <h4 class="title">A.	INTRODUCTION</h4>
        <p>This Privacy Notice is hereby adopted in compliance with Republic Act No. 10173 or the Data Privacy Act of 2012 (DPA), its Implementing Rules and Regulations (IRR) , and other relevant policies, including issuances of the National Privacy Commission. The Company respects and values data privacy rights, and makes sure that all personal data collected are processed in adherence to the general principles of transparency, legitimate purpose, and proportionality. </p>
        <br>
        <p>This Notice shall provide information on data protection and security measures, and may serve as a guide in exercising rights under the DPA and its IRR.</p>
        <br>

        <h4 class="title">B.	DEFINITIONS</h4>
        <p><strong>Data Privacy Act or DPA</strong> refers to Republic Act No. 10173 or the Data Privacy Act of 2012 and its implementing rules and regulations. </p>
        <br>
        <p><strong>Company or PCCI</strong> refers to PanAsiatic Call Centers Inc.</p>
        <br>
        <p><strong>Data Subject </strong> refers to an individual whose personal, sensitive personal or privileged information is processed by the organization. It may refer to officers, employees, consultants, and clients of this organization. </p>
        <br>
        <p><strong>Personal Information </strong> refers to any information whether recorded in a material form or not, from which the identity of an individual is apparent or can be reasonably and directly ascertained by the entity holding the information, or when put together with other information would directly and certainly identify an individual. </p>
        <br>
        <p><strong>Processing </strong> refers to any operation or any set of operations performed upon personal information including, but not limited to, the collection, recording, organization, storage, updating or modification, retrieval, consultation, use, consolidation, blocking, erasure or destruction of data. </p>
        <br>
        <p><strong>Security Incident </strong> is an event or occurrence that affects or tends to affect data protection, or may compromise the availability, integrity and confidentiality of Personal Data. It includes incidents that would result to a personal data breach, if not for safeguards that have been put in place. </p>
        <br>
        <p><strong>Personal information </strong> refers to Personal Data: </p>
        <ol style="padding-left:50px;">
            <li>About an individual’s race, ethnic origin, marital status, age, color, and religious, philosophical or political affiliations; </li>
            <li>About an individual’s health, education, genetic or sexual life, or to any proceeding for any offense committed or alleged to have been committed by such individual, the disposal of such proceedings, or the sentence of any court in such proceedings; </li>
            <li>Issued by government agencies peculiar to an individual which includes, but is not limited to, social security numbers, previous or current health records, licenses or its denials, suspension or revocation, and tax returns; and </li>
            <li>Specifically established by an executive order or an act of Congress to be kept classified. </li>
        </ol>
        <br>

        <h4 class="title">C.	SCOPE AND LIMITATIONS </h4>        
        <p>All personnel of this organization, regardless of the type of employment or contractual arrangement, must comply with the terms set out in this Privacy Notice.</p>
        <br>

        <h4 class="title">D.	PROCESSING AND COLLECTION</h4>  
        <p>PCCI respects and values rights of a Data Subject under Data Privacy Act 2012 (DPA) and its IRR. </p>
        <br>
        <p>PCCI is committed to protecting personal information that it collects, uses and/or processes in accordance with the requirements under the DPA and its IRR. For this purpose, PCCI implements reasonable and appropriate security measures to maintain the confidentiality, integrity and availability of such personal information. </p> 
        <br>
        <p>PCCI’s Agent Scheduling tool is an online module used for shift swap requests and processing as well as filing of voluntary overtime or voluntary time-off slots.</p>
        <br>
        <p>PCCI collects, uses and/or processes personal information by electronic or automated means.  </p>
        <br>
        <p>PCCI collects, uses and/or processes any and all personal information necessary and mandatory for PCCI to perform its aforementioned services. Such personal information may include, but is not limited to, the following:  </p>
        <ul style="list-style-type:disc; padding-left:50px;" >
            <li>Full Name </li>
            <li>Email Address  </li>
        </ul>
        <br>
        <p>Should users choose not to provide any of the above personal information, PCCI may not be able to accommodate their request or perform its Services.  </p>
        <br>
        <p>PCCI collects, uses and/or processes personal information for purposes of providing better agent scheduling processes and performing other services necessary for business operational purposes.</p>
        <br>
        <p>PCCI collects personal information when you provide the same electronically online. PCCI, however, may dispose or delete in a secure manner any such personal information in the event that such personal information is not, or no longer, necessary for the Purposes.  </p>
        <br>
        <p>Only authorized PCCI personnel are granted access to personal information collected by PCCI. Personal information collected via the internet or other electronic methods is stored in a secure database.</p>
        <br>
        <p>PCCI uses the personal information it collects only for the abovementioned Purposes as is without further processing. If necessary, SMFI may subject the personal information to additional processing before using the same for such Purposes.  </p>
        <br>
        <p>PCCI stores and processes personal information only within the Philippines. PCCI shall comply with the applicable laws and regulations should it transfer the storage and/or processing thereof outside the country.  </p>
        <br>
        <p>PCCI will not share your personal information with third parties unless necessary for the abovementioned Purposes and unless you give your consent thereto. Such third parties may include PCCI’s subsidiaries and affiliates as well as business partners, service providers and contractors or subcontractors. Any personal information shared with such third parties shall also be covered by the appropriate agreement to ensure that all personal information is adequately safeguarded. </p>
        <br>
        <p>PCCI retains the personal information it collects only for the period allowed under the applicable laws and regulations, including but not limited to the DPA and its IRR. PCCI shall immediately destroy or dispose in a secure manner any personal information the retention of which is no longer allowed under the said applicable laws and regulations.  </p>
        <br>
        <p>The Data Subjects are responsible for the accuracy and correctness of any personal information provided to PCCI, as well as for the consequences of disclosing personal information to PCCI and failure to provide the accurate, correct and updated personal information.  </p>
        <br>
        <p>The Data Subjects have the right to access and to correct or update the personal information, provided, collected and stored, in accordance with the conditions and requirements under the DPA and its IRR.</p>
        <br>
        <p>To access and/or correct or update such personal information, please contact the Data Protection Officer (DPO) by sending an email to shiftswapsupport@panasiaticcallcenters.com. The DPO will contact you within the time required by the law and regulations. Where correction of personal information is not possible, the DPO shall explain the reason for refusing to make such correction.   </p>
        <br>

        <h4 class="title">E.	ORGANIZATIONAL SECURITY MEASURES </h4>  
        <p>A Data Protection Officer (“DPO”) shall be appointed by the Company.   </p>
        <br>
        <p>The Data Protection Officer shall oversee the compliance of the organization with the DPA, its IRR, and other related policies, including the implementation of security measures, security incident and data breach protocol, and the inquiry and complaints procedure.</p>
        <br>
        <p>Company employees will be asked to sign a Non-Disclosure Agreement. All employees with access to personal data shall operate and hold personal data under strict confidentiality if the same is not intended for public disclosure.</p>
        <br>
        <p>This Manual shall be reviewed and evaluated annually. Privacy and security policies and practices within the organization shall be updated to remain consistent with current data privacy best practices.</p>
        <br>

        <h4 class="title">F.	BREACH AND SECURITY INCIDENTS </h4>  
        <p>PCCI’s security incident and response team is available 24/7 to monitor real-time alerts. The team conducts a daily review of all electronic and physical resources used by the Company in compliance with the Company’s Monitoring and Security Incident Response Policy.</p>
        <br>
        <p>The Company shall review security policies, conduct vulnerability assessments and perform penetration testing within the company on regular schedule as prescribed by industry standards.</p>
        <br>

        <h4 class="title">G.	INQUIRY AND COMPLAINTS</h4>  
        <p>For inquiries regarding the processing of personal information stated in this Privacy Notice, as well as any concerns or complaints regarding data privacy, or the exercise of rights as a Data Subject under the DPA and its IRR, you may contact the DPO as follows, provided that any complaint should be in writing, clearly state the material facts, specify your contact information, include supporting evidence and be submitted to the following office address or email address:  </p>
        <br>
        <ul>
            <li>PanAsiatic Solutions Bldg</li>
            <li>C. Hilado Circumferential Road</li>
            <li>Cor. Burgos Extension</li>
            <li>Bacolod City, Negros Occidental</li>
            <li>Philippines 6100</li>
            <li>Tel: +63 (034) 705 5800</li>
            <li>Email: shiftswapsupport@panasiaticcallcenters.com</li>
        </ul>
        <br>

        <h4 class="title">H.	AMENDMENT</h4>  
        <p>The Company may change this Privacy Notice from time to time by posting the updated version of the Privacy Notice. Users are encouraged to visit this site frequently to stay informed about how PCCI uses personal information. </p>
    </div>
@endsection