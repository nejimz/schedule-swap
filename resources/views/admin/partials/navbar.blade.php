@if(Auth::user()->isRoot())
<nav class="navbar is-dark" role="navigation" aria-label="dropdown navigation">
@else
<nav class="navbar is-info" role="navigation" aria-label="dropdown navigation">
@endif
    <div class="navbar-brand">
        <a href="/admin" class="navbar-item">Admin Portal</a>
        <div class="navbar-burger burger" data-target="navMenu" onclick="toggleBurger()">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>

    <div class="navbar-menu">
        <div class="navbar-start">
            <a class="navbar-item">
                Home
            </a>
        </div>

        <div class="navbar-end">
            <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link">
                    <i class="fa fa-user is-hidden-touch" style="margin-right:5px;"></i>
                    <span>{{ Auth::user()->name }}</span>
                </a>

                <div class="navbar-dropdown is-right">
                    <a href="{{ route('home') }}" class="navbar-item" title="Reset Password">Return to Schedule Swap</a>
                    <a href="javascript::void(0)" class="navbar-item" onclick="document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</nav>

