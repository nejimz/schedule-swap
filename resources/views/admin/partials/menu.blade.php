<aside class="menu" style="padding:0px 15px;">
	<ul class="menu-list">
		<p class="menu-label"> Administration </p>
		 <li>
	    	<a href="/admin"> <small><i class="fa fa-chart-pie"></i> Dashboard</small> </a>
	    </li>
	    <li>
	    	<a href="{{ route('admin.user.index') }}"> <small> <i class="fa fa-user"></i> Users</small> </a>
	    </li>
	    <li>
	    	<a href="{{ route('admin.mail-recipients.index') }}"> <small> <i class="fa fa-address-book"></i> Mail Recipients</small> </a>
	    </li>
	   	<li>
	    	<a href="#"> <small> <i class="fa fa-building"></i> Centers</small> </a>
	    </li>
	    <li>
	    	<a href="{{ route('admin.schedule.index') }}"> <small><i class="fa fa-calendar"></i> Schedules</small> </a>
	    </li>
	    <li>
	    	<a href="{{ route('home-password', 100) }}" target="_blank"> <small><i class="fa fa-lock"></i> Password</small> </a>
	    </li>
		<p class="menu-label"> Forecasts </p>
		    <li><a href="#"><small> <i class="fa fa-clock-o"></i> Overtime</small></a></li>
		    <li><a href="#"><small> <i class="fa fa-clock-o"></i> Timeoff</small></a></li>
		<p class="menu-label"> Reports </p>
			<li><a href="#"><small>Agent Absence/Leave Replacement</small></a></li>
		    <li><a href="#"><small>Schedule Swap</small></a></li>
		    <li><a href="#"><small>Voluntary Overtime</small></a></li>
		    <li><a href="#"><small>Voluntary Timeoff</small></a></li>
		<p class="menu-label"> Tools </p>
			<li><a href="{{ route('admin.password-generator.index') }}"><small>Password Generator</small></a></li>
		<p class="menu-label"> Log Viewer </p>
			<li><a href="/admin/logs"><small>Logs</small></a></li>
	</ul>
</aside>