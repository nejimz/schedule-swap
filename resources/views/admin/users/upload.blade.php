@extends('admin.layouts.app')

@section('content')
  <div class="columns is-marginless">
    <div class="column">
      <nav class="card">
        <header class="card-header">
          <p class="card-header-title title is-4 is-marginless">
            <i class="fa fa-user"></i> &nbsp;Upload User
          </p>
          <a href="{{ route('admin.user.index') }}" class="button is-right" style="margin:10px;">
            <span class="icon is-small">
              <i class="fa fa-arrow-left"></i> 
            </span> 
            <span>Back</span>
          </a>
        </header>
        <div class="card-content is-paddingless">
          <div class="columns">
            <div class="column is-4 ">
              <form id="user-upload" action="{{ $route }}" method="post">
                {!! csrf_field() !!}
                <div class="columns is-multiline is-marginless">
                  <div class="column is-12 ">
                    <div class="field">
                      <label class="label">Center</label>
                      <div class="control">
                        <div class="select">
                          <select id="center_id" name="center_id">
                            <option value="">-- Select --</option>
                          @foreach($centers as $center)
                            <option value="{{ $center->id }}">{{ title_case($center->name) }}</option>
                          @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="columns is-multiline is-marginless">
                  <div class="column is-12 ">
                    <div class="field">
                      <label class="label">File</label>
                      <div class="control">
                        <div class="file">
                          <label class="file-label">
                             <input class="file-input" type="file" id="file" name="file">
                            <span class="fa file-cta">
                              <span class="fa file-icon">
                                <i class="fas fa-upload"></i>
                              </span>
                              <span class="file-label">Choose a CSV file...</span>
                            </span>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="columns is-multiline is-marginless">
                  <div class="column is-12 ">
                    <div class="field">
                      <label class="label"></label>
                      <div class="control">
                        <button class="button is-link" type="submit" id="user-upload-btn" disabled="">Upload</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="column is-12">                        
                  @include('partials.error-message')
                </div>
              </form>
            </div>
            <div class="column is-7 ">
              <div class="notification is-info">
                CSV file format is required.<br/>
                Always remove the Header.<br/>
                Always follow the column arrangement.<br/> 
                Download template <a href="{{ asset('templates/upload-user-template.csv') }}">here</a>.
              </div>
              <div class="error-message"></div>
            </div>
            <div class="column is-1 "></div>
          </div>
        </div>
      </nav>
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
  var xhr_file = null;
  var file_arr = [];
  var file_chunk = [];

  $(function(){
    $('#file').change(function(e){
      var file = e.target.files[0];
      if(file['type'] != 'application/vnd.ms-excel')
      {
        $('.error-message').html('<br /><div class="notification is-warning">Only CSV file extension is allowed!</div><br />');
      }
      else
      {
        Papa.parse(file, {
          header: false,
          dynamicTyping: true,
          skipEmptyLines: true,
          complete: function(results, file) {
            file_arr = results.data;
            file_chunk = results.data;
            console.log(file_arr);
            $('#user-upload-btn').attr('disabled', false);
          }// End complete: function(results, file)
        });
      }

    });

    $('#user-upload').submit(function(e){
      if($('#center_id').val() == '')
      {
        $('.error-message').html('<div class="notification is-warning">Center is required!</div><br />');
        return false;
      }
      if($('#file').val() == '')
      {
        $('.error-message').html('<div class="notification is-warning">File is required!</div><br />');
        return false;
      }

      var method = $(this).attr('method');
      var url = $(this).attr('action');
      var token = $('input[name=_token').val();
      //var center = $('#center').val();
      var parameters = 'rows=' + JSON.stringify(file_chunk) + '&center_id=' + $('#center_id').val() + '&_token=' + token;
      xhr_file = $.ajax({
          type : method,
          url : url,
          data : parameters,
          cache : false,
          async: false,
          dataType : "json",
          beforeSend: function(xhr){
            $('#user-upload-btn').attr('disabled', true);
            $('.error-message').html('<br /><img src="{{ asset("img/loading/2.gif") }}" title="Uploading" />');
            if (xhr_file != null)
            {
                xhr_file.abort();
            }
          }
      }).done(function(data){
        xhr_file = null;
        var messages = 'Done uploading logins!<hr />';
        $.each(data['errors'], function(key, value){
          messages += '' + value + '<br />';
        });

        $('.error-message').html('<br /><div class="notification is-success">' + messages + '</div>');
        //console.log(data);
      }).fail(function(jqXHR, textStatus){
          //console.log('Request failed: ' + textStatus);
      });
      return false;
      e.preventDefault();
    });
  });
</script>
@endsection
