@extends('log-viewer::bulma._master')

@section('content')
    <div class="columns is-marginless">
        <div class="column">
            <nav class="card">
                <header class="card-header">
                    <p class="card-header-title title is-4 is-marginless">
                        <i class="fa fa-user"></i> &nbsp; Users
                    </p>
                    
                    <form action="{{route('admin.user.index')}}" method="get">
                        <div class="field has-addons" style="margin:10px;">
                            <div class="control">
                                <input class="input" name="search" type="text" placeholder="Find a user...">
                            </div>
                            <div class="control">
                                <button type="submit" class="button is-info">Search</button>
                            </div>
                        </div>
                    </form>
                     <a href="{{ route('user.upload.index') }}" class="button is-right" style="margin:10px;">
                        <span class="icon is-small">
                            <i class="fa fa-upload"></i> 
                        </span>
                        <span>Import CSV</span>
                    </a>
                    <a href="#" class="button is-right" style="margin:10px;">
                        <span class="icon is-small">
                            <i class="fa fa-download"></i> 
                        </span>
                        <span>Export CSV</span>
                    </a>
                    <a href="{{ route('admin.user.create') }}" class="button is-right" style="margin:10px;">
                        <span class="icon is-small">
                            <i class="fa fa-plus"></i> 
                        </span>
                        <span>Add</span>
                    </a>
                </header>

                <div class="card-content is-paddingless">
                    <table class="table is-bordered is-stripped is-hoverable is-fullwidth">
                        <thead>
                            <tr>
                                <th class="is-dark" width="11%"></th>
                                <th class="is-dark" width="5%"></th>
                                <th class="is-dark" width="40%">Name</th>
                                <th class="is-dark" width="10%">Username</th>
                                <th class="is-dark" width="24%">Email</th>
                                <th class="is-dark" width="10%">Roles</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>
                                        <a href="{{ route('admin.user.edit', $user->id) }}" class="tag is-link"><i class="fa fa-edit"></i></a>
                                        <a href="{{ route('impersonate', $user->id) }}" title="Impersonate" class="tag is-dark"><i class="fa fa-user-secret"></i></a>
                                    </td>
                                    <td>
                                        <form action="{{ route('admin.user.destroy', $user->id) }}" method="post">
                                            {{ csrf_field() }}
                                            @method('DELETE')
                                            <button class="button is-small is-white" type="submit">
                                                @if(!$user->trashed())
                                                <i class="fa fa-toggle-on fa-2x"></i>
                                                @elseif($user->trashed())
                                                <i class="fa fa-toggle-off fa-2x"></i>
                                                @endif
                                            </button>
                                        </form>
                                        <!-- <a href="" title="Active"
                                            data-method="delete" 
                                            data-token="{{ csrf_token() }}" 
                                            data-confirm="Are you sure?" 
                                            class="">
                                            <i class="fa fa-check-square"></i>
                                        </a>
                                        <a href="{{ route('admin.user.destroy', $user->id) }}" title="Disabled"
                                            data-method="delete" 
                                            data-token="{{ csrf_token() }}" 
                                            data-confirm="Are you sure?" 
                                            class="">
                                            <i class="fa fa-square"></i>
                                        </a> -->
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.user.show', $user->id) }}">
                                            {{ $user->name }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.user.show', $user->id) }}">
                                            {{ $user->username }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.user.show', $user->id) }}">
                                            {{ $user->email }}
                                        </a>
                                    </td>
                                    <td>
                                        {{ $user->role }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </nav>
        </div>
    </div>
@endsection
