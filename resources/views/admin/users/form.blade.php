@extends('admin.layouts.app')

@section('content')
  <div class="columns is-marginless">
    <div class="column">
      <nav class="card">
        <header class="card-header">
          <p class="card-header-title title is-4 is-marginless">
            <i class="fa fa-user"></i> &nbsp;
            @if($id > 0)
              Edit User
            @else
              Create User
            @endif
          </p>
          <a href="{{ route('admin.user.index') }}" class="button is-right" style="margin:10px;">
            <span class="icon is-small">
              <i class="fa fa-arrow-left"></i> 
            </span> 
            <span>Back</span>
          </a>
        </header>
        <div class="card-content is-paddingless">
          <form action="{{ $form_route }}" method="post">
            {!! csrf_field() !!}
            <div class="columns is-multiline is-marginless">
              <div class="column is-6 ">
                <div class="field">
                  <label class="label">First Name</label>
                  <div class="control">
                    <input class="input" type="text" placeholder="First Name" name="first_name" value="{{ $first_name }}">
                  </div>
                </div>
              </div>
              <div class="column is-6 ">
                <div class="field">
                  <label class="label">Last Name</label>
                  <div class="control">
                    <input class="input" type="text" placeholder="Last Name" name="last_name" value="{{ $last_name }}">
                  </div>
                </div>
              </div>
            </div>
            <div class="columns is-multiline is-marginless">
              <div class="column is-6 ">
                <div class="field">
                  <label class="label">Username</label>
                  <div class="control">
                    <input class="input" type="text" placeholder="Username" name="username" value="{{ $username }}">
                  </div>
                </div>
              </div>
              <div class="column is-6 ">
                <div class="field">
                  <label class="label">Employee Number</label>
                  <div class="control">
                    <input class="input" type="text" placeholder="Employee Number" name="employee_number" value="{{ $employee_number }}">
                  </div>
                </div>
              </div>
            </div>
            <div class="columns is-multiline is-marginless">
              <div class="column is-6 ">
                <div class="field">
                  <label class="label">E-mail Address</label>
                  <div class="control">
                    <input class="input" type="email" placeholder="email" name="email" value="{{ $email }}">
                  </div>
                </div>
              </div>
              <div class="column is-3 ">
                <div class="field">
                  <label class="label">Role</label>
                  <div class="control">
                    <div class="select">
                      <select id="role" name="role">
                        <option value="">-- Select --</option>
                      @foreach($roles as $row)
                        <option value="{{ $row->name }}">{{ title_case($row->name) }}</option>
                      @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="column is-3 ">
                <div class="field">
                  <label class="label">Center</label>
                  <div class="control">
                    <div class="select">
                      <select id="center_id" name="center_id">
                        <option value="">-- Select --</option>
                      @foreach($centers as $center)
                        <option value="{{ $center->id }}">{{ title_case($center->name) }}</option>
                      @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="columns is-multiline is-marginless">
              <div class="column is-6 ">
                <div class="field">
                  <label class="label"></label>
                  <div class="control">
                    @if($id > 0)
                    <div class="field has-addons">
                      <p class="control">
                        {{ method_field('PUT') }}
                        <button class="button is-link" type="submit">Update</button>
                      </p>
                      <p class="control">
                        <button class="button is-success" id="generate_password" type="button">Generate Password</button>
                      </p>
                      <p class="control">
                        <input class="input" type="text" id="password" readonly="">
                      </p>
                    </div>
                    @else
                    <button class="button is-link" type="submit">Create</button>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="column is-12">                        
              @include('partials.error-message')
            </div>
          </form>
        </div>
      </nav>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
   $('#role').val('{{ $role }}');
   $('#center_id').val('{{ $center_id }}');
   var xhr_generate_password = null;

   $(function(){
    $('#generate_password').click(function(){
      var confirmation = confirm('Are you sure to reset the password?');

      if(confirmation)
      {
        xhr_generate_password = $.ajax({
          type : 'get',
          url : '{{ route("user.generate-password", $id) }}',
          cache : false,
          dataType : "json",
            async : false, 
          beforeSend: function(xhr){
            if (xhr_generate_password != null)
            {
              xhr_generate_password.abort();
            }
          }
        }).done(function(json) {
          $('#password').val(json['password']);
        }).fail(function(jqXHR, textStatus) {
          alert("Please reload your browser!");
        });
      }
    });
   });
  </script>
@endsection
