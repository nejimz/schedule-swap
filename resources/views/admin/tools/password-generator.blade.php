@extends('admin.layouts.app')

@section('content')
 <div class="columns is-marginless">
        <div class="column">
            <nav class="card">
                <header class="card-header">
                    <p class="card-header-title title is-4 is-marginless">
                        <i class="fa fa-key"></i> &nbsp; Password Generator
                    </p>
                    <p style="padding:10px 10px 0px 0px;">
                        <select name="number" onchange="window.location.assign('{{ route('admin.password-generator.index') }}/'+this.value);">
                            @for($i=1; $i<=100; $i++)
                                <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                        <a id="showbtn" onclick="$('.is-hash').removeClass('is-hidden');  $('#hidebtn').removeClass('is-hidden'); $(this).addClass('is-hidden');" href="#">Show Hash</a>
                         <a id="hidebtn" class="is-hidden" onclick="$('.is-hash').addClass('is-hidden'); $('#showbtn').removeClass('is-hidden'); $(this).addClass('is-hidden');" href="#">Hide Hash</a>
                    </p>
                </header>

                <div class="card-content is-paddingless">
                    <table class="table" style="width:100%;">
                        <thead>
                            <tr>
                                <th class="is-dark">Password</th>
                                <th class="is-hash is-dark is-hidden">Hash Key</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($passwords as $password)
                            <tr>
                                <td>{{ $password->string }}</td>
                                <td class="is-hash is-hidden">{{ $password->hash }}</td>
                            </tr>  
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </nav>
        </div>
    </div>        
@endsection
