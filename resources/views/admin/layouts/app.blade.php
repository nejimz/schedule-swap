<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Admin Portal</title>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
        <link href="{{ asset('css/jqueryui.css') }}" rel="stylesheet">
        <script src="{{ asset('js/app.js') }}"></script>
    </head>
    <body>
        <div id="app">
            @include('admin.partials.navbar')
            <div class="columns is-fullheight is-marginless">
                <div class="column is-2 is-sidebar-menu is-hidden-mobile" style="padding:15px 0px;">
                  @include('admin.partials.menu')
                </div>
                <div class="column is-main-content">
                  @yield('content')
                </div>
             </div>
        </div>
        <style>
            html, body, div#app { overflow: hidden !important; }
        </style>
        @yield('styles')

        @yield('modals')

        @yield('scripts')
        
        <!-- Scripts -->

        <script type="text/javascript">
        
        function toggleBurger()
        {
            var burger = $('.burger');
            var menu = $('.navbar-menu');
            burger.toggleClass('is-active');
            menu.toggleClass('is-active');
        }
        </script>
    </body>
</html>
