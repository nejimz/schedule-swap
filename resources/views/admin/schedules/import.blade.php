 <div class="modal" id="importSchedule">
    <div class="modal-background"></div>
    <div class="modal-card">
        <header class="modal-card-head">
            <p class="modal-card-title">Import Schedule</p>
            <button class="delete" aria-label="close" onclick="modals.import.close()"></button>
        </header>
        <section class="modal-card-body is-paddingless" style="height: 280px;">
            <section class="hero is-info">
                <div class="hero-body" style="padding:12px 24px;">                    
                    <ul>
                        <li><strong>Note!</strong> Only CSV file is allowed.</li> 
                    </ul>
                </div>
            </section>
            <div class="field is-grouped is-grouped-centered">
                <div class="control" style="margin-top:10px;">
                    <div class="select is-link">
                        <select name="center_id" id="" style="width:250px;">
                            <option value=""> -- Select a center -- </option>
                            @foreach(\App\Center::all() as $center)
                                <option value="{{ $center->id }}">{{ $center->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="file has-name is-boxed is-success is-centered" style="margin-top:10px;">
                <label class="file-label">
                    <input class="file-input" type="file" name="resume" onchange="schedules.import.select(this)">
                    <span class="file-cta">
                        <span class="file-icon">
                            <i class="fa fa-upload"></i>
                        </span>
                        <span class="file-label">
                            Choose a file…
                        </span>
                    </span>
                    <span class="file-name" id="import-file-name"></span>
                </label>
            </div>
        </section>
        <footer class="modal-card-foot">
            <button class="button is-success" disabled="disabled">
                <span class="icon is-small is-pulled-right">
                    <i class="fa fa-upload"></i> 
                </span>
                &nbsp;Upload
            </button>
            <button class="button is-danger" onclick="modals.import.close()">Cancel</button>
            
            <!-- <span class="icon is-small is-pulled-right">
                <i class="fa fa-exclamation-circle"></i> 
            </span> -->
            <a class="button is-link" href="{{ asset('templates/schedule.csv') }}" style="position:absolute; right:20px;">
                <span class="icon is-small is-pulled-right">
                    <i class="fa fa-download"></i> 
                </span>
                &nbsp;Template
            </a>
        </footer>
    </div>        
</div>