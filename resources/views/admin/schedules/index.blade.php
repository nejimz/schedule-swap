@extends('log-viewer::bulma._master')

@section('content')
    <div class="columns is-marginless">
        <div class="column">
            <nav class="card">
                <header class="card-header">
                    <p class="card-header-title title is-4 is-marginless">
                        <i class="fa fa-calendar"></i> &nbsp; Schedules
                    </p>
                     <a href="#" class="button is-right is-warning" onclick="modals.import.open()" style="margin:10px;">
                        <span class="icon is-small">
                            <i class="fa fa-upload"></i> 
                        </span>
                        <span>Import CSV</span>
                    </a>
                    <a href="#" class="button is-right is-primary" onclick="modals.export.open()" style="margin:10px;">
                        <span class="icon is-small">
                            <i class="fa fa-download"></i> 
                        </span>
                        <span>Export CSV</span>
                    </a>

                    <a href="{{ route('admin.user.create') }}" class="button is-right is-success" style="margin:10px;">
                        <span class="icon is-small">
                            <i class="fa fa-plus"></i> 
                        </span>
                        <span>Add</span>
                    </a>
                </header>

                <div class="card-content is-paddingless">
                      <table class="table is-bordered is-stripped is-hoverable is-fullwidth">
                        <thead>
                            <tr>
                                <th class="is-dark">Center</th>
                                <th class="is-dark">Avaya</th>
                                <th class="is-dark">Work Pattern</th>
                                <th class="is-dark">Skill</th>                                
                                <th class="is-dark">Organization</th>                                                       
                                <th class="is-dark">Tier</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </nav>
        </div>
    </div>
@endsection

@section('modals')
     <!--    Import Modal    -->
   @include('admin.schedules.import')
    <!--    Script Section    -->
    @include('admin.schedules.export')
@endsection

@section('scripts')
    <script>
        var requestSent = false;
        var modals = {
            import: 
            {
                open : function() { 
                    document.getElementById('importSchedule').classList.add('is-active');
                },
                close : function() { 
                    document.getElementById('importSchedule').classList.remove('is-active');
                }
            },
            export: 
            {
                open : function() { 
                    document.getElementById('exportUser').classList.add('is-active');
                },
                close : function() { 
                    document.getElementById('exportUser').classList.remove('is-active');
                }
            }
        };

        var schedules = {
            import :
            {
                select : function(input) {
                     if(input.files[0])
                    {
                        var file = input.files[0];
                        document.getElementById('import-file-name').innerHTML  = file.name;
                    }
                    else
                    {
                        console.log("nofile");
                    }
                }
            }
        };
    </script>
@endsection

<!-- 
    Style Section 
-->
@section('styles')
    <style lang="">
        .modal {
            flex-direction: column;
        }
    </style>
@endsection
