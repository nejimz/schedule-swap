@extends('log-viewer::bulma._master')

@section('content')
    <div class="columns is-marginless">
        <div class="column">
            <nav class="card">
                <header class="card-header">
                    <p class="card-header-title title is-4 is-marginless">
                        <i class="fa fa-user"></i> &nbsp; Mail Recipients
                    </p>
                    
                    <form action="{{route('admin.user.index')}}" method="get">
                        <div class="field has-addons" style="margin:10px;">
                            <div class="control">
                                <input class="input" name="search" type="text" placeholder="Find a user...">
                            </div>
                            <div class="control">
                                <button type="submit" class="button is-info">Search</button>
                            </div>
                        </div>
                    </form>
                     <!--a href="#" class="button is-right" style="margin:10px;">
                        <span class="icon is-small">
                            <i class="fa fa-upload"></i> 
                        </span>
                        <span>Import CSV</span>
                    </a>
                    <a href="#" class="button is-right" style="margin:10px;">
                        <span class="icon is-small">
                            <i class="fa fa-download"></i> 
                        </span>
                        <span>Export CSV</span>
                    </a-->
                    <a href="{{ route('admin.mail-recipients.create') }}" class="button is-right" style="margin:10px;">
                        <span class="icon is-small">
                            <i class="fa fa-plus"></i> 
                        </span>
                        <span>Add</span>
                    </a>
                </header>

                <div class="card-content is-paddingless">
                    <table class="table is-bordered is-stripped is-hoverable is-fullwidth">
                        <thead>
                            <tr>
                                <th class="is-dark"></th>
                                <th class="is-dark">Center</th>
                                <th class="is-dark">Module</th>
                                <th class="is-dark">Type</th>
                                <th class="is-dark">Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rows as $row)
                                <tr>
                                    <td>
                                        <a href="{{ route('admin.mail-recipients.edit', $row->id) }}" class="tag is-link"><i class="fa fa-edit"></i></a>
                                    </td>
                                    <td>{{ $row->center->name }}</td>
                                    <td>{{ $row->module }}</td>
                                    <td>{{ $row->type }}</td>
                                    <td>{{ $row->mail }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </nav>
        </div>
    </div>
@endsection
