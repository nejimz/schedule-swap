@extends('admin.layouts.app')

@section('content')
<div class="columns is-marginless">
   <div class="column">
      <nav class="card">
        <header class="card-header">
            <p class="card-header-title title is-4 is-marginless">
                <i class="fa fa-user"></i> &nbsp; Create Mail Recipients
            </p>
            <a href="{{ route('admin.mail-recipients.index') }}" class="button is-right" style="margin:10px;">
                <span class="icon is-small">
                    <i class="fa fa-arrow-left"></i> 
                </span> 
                <span>Back</span>
            </a>
        </header>
        <div class="card-content is-paddingless">
          <form action="{{ $route }}" method="post">
            {!! csrf_field() !!}
            <div class="columns is-multiline is-marginless">
              <div class="column is-6">

                  <div class="field">
                     <label class="label">Center</label>
                     <div class="control">
                        <div class="select is-expanded">
                           <select id="center_id" name="center_id">
                           <option value="">-- Select --</option>
                           @foreach($centers as $center)
                              <option value="{{ $center->id }}">{{ $center->name }}</option>
                           @endforeach
                           </select>
                        </div>
                     </div>
                  </div>

                  <div class="field">
                     <label class="label">Module</label>
                     <div class="control">
                        <div class="select is-expanded">
                           <select id="module" name="module">
                              <option value="">-- Select --</option>
                              @foreach($modules as $module)
                                 <option value="{{ $module }}">{{ $module }}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                  </div>

                   <div class="field">
                    <label class="label">Type</label>
                    <div class="control">
                        <div class="select is-expanded">
                           <select id="type" name="type">
                              <option value="">-- Select --</option>
                              @foreach($mail_types as $type)
                                 <option value="{{ $type }}">{{ $type }}</option>
                              @endforeach
                           </select>
                        </div>
                    </div>
                  </div>

                  <div class="field">
                     <label class="label">Type</label>
                     <div class="control">
                        <input class="input" type="mail" placeholder="Email" name="mail" value="">
                     </div>
                  </div>

                  <div class="field">
                     <label class="label">&nbsp;</label>
                     <div class="control">
                        <input class="button is-success" type="submit" value="Submit">
                     </div>
                  </div>

               </div>

               <div class="column is-6 ">&nbsp;</div>
               <div class="column is-5">@include('partials.error-message')</div>
            </form>
         </div>
      </nav>
   </div>
</div>
<script type="text/javascript">
   $('#center_id').val('{{ $center_id }}');
   $('#module').val('{{ $module }}');
   $('#type').val('{{ $type }}');
   $('#mail').val('{{ $mail }}');
</script>
@endsection
