@extends('layouts.app')

@section('content')

    <section class="hero is-info">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">
                    Setup Your Personal E-Mail
                </h1>
            </div>
        </div>
    </section>

    <div class="columns is-marginless is-centered">
        <div class="column is-5">
            <div class="card">

                <div class="card-content">
                    @if (session('status'))
                        <div class="notification is-info">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="password-reset-form" method="POST" action="{{ route('start.security') }}">

                        {{ csrf_field() }}

                        <!-- EMAIL ADDRESSS -->
                        <div class="field">
                            <div class="control">
                                <label class="label">E-Mail Address</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        <input class="input" autocomplete="off" id="text" type="email" name="email" value="{{ old('email') }}" placeholder="{{ 'johndoe@'.strtolower(config('app.name')).'.com' }}" required>
                                    </p>

                                    @if ($errors->has('email'))
                                        <p class="help is-danger">
                                            {{ $errors->first('email') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <hr>
    
                        <!-- SECURITY QUESTIONS -->

                         <div class="field">
                            <div class="field-label has-text-left">
                                <strong> Security Qeustion</strong>
                            </div>
                        </div>
                        @for($i=1; $i<=2;$i++)
                            <div class="field">
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control">
                                            <div class="select is-fullwidth">
                                                <select name="question_{{ $i }}" id="question_{{ $i }}">
                                                    <option value="">-- Question Number {{ $i }} --</option>
                                                    @foreach($challenges as $challenge)
                                                        <option value="{{ $challenge->id }}">{{ $challenge->question }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </p>
                                        @if ($errors->has('question_'.$i))
                                            <p class="help is-danger">
                                                {{ $errors->first('question_'.$i) }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control">
                                            <input class="input" autocomplete="off" id="answer_{{ $i }}" type="text" name="answer_{{ $i }}" required>
                                        </p>
                                        @if ($errors->has('answer_'.$i))
                                            <p class="help is-danger">
                                                {{ $errors->first('answer_'.$i) }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endfor
                        
                        <div class="field">
                            <div class="control">
                                <button type="submit" class="button is-danger is-fullwidth">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function(){
            $('select').on('change', function(){
                $("select:not(#"+$(this).prop('id')+") option").show();
                if($(this).val() != 0){
                  $("select:not(#"+$(this).prop('id')+") option[value="+$(this).val()+"]").hide();
                }
            });
        });
    </script>
@endsection
