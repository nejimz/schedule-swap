@extends('layouts.app')

@section('content')
    <section class="hero is-info">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">
                    Welcome! {{ Auth::user()->name }}
                </h1>
            </div>
        </div>
    </section>

     <div class="columns is-marginless is-centered">
        <div class="column is-4">
        	Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
        	sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
        	Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
        	Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
        	Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est 
        	laborum

        	<div class="field" style="margin-top:40px;">
                <div class="control">
                   <a href="{{ route('start.reset') }}" class="button is-danger is-fullwidth">Continue</a>
                </div>
            </div>
        </div>
     </div>
@endsection