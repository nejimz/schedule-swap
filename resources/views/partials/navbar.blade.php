<nav class="navbar is-fixed-top is-info" role="navigation" aria-label="dropdown navigation">
    <div class="navbar-brand">
        <a href="{{ route('home') }}" title="{{ config('app.name') }}" class="navbar-item"><strong>{{ (Auth::user()->isRoot())? 'PE' : config('app.name') }}</strong></a>
        <div class="navbar-burger burger" data-target="navMenu" onclick="toggleBurger()">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="navbar-menu">
        <div class="navbar-start">
            <!--
                Left Navigation
            -->
            <a class="navbar-item" href="{{ route('home') }}"><i class="fa fa-home"></i>&nbsp;Home</a>

            <!--
                Agent Navigation
            -->
            @if(Auth::user()->isAgent())
                <a class="navbar-item" href="{{ route('shift-swap.index') }}"><i class="fa fa-asterisk"></i>&nbsp;Shift Swap</a>
                @if(Auth::user()->isRoot())
                    <a class="navbar-item" href="{{ route('absence.index') }}"><i class="fa fa-user-times"></i>&nbsp;Absence</a>
                @endif
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link" href="#">
                        <i class="fa fa-clock-o"></i>&nbsp;Overtime
                    </a>
                    <div class="navbar-dropdown">
                        <a class="navbar-item" href="#" title="Request Voluntary Overtime">Request VOT</a>
                        <a class="navbar-item" href="{{ route('vot.index') }}" title="Forecasted Voluntary Overtime">Forecasted VOT</a>
                    </div>
                </div>
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link" href="#">
                        <i class="fa fa-calendar"></i>&nbsp;Time-Off
                    </a>
                    <div class="navbar-dropdown">
                        <a class="navbar-item" href="#" title="Request Voluntary Time-Off">Request VTO</a>
                        <a class="navbar-item" href="{{ route('vto.index') }}" title="Forecasted Voluntary Timeoff">Forecasted VTO</a>
                    </div>
                </div>
            @endif
        
            <!--
                Root Navigation Divider
            -->
            @if(Auth::user()->isRoot())
                <hr class="navbar-divider">
            @endif

            <!--
                Workforce Navigation
            -->
            @if(Auth::user()->isAdmin())
                
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link" href="#">
                        <i class="fa fa-signal"></i>&nbsp;Forecasting
                    </a>
                    <div class="navbar-dropdown">
                        <a class="navbar-item" href="{{ route('forecast.index') }}">Calendar</a>
                        @if(Auth::user()->isRoot())
                            <a class="navbar-item" href="{{ route('forecast.create') }}">Forecast</a>
                        @elseif(Auth::user()->center_id == 9)
                            <a class="navbar-item" href="{{ route('forecast.create') }}">Forecast</a>
                        @endif
                    </div>
                </div>


                

                @if(Auth::user()->isRoot())
                    <a class="navbar-item" href="{{ route('schedules.index') }}"><i class="fa fa-calendar"></i>&nbsp;Schedules</a>
                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link"><i class="fa fa-clipboard"></i>&nbsp;Uploads</a>
                        <div class="navbar-dropdown">
                            <a href="{{ route('schedule-upload.index') }}" class="navbar-item" title="Upload Shift Swap">Schedule</a>
                            <a href="{{ route('forecast.upload.index') }}" class="navbar-item" title="Upload Forecast VTO/VOT">Forecast VTO/VOT</a>
                        </div>
                    </div>
                @endif

                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link"><i class="fa fa-clipboard"></i>&nbsp;Reports</a>
                    <div class="navbar-dropdown">
                        @if(Auth::user()->isRoot())
                            <a href="{{ route('schedule-upload.utilization') }}" class="navbar-item" title="Shift Swap Utilization">Shift Swap Utilization</a>
                            <a href="{{ route('forecast.utilization.index') }}" class="navbar-item" title="VTO/VOT Utilization">VTO/VOT Utilization</a>
                            <a href="{{ route('ss-reports.index') }}" class="navbar-item" title="Shift Swap">Shift Swap</a>
                            <a href="{{ route('vf-reports.index') }}" class="navbar-item" title="Forecasted Voluntary Overtime/Timeoff">Forecasted VTO/VOT</a>
                        @elseif(!Auth::user()->isEnterprise())
                            <a href="{{ route('ss-reports.index') }}" class="navbar-item" title="Shift Swap">Shift Swap</a>
                            <a href="{{ route('vf-reports.index') }}" class="navbar-item" title="Forecasted Voluntary Overtime/Timeoff">Forecasted VTO/VOT</a>
                        @endif
                        @if(Auth::user()->isEnterprise())
                            <a href="{{ route('enterprise-reports.index') }}" class="navbar-item" title="Shift Swap">
                                {{ ((Auth::user()->isRoot())? 'Enterprise' : '') }} Shift Swap
                            </a>
                            <a href="{{ route('vot-enterprise-reports.index') }}" class="navbar-item" title="Forecasted Voluntary Overtime/Timeoff">Forecasted VTO/VOT</a>
                        @endif
                    </div>
                </div>
            @endif

            </div>
            <!--
                Right Navigation
            -->
            <div class="navbar-end">
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link">
                        <i class="fa fa-user is-hidden-touch" style="margin-right:5px;"></i>
                        <span>{{ Auth::user()->name }}</span>
                    </a>

                    <div class="navbar-dropdown is-right">
                        @if(Auth::user()->isRoot())
                            <a href="/admin" class="navbar-item" title="Reset Password">Admin Portal</a>
                        @endif
                        <a href="{{ route('account.edit', Auth::user()->id) }}" class="navbar-item" title="My Account">My Account</a>
                        <a href="javascript::void(0)" class="navbar-item" onclick="document.getElementById('logout-form').submit();">Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none;">{{ csrf_field() }}</form>
                    </div>
                </div>
            </div>
    </div>
    <style>
        .navbar-item{
            padding:8px 12px;
            font-size: 11pt;
        }
    </style>
</nav>

