@extends('layouts.master')

@section('content')
<style>

/*.calendar-date {
  font-size: 18px !important;
}*/
/*.date-item { 
  font-size: 18px !important;
}*/
.calendar-body .calendar-date {
  height: 150px !important;
  /*border: 1px solid #ccc !important;*/
}
.calendar-event { 
  font-size: 11px !important;
}
</style>

<h4 class="title is-4">Absence</h4>
@include('partials.error-message')
<div id="ss_message"></div>
<div class="columns">
  <div class="column is-12">
    @include('vendor.calendar.bulma')
  </div>
</div>
<table id="absences-list" class="table is-bordered is-narrow is-fullwidth">
  <thead>
    <tr>
      <th width="5%"></th>
      <th width="10%" class="has-text-centered">Slot/s</th>
      <th width="20%">Shift Date</th>
      <th width="65%">Schedule</th>
    </tr>
  </thead>
  <tbody></tbody>
</table>
@endsection

@section('scripts')
<script src="{{ asset('js/moment.js') }}"></script>
<script type="text/javascript">
  var xhr_process_absence = null;
  var xhr_absence = null;
  var calendar_month = moment().format('MMMM YYYY');
  var calendar = {
    month:
    {
      next: function() {
        calendar_month = moment(calendar_month, 'MMMM YYYY').add(1, 'month').format('MMMM YYYY');
        calendar.month.update(calendar_month);
      },
      previous: function() {
        calendar_month = moment(calendar_month, 'MMMM YYYY').subtract(1, 'month').format('MMMM YYYY');
        calendar.month.update(calendar_month);
      },
      update: function(calendar_month) {
        document.getElementById('calendar-month').innerHTML = calendar_month;
        $.ajax({
          url: "{{ route('absence.calendar') }}", 
          data: { calendar_month: calendar_month },
          dataType: "html",
          beforeSend: function(xhr) {
            $('.calendar-body').html('<img src="{{ asset("img/loading/2.gif") }}" title="Loading..." />');
          }
        }).done(function(data){
          $('.calendar-body').html(data);
        });
      }
    }
  };

  calendar.month.update(calendar_month);

  function validate(shift_date, schedule)
  {
    var confirmation = confirm("Are you sure you want to forecast your absence on " + shift_date + "?");

    if(confirmation)
    {
      process_absence(shift_date, schedule);
    }
  }

  function process_absence(shift_date, schedule)
  {
    xhr_process_absence = $.ajax({
      type : 'post',
      url : '{{ route("absence.store") }}',
      data: 'shift_date=' + shift_date + '&schedule=' + schedule + '&_token={{ csrf_token() }}',
      cache : false,
      dataType : "json",
      async : false, 
      beforeSend: function(xhr) {
        if (xhr_process_absence != null){ xhr_process_absence.abort(); }
      }
    }).done(function(json) {
      $('#ss_message').html('<div class="notification is-success">' + json['success'] + '</div><br />');
      calendar.month.update(calendar_month);
    }).fail(function(jqXHR, textStatus) {
      //
    });
  }

  function absences(shift_date)
  {
    $('#absences-list tbody').html('<tr><td colspan="6" class="has-text-centered"><img src="{{ asset("img/loading/2.gif") }}" title="Loading..." /></td></tr>');
    window.location = '#absences-list';
    xhr_absence = $.ajax({
      type : 'get',
      url : '{{ route("absence.list") }}',
      data: 'shift_date=' + shift_date,
      cache : false,
      dataType : "json",
      async : false, 
      beforeSend: function(xhr) {
        if (xhr_absence != null){ xhr_absence.abort(); }
      }
    }).done(function(json) {
      var table = '';
      $.each(json['lists'], function(key, value){
        var icon = '<a href="' + value['url'] + '"><i class="fa fa-edit fa-lg"></i></a>';

        if(value['total_count'] <= 0)
        {
          icon = '<i class="fa fa-edit fa-lg"></i>';
        }

        table += '<tr><td class="has-text-centered">' + icon + '</td><td class="has-text-centered">' + value['total_count'] + '</td><td>' + shift_date + '</td><td>' + value['schedule'] + '</td></tr>';
      });
      $('#absences-list tbody').html(table);
    }).fail(function(jqXHR, textStatus) {
      //
    });
  }
</script>
@endsection
