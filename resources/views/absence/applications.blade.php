@extends('layouts.master')

@section('content')

<h4 class="title is-4">Absence Applications</h4>
@include('partials.error-message')
<table id="absences-list" class="table is-bordered is-narrow is-fullwidth">
	<thead>
		<tr>
			<th width="6%" colspan="2"></th>
			<th width="10%" class="has-text-centered"><abbr class="tooltip" data-tooltip="Application Id">AID</abbr></th>
			<th width="34%">Applicant Name</th>
			<th width="10%" class="has-text-centered"><abbr class="tooltip" data-tooltip="Number of Hours">No. of Hrs.</abbr></th>
			<th width="20%">Schedule</th>
			<th width="20%">Created At</th>
		</tr>
	</thead>
	<tbody>
	@foreach($rows as $row)
	<tr>
		@if(is_null($row->status))
			<td class="has-text-centered">
				<a href="{{ route('absence.approve', ['id'=>$row->id, 'absence_id'=>$row->absence_id]) }}" class="tooltip" data-tooltip="Approve"><i class="fa fa-check fa-lg"></i></a>
			</td>
			<td class="has-text-centered">
				<a href="{{ route('absence.deny', ['id'=>$row->id, 'absence_id'=>$row->absence_id]) }}" class="tooltip" data-tooltip="Deny"><i class="fa fa-times fa-lg"></i></a>
			</td>
		@elseif($row->status == 0)
			<td colspan="2" class="has-text-centered"><a href="javascript:void(0)" class="tooltip" data-tooltip="Denied"><i class="fa fa-times fa-lg"></i></a></td>
		@elseif($row->status == 1)
			<td colspan="2" class="has-text-centered"><a href="javascript:void(0)" class="tooltip" data-tooltip="Approved"><i class="fa fa-check fa-lg"></i></a></td>
		@endif
		<td class="has-text-centered">{{ $row->id }}</td>
		<td>{{ $row->user->name }}</td>
		<td class="has-text-centered">{{ $row->hours }}</td>
		<td>{{ $row->schedule }}</td>
		<td>{{ $row->created_at }}</td>
	</tr>
	@endforeach
	</tbody>
</table>

@endsection