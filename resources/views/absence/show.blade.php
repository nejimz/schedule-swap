@extends('layouts.master')

@section('content')

<h4 class="title is-4"></h4>


<div class="container">
  <div class="columns">
    <div class="column is-3"></div>
    <div class="column is-6">
      <div class="card">
        <div class="card-header">
          <label class="card-header-title label" for="search_avaya">Replacement Application</label>
        </div>
        <div class="card-content">
          @include('partials.error-message')
          <form id="ss-download" method="post" action="{{ route('absence.application_post') }}">
            
            <div class="field is-horizontal">
              <div class="field-label">
                <label class="label">Shift Date</label>
              </div>

              <div class="field-body">
                <div class="field">
                  <div class="control">
                    {{ $shift_date }}
                    <input type="hidden" name="shift_date" value="{{ $shift_date }}" />
                  </div>
                </div>
              </div>
            </div>
            
            <div class="field is-horizontal">
              <div class="field-label">
                <label class="label">Schedule</label>
              </div>

              <div class="field-body">
                <div class="field">
                  <div class="control">
                    {{ $schedule }}
                    <input type="hidden" name="schedule_base" value="{{ $schedule }}" />
                  </div>
                </div>
              </div>
            </div>
            
            <div class="field is-horizontal">
              <div class="field-label">
                <label class="label">&nbsp;</label>
              </div>

              <div class="field-body">
                <div class="field">
                  <div class="control">
                    <label class="label">Available Schedule/s</label>
                    @foreach($schedules as $key => $value)
                    <?php
                    #$selected_schedule;
                    ?>
                      @if($selected_schedule == $value)
                      <label class="checkbox disabled">
                        <input type="hidden" name="schedule[{{ $key }}]" value="{{ $value }}" />
                        <input type="checkbox" id="schedule" name="schedule[{{ $key }}]" value="{{ $value }}" checked="checked" disabled /> {{ $value }}
                      </label><br> 
                      @else
                      <label class="checkbox">
                        <input type="checkbox" id="schedule" name="schedule[{{ $key }}]" value="{{ $value }}" /> {{ $value }}
                      </label><br> 
                      @endif
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
            
            <div class="field is-horizontal">
              <div class="field-label">
                <label class="label">&nbsp;</label>
              </div>

              <div class="field-body">
                <div class="field">
                  <div class="control">
                    {!! csrf_field() !!}
                    <button class="button is-info" type="submit">Submit</button>
                  </div>
                </div>
              </div>
            </div>

            <div class="field">
              <div class="control">
                
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
    <div class="column is-3"></div>
  </div>
</div>

@endsection