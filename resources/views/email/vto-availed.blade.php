
@extends('layouts.forecast')

@section('container')
@parent

<h2 style="text-align:center">
	Voluntary Timeoff Approved for {{ $start->format('F d, Y h:i A') . ' - ' . $end->format('h:i A') }}
</h2>
<table cellspacing="0" cellpadding="5" border="1" align="center">
	<thead>
		<tr>
			<th>Rquest ID</th>
			<th>Avaya</th>
			<th>TAS Login</th>
			<th>Name</th>
			<th>Work Pattern</th>
			<th>Organization</th>
			<th>Overtime Date</th>
			<th>Schedule Start</th>
			<th>Schedule End</th>
		</tr>
	</thead>
	<tbody>
	@foreach($rows as $row)
		<tr align="center">
			<td>{{ $row->forecast_vto_request_id }}</td>
			<td>{{ $row->request->user->information->avaya  }}</td>
			<td>{{ $row->request->user->username }}</td>
			<td>{{ $row->request->user->name }}</td>			
			<td>{{ $row->request->user->information->wp  }}</td>			
			<td>{{ $row->request->user->information->skill  }}</td>
			<td>{{ $row->forecast->shift_date }}</td>
			<td>{{ date('h:i A' ,strtotime($row->forecast->time_start)) }} </td>
			<td>{{ date('h:i A' ,strtotime($row->forecast->time_end)) }}</td>
		</tr>
	@endforeach
</table>
<br><br>
Thanks,<br>
Shift Swap Support Team

@stop