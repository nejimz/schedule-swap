@extends('layouts.mail')

@section('container')
@parent
<table cellspacing="0" border="1">
	<tr>
		<td>Request Id</td>
		<td>{{ $row->id }}</td>
	</tr>
	<tr>
		<td>Swap Type</td>
		<td>{{ $row->swap_type_name }}</td>
	</tr>
	<tr>
		<td>Date Confirmed</td>
		<td>{{ $row->updated_at->toDateTimeString() }}</td>
	</tr>
	<tr>
		<td>Week Ending</td>
		<td>{{ $row->week_ending->toDateString() }}</td>
	</tr>
</table>
<br />
<table cellspacing="0" border="1">
	<tr>
		<td colspan="4" align="center">Agent 1</td>
		<td colspan="4" align="center">Agent 2</td>
	</tr>
	<tr>
		<td>Avaya</td>
		<td>Name</td>
		<td>Old Schedule</td>
		<td>New Schedule</td>

		<td>Avaya</td>
		<td>Name</td>
		<td>Old Schedule</td>
		<td>New Schedule</td>
	</tr>
	<tr>
		<td>{{ $row->user_information->avaya }}</td>
		<td>{{ $row->user->name }}</td>
		<td>{{ $row->employee_shift_date->toDateString() }}</td>
		<td>{{ $row->buddy_shift_date->toDateString() }}</td>

		<td>{{ $row->buddy_user_information->avaya }}</td>
		<td>{{ $row->buddy_user->name }}</td>
		<td>{{ $row->buddy_shift_date->toDateString() }}</td>
		<td>{{ $row->employee_shift_date->toDateString() }}</td>
	</tr>
</table>

<br><br>
Thanks,<br>
Shift Swap Support Team

@stop