@component('mail::message')
# Welcome

Hi {{ title_case($user->first_name) }}

You are receiving this email because we received a password reset request for your account.
@component('mail::button', ['url' => $url ])
Reset Password
@endcomponent
If you did not request a password reset, no further action is required.

Regards,
Panasiatic Enterprise - Agent Scheduling

@component('mail::subcopy')
    If you’re having trouble clicking the "Reset Password" button, copy and paste the URL below into your web browser: 
    <a href="{{ $url }}">{{ $url }}</a>
@endcomponent
@endcomponent
