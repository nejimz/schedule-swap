@extends('layouts.mail')

@section('container')
@parent
<table cellspacing="0" cellpadding="5" border="1" align="center">
	<tr>
		<td>Request Id</td>
		<td>{{ $row->id }}</td>
	</tr>
	<tr>
		<td>Swap Type</td>
		<td>{{ $row->swap_type_name }}</td>
	</tr>
	<tr>
		<td>Week Ending</td>
		<td>{{ $row->week_ending->toDateString() }}</td>
	</tr>
	<tr>
		<td>Confirmed Date</td>
		<td>{{ $row->updated_at->toDateTimeString() }}</td>
	</tr>
	<tr>
		<td>Agent 1</td>
		<td>{{ $row->user->name }}</td>
	</tr>
	<tr>
		<td>Agent 2</td>
		<td>{{ $row->buddy_user->name }}</td>
	</tr>
</table>

<br><br>
Thanks,<br>
Shift Swap Support Team

@stop