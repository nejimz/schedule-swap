@component('mail::message')
# Welcome

Hi {{ auth()->user()->name }}

A user has been created for you within {{ config('app.name') }}

@endcomponent
