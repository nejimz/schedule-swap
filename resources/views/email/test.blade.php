@extends('layouts.mail')

@section('container')
@parent

Hi Jezreel,<br /><br />

Your OTP is <strong>{{ str_random(10) }}</strong>.<br /><br />

Thank you,
@stop
