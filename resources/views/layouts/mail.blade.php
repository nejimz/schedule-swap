<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Panasiatic Enterprise</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>

  <body>
        @yield('container', '')
  </body>
</html>
