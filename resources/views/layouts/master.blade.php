<!DOCTYPE html>
<html class="has-navbar-fixed-top">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name') }}</title>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/jqueryui.css') }}" rel="stylesheet">
        <style type="text/css">
            .tooltip-inner {
                white-space: white-space: pre-wrap;
            }
        </style>
    </head>
    <body>
       @impersonating
            <article class="message is-dark">
                <div class="message-header is-radiusless">
                    <p>You are impersonating another user.</p>
                    <a class="delete" aria-label="delete" href="{{ route('impersotate.leave') }}"></a>
                </div>
            </article>
        @endImpersonating
        @include('partials.navbar')
        <div class="section" style="padding-top:24px;">
            @yield('content')
        </div>

        @yield('modals')
        
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>

        <script type="text/javascript">
        function toggleBurger()
        {
            var burger = $('.burger');
            var menu = $('.navbar-menu');
            burger.toggleClass('is-active');
            menu.toggleClass('is-active');
        }
        </script>       

        <script src="{{ asset('js/idle-timer.js') }}"></script>
        @yield('styles')

        @yield('scripts')
    </body>
</html>
