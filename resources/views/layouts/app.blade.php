<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name') }}</title>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <script src="{{ asset('js/app.js') }}"></script>
    </head>
    <body>
        @impersonating
            <article class="message is-dark">
                <div class="message-header is-radiusless">
                    <p>You are impersonating another user.</p>
                    <a class="delete" aria-label="delete" href="{{ route('impersotate.leave') }}"></a>
                </div>
            </article>
        @endImpersonating
        <div id="app">
            @yield('content')
        </div>
    </body>
</html>
