function check_pass()
{
     var val = document.getElementById("password").value;
     var meter = document.getElementById("progress");
     var no = 0;

     if(val != "") {
        if(val.length<8)no=1;
        if(val.length>8 && (val.match(/[A-Z]/) || (val.match(/[a-z]/) || val.match(/\d+/) || val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))))no=2;
        if(val.length>8 && ((val.match(/[a-z,A-Z]/) && val.match(/\d+/)) || (val.match(/\d+/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) || (val.match(/[a-z,A-Z]/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))))no=3;
        if(val.length>8 && val.match(/[A-Z]/) && val.match(/[a-z]/) && val.match(/\d+/) && val.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/))no=4;

        if(no == 1) {
            meter.value="25";
            meter.className = 'progress is-danger';
            document.getElementById("progress").innerHTML="Very Weak";
        }

        if(no == 2) {
            meter.value="50";
            meter.className = 'progress is-warning';
            document.getElementById("progress").innerHTML="Weak";
        }

        if(no == 3) {
            meter.value="75";
             meter.className = 'progress is-primary';
            document.getElementById("progress").innerHTML="Good";
        }

        if(no == 4) {
            meter.value="100";
            meter.className = 'progress is-success';
            document.getElementById("progress").innerHTML="Strong";
        }
    } else {
        meter.value="0";
        meter.className = 'progress';
        document.getElementById("progress").innerHTML="";
    }
}