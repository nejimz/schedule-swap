var idleTime = 0;

$(document).ready(function () {

    //Increment the idle time counter every minute.
    var idleInterval = setInterval(function() {
        idleTime = idleTime + 1;
        if (idleTime > 1) { 
            alert('Your Session Expired');
            location.reload();
        }
    }, 3600000); // 7200000 120 Session Time

    $(this).mousemove(function (e) {
        idleTime = 0;
    });
    $(this).keypress(function (e) {
        idleTime = 0;
    });
});

