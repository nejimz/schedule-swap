<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Mobile App Boot Configuration
#Route::get('/boot', function(){
	/*$config = collect([
		// null if empty
		'allowed_users' => [
			'baq1ktilano',
			'baq1hefontalvo',
			'baq1ysoto',
			'baq1jumelendez',
			'baq1arojas',
			'baq1vihiggins',
			'baq1aaguilar',
			'baq1acoll',
			'baq1mvinachi',
			'baq1agomez1',
			'dgquijote',
			'jctillaflor',
			'cebtestagent01',
			'cebtestagent02',
			'cebtestagent03',

			'baq1crossell',
			'baq1anosorio',
			'baq1ffonseca',
			'baq1jacuenca',
			'baq1dcardona',
			'baq1zzabala',
			'baq1kisilva',
			'baq1korcasita',
			'baq1rurodriguez',
			'baq1lnunez2',
			'baq1ricalvarez',
			'baq1ireyes',
			'baq1dapertuz',
			'baq1amvalverde',
			'baq1alcabrera',
			'baq1ocardenas',
			'baq1imeza',
			'baq1lalarcon2',
			'baq1rosvergara',
			'baq1mjesus',
			'baq1jteran',
			'baq1nfortich',
			'baq1mmontes',
			'baq1cargonzalez',
			'baq1sgutierrez',
			'baq1aoliveros',
			'baq1rcolonna',
			'baq1albarrios',
			'baq1rohernandez',
			'baq1clizeth',

			'dmg1marjlozada',
			'dmg1judomen',
			'dmg1micdy',
			'dmg1acelocia',
			'dmg1avillaflores',
			'dmg1rubcalumpang',
			'dmg1abato',
			'dmg1mteramoto',
			'dmg1jotilos',
			'dmg1sferolin',
			'dmg1elmramirez',
			'dmg1jacruz',
			'dmg1deamiguel',
			'dmg1jjamarolin',
			'dmg1kperojon',
			'dmg1drayoso',
			'dmg1jaligato',
			'dmg1nallosada',
			'dmg1jparong',
			'dmg1anortega',
			'dmg1cabubo',
			'dmg1romapula',
			'dmg1malama',
			'dmg1jalayaay',
			'dmg1mestorco',
			'dmg1nizerna',
			'dmg1gtuangco',
			'dmg1jgirasol',
			'dmg1aabol',
			'dmg1ndelapena'

		],
		'allowed_ceters' => null,
		'version' => 1.0,
	])->toJson();*/

	#return $config;
#});

Route::prefix('/local')
->middleware('auth:api')
->group(function(){
	Route::prefix('/')
	->group(function(){
		Route::get('user', 'MobileUsersController@auth');
	});

});


Route::prefix('/v1')
->middleware('auth:api')
->group(function(){
	Route::prefix('/')
	->group(function(){
		Route::get('user', 'MobileUsersController@auth');
		Route::post('device', 'MobileUserDevicesController@register');
		Route::get('schedule/{id}', 'ShiftSwapInformationController@api_details_json');
	});

	Route::prefix('shift-swap')
	->group(function(){
		$this->get('search', 'ShiftSwapInformationController@search_json'); 
		$this->get('type', 'ShiftSwapController@swap_type'); 
		$this->get('week-ending-and-my-shift-date/{id}', 'ShiftSwapInformationController@ss_we_and_my_sd');
		$this->post('store', 'ShiftSwapController@store');
		$this->post('confirm', 'ShiftSwapController@request_confirmation');
	});

	Route::prefix('firebase')
    ->group(function(){
    	$this->post('send', 'CurlFirebaseCloudMessagingController@send_to_device');
    });
});