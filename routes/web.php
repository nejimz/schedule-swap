<?php
/*
|--------------------------------------------------------------------------
| Guest Routes [/]
|--------------------------------------------------------------------------
*/
#Auth::routes();
$this->get('/', function(){
	return redirect()->route('landing');
});

Route::prefix('policy')
	->group(function(){
	$this->get('/', 'PolicyController@index')->name('policy');
});

$this->get('/login', 'Auth\LoginController@showLoginForm_main')->name('landing');
#$this->name('login')->get('/', 'Auth\LoginController@showLoginForm');
$this->post('/login', 'Auth\LoginController@login')->name('login');

Route::prefix('tf')
->middleware(['center.check'])
->group(function(){
	$this->get('/', 'Auth\LoginController@index');
	$this->get('{center}', 'Auth\LoginController@showLoginForm')->name('center.login');
});


Route::prefix('password')
->group(function(){
	$this->get('reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
	$this->post('reset', 'Auth\ResetPasswordController@reset');
	$this->get('reset/{token}/{u?}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

	// via sq
	$this->get('security', 'Auth\ForgotPasswordController@security')->name('password.security');
	$this->post('security/request', 'Auth\ForgotPasswordController@submitUsername')->name('password.security.request');
	$this->get('security/question/{q}', 'Auth\ForgotPasswordController@request')->name('password.security.question');
	$this->post('security/reset', 'Auth\ForgotPasswordController@submitSecurity')->name('password.security.reset');

	// via email
	$this->post('email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
});

/*
|--------------------------------------------------------------------------
| Authenticated Routes [/]
|--------------------------------------------------------------------------
*/
Route::prefix('portal')
->middleware(['auth', 'noob'])
->group(function(){
	$this->post('logout', 'Auth\LoginController@logout')->name('logout');

	$this->get('/', 'HomeController@index')->name('home');
	$this->get('schedule/{id}', 'HomeController@schedule_json')->name('schedule_json');
    /*
	|--------------------------------------------------------------------------
	| Mail Routes
	|--------------------------------------------------------------------------
	*/
    Route::prefix('mail')
	->group(function(){
		Route::get('welcome', 'MailController@sendWelcomeMail')->name('mail.welcome');
	});
    /*
	|--------------------------------------------------------------------------
	| Starter Routes
	|--------------------------------------------------------------------------
	*/
    Route::prefix('start')
	->middleware('start')
	->group(function(){
		Route::get('/', 'StartController@start')->name('start');
		Route::get('reset', 'StartController@reset')->name('start.reset');
		Route::post('reset', 'StartController@submitReset')->name('start.submitReset');
		Route::get('security', 'StartController@security')->name('start.security');
		Route::post('security', 'StartController@submitSecurity')->name('start.submitSecurity');
	});

	/*
	|--------------------------------------------------------------------------
	| Admin Routes
	|--------------------------------------------------------------------------
	*/
	#Route::resource('users', 'UsersController', [ 'roles' => ['administrator'] ]);

	
	/*
	|--------------------------------------------------------------------------
	| Schedule Routes
	|--------------------------------------------------------------------------
	*/
    Route::prefix('schedules')
    ->middleware('root')
	->group(function(){
		Route::name('ss_center_we')->get('week-ending/{center_id?}', 'ShiftSwapSchedulesController@center_week_ending');
		Route::name('ss_center_schedule')->get('records/{center_id?}/{week_ending?}', 'ShiftSwapSchedulesController@center_schedule');
	});
	Route::resource('schedules', 'ShiftSwapSchedulesController', [ 'roles' => ['administrator'] ]);

	
	/*
	|--------------------------------------------------------------------------
	| Root Routes
	|--------------------------------------------------------------------------
	*/
    Route::prefix('admin')
    	->middleware('root')
		->group(function(){
		Route::name('impersonate')->get('impersonate/take/{userid}', 'Admin\ImpersonateController@take', ['as' => 'admin']);

    	Route::prefix('user')
		->group(function(){
			Route::resource('upload', 'Admin\UsersUploadController', ['as' => 'user'])->only(['index', 'store']);
			#Route::name('user.upload')->get('upload', 'Admin\UsersUploadController@create');
			#Route::name('user.upload-submit')->get('upload-submit', 'Admin\UsersUploadController@store');
			Route::name('user.generate-password')->get('generate-password/{id}', 'Admin\UsersController@generate_password');
		});

		Route::resource('user', 'Admin\UsersController', ['as' => 'admin']);
		Route::resource('schedule', 'Admin\SchedulesController', ['as' => 'admin']);
		Route::resource('mail-recipients', 'Admin\MailRecipientsController', ['as' => 'admin']);
		Route::get('password-generator/{number?}', 'Admin\PasswordGeneratorController@index', ['as' => 'admin'])->name('admin.password-generator.index');

		Route::name('home-password')->get('passwords/{number}', 'HomeController@passwords');
	});

	Route::name('impersotate.leave')->get('impersonate/leave', 'Admin\ImpersonateController@leave', ['as' => 'admin'])->middleware(['start']);
	/*
	|--------------------------------------------------------------------------
	| Shift Swap Routes
	|--------------------------------------------------------------------------
	*/
    Route::prefix('shift-swap')
	->group(function(){
		Route::get('search', 'ShiftSwapInformationController@search_json')->name('ss_search_json');
		Route::get('details-json/{id}', 'ShiftSwapInformationController@details_json')->name('ss_details_json');
		Route::get('record/{id?}', 'ShiftSwapInformationController@shift_swap_record')->name('ss_request_history_json');
		Route::get('week-ending-and-my-shift-date/{id}', 'ShiftSwapInformationController@ss_we_and_my_sd')->name('ss_we_and_my_sd');
		#Route::get('buddy-shift-date', 'ShiftSwapInformationController@ss_buddy_shift_date')->name('ss_buddy_shift_date');

		Route::post('request-confirmation', 'ShiftSwapController@request_confirmation')->name('ss_confirmation');

		Route::get('history/{type}', 'ShiftSwapMyRequestController@index')->name('ss_history');

		Route::group(['middleware' => 'admin'], function(){
	    	Route::resource('ss-reports', 'ShiftSwapReportsController')->only(['index', 'show']);


			Route::get('schedule-upload/utilization', 'ShiftSwapScheduleUploadController@utilization')->name('schedule-upload.utilization');
	    	Route::resource('schedule-upload', 'ShiftSwapScheduleUploadController')->only(['index', 'store']);
	    	Route::resource('enterprise-reports', 'ShiftSwapReportsEnterpriseController')->only(['index', 'show']);
		});
	});
	Route::resource('shift-swap', 'ShiftSwapController')->only(['index', 'store']);
	/*
	|--------------------------------------------------------------------------
	| VTO Routes
	|--------------------------------------------------------------------------
	*/
    Route::prefix('forecast')
	->group(function(){
		Route::name('forecast.calendar')->get('calendar', 'ForecastController@calendar');
		Route::name('forecast.list')->get('list/{date?}', 'ForecastController@list');
		Route::get('skills-wps-json/{center?}', 'ForecastController@skills_wps_json')->name('skills_wps_json');

	    Route::prefix('vot')
		->group(function(){
			Route::name('vot.calendar')->get('calendar', 'ForecastVOTController@calendar');
			Route::name('vot.list')->get('list/{date?}/{schedule?}', 'ForecastVOTController@list');
			Route::name('vot.submit')->post('submit', 'ForecastVOTController@submit');
		});
		Route::resource('vot', 'ForecastVOTController');

	    Route::prefix('vto')
		->group(function(){
			Route::name('vto.calendar')->get('calendar', 'ForecastVTOController@calendar');
			Route::name('vto.list')->get('list/{date?}/{schedule?}', 'ForecastVTOController@list');
			Route::name('vto.submit')->post('submit', 'ForecastVTOController@submit');
			Route::name('vto.mail')->get('mail', 'ForecastCronJobController@vto_email');
		});
		Route::resource('vto', 'ForecastVTOController');

		Route::group(['middleware' => 'admin'], function(){
			Route::resource('vf-reports', 'ForecastReportsController')->only(['index', 'show']);
			Route::resource('vot-enterprise-reports', 'ForecastReportsEnterpriseController')->only(['index', 'show']);
		});
		Route::resource('utilization', 'ForecastUtilizationController', ['as' => 'forecast'])->only(['index', 'create']);
		Route::resource('utilization-detailed', 'ForecastUtilizationDetailedController', ['as' => 'forecast'])->only(['create']);
		Route::resource('upload', 'ForecastUploadController', ['as' => 'forecast'])->only(['index', 'store']);
	});
	Route::resource('forecast', 'ForecastController')->only(['index', 'create', 'store']);
	/*
	|--------------------------------------------------------------------------
	| Absence Replacement
	|--------------------------------------------------------------------------
	*/
    Route::prefix('absence')
	->group(function(){
		Route::name('absence.calendar')->get('calendar', 'AbsenceController@calendar');
		Route::name('absence.list')->get('list', 'AbsenceController@list');
		Route::name('absence.application')->get('application/{shift_date}/{schedule}', 'AbsenceController@application');
		Route::name('absence.application_post')->post('application', 'AbsenceController@application_post');
		Route::name('absence.approve')->get('{id}/{absence_id}/approve', 'AbsenceController@approve');
		Route::name('absence.deny')->get('{id}/{absence_id}/deny', 'AbsenceController@deny');

		Route::resource('replacement', 'AbsenceReplacementController');
	});
	Route::resource('absence', 'AbsenceController');
	/*
	|--------------------------------------------------------------------------
	| VTO Routes
	|--------------------------------------------------------------------------
	*/
	Route::get('account/download', 'UserAccountController@download')->name('account.download');
	Route::resource('account', 'UserAccountController')->only(['store', 'edit', 'update']);
});

Route::prefix('cron')
->group(function(){
	Route::name('vot.mail')->get('vot-mail', 'ForecastCronJobController@vot_email');
	Route::name('vto.mail')->get('vto-mail', 'ForecastCronJobController@vto_email');
});
