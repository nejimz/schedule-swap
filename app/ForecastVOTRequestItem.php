<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForecastVOTRequestItem extends Model
{
    public $table = "forecast_vot_request_item";
    public $timestamps = false;

    protected $fillable = [
    	'forecast_vot_request_id', 'forecast_vot_id'
	];

	public function request()
	{
        return $this->belongsTo('App\ForecastVOTRequest', 'forecast_vot_request_id', 'id');
	}

	public function forecast()
	{
        return $this->belongsTo('App\ForecastVOT', 'forecast_vot_id', 'id');
	}
}
