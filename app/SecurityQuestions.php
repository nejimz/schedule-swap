<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecurityQuestions extends Model
{
    public $table = "security_questions";

    public function user()
    {
        return $this->belongsToMany('App\Users', 'users', 'user_id');
    }
}
