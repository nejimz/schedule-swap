<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForecastVTORequest extends Model
{
    public $table = "forecast_vto_request";
    public $timestamps = true;

    protected $fillable = [
    	'user_id', 'created_at', 'updated_at'
	];

	public function user()
	{
    	return $this->belongsTo('App\User', 'user_id', 'id')->withTrashed();
	}

	public function items()
	{
    	return $this->hasOne('App\ForecastVTORequestItem', 'forecast_vto_request_id', 'id');
	}
}
