<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absence extends Model
{
    public $table = 'absences';
    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
    	'user_id', 'shift_date', 'schedule', 'created_at', 'updated_at' 
	];

	public function replacement()
	{
    	return $this->hasOne('App\AbsenceReplacements', 'absence_id', 'id');
	}

	public function user()
	{
    	return $this->belongsTo('App\User', 'user_id', 'id');
	}

    public function information()
    {
        return $this->hasOne('App\SwapAgentInformation', 'user_id', 'user_id');
    }
}
