<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecurityChallenge extends Model
{
	public $table = "security_challenges";

    public $timestamps = false;

    public function user()
    {
        return $this->belongsToMany('App\Users', 'security_questions', 'security_challenge_id', 'user_id')
        			->withPivot('id', 'answer');
    }
}
