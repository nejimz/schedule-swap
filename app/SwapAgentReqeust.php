<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\SwapType;
use App\SwapAgentSchedule;
use \carbon\Carbon;
use DB;
use Auth;

class SwapAgentReqeust extends Model
{
    public $table = "swap_agent_reqeusts";
    public $timestamps = true;

    protected $dates = [
        'week_ending', 'employee_shift_date', 'buddy_shift_date', 
        'before_expiration_date', 'expiration_date', 
        'created_at', 'updated_at', 'deleted_at'
    ];

    protected $fillable = [
    	'swap_type', 'center_id', 'week_ending', 'status', 
    	'user_id', 'employee_shift_date', 'employee_schedule', 
    	'buddy_user_id', 'buddy_shift_date', 'buddy_schedule', 
        'before_expiration_date', 'expiration_date', 
    	'created_at', 'updated_at', 'deleted_at'
	];

	public function user()
	{
        return $this->belongsTo('App\User', 'user_id', 'id')->withTrashed();
	}

	public function buddy_user()
	{
        return $this->belongsTo('App\User', 'buddy_user_id', 'id')->withTrashed();
	}

    public function user_information()
    {
        return $this->belongsTo('App\SwapAgentInformation', 'user_id', 'user_id');
    }

    public function buddy_user_information()
    {
        return $this->belongsTo('App\SwapAgentInformation', 'buddy_user_id', 'user_id');
    }

    public function getSwapMailTemplateAttribute()
    {
        $swap_type = $this->swap_type;

        if($swap_type == 'sd')
        {
            return 'single-day';
        }
        elseif($swap_type == 'srd')
        {
            return 'single-rest-day';
        }
        elseif($swap_type == '2rd')
        {
            return '2-rest-day';
        }
    }

    public function getSwapTypeNameAttribute()
    {
        $swap_type = $this->swap_type;

        $row = SwapType::whereAbbr($swap_type)->first();

        if(is_null($row))
        {
            return 'SHIFT REST DAY';
        }
        return $row->name;
    }

    public function getStatusTypeAttribute()
    {
        $status = $this->status;
        $expiration_date = $this->expiration_date;
        $datetime = Carbon::now();

        if(is_null($status))
        {
            if($datetime->gte($expiration_date))
            {
                return 'expired';
            }
            return 'pending';
        }
        elseif($status == 0)
        {
            return 'denied';
        }
        elseif($status == 1)
        {
            return 'approved';
        }
        elseif($status == 3)
        {
            return 'cancel';
        }
    }

    public function get_rest_day($week_ending, $user_id)
    {
        $days = '';
        $rows = SwapAgentSchedule::whereUserId($user_id)->whereWeekEnding($week_ending)->whereSchedule('Off')
                ->orderBy('shift_date', 'ASC')->get([ DB::raw('DATE_FORMAT(shift_date, \'%a\') shift_date_f') ]);

        foreach ($rows as $row)
        {
            $days .= $row->shift_date_f . '-';
        }

        return substr($days, 0, -1);
    }

    public function get_rest_date($week_ending, $user_id)
    {
        $days = '';
        $user = Auth::user();
        $two_rd =   SwapAgentReqeust::whereSwapType('2rd')->whereStatus(1)->whereWeekEnding($week_ending)
                ->where(function($query) use ($user_id){
                    $query->where('user_id', $user_id)->orWhere('buddy_user_id', $user_id);
                })->first();

        if(!is_null($two_rd))
        {
            if($two_rd->user_id == $user_id)
            {
                $user_id = $two_rd->buddy_user_id;
            }
            else
            {
                $user_id = $two_rd->user_id;
            }
        }

        $rows = SwapAgentSchedule::whereUserId($user_id)->whereWeekEnding($week_ending)->whereSchedule('Off')->orderBy('shift_date', 'ASC')->get();

        foreach ($rows as $row)
        {
            $shift_date = $row->shift_date;
            if(is_null($two_rd))
            {
                $srd =  SwapAgentReqeust::whereSwapType('srd')->whereStatus(1)->whereWeekEnding($week_ending)
                        ->where(function($query) use ($shift_date){
                            $query->where('employee_shift_date', $shift_date)->orWhere('buddy_shift_date', $shift_date);
                        })
                        ->where(function($query) use ($user_id){
                            $query->where('user_id', $user_id)->orWhere('buddy_user_id', $user_id);
                        })
                        ->first();

                if(!is_null($srd))
                {
                    if($srd->user_id == $user_id)
                    {
                        $shift_date = $srd->buddy_shift_date->toDateString();
                        #$shift_date = $this->date_format($srd->buddy_shift_date);
                    }
                    else
                    {
                        $shift_date = $srd->employee_shift_date->toDateString();
                        #$shift_date = $this->date_format($srd->employee_shift_date);
                    }
                }
            }

            $days .= $this->date_format($shift_date, $user->center_id);
        }

        if($user->center_id == 1)
        {
            return substr($days, 0, -3);
        }

        return substr($days, 0, -5);
    }

    public function date_format($date, $center_id)
    {
        if($center_id == 1)
        {
            return date('D', strtotime($date)) . ' - ';
        }

        return date('Y-m-d', strtotime($date)) . ' and ';
    }
    /*
    public function get_rest_date($week_ending, $user_id)
    {
        $days = '';
        $sr =   SwapAgentReqeust::whereSwapType('2rd')->whereStatus(1)->whereWeekEnding($week_ending)
                ->where(function($query) use ($user_id){
                    $query->where('user_id', $user_id)->orWhere('buddy_user_id', $user_id);
                })->first();

        if(!is_null($sr))
        {
            if($sr->user_id == $user_id)
            {
                $user_id = $sr->buddy_user_id;
            }
            else
            {
                $user_id = $sr->user_id;
            }
        }

        $rows = SwapAgentSchedule::whereUserId($user_id)->whereWeekEnding($week_ending)->whereSchedule('Off')
                ->orderBy('shift_date', 'ASC')->get();


        foreach ($rows as $row)
        {
            $days .= $row->shift_date . ' and ';
        }

        return substr($days, 0, -5);
    }
    */
}
