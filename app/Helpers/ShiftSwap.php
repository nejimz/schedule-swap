<?php
namespace App\Helpers;

use DB;
use VOT;
use VTO;
use App\SwapAgentReqeust;
use Carbon\Carbon;

class ShiftSwap
{
	public static function agent_has_swap_request($swap_type, $week_ending, $employee_shift_date, $buddy_shift_date, $my_user_id, $buddy_user_id)
    {
        if (!is_null($employee_shift_date) && !is_null($buddy_shift_date)) {
            $my_vot_exists = VOT::exists($employee_shift_date, $my_user_id);
            $my_vto_exists = VTO::exists($buddy_shift_date, $my_user_id);
            $buddy_vot_exists = VOT::exists($employee_shift_date, $buddy_user_id);
            $buddy_vto_exists = VTO::exists($buddy_shift_date, $buddy_user_id);

            if ($my_vot_exists) {
                return 'Cannot proess swap. You`ve already applied for Voluntary Overtime on ' . $employee_shift_date . '.';
            } elseif ($my_vto_exists) {
                return 'Cannot proess swap. You`ve already applied for Voluntary Time-Off on ' . $buddy_shift_date . '.';
            } elseif ($buddy_vot_exists) {
                return 'Cannot proess swap. You`re buddy already applied for Voluntary Overtime on ' . $employee_shift_date . '.';
            } elseif ($buddy_vto_exists) {
                return 'Cannot proess swap. You`re buddy already applied for Voluntary Time-Off on ' . $buddy_shift_date . '.';
            }
        }

        $counter = 0;
        $message_bag = '';
        $my_rows =  SwapAgentReqeust::whereWeekEnding($week_ending)->whereStatus(1)
                    ->where(function($query) use ($my_user_id, $buddy_user_id){
                        $query->whereIn('user_id', [$my_user_id, $buddy_user_id])->orWhereIn('buddy_user_id', [$my_user_id, $buddy_user_id]);
                    })
                    ->orderBy('created_at', 'ASC')->get();

        foreach($my_rows as $row)
        {
            $my_name_tag = $row->user->name . ' has';
            $buddy_name_tag = $row->buddy_user->name;
            $row_week_ending = $row->week_ending->toDateString();
            $row_employee_shift_date = (is_null($row->employee_shift_date))? null : $row->employee_shift_date->toDateString();
            $row_buddy_shift_date = (is_null($row->buddy_shift_date))? null : $row->buddy_shift_date->toDateString();
            #$rds_and_shifts = $this->agents_rest_days($row_week_ending, $row->user_id, $row->buddy_user_id);
            $rds_and_shifts = Schedule::agents_rest_days($row_week_ending, $row->user_id, $row->buddy_user_id);
            $rds_and_shifts = array_flatten($rds_and_shifts);
            // Name Tagging
            if($row->user_id == $my_user_id)
            {
                if(auth()->user()->id != $row->user_id){
                    $user = \App\User::find($row->user_id);
                    $my_name_tag = $user->name.' have';
                }else{
                    $my_name_tag = ' You have';
                }
            }
            elseif($row->buddy_user_id == $my_user_id)
            {
                $buddy_name_tag = 'you';
            }
            // Validate Request
            if($row->swap_type == '2rd')
            {
                // check if has 2rd swap
                if($swap_type == $row->swap_type && $week_ending == $row_week_ending)
                {
                    return $my_name_tag . ' a current ' . $row->swap_type_name . ' swap with ' . $buddy_name_tag . '.';
                }
                // has used the rd dates
                if(in_array($employee_shift_date, $rds_and_shifts) || in_array($buddy_shift_date, $rds_and_shifts))
                {
                    return $my_name_tag . ' a current ' . $row->swap_type_name . ' swap with ' . $buddy_name_tag . '.';
                }
            }
            elseif($row->swap_type == 'sd' || $row->swap_type == 'srd' || $row->swap_type == 'shrd')
            {
                //  2 Rest Day
                if($row->swap_type == 'srd' && $swap_type == '2rd')
                {
                    return $my_name_tag . ' a current ' . $row->swap_type_name . ' swap on ' . 
                                    $row_employee_shift_date . ' with ' . $buddy_name_tag . ' on ' . $row_buddy_shift_date . '.';
                }
                // Single Day
                if( in_array($employee_shift_date, [$row_employee_shift_date, $row_buddy_shift_date]) ||  in_array($buddy_shift_date, [$row_employee_shift_date, $row_buddy_shift_date]))
                {
                    return $my_name_tag . ' a current ' . $row->swap_type_name . ' swap on ' . 
                                    $row_employee_shift_date . ' with ' . $buddy_name_tag . ' on ' . $row_buddy_shift_date . '.';
                }
                // Check Rest Days request
                if($row->swap_type == 'shrd' && (in_array($employee_shift_date, $rds_and_shifts) || in_array($buddy_shift_date, $rds_and_shifts)))
                {
                    return $my_name_tag . ' a current ' . $row->swap_type_name . ' Swap on ' . $row_employee_shift_date . ' with ' . 
                            $buddy_name_tag . ' on ' . $row_buddy_shift_date . '.';
                }
            }
        }

        return '';
    }
}