<?php
namespace App\Helpers;
use App\ForecastVTO;
use App\ForecastVTORequestItem;
use \Carbon\Carbon;

class VTO
{
	public static function exists($date, $user_id)
	{
		$vto = 	ForecastVTORequestItem::with(['forecast', 'request'])
				->whereHas('forecast', function($query) use ($date){
					$query->where('shift_date', $date);
				})
				->whereHas('request', function($query) use ($user_id){
					$query->where('user_id', $user_id);
				})->count();
				
		if($vto > 0)
		{
			return true;
		}

		return false;
	}

	public static function check_skip($items)
	{
		$items = array_flatten($items);
		$item_first = array_first($items);
		$item_last = end($items);
		$skip = [];
		$group = [];
		$x = 0;
		$y = 0;
		$schedules = ForecastVTO::whereIn('id', $items)->orderBy('time_start', 'ASC')->get();
		$last_time_end = '';

		foreach ($schedules as $row) {
			$time_start = Carbon::parse($row->shift_date . ' ' . $row->time_start);
			$time_end = Carbon::parse($row->shift_date . ' ' . $row->time_end);
			if ($last_time_end != '' && $row->time_start != $last_time_end) {
				$skip[$y] = $row->id; // add to skip list array
				$y++;
			}
			$last_time_end = $time_end->toTimeString();
		}
		return $skip;
	}

	public static function check_superd($superd, $skill)
	{
		$filtered_superd = str_replace("SUPERD","", strtoupper($superd))." ".auth()->user()->center->code;
		$filtered_skill = str_replace(["SUP", "ERD"],"", strtoupper($skill));

		if(strcasecmp($filtered_skill, $filtered_superd) !== 0) 
		{
			return false;
		}

		return true;
	}

	public static function skip_first_1hour($items)
	{
		$items = array_flatten($items);
		
		$schedule_1 = ForecastVTO::whereIn('id', $items)->orderBy('time_start', 'ASC')->first();
		$schedule_2 = ForecastVTO::whereIn('id', $items)->orderBy('time_start', 'ASC')->offset(1)->limit(1)->first();
		
		$time_start = Carbon::parse($schedule_1->shift_date . ' ' . $schedule_1->time_start);
		#$time_end = Carbon::parse($schedule_2->shift_date . ' ' . $schedule_2->time_start);
		$time_end = ($schedule_2)? Carbon::parse($schedule_2->shift_date . ' ' . $schedule_2->time_start) : Carbon::now();

		$minutes_interval = $time_end->diffInMinutes($time_start);

		if ($minutes_interval > 30) {
			return false;
		}
		return true;
	}

	public static function clean_items($previous_items, $items)
	{
		$n = 0;
		$array = [];
		foreach ($items as $key) {
			if (!in_array($key, $previous_items)) {
				$array[$n] = $key;
				$n++;
			}
		}
		return $array;
	}
}