<?php 
namespace App\Helpers;
use App\SwapAgentReqeust;
use App\SwapAgentSchedule;
use App\ForecastVOTRequestItem;
use App\ForecastVTORequestItem;
use Carbon\Carbon;
use DB;

class Schedule
{
    public static function schedules_home($user_id)
    {
        $n = 0;
        $data = [];
        $rows = SwapAgentSchedule::whereUserId($user_id)
                ->whereRaw('week_ending >= DATE(NOW())')
                ->groupBy('week_ending')->orderBy('week_ending', 'ASC')->get(['week_ending']);

        foreach ($rows as $row)
        {
            $schedules_shift = SwapAgentSchedule::whereUserId($user_id)->whereWeekEnding($row->week_ending)
                                ->where('schedule', '<>', 'Off')->first();
            $schedules = SwapAgentSchedule::whereUserId($user_id)->whereWeekEnding($row->week_ending)
                        ->whereRaw('week_ending >= DATE(NOW())')
                        ->orderBy('shift_date', 'ASC')->get();

            $rd_and_shift_flat = [];
            $requests_2rd = SwapAgentReqeust::whereSwapType('2rd')->whereWeekEnding($row->week_ending)->whereStatus(1)
                        ->where(function($query) use ($user_id){
                            $query->where('user_id', $user_id)->orWhere('buddy_user_id', $user_id);
                        })
                        ->orderBy('created_at', 'ASC')->first();

            if(!is_null($requests_2rd))
            {
                $rd_and_shift = static::agents_rest_days($row->week_ending, $requests_2rd->user_id, $requests_2rd->buddy_user_id);
                $rd_and_shift_flat = array_flatten($rd_and_shift);
            }

            foreach ($schedules as $schedule)
            {
                $status = 0;
                $shift_date = $schedule->shift_date;

                $new_schedule = '';
                $new_shift_date = '';

                // 2 RD
                if(in_array($shift_date, $rd_and_shift_flat))
                {
                    $new_schedule = $schedules_shift->schedule;

                    if($schedule->schedule != 'Off')
                    {
                        $new_schedule = 'Off';
                    }
                }
                // Single Day
                else
                {
                    $requests = SwapAgentReqeust::whereIn('swap_type', ['sd', 'srd', 'shrd'])->whereWeekEnding($schedule->week_ending)->whereStatus(1)
                                ->where(function($query) use ($user_id){
                                    $query->where('user_id', $user_id)->orWhere('buddy_user_id', $user_id);
                                })
                                ->where(function($query) use ($shift_date){
                                    $query->where('employee_shift_date', $shift_date)->orWhere('buddy_shift_date', $shift_date);
                                })
                                ->orderBy('created_at', 'ASC')->get();
                    foreach ($requests as $request)
                    {
                        $status = 1;
                        $request_employee_shift_date = $request->employee_shift_date->toDateString();
                        $request_buddy_shift_date = $request->buddy_shift_date->toDateString();

                        if($request->swap_type == 'sd')
                        {
                            if($schedule->user_id == $request->user_id)
                            {
                                $new_schedule = $request->buddy_schedule;
                                $new_shift_date = $request_buddy_shift_date;
                            }
                            elseif($schedule->user_id == $request->buddy_user_id)
                            {
                                $new_schedule = $request->employee_schedule;
                                $new_shift_date = $request_employee_shift_date;
                            }
                        }
                        elseif($request->swap_type == 'srd')
                        {
                            $opposite_date = $request_employee_shift_date;

                            if($request_employee_shift_date == $schedule->shift_date)
                            {
                                $opposite_date = $request_buddy_shift_date;
                            }

                            $get_other_shift_schedule = SwapAgentSchedule::whereUserId($user_id)->whereShiftDate($opposite_date)->first();
                            $new_schedule = $get_other_shift_schedule->schedule;
                        }
                        elseif($request->swap_type == 'shrd')
                        {
                            $get_query = SwapAgentSchedule::whereShiftDate($shift_date);

                            if($user_id == $request->user_id)
                            {
                                $get_query->whereUserId($request->buddy_user_id);
                            }
                            elseif($user_id == $request->buddy_user_id)
                            {
                                $get_query->whereUserId($request->user_id);
                            }

                            $get_other_shift_schedule = $get_query->first();
                            $new_schedule = $get_other_shift_schedule->schedule;
                        }
                    }
                }

                // VOT & VTO
                #$vot = ForecastVTORequestItem::whereUserId($user_id)->get();
                $last_shift_date = $shift_date;
                $last_schedule = $shift_date;

                if($new_schedule != '' || $new_shift_date != '')
                {
                    $last_shift_date = $new_shift_date;
                    $last_schedule = $new_schedule;
                }

                $vots_count = 0;
                $vots = [];
                $vtos_count = 0;
                $vtos = [];

                $vot_items = ForecastVOTRequestItem::with(['forecast', 'request'])
                        ->whereHas('forecast', function($query) use ($last_shift_date){
                            $query->where('shift_date', $last_shift_date);
                        })
                        ->whereHas('request', function($query) use ($user_id){
                            $query->where('user_id', $user_id);
                        })->get();

                if($vot_items)
                {
                    foreach ($vot_items as $row)
                    {
                        $vots[$vots_count] = date('h:i A', strtotime($row->forecast->time_start)) . ' - ' . date('h:i A', strtotime($row->forecast->time_end));
                        $vots_count++;
                    }
                }

                $vto_items = ForecastVTORequestItem::with(['forecast', 'request'])
                        ->whereHas('forecast', function($query) use ($last_shift_date){
                            $query->where('shift_date', $last_shift_date);
                        })
                        ->whereHas('request', function($query) use ($user_id){
                            $query->where('user_id', $user_id);
                        })->get();

                if($vto_items)
                {
                    foreach ($vto_items as $row)
                    {
                        $vtos[$vtos_count] = date('h:i A', strtotime($row->forecast->time_start)) . ' - ' . date('h:i A', strtotime($row->forecast->time_end));
                        $vtos_count++;
                    }
                }

                $data[$schedule->week_ending][$n] = [
                    'status' => $status, 
                    'shift_date' => $shift_date, 
                    'week_ending' => $schedule->week_ending, 
                    'schedule' => $schedule->schedule, 
                    'restrictions' => $schedule->restrictions, 
                    'shift_date_format' => $schedule->shift_date, 
                    'new_schedule' => $new_schedule, 
                    'new_shift_date' => $new_shift_date, 
                    'vots' => $vots, 
                    'vtos' => $vtos 
                ];
                $n++;
            }
        }

        #dd($data);
        return $data;
    }

    public static function schedules($user_id)
    {
        $n = 0;
        $data = [];
        $rows = SwapAgentSchedule::whereUserId($user_id)
                ->whereRaw('expiration_date >= NOW()')
                ->groupBy('week_ending')->orderBy('week_ending', 'ASC')->get(['week_ending']);

        foreach ($rows as $row)
        {
            $schedules_shift = SwapAgentSchedule::whereUserId($user_id)->whereWeekEnding($row->week_ending)
                                ->where('schedule', '<>', 'Off')->first();
            $schedules = SwapAgentSchedule::whereUserId($user_id)->whereWeekEnding($row->week_ending)
                        ->whereRaw('expiration_date >= NOW()')
                        ->orderBy('shift_date', 'ASC')->get();

            $rd_and_shift_flat = [];
            $requests_2rd = SwapAgentReqeust::whereSwapType('2rd')->whereWeekEnding($row->week_ending)->whereStatus(1)
                        ->where(function($query) use ($user_id){
                            $query->where('user_id', $user_id)->orWhere('buddy_user_id', $user_id);
                        })
                        ->orderBy('created_at', 'ASC')->first();

            if(!is_null($requests_2rd))
            {
                $rd_and_shift = static::agents_rest_days($row->week_ending, $requests_2rd->user_id, $requests_2rd->buddy_user_id);
                $rd_and_shift_flat = array_flatten($rd_and_shift);
            }

            foreach ($schedules as $schedule)
            {
                $status = 0;
                $shift_date = $schedule->shift_date;

                $new_schedule = '';
                $new_shift_date = '';

                // 2 RD
                if(in_array($shift_date, $rd_and_shift_flat))
                {
                    $new_schedule = $schedules_shift->schedule;

                    if($schedule->schedule != 'Off')
                    {
                        $new_schedule = 'Off';
                    }
                }
                // Single Day
                else
                {
                    $requests = SwapAgentReqeust::whereIn('swap_type', ['sd', 'srd', 'shrd'])->whereWeekEnding($schedule->week_ending)->whereStatus(1)
                                ->where(function($query) use ($user_id){
                                    $query->where('user_id', $user_id)->orWhere('buddy_user_id', $user_id);
                                })
                                ->where(function($query) use ($shift_date){
                                    $query->where('employee_shift_date', $shift_date)->orWhere('buddy_shift_date', $shift_date);
                                })
                                ->orderBy('created_at', 'ASC')->get();
                    foreach ($requests as $request)
                    {
                        $status = 1;
                        $request_employee_shift_date = $request->employee_shift_date->toDateString();
                        $request_buddy_shift_date = $request->buddy_shift_date->toDateString();

                        if($request->swap_type == 'sd')
                        {
                            if($schedule->user_id == $request->user_id)
                            {
                                $new_schedule = $request->buddy_schedule;
                                $new_shift_date = $request_buddy_shift_date;
                            }
                            elseif($schedule->user_id == $request->buddy_user_id)
                            {
                                $new_schedule = $request->employee_schedule;
                                $new_shift_date = $request_employee_shift_date;
                            }
                        }
                        elseif($request->swap_type == 'srd')
                        {
                            $opposite_date = $request_employee_shift_date;

                            if($request_employee_shift_date == $schedule->shift_date)
                            {
                                $opposite_date = $request_buddy_shift_date;
                            }

                            $get_other_shift_schedule = SwapAgentSchedule::whereUserId($user_id)->whereShiftDate($opposite_date)->first();
                            $new_schedule = $get_other_shift_schedule->schedule;
                        }
                        elseif($request->swap_type == 'shrd')
                        {
                            $get_query = SwapAgentSchedule::whereShiftDate($shift_date);

                            if($user_id == $request->user_id)
                            {
                                $get_query->whereUserId($request->buddy_user_id);
                            }
                            elseif($user_id == $request->buddy_user_id)
                            {
                                $get_query->whereUserId($request->user_id);
                            }

                            $get_other_shift_schedule = $get_query->first();
                            $new_schedule = $get_other_shift_schedule->schedule;
                        }
                    }
                }

                $data[$schedule->week_ending][$n] = [
                    'status' => $status, 
                    'shift_date' => $shift_date, 
                    'week_ending' => $schedule->week_ending, 
                    'schedule' => $schedule->schedule, 
                    'restrictions' => $schedule->restrictions, 
                    'shift_date_format' => $schedule->shift_date, 
                    'new_schedule' => $new_schedule, 
                    'new_shift_date' => $new_shift_date, 
                ];
                $n++;
            }
        }

        #dd($data);
        return $data;
    }


    public static function schedule_shift_date($user_id, $shift_date)
    {
        $n = 0;
        $data = [];
        $rows = SwapAgentSchedule::whereUserId($user_id)->whereShiftDate($shift_date)
                ->whereRaw('shift_date >= DATE(NOW())')->orderBy('week_ending', 'ASC')->get(['week_ending']);

        foreach ($rows as $row)
        {
            $schedules_shift = SwapAgentSchedule::whereUserId($user_id)->whereShiftDate($shift_date)->where('schedule', '<>', 'Off')->first();
            $schedules = SwapAgentSchedule::whereUserId($user_id)->whereShiftDate($shift_date)->orderBy('shift_date', 'ASC')->get();

            $rd_and_shift_flat = [];
            $requests_2rd = SwapAgentReqeust::whereSwapType('2rd')->whereWeekEnding($row->week_ending)->whereStatus(1)
                        ->where(function($query) use ($user_id){
                            $query->where('user_id', $user_id)->orWhere('buddy_user_id', $user_id);
                        })
                        ->orderBy('created_at', 'ASC')->first();

            if(!is_null($requests_2rd))
            {
                #$rd_and_shift = $this->agents_rest_days($row->week_ending, $requests_2rd->user_id, $requests_2rd->buddy_user_id);
                $rd_and_shift = static::agents_rest_days($row->week_ending, $requests_2rd->user_id, $requests_2rd->buddy_user_id);
                $rd_and_shift_flat = array_flatten($rd_and_shift);
            }

            foreach ($schedules as $schedule)
            {
                $status = 0;
                $shift_date = $schedule->shift_date;

                $new_schedule = '';
                $new_shift_date = '';

                // 2 RD
                if(in_array($shift_date, $rd_and_shift_flat))
                {
                    #dump($schedule);dump($rd_and_shift_flat);dump($schedules_shift);
                    #$new_schedule = $schedules_shift->schedule;
                    if(!is_null($schedules_shift))
                    {
                        $new_schedule = $schedules_shift->schedule;
                    }

                    if($schedule->schedule != 'Off')
                    {
                        $new_schedule = 'Off';
                    }
                }
                // Single Day
                else
                {
                    $requests = SwapAgentReqeust::whereIn('swap_type', ['sd', 'srd', 'shrd'])->whereWeekEnding($schedule->week_ending)->whereStatus(1)
                                ->where(function($query) use ($user_id){
                                    $query->where('user_id', $user_id)->orWhere('buddy_user_id', $user_id);
                                })
                                ->where(function($query) use ($shift_date){
                                    $query->where('employee_shift_date', $shift_date)->orWhere('buddy_shift_date', $shift_date);
                                })
                                ->orderBy('created_at', 'ASC')->get();
                    foreach ($requests as $request)
                    {
                        $status = 1;
                        $request_employee_shift_date = $request->employee_shift_date->toDateString();
                        $request_buddy_shift_date = $request->buddy_shift_date->toDateString();

                        if($request->swap_type == 'sd')
                        {
                            if($schedule->user_id == $request->user_id)
                            {
                                $new_schedule = $request->buddy_schedule;
                                $new_shift_date = $request_buddy_shift_date;
                            }
                            elseif($schedule->user_id == $request->buddy_user_id)
                            {
                                $new_schedule = $request->employee_schedule;
                                $new_shift_date = $request_employee_shift_date;
                            }
                        }
                        elseif($request->swap_type == 'srd')
                        {
                            $opposite_date = $request_employee_shift_date;

                            if($request_employee_shift_date == $schedule->shift_date)
                            {
                                $opposite_date = $request_buddy_shift_date;
                            }

                            $get_other_shift_schedule = SwapAgentSchedule::whereUserId($user_id)->whereShiftDate($opposite_date)->first();
                            $new_schedule = $get_other_shift_schedule->schedule;
                        }
                        elseif($request->swap_type == 'shrd')
                        {
                            $get_query = SwapAgentSchedule::whereShiftDate($shift_date);

                            if($user_id == $request->user_id)
                            {
                                $get_query->whereUserId($request->buddy_user_id);
                            }
                            elseif($user_id == $request->buddy_user_id)
                            {
                                $get_query->whereUserId($request->user_id);
                            }

                            $get_other_shift_schedule = $get_query->first();
                            $new_schedule = $get_other_shift_schedule->schedule;
                        }
                    }
                }

                $data = [
                    'status' => $status, 
                    'shift_date' => $shift_date, 
                    'week_ending' => $schedule->week_ending, 
                    'schedule' => $schedule->schedule, 
                    'restrictions' => $schedule->restrictions, 
                    'shift_date_format' => $schedule->shift_date, 
                    'restrictions' => $row->restrictions, 
                    'new_schedule' => $new_schedule, 
                    'new_shift_date' => $new_shift_date 
                ];
                $n++;
            }
        }

        #dd($data);
        return $data;
    }

    public static function api_schedules($user_id)
    {
        $data = [];
        $rows = SwapAgentSchedule::whereUserId($user_id)
                ->whereRaw('expiration_date >= NOW()')
                ->groupBy('week_ending')->orderBy('week_ending', 'ASC')->get(['week_ending']);

        foreach ($rows as $row)
        {
            $schedules_shift = SwapAgentSchedule::whereUserId($user_id)->whereWeekEnding($row->week_ending)
                                ->where('schedule', '<>', 'Off')->first();
            $schedules = SwapAgentSchedule::whereUserId($user_id)->whereWeekEnding($row->week_ending)
                        ->whereRaw('expiration_date >= NOW()')
                        ->orderBy('shift_date', 'ASC')->get();

            $rd_and_shift_flat = [];
            $requests_2rd = SwapAgentReqeust::whereSwapType('2rd')->whereWeekEnding($row->week_ending)->whereStatus(1)
                        ->where(function($query) use ($user_id){
                            $query->where('user_id', $user_id)->orWhere('buddy_user_id', $user_id);
                        })
                        ->orderBy('created_at', 'ASC')->first();

            if(!is_null($requests_2rd))
            {
                $rd_and_shift = static::agents_rest_days($row->week_ending, $requests_2rd->user_id, $requests_2rd->buddy_user_id);
                $rd_and_shift_flat = array_flatten($rd_and_shift);
            }

            foreach ($schedules as $schedule)
            {
                $status = 0;
                $shift_date = $schedule->shift_date;

                $new_schedule = '';
                $new_shift_date = '';

                // 2 RD
                if(in_array($shift_date, $rd_and_shift_flat))
                {
                    $new_schedule = $schedules_shift->schedule;

                    if($schedule->schedule != 'Off')
                    {
                        $new_schedule = 'Off';
                    }
                }
                // Single Day
                else
                {
                    $requests = SwapAgentReqeust::whereIn('swap_type', ['sd', 'srd', 'shrd'])->whereWeekEnding($schedule->week_ending)->whereStatus(1)
                                ->where(function($query) use ($user_id){
                                    $query->where('user_id', $user_id)->orWhere('buddy_user_id', $user_id);
                                })
                                ->where(function($query) use ($shift_date){
                                    $query->where('employee_shift_date', $shift_date)->orWhere('buddy_shift_date', $shift_date);
                                })
                                ->orderBy('created_at', 'ASC')->get();
                    foreach ($requests as $request)
                    {
                        $status = 1;
                        $request_employee_shift_date = $request->employee_shift_date->toDateString();
                        $request_buddy_shift_date = $request->buddy_shift_date->toDateString();

                        if($request->swap_type == 'sd')
                        {
                            if($schedule->user_id == $request->user_id)
                            {
                                $new_schedule = $request->buddy_schedule;
                                $new_shift_date = $request_buddy_shift_date;
                            }
                            elseif($schedule->user_id == $request->buddy_user_id)
                            {
                                $new_schedule = $request->employee_schedule;
                                $new_shift_date = $request_employee_shift_date;
                            }
                        }
                        elseif($request->swap_type == 'srd')
                        {
                            $opposite_date = $request_employee_shift_date;

                            if($request_employee_shift_date == $schedule->shift_date)
                            {
                                $opposite_date = $request_buddy_shift_date;
                            }

                            $get_other_shift_schedule = SwapAgentSchedule::whereUserId($user_id)->whereShiftDate($opposite_date)->first();
                            $new_schedule = $get_other_shift_schedule->schedule;
                        }
                        elseif($request->swap_type == 'shrd')
                        {
                            $get_query = SwapAgentSchedule::whereShiftDate($shift_date);

                            if($user_id == $request->user_id)
                            {
                                $get_query->whereUserId($request->buddy_user_id);
                            }
                            elseif($user_id == $request->buddy_user_id)
                            {
                                $get_query->whereUserId($request->user_id);
                            }

                            $get_other_shift_schedule = $get_query->first();
                            $new_schedule = $get_other_shift_schedule->schedule;
                        }
                    }
                }

                $data[] = [
                    'status' => $status, 
                    'shift_date' => $shift_date, 
                    'week_ending' => $schedule->week_ending, 
                    'schedule' => $schedule->schedule, 
                    'restrictions' => $schedule->restrictions, 
                    'shift_date_format' => $schedule->shift_date, 
                    'new_schedule' => $new_schedule, 
                    'new_shift_date' => $new_shift_date, 
                ];
            }
        }
        return $data;
    }

    public static function shift($user_id, $shift_date)
    {
        $n = 0;
        $data = [];
        $rows = SwapAgentSchedule::whereUserId($user_id)->whereShiftDate($shift_date)
                ->whereRaw('expiration_date >= NOW()')->orderBy('week_ending', 'ASC')->get(['week_ending']);

        foreach ($rows as $row)
        {
            $schedules_shift = SwapAgentSchedule::whereUserId($user_id)->whereShiftDate($shift_date)
                                ->where('schedule', '<>', 'Off')->first();
            $schedules = SwapAgentSchedule::whereUserId($user_id)->whereShiftDate($shift_date)
                        ->whereRaw('expiration_date >= NOW()')
                        ->orderBy('shift_date', 'ASC')->get();

            $rd_and_shift_flat = [];
            $requests_2rd = SwapAgentReqeust::whereSwapType('2rd')->whereWeekEnding($row->week_ending)->whereStatus(1)
                        ->where(function($query) use ($user_id){
                            $query->where('user_id', $user_id)->orWhere('buddy_user_id', $user_id);
                        })
                        ->orderBy('created_at', 'ASC')->first();

            if(!is_null($requests_2rd))
            {
                $rd_and_shift = static::agents_rest_days($row->week_ending, $requests_2rd->user_id, $requests_2rd->buddy_user_id);
                $rd_and_shift_flat = array_flatten($rd_and_shift);
            }

            foreach ($schedules as $schedule)
            {
                $status = 0;
                $shift_date = $schedule->shift_date;

                $new_schedule = '';
                $new_shift_date = '';

                // 2 RD
                if(in_array($shift_date, $rd_and_shift_flat))
                {
                    $new_schedule = $schedules_shift->schedule;

                    if($schedule->schedule != 'Off')
                    {
                        $new_schedule = 'Off';
                    }
                }
                // Single Day
                else
                {
                    $requests = SwapAgentReqeust::whereIn('swap_type', ['sd', 'srd', 'shrd'])->whereWeekEnding($schedule->week_ending)->whereStatus(1)
                                ->where(function($query) use ($user_id){
                                    $query->where('user_id', $user_id)->orWhere('buddy_user_id', $user_id);
                                })
                                ->where(function($query) use ($shift_date){
                                    $query->where('employee_shift_date', $shift_date)->orWhere('buddy_shift_date', $shift_date);
                                })
                                ->orderBy('created_at', 'ASC')->get();
                    foreach ($requests as $request)
                    {
                        $status = 1;
                        $request_employee_shift_date = $request->employee_shift_date->toDateString();
                        $request_buddy_shift_date = $request->buddy_shift_date->toDateString();

                        if($request->swap_type == 'sd')
                        {
                            if($schedule->user_id == $request->user_id)
                            {
                                $new_schedule = $request->buddy_schedule;
                                $new_shift_date = $request_buddy_shift_date;
                            }
                            elseif($schedule->user_id == $request->buddy_user_id)
                            {
                                $new_schedule = $request->employee_schedule;
                                $new_shift_date = $request_employee_shift_date;
                            }
                        }
                        elseif($request->swap_type == 'srd')
                        {
                            $opposite_date = $request_employee_shift_date;

                            if($request_employee_shift_date == $schedule->shift_date)
                            {
                                $opposite_date = $request_buddy_shift_date;
                            }

                            $get_other_shift_schedule = SwapAgentSchedule::whereUserId($user_id)->whereShiftDate($opposite_date)->first();
                            $new_schedule = $get_other_shift_schedule->schedule;
                        }
                        elseif($request->swap_type == 'shrd')
                        {
                            $get_query = SwapAgentSchedule::whereShiftDate($shift_date);

                            if($user_id == $request->user_id)
                            {
                                $get_query->whereUserId($request->buddy_user_id);
                            }
                            elseif($user_id == $request->buddy_user_id)
                            {
                                $get_query->whereUserId($request->user_id);
                            }

                            $get_other_shift_schedule = $get_query->first();
                            $new_schedule = $get_other_shift_schedule->schedule;
                        }
                    }
                }

                $data = [
                    'status' => $status, 
                    'shift_date' => $shift_date, 
                    'week_ending' => $schedule->week_ending, 
                    'schedule' => $schedule->schedule, 
                    'restrictions' => $schedule->restrictions, 
                    'shift_date_format' => $schedule->shift_date, 
                    'restrictions' => $row->restrictions, 
                    'new_schedule' => $new_schedule, 
                    'new_shift_date' => $new_shift_date 
                ];
                $n++;
            }
        }

        #dd($data);
        return $data;
    }

    public static function agents_rest_days($week_ending, $my_user_id, $buddy_user_id)
    {
        $rows = SwapAgentSchedule::whereIn('user_id', [$my_user_id, $buddy_user_id])->whereWeekEnding($week_ending)
                ->whereSchedule('Off')
                ->orderBy('shift_date', 'ASC')
                ->get(['shift_date']);

        return $rows->toArray();
    }

    public static function agents_4_rest_day_with_shift_dates($week_ending, $my_user_id, $buddy_user_id)
    {
        #$agents_rest_days = $this->agents_rest_days($week_ending, $my_user_id, $buddy_user_id);
        $agents_rest_days = static::agents_rest_days($week_ending, $my_user_id, $buddy_user_id);

        $rows = SwapAgentSchedule::whereIn('user_id', [$my_user_id, $buddy_user_id])->whereWeekEnding($week_ending)
                ->whereIn('shift_date', $agents_rest_days)
                ->orderBy('shift_date', 'ASC')->get();

        return $rows->toArray();
    }

    public static function agents_4_rest_day_with_shift_dates_has_restrictions($week_ending, $my_user_id, $buddy_user_id)
    {
        #$agents_rest_days = $this->agents_rest_days($week_ending, $my_user_id, $buddy_user_id);
        $agents_rest_days = static::agents_rest_days($week_ending, $my_user_id, $buddy_user_id);

        $rows = SwapAgentSchedule::whereIn('user_id', [$my_user_id, $buddy_user_id])->whereWeekEnding($week_ending)
                ->whereIn('shift_date', $agents_rest_days)->whereNotNull('restrictions')
                ->orderBy('shift_date', 'ASC')
                ->get(['shift_date', 'restrictions', 'user_id', DB::raw('(SELECT CONCAT(first_name, " ", last_name) AS name FROM users WHERE id=swap_agent_schedules.user_id ORDER BY id DESC LIMIT 1) AS name')]);

        return $rows;
    }

    public static function shift_date_restrictions($week_ending, $my_user_id, $buddy_user_id, $employee_shift_date, $buddy_shift_date)
    {
        $message = '';
        $rows = SwapAgentSchedule::whereWeekEnding($week_ending)->whereNotNull('restrictions')
                ->whereIn('user_id', [$my_user_id, $buddy_user_id])
                ->whereIn('shift_date', [$employee_shift_date, $buddy_shift_date])
                ->orderBy('shift_date', 'ASC')
                ->get();

        foreach ($rows as $row)
        {
            $name_tag = 'Agent ' . $row->user->name . ' has';
            if($row->user_id == $my_user_id)
            {
                $name_tag = 'You have';
            }
            $message = 'Swap not allowed. ' . $name_tag . ' a ' . $row->restrictions . ' on ' . $row->shift_date . '.';
        }
        return $message;
    }

    public static function srd_other_shift($user_id, $shift_date)
    {
        $row = SwapAgentSchedule::whereUserId($user_id)->whereShiftDate($shift_date)->first();

        if($row->schedule == 'Off')
        {
            return true;
        }

        return false;
    }

    public static function earliest_date($my_shift, $my_shift_schedule, $buddy_shift, $buddy_schedule)
    {
        $dates = [$my_shift, $buddy_shift];
        $dates_sorted = array_sort($dates);

        if(strtotime($my_shift_schedule) || strtotime($buddy_schedule))
        {

            if($dates_sorted[0] == $my_shift)
            {
                return $dates_sorted[0] . ' ' . trim($my_shift_schedule);
            }
            elseif($dates_sorted[0] == $my_shift)
            {
                return $dates_sorted[0] . ' ' . trim($buddy_shift_schedule);
            }
        }
        else
        {
            return $dates_sorted[0] . ' 00:00:00';
        }
    }
}