<?php
namespace App\Helpers;

use Illuminate\Http\Request;
use App\ActivityLog;
use Auth;

class ActivityLogger
{
	public static function store($segment, $action)
	{
		$user = Auth::user();
		$platform = 'mobile';

		if($segment == 'portal') {
			$platform = 'web';
		} elseif($segment == 'login') {
			$platform = 'web';
		}

        ActivityLog::create([
            'center_id' => $user->center_id,
            'user_id' => $user->id,
            'platform' => $platform,
            'action' => $action
        ]);
	}
}