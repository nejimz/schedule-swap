<?php
namespace App\Helpers;
use App\MailRecipients;
use Mail;

class MaileRecipientsHelper
{
    public static function mailer($row, $subject, $module)
    {
        $data = [];
        $from = [];
        $to = [];
        $cc = [];
        $bcc = [];
        $n = 0;
        $recipients = MailRecipients::whereCenterId($row->center_id)->whereModule($module)->get();

        if(count($recipients) > 0)
        {
	        foreach($recipients as $recipient)
	        {
	            if($recipient->type == 'from') { $from[$n] = $recipient->mail; }
	            if($recipient->type == 'to') { $to[$n] = $recipient->mail; }
	            if($recipient->type == 'cc') { $cc[$n] = $recipient->mail; }
	            if($recipient->type == 'bcc') { $bcc[$n] = $recipient->mail; }
	            $n++;
	        }
	        $data = compact('row');
	        Mail::send('email.shift-swap.' . $row->swap_mail_template, $data, function ($m) use ($from, $to, $cc, $subject) {
	            $m->from($from);
	            $m->to($to);
	            $m->cc($cc);
	            $m->bcc($cc);
	            $m->subject($subject);
	        });
        }
    }
}