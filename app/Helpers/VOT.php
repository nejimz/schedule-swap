<?php
namespace App\Helpers;
use App\ForecastVOT;
use App\ForecastVOTRequestItem;
use \Carbon\Carbon;
use Schedule;

class VOT
{
	public static function exists($date, $user_id)
	{
		$vot = 	ForecastVOTRequestItem::with(['forecast', 'request'])
				->whereHas('forecast', function($query) use ($date){
					$query->where('shift_date', $date);
				})
				->whereHas('request', function($query) use ($user_id){
					$query->where('user_id', $user_id);
				})->count();

		if($vot > 0)
		{
			return true;
		}

		return false;
	}

	public static function check_skip($items)
	{
		$items = array_flatten($items);
		$item_first = array_first($items);
		$item_last = end($items);
		$pre_skip = [];
		$post_skip = [];
		$skip = [];
		$group = [];
		$x = 0;
		$y = 0;
		$schedules = ForecastVOT::whereIn('id', $items)->orderBy('time_start', 'ASC')->get();
		$last_time_end = '';

		$pre_count = 0;
		$post_count = 0;
		$last_preshift_time_end=''; 
		$last_postshift_time_start='';

		$explode_schedule = [];

		foreach ($schedules as $row) {
			$time_start = Carbon::parse($row->shift_date . ' ' . $row->time_start);
			$time_end = Carbon::parse($row->shift_date . ' ' . $row->time_end);

			$schedule_shift_date = Schedule::schedule_shift_date(auth()->user()->id, $row->shift_date);

			if($schedule_shift_date['schedule'] != 'Off') {
				$explode_schedule = explode(' - ', $schedule_shift_date['schedule']);

				$explode_schedule[0] = Carbon::parse(date('Y-m-d H:i:s',strtotime($row->shift_date.' '.$explode_schedule[0])));
				$explode_schedule[1] = Carbon::parse(date('Y-m-d H:i:s',strtotime($row->shift_date.' '.$explode_schedule[1])));

				if($last_postshift_time_start == '' && $explode_schedule[0]->lt($time_end)) {
					$last_postshift_time_start = $time_start; 
				}
			}
			

			if ($last_time_end != '' && $row->time_start != $last_time_end) {

				if($schedule_shift_date['schedule'] == 'Off') {
					$skip[$y] = $row->id; // add to skip list array
					$y++;
				} else{
					if($explode_schedule[0]->gt($time_start)) {
					$pre_skip[$pre_count] = $row->id;
					$pre_count++;
					}

					if($explode_schedule[1]->lt($time_start) && $explode_schedule[1]->diffInMinutes($last_postshift_time_start) >= 30) {
						$last_postshift_time_start = '';
						$post_skip[$post_count] = $row->id;
						$post_count++;
					}
				}				
				
			}
			if($schedule_shift_date['schedule'] != 'Off') {
				if($explode_schedule[0]->gt($time_start)) {
					$last_preshift_time_end = $time_end; 
				}
			}

			$last_time_end = $time_end->toTimeString();
		}

		if($last_preshift_time_end != '' && $last_preshift_time_end != $explode_schedule[0]->toTimeString() && $explode_schedule[0]->diffInMinutes($last_preshift_time_end) >= 30){
			$pre_count++;
		}

		if($last_postshift_time_start != '' && $last_preshift_time_end == '' && $last_postshift_time_start != $explode_schedule[1]->toTimeString() && $explode_schedule[1]->diffInMinutes($last_postshift_time_start) >= 30){
			$post_count++;
		}

		if(count($explode_schedule) != 0) {	
			$skip = $post_skip;

			if($pre_count>$post_count) {
				$skip = $pre_skip;
			}
		}
		return $skip;
	}

	public static function check_superd($superd, $skill)
	{
		$filtered_superd = str_replace("SUPERD","", strtoupper($superd))." ".auth()->user()->center->code;
		$filtered_skill = str_replace(["SUP", "ERD"],"", strtoupper($skill));

		if(strcasecmp($filtered_skill, $filtered_superd) !== 0) {
			return false;
		}

		return true;
	}

	public static function skip_first_1hour($items)
	{
		$items = array_flatten($items);
		$schedule_1 = ForecastVOT::whereIn('id', $items)->orderBy('time_start', 'ASC')->first();
		$schedule_2 = ForecastVOT::whereIn('id', $items)->orderBy('time_start', 'ASC')->offset(1)->limit(1)->first();
		
		$time_start = Carbon::parse($schedule_1->shift_date . ' ' . $schedule_1->time_start);
		$time_end = ($schedule_2)? Carbon::parse($schedule_2->shift_date . ' ' . $schedule_2->time_start) : Carbon::now();

		$minutes_interval = $time_end->diffInMinutes($time_start);

		if ($minutes_interval > 30) {
			return false;
		}
		return true;
	}

	public static function check_skip_old($items)
	{
		asort($items);
		$item_first = array_first($items);
		$item_last = end($items);
		$skip = [];
		$group = [];
		$x = 0;
		$y = 0;
		for ($i = $item_first; $i <= $item_last; $i++) {
			if (!in_array($i, $items)) { //  check if item id exists
				$group[$x] = $i; // add to group of items
				$x++;
			} elseif (count($group) > 0) { // check if group has items
				$x = 0; //  clear increment x
				$skip[$y] = $group; // add to skip list array
				$group = []; // clear group of items
				$y++;
			}
		}

		return $skip;
	}

	public static function clean_items($previous_items, $items)
	{
		$n = 0;
		$array = [];
		foreach ($items as $key) {
			if (!in_array($key, $previous_items)) {
				$array[$n] = $key;
				$n++;
			}
		}
		return $array;
	}
}