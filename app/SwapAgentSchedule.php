<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SwapAgentSchedule extends Model
{
    public $table = "swap_agent_schedules";
    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
    	'user_id', 'center_id', 'shift_date', 'schedule', 'restrictions', 'week_ending', 'created_at', 'updated_at' 
	];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
