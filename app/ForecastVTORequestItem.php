<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForecastVTORequestItem extends Model
{
    public $table = "forecast_vto_request_item";
    public $timestamps = false;

    protected $fillable = [
    	'forecast_vto_id', 'forecast_vto_request_id'
	];

	public function request()
	{
        return $this->belongsTo('App\ForecastVTORequest', 'forecast_vto_request_id', 'id');
	}

	public function forecast()
	{
        return $this->belongsTo('App\ForecastVTO', 'forecast_vto_id', 'id');
	}
}
