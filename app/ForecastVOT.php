<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ForecastVOTRequestItem;
use Auth;

class ForecastVOT extends Model
{
    public $table = "forecast_vot";
    public $timestamps = true;

    protected $fillable = [
    	'center_id', 'organization', 'slots', 'shift_date', 'time_start', 
    	'time_end', 'user_id', 'expired_at', 'created_at', 'updated_at'
	];

	public function user()
	{
    	return $this->belongsTo('App\User', 'user_id', 'id');
	}

	public function items()
	{
    	return $this->hasOne('App\ForecastVOTRequestItem', 'forecast_vot_id', 'id');
	}

	public function getRemainingSlotsAttribute()
	{
		$count = ForecastVOTRequestItem::whereForecastVotId($this->id)->count();
		return $this->slots - $count;
	}

	public function getItemExistsAttribute()
	{
		$user = Auth::user();
		$count = ForecastVOTRequestItem::whereHas('request', function($query) use ($user){
					$query->where('user_id', $user->id);
				})->whereForecastVotId($this->id)->count();
		return $count;
	}

	public function getScheduleAttribute()
	{
		return date('h:i A', strtotime($this->time_start)) . ' - ' . date('h:i A', strtotime($this->time_end));
	}
}
