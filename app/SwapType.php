<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SwapType extends Model
{
    public $table = "swap_type";
    public $timestamps = true;

    protected $fillable = [
    	'abbr', 'name', 'created_at', 'updated_at'
	];
}
