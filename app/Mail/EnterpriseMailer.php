<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EnterpriseMailer extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = "Test Subject";

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        #return $this->view('view.name');
        #return $this->from('example@example.com')->view('view.email.welcome');
        return $this->markdown('email.test');
    }
}
