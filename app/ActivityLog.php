<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
    public $table = 'activity_logs';
    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
    	'center_id', 'user_id', 'platform', 'action', 'created_at', 'updated_at' 
	];
}
