<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShiftSwapStorePost extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'swap_type'         => 'required',
            'my_shift_date'     => 'required_if:swap_type,sd,srd,shrd',
            'week_ending'       => 'required_if:swap_type,2rd',
            'my_schedule'       => 'required_if:swap_type,sd,srd,shrd',
            'buddy_avaya'       => 'required',
            'buddy_shift_date'  => 'required_if:swap_type,sd,srd,shrd',
            'buddy_schedule'    => 'required_if:swap_type,sd,srd,shrd'
        ];
    }

    public function attributes()
    {
        return [
            'swap_type'         => 'Swap Type',
            'week_ending'       => 'Week Ending',
            'my_shift_date'     => 'My Shift Date',
            'my_schedule'       => 'My Schedule',
            'buddy_avaya'       => 'Buddy Avaya',
            'buddy_shift_date'  => 'Buddy Shift Date',
            'buddy_schedule'    => 'Buddy Schedule'
        ];
    }

    public function messages()
    {
        return [
            'required_if'   => ':attribute is required.',
            'required'      => ':attribute is required.',
            'in'            => ':attribute is invalid.'
        ];
    }
}
