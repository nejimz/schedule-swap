<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'username' => 'required|unique:users,username,' . $this->user . ',id',
            'first_name' => 'required',
            'last_name' => 'required',
            'role' => 'required',
            'center_id' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'username'          => 'Username',
            'first_name'        => 'First Name',
            'last_name'         => 'Last Name',
            'role'              => 'Role',
            'center_id'         => 'Center'
        ];
    }

    public function messages()
    {
        return [
            'unique'    => ':attribute is already exists.',
            'required'  => ':attribute is required.',
            'in'        => ':attribute is invalid.'
        ];
    }
}
