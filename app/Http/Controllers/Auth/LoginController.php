<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\Center;
use App\ActivityLog;
use ActivityLogger;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/portal';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if(!$request->has('policy')) {
            return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([
                'policy' => 'You must accept the terms and conditions in order to proceed.',
            ]);
        }

        $user = \App\User::withTrashed()->whereUsername($request->username)->first();

        if(!is_null($user))
        {
            if($user->trashed())
            {
                return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors([
                    $this->username() => 'Your account has not been deactivated.',
                ]);
            }
        }

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
    
    public function username()
    {
        return 'username';
    }

    public function index()
    {
        Auth::logout();
        abort(404);
    }

    public function logout(Request $request)
    {
        $center_id = $request->user()->center_id;
        $row = Center::whereId($center_id)->first();
        $center = strtolower($row->code);

        ActivityLogger::store($request->segment(1), 'Logout');

        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/tf/' . $center);
    }

    public function showLoginForm($center)
    {
        $data = compact('center');

        return view('auth.login', $data);
    }

    public function showLoginForm_main()
    {
        $data = compact('center');

        return view('auth.login', $data);
    }

    protected function authenticated(Request $request, $user)
    {
        $login_abbr = strtolower($request->center);
        $abbr = strtolower($user->center->code);

        if($login_abbr != $abbr)
        {
            Auth::logout();
            return redirect()->route('center.login', $abbr)->withErrors(['username'=>'These credentials do not match to this center.']);
        }

        ActivityLogger::store($request->segment(1), 'Login');
    }
}
