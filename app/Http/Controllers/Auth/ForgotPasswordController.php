<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Validation\Rule;
use Auth;
use Crypt;
use Illuminate\Support\Arr;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function security()
    {
        return view('auth.passwords.security-request');
    }

    public function submitUsername(Request $request)
    {
        $request->validate([
            'username' => [
                'required',
                'exists:users,username',
            ]
        ]);

        $user = \App\User::with('questions')->where('username', $request->username)->orderBy('created_at', 'DESC')->first();

        $data = ['q' => Crypt::encryptString($user)];
        return redirect()->route('password.security.question', $data);
    }

    public function request($q)
    {
        $user = Crypt::decryptString($q);
       
        $user = (object) json_decode($user, TRUE);

        $data = compact(['user']);
        
        return view('auth.passwords.security-reset', $data);
    }

    public function submitSecurity(Request $request)
    {
        $answer = $request->answer;

        $user = Crypt::decryptString($request->user);

        $decodeUser = json_decode($user, true);

        $model = new \App\User();

        $model->fill($decodeUser);

        $request->validate([
            'question' => 'required',
            'answer' => [ 
                'required', 
                Rule::exists('security_questions')->where(function ($query) use($answer, $decodeUser){
                    $query->where('answer', $answer)->where('user_id', $decodeUser["id"]);
                }), 
            ]
        ]);


        $token = app('auth.password.broker')->createToken($model);

        return redirect()->route('password.reset', ['token' => $token, 'u' => $request->user]);
    }
    
     /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->sendResetLink(
            $request->only('email')
        );
        if(is_null($response))
        {
            $response = "Reset password link has been sent to your email. If you don't see it in your inbox within a few minutes, try looking in your spam or junk folder.";
        }
        return back()->with('status', trans($response));
    }

    /**
     * Send a password reset link to a user.
     *
     * @param  array  $credentials
     * @return string
     */
    public function sendResetLink(array $credentials)
    {
        // First we will check to see if we found a user at the given credentials and
        // if we did not we will redirect back to this current URI with a piece of
        // "flash" data in the session to indicate to the developers the errors.
        $credentials = ['email' => $credentials['email']];
        $user = $this->getUser($credentials);

        if (is_null($user)) {
            return 'Email is invalid!';
        }

        // Once we have the reset token, we are ready to send the message out to this
        // user with a link to reset their password. We will then redirect back to
        // the current URI having nothing set in the session to indicate errors.
        app('App\Http\Controllers\MailController')->sendResetPasswordMail($user);
    }

    /**
     * Get the user for the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\CanResetPassword|null
     *
     * @throws \UnexpectedValueException
     */
    public function getUser(array $credentials)
    {
        $credentials = Arr::except($credentials, ['token']);
        $user = \App\User::where('email', $credentials['email'])->first();
        return $user;
    }
}
