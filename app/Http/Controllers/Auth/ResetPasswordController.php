<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Crypt;
use DB;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/portal';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

     /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token' => 'required',
            'password' => 'required|confirmed|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[@*&`~!$#%]).*$/',
        ];
    }

    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [
            'regex' => 'Password must contain: </br>
                        a minimum of 1 lower case letter [a-z] and </br>
                        a minimum of 1 upper case letter [A-Z] and </br>
                        a minimum of 1 numeric character [0-9] and </br>
                        a minimum of 1 special character: @!$#%*'
        ];
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $token = null)
    {
        if(isset($request->u))
        {
            $user = (object) json_decode(Crypt::decryptString($request->u), TRUE);
            $request->email = $user->email;
            $passreset = DB::table('password_resets')
                        ->where('email','=',$user->email)
                        ->where('created_at','>', Carbon::now()->subHour())
                        ->first();
            if(is_null($passreset))
            {
                abort('498', 'Password link has already expired.');
                // return response("Link has expired", 498);
            }
        }
        else
        {
            abort('404');
        }
        
        return view('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
}
