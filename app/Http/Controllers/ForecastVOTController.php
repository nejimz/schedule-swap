<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Center;
use App\ForecastVOT;
use App\ForecastVOTRequest;
use App\ForecastVOTRequestItem;
use App\SwapAgentInformation;
use App\ActivityLog;
use \Carbon\Carbon;
use DB;
use Auth;
use Validation;
use Schedule;
use VOT;
use VTO;
use ActivityLogger;

class ForecastVOTController extends Controller
{
	public function index()
	{
		return view('vot.index');
	}

	public function overtime_allowed_hours($shift_date, $schedule, $maximum_item_per_day)
	{
		$schedule_arr = explode('-', $schedule);
		$shift_start = Carbon::parse($shift_date . ' ' . $schedule_arr[0]);
		$shift_end = Carbon::parse($shift_date . ' ' . $schedule_arr[1]);
		#return $shift_end->toTimeString();
		if ($shift_end->toTimeString() == '00:00:00') {
			$shift_end->addDay();
		}

		$hours = $shift_end->diffInHours($shift_start);
		#return $hours;
		return $maximum_item_per_day - ($hours * 2);
	}

	public function shift_gap($items, $shift_start_schedule, $shift_end_schedule, $shift_date)
	{
			$mod_items = ForecastVOT::whereIn('id', $items)->orderBy('time_start', 'ASC')->get();
			#dump($mod_items->toArray());
			$shift_start_schedule_slice = explode('-', $shift_start_schedule);
			$shift_end_schedule_slice = explode('-', $shift_end_schedule);

			$shift_time_start = Carbon::parse($shift_date . ' ' . $shift_start_schedule_slice[0]);
			$shift_time_end = Carbon::parse($shift_date . ' ' . $shift_end_schedule_slice[1]);
			$pre = 0;
			$pre_shift_ot_gap = 0;
			$pre_gap_counter = 0;
			$pre_last_shift_time_end = null;
			$post = 0;
			$post_shift_ot_gap = 0;
			$post_gap_counter = 0;
			$post_last_shift_time_end = Carbon::parse($shift_date . ' ' . $shift_end_schedule_slice[1]);
			foreach ($mod_items as $row) {
				$forecast_time_start = Carbon::parse($row->shift_date . ' ' . $row->time_start);
				$forecast_time_end = Carbon::parse($row->shift_date . ' ' . $row->time_end);
				if($shift_time_start->gt($forecast_time_start)) {
					#dump($forecast_time_start);
					// Pre Shift
					$pre++;
					if ($pre_gap_counter != 0) {
						$interval = $pre_last_shift_time_end->diffInMinutes($forecast_time_start);
						if ($interval > 0) {
							$pre_shift_ot_gap++;
						}
					} 
					$pre_last_shift_time_end = Carbon::parse($row->shift_date . ' ' . $row->time_end);
					$pre_gap_counter++;
				} else {
					#dump($forecast_time_start);
					// Post Shift
					$post++;
					$interval = $post_last_shift_time_end->diffInMinutes($forecast_time_start);
					// Check Interval and counter (skip automatically the first loop)
					if ($interval > 0 && $post_gap_counter != 0) {
						$post_shift_ot_gap++;
					}
					$post_last_shift_time_end = Carbon::parse($row->shift_date . ' ' . $row->time_end);
					$post_gap_counter++;
				}
			}

			return ['only_even_number_pre' => ($pre%2), 'pre_gaps' => $pre_shift_ot_gap, 
					'only_even_number_post' => ($post%2), 'post_gaps' => $post_shift_ot_gap];
	}

	public function get_schedule_list($shift_date)
	{
		$agent_schedule = \App\SwapAgentSchedule::whereUserId(auth()->user()->id)->whereShiftDate($shift_date)->first()->schedule;

		$explode_schedule = explode("-", $agent_schedule);

		if(count($explode_schedule) < 2) {
			$schedule_list[0] = "";
			$schedule_list[1] = "";
		} else {
			$schedule_list[0] = Carbon::parse(date('Y-m-d H:i:s',strtotime($shift_date.' '.trim($explode_schedule[0]))));
			$schedule_list[1] = Carbon::parse(date('Y-m-d H:i:s',strtotime($shift_date.' '.trim($explode_schedule[1]))));

			if($schedule_list[0]->format('i') < 30 && $schedule_list[0]->format('i') > 0) {
				$schedule_list[0] = $schedule_list[0]->setTime($schedule_list[0]->format('H'), 00, 00, 000)->format("h:i A") . " - " . $schedule_list[0]->addMinutes(30)->format("h:i A");
				$schedule_list[1] = $schedule_list[1]->setTime($schedule_list[1]->format('H'), 00, 00, 000)->format("h:i A") . " - " . $schedule_list[1]->addMinutes(30)->format("h:i A");
			} elseif($schedule_list[0]->format('i') > 30) {
				$schedule_list[0] = $schedule_list[0]->setTime($schedule_list[0]->format('H'), 30, 00, 000)->format("h:i A") . " - " . $schedule_list[0]->addMinutes(30)->format("h:i A");
				$schedule_list[1] = $schedule_list[1]->setTime($schedule_list[1]->format('H'), 30, 00, 000)->format("h:i A") . " - " . $schedule_list[1]->addMinutes(30)->format("h:i A");
			} else {
				$schedule_list[0] = $schedule_list[0]->format("h:i A") . " - " . $schedule_list[0]->addMinutes(30)->format("h:i A");
				$schedule_list[1] = $schedule_list[1]->format("h:i A") . " - " . $schedule_list[1]->addMinutes(30)->format("h:i A");
			}
		}
	}

	public function get_schedule_start_end($shift_date, $shift_schedule)
	{
		#$shift_schedule = '05:45 PM - 01:45 AM';
		$shift_schedule_arr = explode('-', $shift_schedule);
		$shift_start_str = trim($shift_schedule_arr[0]);
		$shift_end_str = trim($shift_schedule_arr[1]);

		$shift_start = Carbon::parse($shift_date . ' ' . $shift_start_str);
		$shift_end = Carbon::parse($shift_date . ' ' . $shift_end_str);

		if (in_array($shift_end->hour, [0, 1, 2, 3, 4])) {
			$shift_end->addDay();
		}

		$start = '';
		$end = '';
		$start_minute = $shift_start->minute;
		$end_minute = $shift_end->minute;

		if ($start_minute >= 0 && $start_minute < 30) {
			$start = $shift_start->format('h:00 A') . ' - ' . $shift_start->format('h:30 A');
			$end = $shift_end->subMinutes(30)->format('h:i A') . ' - ' . $shift_end->addMinutes(30)->format('h:i A');
		} elseif ($start_minute >= 30 && $start_minute < 59) {
			$start = $shift_start->format('h:30 A') . ' - ' . $shift_start->addHour()->format('h:00 A');
			$end = $shift_end->format('h:30 A') . ' - ' . $shift_end->addHour()->format('h:00 A');
		}

		return [$start, $end];
	}

	public function store(Request $request)
	{
		$maximum_hrs_per_day = 12;
		$maximum_item_per_day = $maximum_hrs_per_day * 2;
		$items = ($request->has('items')) ? $request->items : [];
		$prev_items = ($request->has('prev_items'))? $request->prev_items : [];
		$shift_date = $request->vot_shift_date;
		$shift_schedule = $request->vot_schedule;

		/*if($request->has('schedule_list')) {
			$schedule_list = $this->get_schedule_list($shift_date);
			$shift_start_schedule = $schedule_list[0];
			$shift_end_schedule = $schedule_list[1];
		} else {
			$schedule_list = $request->schedule_list;
			$shift_start_schedule = $schedule_list[0];
			$shift_end_schedule = $schedule_list[count($schedule_list)-1];
		}*/

		#$schedule_list = $this->get_schedule_start_end($shift_date, $shift_schedule);
		#$shift_start_schedule = $schedule_list[0];
		#$shift_end_schedule = $schedule_list[1];

		#dd($request->all());

		if ($request->has('items')) {
			$clean_items = VOT::clean_items($prev_items, $items);
			if ($request->has('expr_items')) {
				$expr_items = $request->expr_items;
				$items = VOT::clean_items($expr_items, $items);
				$clean_items = VOT::clean_items($prev_items, $items);
				$request->request->add(['items' => $items]);
			}
			$request->request->add(['minimum' => 0]);
		} else {		
            $json['errors']['buddy_schedule'][0] = 'No slots has been selected.';
			return response()->json((object) $json, 422);
		}

		if($shift_schedule == 'Off') {
			$skip = VOT::check_skip($items);
			$only_even_number = count($clean_items)%2;
			$request->request->add(['skip' => $skip, 'only_even_number' => $only_even_number]);
			$request->validate([
				'confirmation' => 'accepted',
				'items' => 'required|array|min:2|max:' . $maximum_item_per_day,
				'skip' => 'max:0',
				'only_even_number' => 'lte:minimum'
			],[
				'confirmation' => 'Confirmation',
				'items.array' => 'Please select a Schedule.',
				'items.required' => 'Please select a Schedule.',
				'items.min' => '1 hour is required.',
				'items.max' => 'You are not allowed to take more than 12 hours of overtime.',
				'skip.max' => 'Gaps on Rest Day are not allowed! Please plot your overtime hours consecutively',
				'only_even_number.lte' => 'Please plot in an hourly interval.'
			]);
		} else {

			$schedule_list = $this->get_schedule_start_end($shift_date, $shift_schedule);
			$shift_start_schedule = $schedule_list[0];
			$shift_end_schedule = $schedule_list[1];

			$shift_gap = $this->shift_gap($items, $shift_start_schedule, $shift_end_schedule, $shift_date);
			$overtime_allowed_hours = $this->overtime_allowed_hours($shift_date, $shift_schedule, $maximum_item_per_day);
			$request->request->add($shift_gap);
			#dump($shift_gap);
			#dd($overtime_allowed_hours);
			$request->validate([
				'confirmation' => 'accepted',
				'items' => 'required|array|min:2|max:' . $overtime_allowed_hours,
				'only_even_number_pre' => 'lte:minimum',
				'only_even_number_post' => 'lte:minimum',
				'pre_gaps' => 'lte:minimum',
				'post_gaps' => 'lte:minimum'
			],[
				'confirmation' => 'Confirmation',
				'items.array' => 'Please select a Schedule.',
				'items.required' => 'Please select a Schedule.',
				'items.min' => '1 hour is required.',
				'items.max' => ' You are only allowed ' . ($overtime_allowed_hours/2) . ' hours of overtime and not allowed to take more than 12-hour of staff hours per shift.',
				'only_even_number_pre.lte' => 'Please plot in an hourly interval on Pre-Shift.',
				'only_even_number_post.lte' => 'Please plot in an hourly interval on Post-Shift.',
				'pre_gaps.lte' => 'Only one gap is allowed on Pre-Shift.',
				'post_gaps.lte' => 'Only one gap is allowed on Post-Shift.'

			]);
		}
		$user = Auth::user();
		#$date = $request->vot_shift_date;
		
		if(count($clean_items) < 1) {
            $json['errors']['buddy_schedule'][0] = 'Please select a Schedule.';
			return response()->json((object) $json, 422);
		}
		
		$exists = VTO::exists($shift_date, $user->id);
		
		if($exists) {
            $json['errors']['buddy_schedule'][0] = 'Cannot avail Overtime. You`ve already applied an Time-Off.';
			return response()->json((object) $json, 422);
		}
		
		$json = [];
		$now = Carbon::now();

		foreach($clean_items as $item) {

			$vot = ForecastVOT::whereId($item)->first();
			$expired_at = Carbon::parse($vot->expired_at);
			$vot_item = ForecastVOTRequestItem::leftJoin('forecast_vot_request', 'forecast_vot_request_item.forecast_vot_request_id', '=', 'forecast_vot_request.id')->where('user_id', $user->id)->whereForecastVotId($item)->count();

			if($vot_item > 0) {
                $json['errors']['buddy_schedule'][0] = 'You`ve already applied for ' . $vot->schedule . '.';
				return response()->json((object) $json, 422);
			} elseif($now->gte($expired_at)) {
                $json['errors']['buddy_schedule'][0] = $vot->schedule . ' already expired.';
				return response()->json((object) $json, 422);
			} elseif($vot->item_exists == 0 && $vot->remaining_slots == 0) {
                $json['errors']['buddy_schedule'][0] = 'No slot/s available for ' . $vot->schedule . '.';
				return response()->json((object) $json, 422);
			}
		}
		#dd($request->all());

		$user = Auth::user();
		$row = ForecastVOTRequest::create([ 'user_id'=>$user->id ]);

		foreach ($request->items as $item) {
			$vot = ForecastVOT::whereId($item)->first();
			if($vot->item_exists == 0) {
				ForecastVOTRequestItem::create([
					'forecast_vot_request_id'=>$row->id,
					'forecast_vot_id'=>$item
				]);
			}
		}

        $action = 'VOT Request (' . $row->id . ') - Shift Swap';
        ActivityLogger::store($request->segment(1), $action);

        return response()->json(['success'=>'Voluntary Overtime successfully plotted!', 'shift_date'=>$shift_date, 'schedule'=>$shift_schedule]);
	}

	/*

	*/

	public function list($date, $schedule)
	{
		$list = [];
		$user = Auth::user();
		$user_id = $user->id;
		$now = Carbon::now();
		$information = SwapAgentInformation::whereUserId($user->id)->first();

		$schedules = [];

		if(!in_array(strtolower($schedule), ['leave', 'mloa', 'vl', 'suspension'])) {
			if($schedule != 'Off') {
				$schedule = explode('-', $schedule);
				$start = Carbon::parse($date . ' ' . trim($schedule[0]));
				$end = Carbon::parse($date . ' ' . trim($schedule[1]));

				#if($end->toTimeString() == '00:00:00')
				if(in_array($end->hour, [0, 1, 2, 3, 4])) {
					$end->addDay();
				}

				/*$total_hours = $end->diffInHours($start);
				$schedules[0] = [$start->format('Y-m-d H:i'), $start->addHour()->format('Y-m-d H:i')];

				for ($i = 1; $i < $total_hours; $i++)
				{
					$schedules[$i] = [$start->format('Y-m-d H:i'), $start->addHour()->format('Y-m-d H:i')];
				}*/
			} else {
				$start = Carbon::parse($date . ' 00:00:00');
				$end = Carbon::parse($date . ' 23:59:59');
			}

			$rows = ForecastVOT::whereIn('center_id', [$user->center_id, 9])->whereShiftDate($date)
					->where(function($query) use ($information){
						$query->whereOrganization($information->skill_clean);
						if ($information->is_tier_3) {
							// $skill = strtoupper($information->skill);
				            // if(strpos( $skill, "ERD" )) {
				            //   $skill = str_replace("ERD ".auth()->user()->center->code, "SUPERD", $skill);
				            // }else{
				            //   $skill = str_replace("SUP ".auth()->user()->center->code, "SUPERD", $skill);
							// }
							$skill = $information->skill_clean;
							$skill = $skill = str_replace(" ERD", "", strtoupper($skill)); // Remove ERD;
							$skill = $skill = str_replace(" SUP", "", strtoupper($skill)); // Remove SUP;
				            
							$query = $query->orWhere('organization', 'LIKE', '%'.$skill.'%')
											->where('organization', 'LIKE' , '%SUPERD%');
						}
					})->orderBy('time_start', 'ASC')->get();
			
			$vot_items = ForecastVOTRequestItem::whereHas('forecast', function($query) use ($date){
	                    $query->where('shift_date', $date);
	                })->whereHas('request', function($query) use ($user_id){
	                    $query->where('user_id', $user_id);
	                })->get(['forecast_vot_id'])->toArray();
			$vot_items = array_flatten($vot_items);
			$n = 0;
			#dump($schedules);dd($rows->toArray());
			#dump($start);dump($end);dd($rows->toArray());
			foreach ($rows as $row) {
				$status = 0;
				$start_time = Carbon::parse($row->shift_date . ' ' . $row->time_start);
				$end_time = Carbon::parse($row->shift_date . ' ' . $row->time_end);
				$expired_at = Carbon::parse($row->expired_at);
				if($end_time->format('H:i') == '00:00') {
					$end_time->addDay();
				}

				if(in_array($row->id, $vot_items)) {
					// on schedule
					$status = 2;
				} elseif($schedule == 'Off') {
					$status = 0;
				} else {
					// not on shift
					if($start->gte($start_time) && $start->lt($end_time)) {
						$status = 1;
					} elseif(($start_time->gte($start) && $start_time->lt($end)) && ($end_time->gte($start) && $end_time->lt($end))) {
						$status = 1;
					} elseif($end->gt($start_time) && $end->lte($end_time)) {
						$status = 1;
					}

				}

				if (strpos($row->organization, 'SUPERD') !== false) {
					if(!VOT::check_superd($row->organization, $information->skill)) {
						$status = 1;
					}
				}
				
				if($now->gte($expired_at)) {
					$status = 3;
				}

				$list[$n] = [
					'id' => $row->id,
					'status' => $status,
					'schedule' => $row->schedule,
					'slots' => $row->remaining_slots
				];
				$n++;
			}
		}
		#dump($schedules);dd($list);

        return response()->json($list);
	}

	public function calendar(Request $request)
	{
		$calendar_dates = '';
		$date = Carbon::parse($request->calendar_month);
		$tempDate = Carbon::createFromDate($date->year, $date->month, 1);
		$skip = $tempDate->dayOfWeek;

		$user = Auth::user();
		$information = SwapAgentInformation::whereUserId($user->id)->first();
		#dd($information->skill_clean);
		for($i = 0; $i < $skip; $i++) 
		{
			$tempDate->subDay();
		}
		
		do 
		{
			for($i = 0; $i < 7; $i++) 
			{
				$onclick = '';
				$highlight_active_temp = '';
				$highlight_active = '';
				$title = '';
				#$schedule = $this->schedule($user->id, $tempDate->toDateString());
				$schedule = Schedule::schedule_shift_date($user->id, $tempDate->toDateString());
				#dump($schedule);
				if(count($schedule) > 0) {
					$shift_date = ($schedule['new_shift_date'] == '')? $schedule['shift_date'] : $schedule['new_shift_date'];
					$shift_schedule = ($schedule['new_schedule'] == '')? $schedule['schedule'] : $schedule['new_schedule'];

					$vots = ForecastVOT::whereIn('center_id', [$user->center_id, 9])->whereShiftDate($tempDate->toDateString())
							->whereOrganization($information->skill_clean)->orderBy('time_start', 'ASC')->first();

					if($vots) {
						$highlight_active = 'is-active';
						$title = 'Voluntary Overtime Slot';
						$onclick = "onclick=\"vot_request('$shift_date', '$shift_schedule')\"";
					} elseif ($information->is_tier_3) {
						$skill = strtoupper($information->skill);

			            // if(strpos( $skill, "ERD" )) {
			            //   $skill = str_replace("ERD ".auth()->user()->center->code, "SUPERD", $skill);
			            // }else{
			            //   $skill = str_replace("SUP ".auth()->user()->center->code, "SUPERD", $skill);
						// }
						
						$skill = $information->skill_clean;
						$skill = $skill = str_replace(" ERD", "", strtoupper($skill)); // Remove ERD;
						$skill = $skill = str_replace(" SUP", "", strtoupper($skill)); // Remove SUP;

			            $superd = ForecastVOT::whereIn('center_id', [$user->center_id, 9])
								->whereShiftDate($tempDate->toDateString())
								->where('organization', 'LIKE', '%SUPERD%')
								->where('organization', 'LIKE', '%'.$skill.'%')
								->orderBy('time_start', 'ASC')->first();

						// $superd = ForecastVOT::whereIn('center_id', [$user->center_id, 9])->whereShiftDate($tempDate->toDateString())
						// 		->where('organization', 'LIKE', '%SUPERD%')
						// 		->where(function($query) {
						// 			$query->where('organization', 'LIKE', '%ERD%')
						// 				  ->orWhere('organization', 'LIKE', '%SUP%');
						// 		})->orderBy('time_start', 'ASC')->first();
						// dump($superd);
						if ($superd) {	
							if(VOT::check_superd($superd->organization, $information->skill))
							{
								$highlight_active = 'is-active';
								$title = 'Voluntary Overtime Slot';
								$onclick = "onclick=\"vot_request('$shift_date', '$shift_schedule')\"";
							}
						}
					}

				}

				if($tempDate->month == $date->month) {
					$calendar_dates .= '<div class="calendar-date"><button class="date-item ' . $highlight_active . '" title="' . $title . '" ' . $onclick . '>'. $tempDate->day . '</button></div>';
				} else {
					$calendar_dates .= '<div class="calendar-date is-disabled"><button class="date-item ' . $highlight_active . '">' . $tempDate->day . '</button></div>';
				}
				
				$tempDate->addDay();
			}
		}
		while($tempDate->month == $date->month);
		return $calendar_dates;
	}
}
