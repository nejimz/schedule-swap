<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Center;
use App\ForecastVOTRequestItem;
use App\ForecastVTORequestItem;
use App\SwapAgentInformation;
use App\SwapType;
use App\UserAdminCenter;
use \Carbon\Carbon;
use DB;
use Auth;
use Validation;
use Response;
use ActivityLogger;

class ForecastReportsEnterpriseController extends Controller
{
    public function index(Request $request)
    {
        $user_id = Auth::user()->id;
        $now = Carbon::now();
        $date_now = $now->toDateString();
        $skills = SwapAgentInformation::groupBy('skill')->orderBy('skill', 'ASC')->get(['skill']);
        $swap_types = SwapType::orderBy('id', 'ASC')->get();
        $centers = Center::orderBy('name', 'ASC')->get();
        $assigned_centers = UserAdminCenter::whereUserId($user_id)->get(['center_id'])->toArray();
        if (count($assigned_centers) > 0) {
        	$centers = Center::whereIn('id', $assigned_centers)->orderBy('name', 'ASC')->get();
        }

        $data = compact('date_now', 'skills', 'tiers',  'centers');

        return view('vforecast.reports.enterprise', $data);
    }

	public function show(Request $request)
	{
		$input = $request->only('type', 'date_confirmed');
		$request->validate([ 
            'type' => 'required', 
            #'center_id' => 'required',
            #'organization' => 'required', 
            'shift_date' => 'required'
        ]);
        
		$user = Auth::user();
        $type = $request->type;
		$center_id = ($request->center_id == 0)? 9 : $request->center_id;
        $organization = $request->organization;
		$date_confirmed = Carbon::parse($request->shift_date)->toDateString();
        $now = Carbon::now();
		$view = '';
		$file = '';
		$type_id = '';

		if($type == 'ot')
		{
			$type_id = 'forecast_vot_id';
			$file = 'Voluntary Overtime';
			$view = 'vforecast.reports.voluntary-overtime';
			$query = ForecastVOTRequestItem::with(['forecast', 'request']);
			/*$rows = ForecastVOTRequestItem::with(['forecast', 'request'])
					->whereHas('forecast', function($query) use ($center_id, $organization){
                        $query->where('center_id', $center_id);
						if($organization != '')
						{
                        	$query->where('organization', $organization);
						}
					})
					->whereHas('request', function($query) use ($date_confirmed){
						$query->whereRaw("DATE(created_at) = '$date_confirmed'");
					})->orderBy('forecast_vot_id', 'ASC')->get();*/
		}
		elseif($type == 'to')
		{
			$type_id = 'forecast_vto_id';
			$file = 'Voluntary Timeoff';
			$view = 'vforecast.reports.voluntary-timeoff';
			$query = ForecastVTORequestItem::with(['forecast', 'request']);
			/*$rows = ForecastVTORequestItem::with(['forecast', 'request'])
					->whereHas('forecast', function($query) use ($center_id, $organization){
                        $query->where('center_id', $center_id);
						if($organization != '')
						{
                        	$query->where('organization', $organization);
						}
					})
					->whereHas('request', function($query) use ($date_confirmed){
						$query->whereRaw("DATE(created_at) = '$date_confirmed'");
					})->orderBy('forecast_vto_id', 'ASC')->get();*/
		}

		if($organization != '' || !is_null($organization))
		{
			$query->whereHas('forecast', function($query) use ($organization){
	            $query->where('organization', $organization);
			});
		}
		
		if($center_id != 9)
		{
			$query->whereHas('request.user', function($query) use ($center_id){
                $query->where('center_id', $center_id);
			});
		}

		$query->whereHas('forecast', function($query) use ($organization){
            $query->whereNotNull('organization');
		});

		$rows = $query->whereHas('request', function($query) use ($date_confirmed){
			$query->whereRaw("DATE(created_at) = '$date_confirmed'");
		})->orderBy($type_id, 'ASC')->get();

		# dd($rows);

		$data = compact('rows', 'file');

        $action = 'Enterprise Download V' . strtoupper($type) . ' for ' . $date_confirmed;
        ActivityLogger::store($request->segment(1), $action);

    	return response()->view($view, $data, 200)
    			->header('Content-Disposition', 'attachment; filename="'. $file .' ('. $now->toDateTimeString() .').xls"')
    			->header('Content-Type', 'application/vnd.ms-excel');
	}
}
