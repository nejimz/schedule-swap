<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SwapAgentInformation;
use App\ForecastVOT;
use App\ForecastVTO;
use App\Center;
use App\UserAdminCenter;
use \Carbon\Carbon;
use DB;
use Auth;

class ForecastController extends Controller
{

   public function auth()
   {
      $user = Auth::user();
      if(!$user->isAdmin())
      {
         abort(404);
      }
   }

   public function index()
   {
      $this->auth();
      return view('vforecast.index');
   }

   public function create()
   {
      $this->auth();
      $route = route('forecast.store');
      $user = Auth::user();
      $centers = [];
      $enterprise = ($user->center_id == 9 || $user->isRoot());
      #$enterprise = ($user->center_id == 9);

      if($enterprise)
      {
         $centers = Center::where('id', '<>', '9')->orderBy('name', 'ASC')->get();
         $assigned_centers = UserAdminCenter::whereUserId($user->id)->get(['center_id'])->toArray();
         if (count($assigned_centers) > 0) {
            $centers = Center::whereIn('id', $assigned_centers)->orderBy('name', 'ASC')->get();
         }
      }

      $hours = $this->timeList();
      #dd($hours);
      $data = compact('route', 'user',  'centers', 'enterprise', 'hours');

      return view('vforecast.create', $data);
   }

   public function timeList()
   {
      $n = 0;
      $hours = [];
      for ($hour = 0; $hour < 24; $hour++) { 
         $start = Carbon::now();
         $end = Carbon::now();
         $start->hour = $hour;
         $start->minute = 0;
         $start->second = 0;
         $end->hour = $hour;
         $end->minute = 0;
         $end->second = 0;
         $end->addMinutes(30);

         $hours[$n] = $start->format('H:i') . ' - ' . $end->format('H:i');
         $n++;
         $start->addMinutes(30);
         $end->addMinutes(30);
         $hours[$n] = $start->format('H:i') . ' - ' . $end->format('H:i');
         $n++;
      }
      /*for ($hour = 0; $hour < 24; $hour++) {
         $hours[] = date('H:i', strtotime($hour . ':00')) . ' - ' . date('H:i', strtotime(($hour+1) . ':00'));
      }*/

      return $hours;
   }

   public function store(Request $request)
   {
      $this->auth();
      $request->validate([
         #'center_id' => 'required',
         'type' => 'required',
         'organization' => 'required',
         #'shift_date' => 'required|after:today',
         'slot' => 'required|numeric',
         'timeType' => 'required',
         'wp' => 'required_if:timeType,wp',
         'hours' => 'required_if:timeType,ti'
      ]);

      $timeType = $request->timeType;
      $center_id = ($request->center_id == 0)? 9 : $request->center_id;
      $organization = $request->organization;
      $shift_date = $request->shift_date;
      $slot = $request->slot;
      $type = $request->type;
      $user = Auth::user();

      if($timeType == 'wp')
      {
         $forecast = $this->wp_forecast($center_id, $organization, $shift_date, $slot, $user, $type, $request->wp);
      }
      else
      {
         $forecast = $this->it_forecast($center_id, $organization, $shift_date, $slot, $user, $type, $request->hours);
         #dd($forecast);
      }

      $message_type = '';

      if($request->type == 'ot')
      {
         $message_type = 'Overtime';
         ForecastVOT::insert($forecast);
      }
      else
      {
         $message_type = 'Time-off';
         ForecastVTO::insert($forecast);
      }

      return redirect()->route('forecast.create')->with('success', $message_type . ' details successfully forcasted!');
   }

   public function it_forecast($center_id, $organization, $shift_date, $slot, $user, $type, $hours)
   {
      $number_of_hrs_expired_at = 0;
      $number_of_hrs = count($hours);
      $forecast = [];
      $datetime = Carbon::now();
      foreach ($hours as $hour)
      {
         $hour_slice = explode(' - ', $hour);
         $start_datetime = Carbon::parse($shift_date . ' ' . $hour_slice[0]);
         $end_datetime = Carbon::parse($shift_date . ' ' . $hour_slice[1]);

         $expired_at = Carbon::parse($shift_date . ' ' . $start_datetime->toTimeString());
         $expired_at->subHours($number_of_hrs_expired_at);

         if($type == 'ot')
         {
            $check_exist = ForecastVOT::whereCenterId($center_id)
               ->whereOrganization($organization)
               ->whereShiftDate(date('Y-m-d', strtotime($shift_date)))
               ->whereTimeStart($start_datetime->toTimeString())
               ->whereTimeEnd($end_datetime->toTimeString())
               ->count();
         }
         else 
         {
            $check_exist = ForecastVTO::whereCenterId($center_id)
               ->whereOrganization($organization)
               ->whereShiftDate(date('Y-m-d', strtotime($shift_date)))
               ->whereTimeStart($start_datetime->toTimeString())
               ->whereTimeEnd($end_datetime->toTimeString())
               ->count();
         }

         if($check_exist == 0)
         {
            $forecast[] = [
               'center_id' => $center_id, 
               'organization' => $organization, 
               'slots' => $slot, 
               'shift_date' => date('Y-m-d', strtotime($shift_date)), 
               'time_start' => $start_datetime->toTimeString(), 
               'time_end' => $end_datetime->toTimeString(), 
               'user_id' => $user->id, 
               'expired_at' => $expired_at->toDateTimeString(), 
               'created_at' => $datetime->toDateTimeString(), 
               'updated_at' => $datetime->toDateTimeString()
            ];
         }
         else
         {
            if($type == 'ot')
            {
               $increment = ForecastVOT::whereCenterId($center_id)
               ->whereOrganization($organization)
               ->whereShiftDate(date('Y-m-d', strtotime($shift_date)))
               ->whereTimeStart($start_datetime->toTimeString())
               ->whereTimeEnd($end_datetime->toTimeString())
               ->first();
            }
            else
            {
               $increment = ForecastVTO::whereCenterId($center_id)
               ->whereOrganization($organization)
               ->whereShiftDate(date('Y-m-d', strtotime($shift_date)))
               ->whereTimeStart($start_datetime->toTimeString())
               ->whereTimeEnd($end_datetime->toTimeString())
               ->first();
            }

            $increment->slots = $increment->slots + $slot;
            $increment->save();    
         }
      }

      return $forecast;
   }

   public function wp_forecast($center_id, $organization, $shift_date, $slot, $user, $type, $wp)
   {
      $wp = explode('x', $wp);
      $from_time_wp = explode(' ', trim($wp[0]));
      $to_time_wp = $from_time_wp[1];

      preg_match_all('!\d+!', $from_time_wp[0], $numbers_only);

      // $from_time_substr = substr($from_time_wp[0], 0, 1);
      $from_time_substr = $numbers_only[0][0];
      $get_clock_12hr = substr($from_time_wp[0], -1, 1);
      $clock_12hr = ($get_clock_12hr == 'A')? 'AM' : 'PM';

      $number_of_hours_work = substr($to_time_wp, 0, 1);

      $from_time = date('Y-m-d h:i A', strtotime($from_time_substr . ':00 ' . $clock_12hr));
      $from_time = Carbon::parse($from_time);
      $to_time = Carbon::parse($from_time);
      $to_time->addHours($number_of_hours_work);

      $number_of_hrs = $to_time->diffInHours($from_time);
      // reset
      $start_datetime = Carbon::parse($from_time->toDateTimeString());
      $end_datetime = Carbon::parse($from_time->toDateTimeString());

      $forecast = [];
      $datetime = Carbon::now();
      $number_of_hrs_expired_at = 30;

      for ($i = 1; $i <= $number_of_hrs; $i++)
      {
         $expired_at = Carbon::parse($shift_date . ' ' . $start_datetime->toTimeString());
         $expired_at->subMinutes($number_of_hrs_expired_at);
         
         $end_datetime->addHour();
         if($type == 'ot')
         {
            $check_exist = ForecastVOT::whereCenterId($center_id)
               ->whereOrganization($organization)
               ->whereShiftDate(date('Y-m-d', strtotime($shift_date)))
               ->whereTimeStart($start_datetime->toTimeString())
               ->whereTimeEnd($end_datetime->toTimeString())
               ->count();
         }
         else 
         {
            $check_exist = ForecastVTO::whereCenterId($center_id)
               ->whereOrganization($organization)
               ->whereShiftDate(date('Y-m-d', strtotime($shift_date)))
               ->whereTimeStart($start_datetime->toTimeString())
               ->whereTimeEnd($end_datetime->toTimeString())
               ->count();
         }

         if($check_exist == 0)
         {
            $forecast[] = [
               'center_id' => $center_id, 
               'organization' => $organization, 
               'slots' => $slot, 
               'shift_date' => date('Y-m-d', strtotime($shift_date)), 
               'time_start' => $start_datetime->toTimeString(), 
               'time_end' => $end_datetime->toTimeString(), 
               'user_id' => $user->id, 
               'expired_at' => $expired_at->toDateTimeString(), 
               'created_at' => $datetime->toDateTimeString(), 
               'updated_at' => $datetime->toDateTimeString()
            ];
         }
         else
         {
            if($type == 'ot')
            {
               $increment = ForecastVOT::whereCenterId($center_id)
               ->whereOrganization($organization)
               ->whereShiftDate(date('Y-m-d', strtotime($shift_date)))
               ->whereTimeStart($start_datetime->toTimeString())
               ->whereTimeEnd($end_datetime->toTimeString())
               ->first();
            }
            else
            {
               $increment = ForecastVTO::whereCenterId($center_id)
               ->whereOrganization($organization)
               ->whereShiftDate(date('Y-m-d', strtotime($shift_date)))
               ->whereTimeStart($start_datetime->toTimeString())
               ->whereTimeEnd($end_datetime->toTimeString())
               ->first();
            }

            $increment->slots = $increment->slots + $slot;
            $increment->save();      
         }
         
         $start_datetime->addHour();
      }

      return $forecast;
   }




   public function list($date)
   {
      $list = [];
      $user = Auth::user();
      $user_id = $user->id;
      $now = Carbon::now();

      $vots = ForecastVOT::whereIn('center_id', [$user->center_id, 9])->whereShiftDate($date)->orderBy('time_start', 'ASC')->get();
      $vtos = ForecastVTO::whereIn('center_id', [$user->center_id, 9])->whereShiftDate($date)->orderBy('time_start', 'ASC')->get();
      $n = 0;

      foreach ($vots as $row)
      {
         $start_time = Carbon::parse($date . ' ' . $row->time_start);
         $end_time = Carbon::parse($date . ' ' . $row->time_end);
         $expired_at = Carbon::parse($row->expired_at);

         $list[$n] = [
            'id' => $row->id,
            'schedule' => $start_time->format('h:i A') . ' - ' . $end_time->format('h:i A'),
            'skill' => $row->organization,
            'type' => 'Overtime',
            'slots' => $row->remaining_slots
         ];
         $n++;
      }

      foreach ($vtos as $row)
      {
         $start_time = Carbon::parse($date . ' ' . $row->time_start);
         $end_time = Carbon::parse($date . ' ' . $row->time_end);
         $expired_at = Carbon::parse($row->expired_at);

         $list[$n] = [
            'id' => $row->id,
            'schedule' => $start_time->format('h:i A') . ' - ' . $end_time->format('h:i A'),
            'skill' => $row->organization,
            'type' => 'Time-Off',
            'slots' => $row->remaining_slots
         ];
         $n++;
      }

      return response()->json($list);
   }

   public function calendar(Request $request)
   {
      $calendar_dates = '';
      $date = Carbon::parse($request->calendar_month);
      $tempDate = Carbon::createFromDate($date->year, $date->month, 1);
      $skip = $tempDate->dayOfWeek;

      $user = Auth::user();
      $information = SwapAgentInformation::whereUserId($user->id)->first();
      #dd($information->skill_clean);
      for($i = 0; $i < $skip; $i++)
      {
         $tempDate->subDay();
      }

      do
      {
         for($i = 0; $i < 7; $i++)
         {
            $onclick = '';
            $highlight_active_temp = '';
            $highlight_active = '';
            $title = '';
            #dump($schedule);
            $shift_date = $tempDate->toDateString();

            $vot = ForecastVOT::whereIn('center_id', [$user->center_id, 9])->whereShiftDate($tempDate->toDateString())->count();
            $vto = ForecastVTO::whereIn('center_id', [$user->center_id, 9])->whereShiftDate($tempDate->toDateString())->count();
            #dump($vot);dump($vto);
            if($vot > 0 || $vto > 0)
            {
               $highlight_active = 'is-active';
               $title = 'Forcasted';
               $onclick = "onclick=\"forecasted('$shift_date')\"";
            }

            if($tempDate->month == $date->month)
            {
               $calendar_dates .= '<div class="calendar-date"><button class="date-item ' . $highlight_active . '" title="' . $title . '" ' .  $onclick . '>'. $tempDate->day . '</button></div>';
            }
            else
            {
               $calendar_dates .= '<div class="calendar-date is-disabled""><button class="date-item ' . $highlight_active . '">' . $tempDate ->day . '</button></div>';
            }
            
            $tempDate->addDay();
         }
      }
      while($tempDate->month == $date->month);
      return $calendar_dates;
   }

   public function skills_wps_json($center_id)
   {
      return response()->json([
         'skills' => $this->skills($center_id),
         'wps' => $this->wps($center_id),
      ]);
   }

   public function skills($center_id)
   {
      $n = 0;
      $list = [];
      if($center_id == 0) {
         $rows = SwapAgentInformation::groupBy('skill')->orderBy('skill', 'ASC')->get(['skill']);
      } else {
         $rows = SwapAgentInformation::whereCenterId($center_id)->groupBy('skill')->orderBy('skill', 'ASC')->get(['skill']);
      }

      foreach($rows as $row) {
         $skill = $row->skill_clean;
         if(!in_array($skill, $list)) {
            if ($row->is_erd) {
               $list[$n] = str_replace('ERD', 'SUPERD', $skill);
               $n++;
            }
            $list[$n] = $skill;
            $n++;
         }
      }
      return $list;
   }

   public function wps($center_id)
   {
      $n = 0;
      $list = [];

      if($center_id == 0) {
         $rows = SwapAgentInformation::groupBy('wp')->orderBy('wp', 'ASC')->get(['wp']);
      } else {
         $rows = SwapAgentInformation::whereCenterId($center_id)->groupBy('wp')->orderBy('wp', 'ASC')->get(['wp']);
      }
      foreach($rows as $row) {
         $wp = $row->wp;
         if(!in_array($wp, $list)) {
            $list[$n] = $wp;
            $n++;
         }
      }
      return $list;
   }
}
