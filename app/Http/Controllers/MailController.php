<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Mail\SystemWelcome;
use Crypt;

class MailController extends Controller
{
    public function sendWelcomeMail()
    {
    	Mail::to(auth()->user()->email)->send(new SystemWelcome());
    	return redirect()->route('home');
    }

    public function sendResetPasswordMail($user)
    {
        $u = Crypt::encryptString($user);
        $token = app('auth.password.broker')->createToken($user);
        $url = route('password.reset', ['token' => $token, 'u' => $u]);
    	Mail::to($user->email)->send(new \App\Mail\PasswordReset($user, $url));
    }
}
