<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ForecastVOT;
use App\ForecastVOTRequest;
use App\ForecastVOTRequestItem;
use App\ForecastVTO;
use App\ForecastVTORequest;
use App\ForecastVTORequestItem;
use App\Center;
use App\MailRecipients;
use \Carbon\Carbon;
use DB;
use Auth;
use Mail;

class ForecastCronJobController extends Controller
{
	public function vot_email()
	{
        $type = 'vot';
		//$user = Auth::user();
		$start = Carbon::now();
		$end = Carbon::now();
		#$start->hour = 6;$end->hour = 6;
		$start->minute = 0;
		$start->second = 0;
		$end->minute = 0;
		$end->second = 0;
		// subtract hours
		//$start->subHour();
		//$end->subMinutes(1);
		$end->minute = 59;
		#dump($start);dd($end);
		$subject = 'Voluntary Overtime';
		$centers = Center::get();
		foreach ($centers as $center)
		{
			$center_id = $center->id;
        	$recipients = MailRecipients::whereCenterId($center_id)->whereModule($type)->get();

        	if(count($recipients) > 0)
        	{
				$rows = ForecastVOTRequestItem::with(['forecast', 'request'])
						->whereHas('forecast', function($query) use ($center_id){
							$query->where('center_id', $center_id);
						})
						->whereHas('request', function($query) use ($start, $end){
							$query->where('created_at', '>=', $start)->where('created_at', '<=', $end);
						})->get();
				$this->mail($subject, $type, $rows, $center_id, $start, $end, $recipients);
        	}
		}
	}
	
	public function vto_email()
	{
        $type = 'vto';
		//$user = Auth::user();
		$start = Carbon::now();
		$end = Carbon::now();
		#$start->day = 5;$end->day = 5;
		#$start->hour = 17;$end->hour = 17;
		$start->minute = 0;
		$start->second = 0;
		$end->minute = 0;
		$end->second = 0;
		// subtract hours
		//$start->subHour();
		//$end->subMinutes(1);
		$end->minute = 59;
		#dump($start);dd($end);
		$subject = 'Voluntary Timeoff';
		$centers = Center::get();
		foreach ($centers as $center)
		{
			$center_id = $center->id;
        	$recipients = MailRecipients::whereCenterId($center_id)->whereModule($type)->get();

        	if(count($recipients) > 0)
        	{
				$rows = ForecastVTORequestItem::with(['forecast', 'request'])
						->whereHas('forecast', function($query) use ($center_id){
							$query->where('center_id', $center_id);
						})
						->whereHas('request', function($query) use ($start, $end){
							$query->where('created_at', '>=', $start)->where('created_at', '<=', $end);
						})->get();
				$this->mail($subject, $type, $rows, $center_id, $start, $end, $recipients);
        	}
		}
	}

    public function mail($subject, $type, $rows, $center_id, $start, $end, $recipients)
    {
        #$to = 'DUMAswapalert.distro@qualfon.com';
        #$to = 'cebshiftswap.distro@qualfon.com';
        #$to = 'Shiftswaps@bilateralbaq.com';

        $from = 'no-reply@' . $type . '.com';
        $from_name = 'Panasiatic Enterprise';
        
        $cc = 'helpdesk.teamware@gmail.com';
        $cc_name = 'HELPDESK';

        $data = [];
        $from = [];
        $to = [];
        $cc = [];
        $bcc = [];
        $n = 0;

        foreach($recipients as $recipient)
        {
            if($recipient->type == 'from') { $from[$n] = $recipient->mail; }
            if($recipient->type == 'to') { $to[$n] = $recipient->mail; }
            if($recipient->type == 'cc') { $cc[$n] = $recipient->mail; }
            if($recipient->type == 'bcc') { $bcc[$n] = $recipient->mail; }
            $n++;
        }

        $data = compact('rows', 'start', 'end');

        #Mail::later(5, 'email.vot-availed', $data, function($m) use ($to, $to_name, $from, $from_name, $cc, $cc_name, $subject) {
        Mail::send('email.' . $type . '-availed', $data, function ($m) use ($to, $from, $cc, $subject) {
            $m->from($from);
            $m->to($to);
            $m->cc($cc);
            $m->subject($subject);
        });
    }
}
