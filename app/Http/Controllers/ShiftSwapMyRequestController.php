<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\SwapType;
use App\SwapAgentReqeust;
use App\SwapAgentInformation;
use App\SwapAgentSchedule;
use \Carbon\Carbon;
use DB;
use Auth;
use Validation;

class ShiftSwapMyRequestController extends Controller
{
    public function index($type)
    {
        $user = Auth::user();
        $now = Carbon::now();
    	$datetime_now = $now->toDateTimeString();
        
        if($type == 'request')
        {
        	$type = 'My Request';
    		$rows = SwapAgentReqeust::whereUserId($user->id)->orderBy('created_at', 'DESC')->paginate(30);
        }
        else
        {
        	$type = 'Received Request';
    		$rows = SwapAgentReqeust::whereBuddyUserId($user->id)->orderBy('created_at', 'DESC')->paginate(30);
        }

    	$data = compact('type', 'rows');

    	return view('shift-swap.my-request', $data);
    }
}
