<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Schedule;
use App\SwapAgentReqeust;
use App\SwapAgentSchedule;
use App\SwapAgentInformation;
use App\Absence;
use App\AbsenceReplacements;
use \Carbon\Carbon;
use Auth;
use DB;

class AbsenceController extends Controller
{
	public function index()
	{
		return view('absence.index');
	}

	public function store(Request $request)
	{
        $user = Auth::user();
        $input = $request->only('shift_date', 'schedule');
        $input['center_id'] = $user->center_id;
        $input['user_id'] = $user->id;

        $schedule = explode('-', $request->schedule);
        $expired_at = Carbon::parse($request->shift_date . ' ' . $schedule[0]);
        $expired_at->subHours(4);

        $input['expired_at'] = $expired_at->toDateTimeString();

        Absence::create($input);

        return response()->json(['success'=>'Absence successfully plotted!']);
	}

    public function edit($id)
    {
        $rows = AbsenceReplacements::whereAbsenceId($id)->get();
        $data = compact('rows');
        return view('absence.applications', $data);
    }


    public function show($id)
    {
        $user = Auth::user();
        $skill = $user->information->skill;
        $row =  Absence::whereHas('information', function($query) use ($skill){
                    $query->where('skill', $skill);
                })->whereId($id)->first();

        if(!$row)
        {
            abort(404);
        }

        $absence_schedule_base = explode('-', $row->schedule);
        $shift_start = Carbon::parse($row->shift_date . ' ' . $absence_schedule_base[0]);
        $shift_mid = Carbon::parse($row->shift_date . ' ' . $absence_schedule_base[0]);
        $shift_end = Carbon::parse($row->shift_date . ' ' . $absence_schedule_base[1]);

        if(trim($absence_schedule_base[1]) == '12:00 AM')
        {
            $shift_end->addDay();
        }

        $half_of_shift_in_minutes = (($shift_end->diffInHours($shift_start)) * 60) / 2;
        // Add minutes to get half of shift
        $shift_mid->addMinutes($half_of_shift_in_minutes);

        $schedules = [
            $shift_start->format('h:i A') . ' - ' . $shift_end->format('h:i A'),
            $shift_start->format('h:i A') . ' - ' . $shift_mid->format('h:i A'),
            $shift_mid->format('h:i A') . ' - ' . $shift_end->format('h:i A')
        ];

        $data = compact('row', 'schedules');
        return view('absence.show', $data);
    }





    public function application($shift_date, $schedule)
    {
        $selected_schedule = '';
        $user = Auth::user();
        $row_count = Absence::whereUserId($user->id)->whereShiftDate($shift_date)->count();
        
        if($row_count > 0)
        {
            #abort(404);
            return redirect()->route('absence.index')->withErrors(['You`ve forecasted an absence on ' . $shift_date . '.']);
        }

        $row = AbsenceReplacements::whereUserId($user->id)->whereShiftDate($shift_date)->first();

        if($row)
        {
            $selected_schedule = $row->schedule;
        }

        $skill = $user->information->skill;

        $absence_schedule_base = explode('-', $schedule);
        $shift_start = Carbon::parse($shift_date . ' ' . $absence_schedule_base[0]);
        $shift_mid = Carbon::parse($shift_date . ' ' . $absence_schedule_base[0]);
        $shift_end = Carbon::parse($shift_date . ' ' . $absence_schedule_base[1]);

        if(trim($absence_schedule_base[1]) == '12:00 AM')
        {
            $shift_end->addDay();
        }

        $half_of_shift_in_minutes = (($shift_end->diffInHours($shift_start)) * 60) / 2;
        // Add minutes to get half of shift
        $shift_mid->addMinutes($half_of_shift_in_minutes);

        $schedules = [
            $shift_start->format('h:i A') . ' - ' . $shift_end->format('h:i A'),
            $shift_start->format('h:i A') . ' - ' . $shift_mid->format('h:i A'),
            $shift_mid->format('h:i A') . ' - ' . $shift_end->format('h:i A')
        ];

        $data = compact('shift_date', 'schedule', 'schedules', 'selected_schedule');
        return view('absence.show', $data);
    }

    public function application_post(Request $request)
    {
        $request->validate([
            'shift_date' => 'required', 
            'schedule_base' => 'required',
            'schedule' => 'required'
        ],[
            'shift_date.required' => 'Schedule is required!',
            'schedule_base.required' => 'Schedule is required!',
            'schedule.required' => 'Schedule is required!'
        ]);


        $shift_date = $request->shift_date;
        $schedule_base = $request->schedule_base;
        $absence_shift_interval = 0;
        $my_shift_interval = 0;
        $user = Auth::user();
        $sched = new Schedule;
        #$row = Absence::whereId($id)->first();
        $absence_remaining = $this->absence_remaining($user->information->skill, $shift_date, $schedule_base);

        if($absence_remaining < 1)
        {
            return redirect()->route('absence.application', ['shift_date'=>$shift_date, 'schedule'=>$schedule_base])->withErrors(['No more slot/s available!']);
        }

        $row_count = AbsenceReplacements::whereUserId($user->id)->whereShiftDate($shift_date)->count();

        if($row_count > 0)
        {
            return redirect()->route('absence.application', ['shift_date'=>$shift_date, 'schedule'=>$schedule_base])->withErrors(['You`ve already filed a replacement application!']);
        }

        #$schedule = $this->schedule($user->id, $shift_date);
        $schedule = $sched->shift($user->id, $shift_date);
        $my_shift_schedule = ($schedule['new_schedule'] == '')? $schedule['schedule'] : $schedule['new_schedule'];

        if($my_shift_schedule != 'Off')
        {
            $my_shift_schedule_base = explode('-', $my_shift_schedule);
            $my_shift_start = Carbon::parse($shift_date . ' ' . $my_shift_schedule_base[0]);
            $my_shift_end = Carbon::parse($shift_date . ' ' . $my_shift_schedule_base[1]);
            if(trim($my_shift_schedule_base[1]) == '12:00 AM')
            {
                $my_shift_end->addDay();
            }
            $my_shift_interval = $my_shift_end->diffInHours($my_shift_start);
            #dump($my_shift_schedule_base);
            #dump($my_shift_start);
            #dump($my_shift_end);
        }

        $absence_schedule_base = explode('-', $schedule_base);
        $my_shift_schedule_base = explode('-', $my_shift_schedule);

        $absence_schedule_base[0] = trim($absence_schedule_base[0]);
        $absence_schedule_base[1] = trim($absence_schedule_base[1]);

        if($my_shift_schedule == 'Off' && $request->has('schedule.0') && ($request->has('schedule.1') || $request->has('schedule.2')))
        {
            $shift_text = ($request->has('schedule.1'))? $request->schedule[1] : $request->schedule[2];
            return redirect()->route('absence.application', ['shift_date'=>$shift_date, 'schedule'=>$schedule_base])->withErrors(['You`re not allowed to apply for <strong>' . $shift_text , '</strong> shift, because you`ve selected <strong>' . $request->schedule[0] . '</strong> shift.']);
        }
        elseif($my_shift_interval > 0 && $request->has('schedule.0'))
        {
            return redirect()->route('absence.application', ['shift_date'=>$shift_date, 'schedule'=>$schedule_base])
                    ->withErrors(['You`re not allowed to apply for <strong>' . $request->schedule[0] . '</strong> shift, because you have <strong>' . $my_shift_schedule . '</strong> shift. Overtime more than 4hrs is not allowed!']);
        }
        elseif($my_shift_interval > 0 && ($request->has('schedule.1') && $request->has('schedule.2')))
        {
            return redirect()->route('absence.application', ['shift_date'=>$shift_date, 'schedule'=>$schedule_base])
                    ->withErrors(['You`re not allowed to apply for <strong>' . $request->schedule[1] . '</strong> and <strong>' . $request->schedule[2] . '</strong> shift, because you have <strong>' . $my_shift_schedule . '</strong> shift. Overtime more than 4hrs is not allowed!']);
        }
        
        #dump($my_shift_interval);
        #dd($request->all());

        /*$absence_schedule_base = explode('-', $schedule_base);
        $my_shift_schedule_base = explode('-', $my_shift_schedule);

        $absence_schedule_base[0] = trim($absence_schedule_base[0]);
        $absence_schedule_base[1] = trim($absence_schedule_base[1]);

        $absence_shift_start = Carbon::parse($shift_date . ' ' . $absence_schedule_base[0]);
        if($my_shift_schedule != 'Off')
        {
            $my_shift_schedule_base[0] = trim($my_shift_schedule_base[0]);
            $my_shift_schedule_base[1] = trim($my_shift_schedule_base[1]);

            $applicant_shift_end = Carbon::parse($shift_date . ' ' . $my_shift_schedule_base[1]);
            $shift_interval = $applicant_shift_end->diffInMinutes($absence_shift_start);
        }

        if($my_shift_schedule == 'Off' && $request->has('schedule.0') && ($request->has('schedule.1') || $request->has('schedule.2')))
        {
            $shift_text = ($request->has('schedule.1'))? $request->schedule[1] : $request->schedule[2];
            return redirect()->route('absence.application', ['shift_date'=>$shift_date, 'schedule'=>$schedule_base])->withErrors(['You`re not allowed to apply for <strong>' . $shift_text , '</strong> shift, because you`ve selected <strong>' . $request->schedule[0] . '</strong> shift.']);
        }
        elseif($my_shift_schedule != 'Off' && $absence_schedule_base[0] == $my_shift_schedule_base[0])
        {
            return redirect()->route('absence.application', ['shift_date'=>$shift_date, 'schedule'=>$schedule_base])->withErrors(['You`re currently on <strong>' . $my_shift_schedule . '</strong> shift!']);
        }
        elseif($shift_interval > 0 && $request->has('schedule.0'))
        {
            return redirect()->route('absence.application', ['shift_date'=>$shift_date, 'schedule'=>$schedule_base])->withErrors(['You`re not allowed to apply for <strong>' . $request->schedule[0] . '</strong> shift, because you have <strong>' . $my_shift_schedule . '</strong> shift. Overtime more than 4hrs is not allowed!']);
        }
        elseif($shift_interval == 0 && ($request->has('schedule.1') && $request->has('schedule.2')))
        {
            return redirect()->route('absence.application', ['shift_date'=>$shift_date, 'schedule'=>$schedule_base])->withErrors(['You`re not allowed to apply for <strong>' . $request->schedule[1] . '</strong> and <strong>' . $request->schedule[2] . '</strong> shift, because you have <strong>' . $my_shift_schedule . '</strong> shift. Overtime more than 4hrs is not allowed!']);
        }*/

        foreach ($request->schedule as $item)
        {
            $item_base = explode('-', $item);
            $item_shift_start = Carbon::parse($shift_date . ' ' . $item_base[0]);
            $item_shift_end = Carbon::parse($shift_date . ' ' . $item_base[1]);

            if(trim($item_base[1]) == '12:00 AM')
            {
                $item_shift_end->addDay();
            }

            $number_of_hours = $item_shift_end->diffInMinutes($item_shift_start) / 60;
            $data = [
                'user_id' => $user->id, 
                'shift_date' => $shift_date, 
                'schedule_base' => $schedule_base, 
                'hours' => $number_of_hours, 
                'schedule' => $item
            ];
            AbsenceReplacements::create($data);
        }

        return redirect()->route('absence.application', ['shift_date'=>$shift_date, 'schedule'=>$schedule_base])->with('success', 'Application successfully submitted!');
    }






    public function absence_remaining($skill, $shift_date, $schedule)
    {
        $absence = Absence::with(['information'])
                ->whereHas('information', function($query) use ($skill){
                    $query->where('skill', 'LIKE', "$skill%");
                })->whereShiftDate($shift_date)->whereSchedule($schedule)
                ->count();

        $replacement = AbsenceReplacements::with(['information'])
                ->whereHas('information', function($query) use ($skill){
                    $query->where('skill', 'LIKE', "$skill%");
                })->whereShiftDate($shift_date)->whereScheduleBase($schedule)
                ->count();

        return $absence - $replacement;
    }

    public function absence_shift_date_remaining($skill, $shift_date)
    {
        $absence = Absence::with(['information'])
                ->whereHas('information', function($query) use ($skill){
                    $query->where('skill', 'LIKE', "$skill%");
                })->whereShiftDate($shift_date)->count();

        return $absence;
    }

    public function list(Request $request)
    {
        $user = Auth::user();
        $n = 0;
        $lists = [];
        $skill = $user->information->skill;
        $shift_date = $request->shift_date;
        $rows = Absence::with(['information'])
                ->whereHas('information', function($query) use ($skill){
                    $query->where('skill', 'LIKE', "$skill%");
                })->whereShiftDate($shift_date)
                ->groupBy('schedule')->orderBy('schedule', 'ASC')
                ->get(['schedule', DB::raw('COUNT(schedule) AS total_count')]);
        foreach ($rows as $row)
        {
            $status = '';
            $url = '';
            $url = route('absence.application', ['shift_date'=>$shift_date, 'schedule'=>$row->schedule]);
            /*$absence_forecast_count =  AbsenceReplacements::with(['information'])
                        ->whereHas('information', function($query) use ($skill){
                            $query->where('skill', 'LIKE', "$skill%");
                        })->whereShiftDate($shift_date)->whereScheduleBase($row->schedule)->count();
            $total_count = $row->total_count - $absence_forecast_count;*/
            $total_count = $this->absence_remaining($skill, $shift_date, $row->schedule);

            $lists[$n] = [
                'url' => $url,
                'schedule' => $row->schedule,
                'total_count' => $total_count,
            ];
            $n++;
        }
        return response()->json([
            'lists'=>$lists
        ]);
    }

	public function calendar(Request $request)
	{
		$calendar_dates = '';
		$date = Carbon::parse($request->calendar_month);
		$tempDate = Carbon::createFromDate($date->year, $date->month, 1);
		$skip = $tempDate->dayOfWeek;
		$user = Auth::user();
        $sched = new Schedule;
		#$information = SwapAgentInformation::whereUserId($user->id)->first();
        $skill = '';
        if($user->information)
        {
            $skill = $user->information->skill_clean;
        }

		for($i = 0; $i < $skip; $i++)
		{
			$tempDate->subDay();
		}
		
		do
    	{
			for($i = 0; $i < 7; $i++)
			{

				if($tempDate->month == $date->month)
				{
                    $onclick = '';
                    $highlight_active = '';
                    $event = '';
                    $event_color = 'info';
                    $tooltip = '';
                    $data_tooltip = '';
                    $schedule = $sched->shift($user->id, $tempDate->toDateString());
                    #dump($schedule);
                    if(count($schedule) > 0)
                    {
                        $shift_date = ($schedule['new_shift_date'] == '')? $schedule['shift_date'] : $schedule['new_shift_date'];
                        $shift_schedule = ($schedule['new_schedule'] == '')? $schedule['schedule'] : $schedule['new_schedule'];

                        $absence_forecast =  Absence::whereShiftDate($shift_date)->whereuserId($user->id)->count();

                        if($absence_forecast < 1)
                        {
                            $onclick = "onclick=\"validate('$shift_date', '$shift_schedule')\"";
                        }

                        if($absence_forecast > 0)
                        {
                            $tooltip = 'tooltip';
                            $data_tooltip = 'data-tooltip="You`ve forecasted an absence!"';
                            $event = '<a class="calendar-event is-warning" ' . $onclick . '>' . $shift_schedule . '</a>';
                        }
                        else
                        {
                            $event = '<a class="calendar-event is-info" ' . $onclick . '>' . $shift_schedule . '</a>';
                        }

                        if($shift_schedule == 'Off')
                        {
                            $event = '<a class="calendar-event is-danger">' . $shift_schedule . '</a>';
                        }

                        $replacement_forecast =  AbsenceReplacements::whereShiftDate($shift_date)->whereuserId($user->id)->count();

                        if($replacement_forecast > 0)
                        {
                            $tooltip = 'tooltip';
                            $data_tooltip = 'data-tooltip="You`ve applied as a replacement!"';
                            $event = '<a class="calendar-event is-dark" ' . $onclick . '>' . $shift_schedule . '</a>';
                        }

                        $absence_shift_date_remaining = $this->absence_shift_date_remaining($skill, $shift_date);

                        if($absence_shift_date_remaining > 0)
                        {
                            $event .= '<a class="calendar-event is-success" onclick="absences(\'' . $shift_date . '\')" title="Number of agent/s forecasted an absence">' . $absence_shift_date_remaining . ' agent forecast</a>';
                        }
                    }

					$calendar_dates .= '<div class="calendar-date ' . $tooltip . '" ' . $data_tooltip . '><button class="date-item ' . $highlight_active . '">' . $tempDate->day . '</button>' . $event . '</div>';
				}
				else
				{
					$calendar_dates .= '<div class="calendar-date is-disabled""><button class="date-item">' . $tempDate->day . '</button></div>';
				}
				
				$tempDate->addDay();
			}
		}
		while($tempDate->month == $date->month);
		return $calendar_dates;
	}

    /*public function schedule($user_id, $shift_date)
    {
        $n = 0;
        $data = [];
        $rows = SwapAgentSchedule::whereUserId($user_id)->whereShiftDate($shift_date)
                ->whereRaw('expiration_date >= NOW()')->orderBy('week_ending', 'ASC')->get(['week_ending']);

        foreach ($rows as $row)
        {
            $schedules_shift = SwapAgentSchedule::whereUserId($user_id)->whereShiftDate($shift_date)
                                ->where('schedule', '<>', 'Off')->first();
            $schedules = SwapAgentSchedule::whereUserId($user_id)->whereShiftDate($shift_date)
                        ->whereRaw('expiration_date >= NOW()')
                        ->orderBy('shift_date', 'ASC')->get();

            $rd_and_shift_flat = [];
            $requests_2rd = SwapAgentReqeust::whereSwapType('2rd')->whereWeekEnding($row->week_ending)->whereStatus(1)
                        ->where(function($query) use ($user_id){
                            $query->where('user_id', $user_id)->orWhere('buddy_user_id', $user_id);
                        })
                        ->orderBy('created_at', 'ASC')->first();

            if(!is_null($requests_2rd))
            {
                $rd_and_shift = $this->agents_rest_days($row->week_ending, $requests_2rd->user_id, $requests_2rd->buddy_user_id);
                $rd_and_shift_flat = array_flatten($rd_and_shift);
            }

            foreach ($schedules as $schedule)
            {
                $status = 0;
                $shift_date = $schedule->shift_date;

                $new_schedule = '';
                $new_shift_date = '';

                // 2 RD
                if(in_array($shift_date, $rd_and_shift_flat))
                {
                    $new_schedule = $schedules_shift->schedule;

                    if($schedule->schedule != 'Off')
                    {
                        $new_schedule = 'Off';
                    }
                }
                // Single Day
                else
                {
                    $requests = SwapAgentReqeust::whereIn('swap_type', ['sd', 'srd', 'shrd'])->whereWeekEnding($schedule->week_ending)->whereStatus(1)
                                ->where(function($query) use ($user_id){
                                    $query->where('user_id', $user_id)->orWhere('buddy_user_id', $user_id);
                                })
                                ->where(function($query) use ($shift_date){
                                    $query->where('employee_shift_date', $shift_date)->orWhere('buddy_shift_date', $shift_date);
                                })
                                ->orderBy('created_at', 'ASC')->get();
                    foreach ($requests as $request)
                    {
                        $status = 1;
                        $request_employee_shift_date = $request->employee_shift_date->toDateString();
                        $request_buddy_shift_date = $request->buddy_shift_date->toDateString();

                        if($request->swap_type == 'sd')
                        {
                            if($schedule->user_id == $request->user_id)
                            {
                                $new_schedule = $request->buddy_schedule;
                                $new_shift_date = $request_buddy_shift_date;
                            }
                            elseif($schedule->user_id == $request->buddy_user_id)
                            {
                                $new_schedule = $request->employee_schedule;
                                $new_shift_date = $request_employee_shift_date;
                            }
                        }
                        elseif($request->swap_type == 'srd')
                        {
                            $opposite_date = $request_employee_shift_date;

                            if($request_employee_shift_date == $schedule->shift_date)
                            {
                                $opposite_date = $request_buddy_shift_date;
                            }

                            $get_other_shift_schedule = SwapAgentSchedule::whereUserId($user_id)->whereShiftDate($opposite_date)->first();
                            $new_schedule = $get_other_shift_schedule->schedule;
                        }
                        elseif($request->swap_type == 'shrd')
                        {
                            $get_query = SwapAgentSchedule::whereShiftDate($shift_date);

                            if($user_id == $request->user_id)
                            {
                                $get_query->whereUserId($request->buddy_user_id);
                            }
                            elseif($user_id == $request->buddy_user_id)
                            {
                                $get_query->whereUserId($request->user_id);
                            }

                            $get_other_shift_schedule = $get_query->first();
                            $new_schedule = $get_other_shift_schedule->schedule;
                        }
                    }
                }

                $data = [
                    'status' => $status, 
                    'shift_date' => $shift_date, 
                    'week_ending' => $schedule->week_ending, 
                    'schedule' => $schedule->schedule, 
                    'restrictions' => $schedule->restrictions, 
                    'shift_date_format' => $schedule->shift_date, 
                    'restrictions' => $row->restrictions, 
                    'new_schedule' => $new_schedule, 
                    'new_shift_date' => $new_shift_date 
                ];
                $n++;
            }
        }

        #dd($data);
        return $data;
    }*/
}
