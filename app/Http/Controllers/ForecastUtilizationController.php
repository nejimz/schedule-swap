<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Center;
use \Carbon\Carbon;
use Response;

class ForecastUtilizationController extends Controller
{
	public function index()
	{
		$centers = Center::orderBy('id', 'ASC')->get();
		$data = compact('centers');
		return view('vforecast.reports.utilization', $data);
	}

	public function create(Request $request)
	{
		$request->validate([
			'date_from' => 'required',
			'date_to' => 'required'
		],[
			'date_from.required' => 'Date From is required',
			'date_to.required' => 'Date To is required.'
		]);

        $date_now = Carbon::now()->toDateTimeString();
        $center_id = $request->center_id;
        $date_from = Carbon::parse($request->date_from);
        $date_to = Carbon::parse($request->date_to);
        $date_interval = $date_to->diffInDays($date_from);
        $dates = [];
		for ($date_from = $date_from; $date_from->lte($date_to); $date_from->addDay()) {
			#$week_endings[$i] = $date_to->subDays(7)->toDateString();
			$dates[] = $date_from->toDateString();
		}

        #dump($dates);

        if ($request->has('center_id') && $center_id != '') {
			$centers = Center::whereId($center_id)->orderBy('id', 'ASC')->get();
        } else {
			$centers = Center::orderBy('id', 'ASC')->get();
        }

		$data = compact('dates', 'centers');
		#return view('vforecast.reports.utilization-download', $data);

    	return response()->view('vforecast.reports.utilization-download', $data, 200)
    			->header('Content-Disposition', 'attachment; filename="VOT/VTO Utilization(' . $date_now . ').xls"')
    			->header('Content-Type', 'application/vnd.ms-excel');
	}
}
