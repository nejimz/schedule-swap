<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Center;
use App\SwapAgentSchedule;
use App\SwapAgentInformation;
use App\SwapType;
use \Carbon\Carbon;
use Auth;
use ActivityLogger;

class ShiftSwapScheduleUploadController extends Controller
{
    public function index()
	{
		$centers = Center::orderBy('name', 'ASC')->get();

		$data = compact('centers');

		return view('shift-swap.schedule-upload', $data);
	}

	public function store(Request $request)
	{
		$admin = Auth::user();
		$sub_hour = 4;
		$n = 0;
		$errors = [];
        $rows = json_decode($request->row, TRUE);
        $datetime_now = Carbon::now();
        #dd($rows);
        #for ($x = 0; $x < count($rows); $x++)
	    $file_center_id = 0;
        foreach($rows as $row) {
        	#dump((isset($row[13]))? 1 : 0);dd($row);
            $username = $row[0];
            $avaya = $row[1];
            $tier = $row[2];
            $wp = $row[3];
            $skill = $row[4];
			$sun = date('Y-m-d', strtotime($row[5]));

			$user = User::whereUsername($username)->whereNull('deleted_at')->orderBy('id', 'DESC')->first();
			// has user?
			if(is_null($user))
			{
				$errors[$n] = $username . ' login does not exists and schedule not uploaded!';
			}
			else
			{
				$file_center_id = $user->center_id;
	        	$center = $user->center_id;
				$information_data = [
					'center_id' => $center,
					'avaya' => $avaya,
					'tier' => $tier,
					'wp' => $wp,
					'skill' => $skill
				];
				$information = SwapAgentInformation::whereUserId($user->id)->first();
				$schedule_count = SwapAgentSchedule::whereUserId($user->id)->whereWeekEnding($sun)->count();

				// has information?
				if(is_null($information))
				{
					$information_data['user_id'] = $user->id;
					SwapAgentInformation::create($information_data);
				}
				else
				{
					SwapAgentInformation::whereUserId($user->id)->update($information_data);
				}
				
				// has schedule?
				if($schedule_count > 0)
				{
					$errors[$n] = $username . ' already has a schedule for Week Ending ' . $sun;
				}
				else
				{
					$mon = date('Y-m-d', strtotime('-6 day', strtotime($sun)));
					$tue = date('Y-m-d', strtotime('-5 day', strtotime($sun)));
					$wed = date('Y-m-d', strtotime('-4 day', strtotime($sun)));
					$thu = date('Y-m-d', strtotime('-3 day', strtotime($sun)));
					$fri = date('Y-m-d', strtotime('-2 day', strtotime($sun)));
					$sat = date('Y-m-d', strtotime('-1 day', strtotime($sun)));

					$mon_restrictions = (isset($row[13]))? trim($row[13]) :  null;
					$tue_restrictions = (isset($row[14]))? trim($row[14]) :  null;
					$wed_restrictions = (isset($row[15]))? trim($row[15]) :  null;
					$thu_restrictions = (isset($row[16]))? trim($row[16]) :  null;
					$fri_restrictions = (isset($row[17]))? trim($row[17]) :  null;
					$sat_restrictions = (isset($row[18]))? trim($row[18]) :  null;
					$sun_restrictions = (isset($row[19]))? trim($row[19]) :  null;

					$mon_schedule = trim($row[6]);
					$tue_schedule = trim($row[7]);
					$wed_schedule = trim($row[8]);
					$thu_schedule = trim($row[9]);
					$fri_schedule = trim($row[10]);
					$sat_schedule = trim($row[11]);
					$sun_schedule = trim($row[12]);

					$mon_expiration_start = $this->get_start_time($mon_schedule);
					$tue_expiration_start = $this->get_start_time($tue_schedule);
					$wed_expiration_start = $this->get_start_time($wed_schedule);
					$thu_expiration_start = $this->get_start_time($thu_schedule);
					$fri_expiration_start = $this->get_start_time($fri_schedule);
					$sat_expiration_start = $this->get_start_time($sat_schedule);
					$sun_expiration_start = $this->get_start_time($sun_schedule);

					$mon_expiration_date = Carbon::parse($mon . ' ' . $mon_expiration_start);
					$mon_expiration_date->subHours($sub_hour);
					$tue_expiration_date = Carbon::parse($tue . ' ' . $tue_expiration_start);
					$tue_expiration_date->subHours($sub_hour);
					$wed_expiration_date = Carbon::parse($wed . ' ' . $wed_expiration_start);
					$wed_expiration_date->subHours($sub_hour);
					$thu_expiration_date = Carbon::parse($thu . ' ' . $thu_expiration_start);
					$thu_expiration_date->subHours($sub_hour);
					$fri_expiration_date = Carbon::parse($fri . ' ' . $fri_expiration_start);
					$fri_expiration_date->subHours($sub_hour);
					$sat_expiration_date = Carbon::parse($sat . ' ' . $sat_expiration_start);
					$sat_expiration_date->subHours($sub_hour);
					$sun_expiration_date = Carbon::parse($sun . ' ' . $sun_expiration_start);
					$sun_expiration_date->subHours($sub_hour);

		    		$schedules = [
		    			[
		    				'user_id' => $user->id, 
		    				'center_id' => $center, 
		    				'shift_date' => $mon, 
		    				'schedule' => $mon_schedule, 
		    				'restrictions' => $mon_restrictions, 
		    				'week_ending' => $sun, 
		    				'expiration_date' => $mon_expiration_date->toDateTimeString(), 
		    				'created_at' => $datetime_now->toDateTimeString(), 
		    				'updated_at' => $datetime_now->toDateTimeString() 
		    			],
		    			[
		    				'user_id' => $user->id, 
		    				'center_id' => $center, 
		    				'shift_date' => $tue, 
		    				'schedule' => $tue_schedule, 
		    				'restrictions' => $tue_restrictions, 
		    				'week_ending' => $sun, 
		    				'expiration_date' => $tue_expiration_date->toDateTimeString(), 
		    				'created_at' => $datetime_now->toDateTimeString(), 
		    				'updated_at' => $datetime_now->toDateTimeString() 
		    			],
		    			[
		    				'user_id' => $user->id, 
		    				'center_id' => $center, 
		    				'shift_date' => $wed, 
		    				'schedule' => $wed_schedule, 
		    				'restrictions' => $wed_restrictions, 
		    				'week_ending' => $sun, 
		    				'expiration_date' => $wed_expiration_date->toDateTimeString(), 
		    				'created_at' => $datetime_now->toDateTimeString(), 
		    				'updated_at' => $datetime_now->toDateTimeString() 
		    			],
		    			[
		    				'user_id' => $user->id, 
		    				'center_id' => $center, 
		    				'shift_date' => $thu, 
		    				'schedule' => $thu_schedule, 
		    				'restrictions' => $thu_restrictions, 
		    				'week_ending' => $sun, 
		    				'expiration_date' => $thu_expiration_date->toDateTimeString(), 
		    				'created_at' => $datetime_now->toDateTimeString(), 
		    				'updated_at' => $datetime_now->toDateTimeString() 
		    			],
		    			[
		    				'user_id' => $user->id, 
		    				'center_id' => $center, 
		    				'shift_date' => $fri, 
		    				'schedule' => $fri_schedule, 
		    				'restrictions' => $fri_restrictions, 
		    				'week_ending' => $sun, 
		    				'expiration_date' => $fri_expiration_date->toDateTimeString(), 
		    				'created_at' => $datetime_now->toDateTimeString(), 
		    				'updated_at' => $datetime_now->toDateTimeString() 
		    			],
		    			[
		    				'user_id' => $user->id, 
		    				'center_id' => $center, 
		    				'shift_date' => $sat, 
		    				'schedule' => $sat_schedule, 
		    				'restrictions' => $sat_restrictions, 
		    				'week_ending' => $sun, 
		    				'expiration_date' => $sat_expiration_date->toDateTimeString(), 
		    				'created_at' => $datetime_now->toDateTimeString(), 
		    				'updated_at' => $datetime_now->toDateTimeString() 
		    			],
		    			[
		    				'user_id' => $user->id, 
		    				'center_id' => $center, 
		    				'shift_date' => $sun, 
		    				'schedule' => $sun_schedule, 
		    				'restrictions' => $sun_restrictions, 
		    				'week_ending' => $sun, 
		    				'expiration_date' => $sun_expiration_date->toDateTimeString(), 
		    				'created_at' => $datetime_now->toDateTimeString(), 
		    				'updated_at' => $datetime_now->toDateTimeString() 
		    			]
		    		];
		    		#dd($schedules);

		            SwapAgentSchedule::insert($schedules);
				}
			}

			$n++;
	    }

	    $cntr = Center::whereId($file_center_id)->first();
        $action = 'Schedule Upload for ' . (($cntr) ? $cntr->name : '');
        ActivityLogger::store($request->segment(1), $action);

        return response()->json(['message'=>'success', 'errors'=>$errors]);
	}

	public function get_start_time($schedule)
	{
		$schedule = explode('-', $schedule);

		if(count($schedule) > 1)
		{
			return trim($schedule[0]);
		}
		else
		{
			return '00:00';
		}
	}

	public function utilization(Request $request)
	{
		$type = '';
		$week_endings = [];
		$centers = Center::whereNotIn('code', ['ENT'])->orderBy('code', 'ASC')->get();

		$swap_types = SwapType::whereNotIn('id', [4])->orderBy('abbr', 'ASC')->get();
		$last_week_ending =SwapAgentSchedule::orderBy('week_ending', 'DESC')->first();

		$end_date = $last_week_ending->week_ending;
		$start_date = Carbon::parse($end_date)->subDays(7*5)->toDateString();

		if($request->has('type') && $request->type != '') {
			$type = trim($request->type);
		}

		if($request->has('start_date')) {
			$start_date = trim($request->start_date);
		}

		if($request->has('end_date') && $request->end_date != '') {
			$end_date = trim($request->end_date);
		}

		$date_from = Carbon::parse($start_date);
		$date_to = Carbon::parse($end_date);

		for ($date_from = $date_from; $date_from->lte($date_to); $date_from->addDays(7)) {
			#$week_endings[$i] = $date_to->subDays(7)->toDateString();
			$week_endings[] = $date_from->toDateString();
		}

		asort($week_endings);

		$swap_types_count = count($swap_types) + 2;
		$week_endings_count = count($week_endings) + 1;

		$data = compact('centers', 'swap_types', 'type', 'start_date', 'end_date', 'swap_types_count', 'week_endings', 'week_endings_count');

		return view('shift-swap.utilization', $data);
	}
}
