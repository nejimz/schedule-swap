<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class ImpersonateController extends Controller
{
    public function take($id)
    {
        $user = User::find($id);
        Auth::user()->impersonate($user);
        return redirect()->route('home');
    }

    public function leave()
    {
        Auth::user()->leaveImpersonation();
        return redirect()->route('admin.user.index');
    }
}
