<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PasswordGeneratorController extends Controller
{
    public function index($number=null)
    {
        $passwords = [];
        if (isset($number)) {
            for($i=0; $i<=$number; $i++){
                $string = $this->generate(8);
                $passwords[] = (object) [ 
                    'string' => $string,
                    'hash' => bcrypt($string),
                ];
            }
        } else {
            $string = $this->generate(8);
            $passwords[] = (object) [ 
                'string' => $string,
                'hash' => bcrypt($string),
            ];
        }
        
        $data = compact('passwords');
        
        return view('admin.tools.password-generator', $data);
    }

    public function generate($length, $strength=8){
        $vowels = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';
        if ($strength >= 1) {
            $consonants .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($strength >= 2) {
            $vowels .= "AEUY";
        }
        if ($strength >= 4) {
            $consonants .= '23456789';
        }
        if ($strength >= 8) {
            $consonants .= '@#$%';
        }

        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $password;
    }
}
