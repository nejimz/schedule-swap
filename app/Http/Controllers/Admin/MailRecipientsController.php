<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Center;
use App\MailRecipients;
use Auth;

class MailRecipientsController extends Controller
{
    public function index(Request $request)
    {
        $search = '';
        $query = new MailRecipients;

        if($request->has('search'))
        {
            $search = $request->search;

            $query->where('name', 'LIKE', "$search%")->orWhere('email', 'LIKE', "$search%");
        }
        
        $rows = $query->paginate(50);

        $data = compact('rows', 'search');

        return view('admin.mail.recipients.index', $data);
    }
    
    public function create()
    {
    	$route = route('admin.mail-recipients.store');
    	$centers = Center::get();
    	$modules = ['shift swap', 'vot', 'vto'];
    	$mail_types = ['to', 'cc', 'from', 'bcc'];
    	
    	$center_id = old('center_id');
    	$module = old('module');
    	$type = old('type');
    	$mail = old('mail');

    	$data = compact('route', 'centers', 'modules', 'mail_types', 'center_id', 'module', 'type', 'mail');

        return view('admin.mail.recipients.form', $data);
    }
    
    public function store(Request $request)
    {
    	$request->validate(
    		[
    			'center_id' => 'required', 
    			'module' => 'required', 
    			'type' => 'required', 
    			'mail' => 'required|email'  
    		]
    	);

    	$count = MailRecipients::whereCenterId($request->center_id)->whereModule($request->module)->whereType($request->type)->whereMail($request->mail)->count();
    	if($count > 0)
    	{
    		return redirect()->route('admin.mail-recipients.create')->withErrors(['Email already exists!']);
    	}

        $input = $request->only(['center_id', 'module', 'type', 'mail']);
        MailRecipients::create($input);

        return redirect()->route('admin.mail-recipients.create')->with('success', 'Recipients Successfully added!');
    }
    
    public function edit($id)
    {
    	$row = MailRecipients::whereId($id)->first();
    	if(!$row)
    	{
    		abort(404);
    	}
    	$route = route('admin.mail-recipients.update', $id);
    	$centers = Center::get();
    	$modules = ['shift swap', 'vot', 'vto'];
    	$mail_types = ['to', 'cc', 'from', 'bcc'];

    	$center_id = $row->center_id;
    	$module = $row->module;
    	$type = $row->type;
    	$mail = $row->mail;

    	$data = compact('route', 'centers', 'modules', 'mail_types', 'center_id', 'module', 'type', 'mail');

        return view('admin.mail.recipients.form', $data);
    }
    
    public function update($id, Request $request)
    {
    	$request->validate(
    		[
    			'center_id' => 'required', 
    			'module' => 'required', 
    			'type' => 'required', 
    			'mail' => 'required|email'  
    		]
    	);

    	$count = MailRecipients::whereCenterId($request->center_id)->whereModule($request->module)->whereType($request->type)->whereMail($request->mail)->count();
    	if($count > 0)
    	{
    		return redirect()->route('admin.mail-recipients.create')->withErrors(['Email already exists!']);
    	}

        $input = $request->only(['center_id', 'module', 'type', 'mail']);
        MailRecipients::whereId($id)->upadte($input);

        return redirect()->route('admin.mail-recipients.create')->with('success', 'Recipients Successfully updated!');
    }
}
