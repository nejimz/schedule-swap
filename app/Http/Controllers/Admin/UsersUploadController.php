<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use \Carbon\Carbon;
use Auth;

class UsersUploadController extends Controller
{

	public function index()
	{
		$route = route('user.upload.store');
		$centers = \App\Center::get();
		$data = compact('route', 'centers');
		return view('admin.users.upload', $data);
	}

	public function store(Request $request)
	{
        $rows = json_decode($request->rows, TRUE);
        $center_id = trim($request->center_id);
        $errors = [];
        $i = 0;
        foreach($rows as $row)
        {
            $username = trim($row[0]);
            $first_name = trim($row[1]);
            $last_name = trim($row[2]);
            $password = trim($row[3]);


            $user_exists = User::whereUsername($username)->count();

            if($user_exists <= 0)
            {
	        	$input = [
	        		'employee_number' => $username, 
	        		'username' => $username, 
	        		'first_name' => $first_name, 
	        		'last_name' => $last_name, 
	        		'password' => bcrypt($password), 
	        		'center_id' => $center_id
	        	];
            	User::create($input);
            }
            else
            {
            	$errors[$i] = $username . ' already exists!';
            	$i++;
            }
        }

        return response()->json(['message'=>'success', 'errors'=>$errors]);
	}
}
