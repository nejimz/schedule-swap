<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\SecurityQuestions;
use App\SwapAgentInformation;
use \Carbon\Carbon;
use Auth;

class UsersController extends Controller
{
	private $roles;

	public function __construct()
	{
		$this->roles = $this->getRoles();
	}

	public function getRoles()
	{
		return collect([
			(object) ['name' => 'agent'],
			(object) ['name' => 'admin'],
			(object) ['name' => 'root']
		]);
	}

	public function index(Request $request)
	{
		$usersQuery = \App\User::withTrashed()->with(['information']);
		if($request->has('search'))
		{ 
			$search = $request->search;
			if($search != "")
			{
				$usersQuery->where(function($q) use($search){ 
					$q->orWhere('username', 'LIKE', '%'.$search.'%');
					$q->orWhere('first_name', 'LIKE', '%'.$search.'%');
					$q->orWhere('last_name', 'LIKE', '%'.$search.'%');
				});
			}
		}
		$users = $usersQuery->take(50)->get();
		return view('admin.users.index', compact('users'));
	}

	public function create()
	{
		$id = 0;
		$roles = $this->roles;
		$roles->pop(); 

		$centers = \App\Center::get();
		$form_route = route('admin.user.store');

        $username = old('username');
        $email = old('email');
        $employee_number = old('employee_number');
        $first_name = old('first_name');
        $last_name = old('last_name');
        $role = old('role');
        $center_id = old('center_id');

        $data = compact('id', 'roles', 'centers', 'form_route', 'username', 'email', 'employee_number', 'first_name', 'last_name', 'role', 'center_id');
		return view('admin.users.form', $data);
	}

	public function store(Requests\UserStoreRequest $request)
	{
        $input = $request->only('first_name', 'last_name', 'username', 'employee_number', 'email', 'role', 'center_id');
        $input['password'] = '';

        User::create($input);
        return redirect()->route('admin.user.create')->with('success', 'User Successfully added!');
	}

	public function edit($id)
	{
		$user = \App\User::find($id);
		$roles = $this->roles;
		$roles->pop();
		$centers = \App\Center::get();
		$form_route = route('admin.user.update', $id);

        $username = $user->username;
        $email = $user->email;
        $employee_number = $user->employee_number;
        $first_name = $user->first_name;
        $last_name = $user->last_name;
        $role = $user->role;
        $center_id = $user->center_id;

        $data = compact('id', 'roles', 'centers', 'form_route', 'username', 'email', 'employee_number', 'first_name', 'last_name', 'role', 'center_id');
		return view('admin.users.form', $data);
	}

	public function update($id, Requests\UserUpdateRequest $request)
	{
        $input = $request->only('first_name', 'last_name', 'username', 'employee_number', 'email', 'role', 'center_id');
        $input['password'] = '';

        User::whereId($id)->update($input);
        return redirect()->route('admin.user.edit', $id)->with('success', 'User Successfully updated!');
	}

	public function show($id)
	{
		$user = \App\User::find($id);
		return view('admin.users.show', compact('user'));
	}

	public function destroy($id)
	{
		$user = \App\User::withTrashed()->find($id);
		$information = SwapAgentInformation::withTrashed()->whereUserId($id)->first();
		/*
			Disable deletion of root users
		 */
		/*if($user->role == 'root')
		{
			return redirect()->route('admin.user.index');
		}*/

		if($user->trashed())
		{
			$user->restore();
			$information->restore();
		}
		else
		{
			$user->delete();
			$information->delete();
		}

		return redirect()->route('admin.user.index');
	}

    public function generate_password($id)
    {
    	$password = str_random(10);
        $password_bcrypt = bcrypt($password);
        $now = Carbon::now();

        SecurityQuestions::whereUserId($id)->delete();
        User::whereId($id)->update([ 'email'=>null, 'password'=>$password_bcrypt, 'created_at'=>$now, 'updated_at'=>$now ]);

        return response()->json(['password'=>$password]);
    }
}
