<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class CurlFirebaseCloudMessagingController extends Controller
{

	public function send_to_device(Request $request)
	{
		// return $request;
		$user_devices = new \App\MobileUserDevices;
		$devices = $user_devices->where('user_id', "=", intval($request->user_id))->orderBy('updated_at', 'DESC')->get();
		if($devices->count() >= 1)
		{
			// send the message
			foreach($devices as $device)
			{
				return $this->send($request->title, $request->body, $request->message, $device->registration_token);
			}
		}
	}

	public function send($title, $body, $message, $device_token)
	{
		$token = "AAAAzLL-DF8:APA91bGrlrISaPNHegtrvE3cEx1zzF1LMJzTmVkkHcCANt10TTUEB-2UxiF2HPRz_nQaMnC5MpqjJp1REhpWGLTBuRg2yKqz10eVAMGR_6ElnvlGU9-I2nszCGoLOHPasx5tB6sF3yoKYuvSIxI_FHRRCwxCegwRag";

 		$notification = array
		(
		    'body'		=> $body,
		    'title'		=> $title,
		    'vibrate'	=> 1,
		    'sound'		=> 'default',
		);

		$android = array
		(
			'priority' => 'normal'
		);

		$data = array
		(
			'data' => array
			(
				'title'		=> $title,
				'message'	=> $body,
				'timestamp' => date('Y-m-d G:i:s')
			)
		);

		$fields = array
		(
		    'to'			=> $device_token,
		    'notification'	=> $notification,
		    'data'			=> $data,
		    'android'		=> $android
		);

		$headers = array
		(
		    'Authorization: key=' . $token,
		    'Content-Type: application/json'
		);

		// return $fields;

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		echo $result;
	}	

    public function topic_send(Request $request)
	{
		// $token = $request->token;
		$topic = $request->topic;
		$body = $request->body;
		$title = $request->title;
		$token = "AAAAzLL-DF8:APA91bGrlrISaPNHegtrvE3cEx1zzF1LMJzTmVkkHcCANt10TTUEB-2UxiF2HPRz_nQaMnC5MpqjJp1REhpWGLTBuRg2yKqz10eVAMGR_6ElnvlGU9-I2nszCGoLOHPasx5tB6sF3yoKYuvSIxI_FHRRCwxCegwRag";

 		$msg = array
		(
		    'body'  => $body,
		    'title'     => $title,
		    'vibrate'   => 1,
		    'sound'     => 1,
		);

		$fields = array
		(
		    'to'  => '/topics/'.$topic,
		    'notification' => $msg
		);

		$headers = array
		(
		    'Authorization: key=' . $token,
		    'Content-Type: application/json'
		);

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		echo $result;
	}	
}
