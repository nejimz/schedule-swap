<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Center;
use App\SwapType;
use App\SwapAgentReqeust;
use App\SwapAgentInformation;
use App\SwapAgentSchedule;
use App\UserAdminCenter;
use \Carbon\Carbon;
use DB;
use Auth;
use Validation;
use Response;
use ActivityLogger;

class ShiftSwapReportsEnterpriseController extends Controller
{
	public function index(Request $request)
	{
        $user_id = Auth::user()->id;
        $now = Carbon::now();
    	$date_now = $now->toDateString();
        $skills = SwapAgentInformation::groupBy('skill')->orderBy('skill', 'ASC')->get(['skill']);
        $tiers = SwapAgentInformation::groupBy('tier')->orderBy('tier', 'ASC')->get(['tier']);
        $swap_types = SwapType::orderBy('id', 'ASC')->get();
        $centers = Center::orderBy('name', 'ASC')->get();
        $assigned_centers = UserAdminCenter::whereUserId($user_id)->get(['center_id'])->toArray();
        if (count($assigned_centers) > 0) {
        	$centers = Center::whereIn('id', $assigned_centers)->orderBy('name', 'ASC')->get();
        }

    	$data = compact('date_now', 'skills', 'tiers',  'centers', 'swap_types');


    	return view('shift-swap.reports.enterprise', $data);
	}

	public function show(Request $request)
	{
		$request->validate([ 
			'shift_date' => 'required',
			'swap_type' => 'required'
		]);

		if($request->organization == '' && $request->center_id == '')
		{
			return redirect()->route('enterprise-reports.index')
					->withErrors(['Selecting all Organization and Centers are not allowed!']);
		}

		$user = Auth::user();
		$organization = $request->organization;
		$tier = $request->tier;
		$shift_date = Carbon::parse($request->shift_date)->toDateString();
		$center_id = $request->center_id;
		$swap_type = $request->swap_type;

		$query = SwapAgentReqeust::with(['user_information', 'buddy_user_information']);

		$query->wherehas('user_information', function($query) use ($organization, $tier, $center_id){
			if($center_id != ''){ $query->whereCenterId($center_id); }
			if($organization != ''){ $query->where('skill', 'LIKE',"$organization%"); }
			if($tier != ''){ $query->whereTier($tier); }
		})
		->wherehas('buddy_user_information', function($query) use ($organization, $tier, $center_id){
			if($center_id != ''){ $query->whereCenterId($center_id); }
			if($organization != ''){ $query->where('skill', 'LIKE',"$organization%"); }
			if($tier != ''){ $query->whereTier($tier); }
		});

		$rows = $query->whereCenterId($center_id)->whereSwapType($swap_type)->whereStatus(1)
				->whereRaw("DATE(updated_at) = '$shift_date'")
				->orderBy('created_at', 'ASC')->get();
		#dd($rows);
		$view = '';
        $date_now = Carbon::now()->toDateTimeString();
		$file = SwapType::whereAbbr($swap_type)->first();

		if($swap_type == 'sd')
		{
			$view = 'shift-swap.reports.single-day.show';
		}
		elseif($swap_type == 'srd')
		{
			$view = 'shift-swap.reports.single-rest-day.show';
		}
		elseif($swap_type == '2rd')
		{
			$view = 'shift-swap.reports.2-rest-day.show';
		}
		elseif($swap_type == 'shrd')
		{
			$view = 'shift-swap.reports.shift-rest-day.show';
		}

		$data = compact('rows');

        $action = 'Enterprise Download ' . $file->name . ' Shift Swap for ' . $shift_date;
        ActivityLogger::store($request->segment(1), $action);

    	return response()->view($view, $data, 200)
    			->header('Content-Disposition', 'attachment; filename="' . $file->name . ' Swap (' . $date_now . ').xls"')
    			->header('Content-Type', 'application/vnd.ms-excel');
	}
}
