<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ForecastVOT;
use App\ForecastVOTRequest;
use App\ForecastVOTRequestItem;
use App\ForecastVTO;
use App\ForecastVTORequest;
use App\ForecastVTORequestItem;
use \Carbon\Carbon;
use Auth;
use ActivityLogger;

class ForecastReportsController extends Controller
{
	public function index(Request $request)
	{
        $user = Auth::user();
        $now = Carbon::now();

    	$data = compact('user', 'now');
    	return view('vforecast.reports.index', $data);
	}

	public function show(Request $request)
	{
		$user = Auth::user();
		$input = $request->only('type', 'date_confirmed');
		$request->validate([ 'type' => 'required', 'date_confirmed' => 'required' ]);

		$center_id = Auth::user()->center_id;
		$type = $request->type;
		$date_confirmed = Carbon::parse($request->date_confirmed)->toDateString();
        $now = Carbon::now();
		$view = '';
		$file = '';

		if($type == 'ot')
		{
			$file = 'Voluntary Overtime';
			$view = 'vforecast.reports.voluntary-overtime';
			$rows = ForecastVOTRequestItem::with(['forecast', 'request'])
					->whereHas('forecast', function($query) use ($center_id){
						$query->whereIn('center_id', [$center_id, 9]);
					})
					->whereHas('request', function($query) use ($date_confirmed){
						$query->whereRaw("DATE(created_at) = '$date_confirmed'");
					})
					->whereHas('request.user', function($query) use ($center_id){
						$query->where('center_id', $center_id);
					})->orderBy('forecast_vot_id', 'ASC')->get();
		}
		elseif($type == 'to')
		{
			$file = 'Voluntary Timeoff';
			$view = 'vforecast.reports.voluntary-timeoff';
			$rows = ForecastVTORequestItem::with(['forecast', 'request'])
					->whereHas('forecast', function($query) use ($center_id){
						$query->whereIn('center_id', [$center_id, 9]);
					})
					->whereHas('request', function($query) use ($date_confirmed){
						$query->whereRaw("DATE(created_at) = '$date_confirmed'");
					})
					->whereHas('request.user', function($query) use ($center_id){
						$query->where('center_id', $center_id);
					})->orderBy('forecast_vto_id', 'ASC')->get();
		}

		$data = compact('rows', 'file');

        $action = 'Download V' . strtoupper($type) . ' for ' . $date_confirmed;
        ActivityLogger::store($request->segment(1), $action);

    	return response()->view($view, $data, 200)
    			->header('Content-Disposition', 'attachment; filename="' . $file . ' (' . $now->toDateTimeString() . ').xls"')
    			->header('Content-Type', 'application/vnd.ms-excel');
	}
}
