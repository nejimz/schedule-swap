<?php

namespace App\Http\Controllers;

use Illuminate\Auth\AuthenticationException;
use Schedule;
use App\SwapAgentReqeust;
use App\SwapAgentSchedule;
use App\SwapAgentInformation;
use App\ForecastVOTRequestItem;
use App\ForecastVTORequestItem;
use Carbon\Carbon;
use Auth;
use Mail;
#use App\Mail\EnterpriseMailer;
use App\Jobs\EnterpriseMailerJob;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }

    public function schedule_json($id)
    {
        $user_id = $id;
        $now = Carbon::now();
    	$datetime_now = $now->toDateTimeString();
    	$information = SwapAgentInformation::whereUserId($user_id)->first(['avaya', 'tier', 'skill', 'wp']);
        $schedules = Schedule::schedules_home($user_id);

        if(is_null($information))
        {
            $information = [];
        }

    	$data = [ 	'information'=>$information, 'schedules'=>$schedules ];

    	return response()->json($data);
    }

    public function passwords($number)
    {
        /*
        #$to = ['amber.goylos@panasiaticcallcenters.com', 'BAC_IT@panasiaticcallcenters.com', 'jaasong@panasiaticsolutions.com', 'nejimz1101890@gmail.com', 'ansleytroy@yahoo.com', 'atdquinit@panasiaticsolutions.com'];
        $to = ['jaasong@panasiaticsolutions.com', 'jccajeda@panasiaticsolutions.com'];
        #$to = ['amber.goylos@panasiaticcallcenters.com', 'nbzaragoza@panasiaticsolutions.com'];
        $from = 'no-reply@bac-jumpsrv01.com';
        $subject = 'One Time Password';
        #Mail::to($to)->send(new EnterpriseMailer());
        Mail::send('email.test', [], function($mail) use ($to, $from, $subject) {
            $mail->from($from);
            $mail->to($to);
            $mail->subject($subject);
        });
        echo 'Sent';
        exit;*/
        

        $when = Carbon::now()->addSeconds(5);
        $job = (new EnterpriseMailerJob())->delay($when);
        dispatch($job);
        echo Carbon::now() . '<br />';
        echo '<table>';
        for ($i = 0; $i < $number; $i++)
        { 
            $password = str_random(8);
            #echo '<tr><td>' . $password . '</td><td>' . bcrypt($password) . '</td></tr>';
            echo '<tr><td>' . $password . '</td></tr>';
        }
        echo '</table>';
        exit;
    }
}
