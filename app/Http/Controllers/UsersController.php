<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use Auth;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $search = '';
        $query = new User;

        if($request->has('search'))
        {
            $search = $request->search;

            $query->where('name', 'LIKE', "$search%")
            ->orWhere('email', 'LIKE', "$search%");
        }
        
        $users = $query->where('center_id', Auth::user()->center->id)->paginate(50);

        $data = compact('users', 'search');

        return view('users.index', $data);
    }
    
    public function create()
    {
    	$name = old('name');
    	$email = old('email');
    	$access = old('access');
    	$active = old('active');
    	$route = route('users.store');

    	$data = compact('route', 'name', 'email', 'access', 'active');

        return view('users.form', $data);
    }
    
    public function store(Requests\UserStoreRequest $request)
    {
        $input = $request->except('_token');
        $input['password'] = "";
        $input['center_id'] = Auth::user()->center->id;

        User::create($input);
        
        if($request->ajax())
        {
            return response()->json(['success' => 'User Successfully added!']);
        }
        return redirect()->route('users.create')->with('success', 'User Successfully added!');
    }
    
    public function edit($id)
    {
    	$route = route('users.update', $id);
        $row = User::whereId($id)->first();
    	$name = $row->name;
    	$email = $row->email;
    	$access = $row->access;
    	$active = $row->active;

    	$data = compact('route', 'name', 'email', 'access', 'active');

        return view('user.create-form', $data);
    }
    
    public function update($id, Request $request)
    {
        #dd($request->all());
        $input = $request->except('_token', '_method');

        User::whereId($id)->update($input);

        return redirect()->route('users.edit', $id)->with('success', 'User Successfully updated!');
    }
    
    public function show($id)
    {
        $row = User::whereId($id)->first();
        $name = $row->name;
        $email = $row->email;
        $access = $row->access;
        $active = $row->active;

        $data = compact('name', 'email', 'access', 'active');

        return view('user.show', $data);
    }

    public function generate_password(Request $request)
    {
        $password = str_random(10);

        User::whereId($request->id)->update([ 'password'=>bcrypt($password) ]);

        return response()->json([ 'password'=>$password ]);
    }

    public function reset_password_form()
    {
        $route = route('reset_password');

        $data = compact('route');

        return view('user.reset', $data);
    }

    public function reset_password(Request $request)
    {
        $request->validate([
            'password' => 'required',
            'confirm_password' => 'required|same:password'
        ]);

        $password = bcrypt($request->password);

        User::whereId(Auth::user()->id)->update([ 'password'=>$password ]);

        return redirect()->route('reset_password_form')->with('success', 'Password Successfully changed!');
    }
}
