<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Validation;
use ActivityLogger;

class UserAccountController extends Controller
{
	public function edit($id)
	{
		$user = Auth::user();
		$row = User::whereId($user->id)->first();
		$route = route('account.update', $id);

		$data = compact('row', 'route');

        return view('users.account.index', $data);
	}

	public function update($id, Request $request)
	{
		
		if($request->has('email')) // Update Email
		{
			$request->validate([
				'email' => 'required|email|unique:users,email,' . auth()->user()->id . ',id'
			]);
			$input = $request->only('email');
			User::whereId($id)->update($input);
		} 
		elseif($request->has('answer')) // Update Security Qeustion
		{
			$request->validate([
				'security_challenge' => 'required',
				'answer' => 'required'
			]);
			$user = User::find($id);

			$security_question = \App\SecurityQuestions::find($request->security_question_id);

			if($security_question->security_challenge_id == $request->security_challenge)
			{

				if($security_question->answer == $request->answer)
				{
					return redirect()->route('account.edit', $id)->withErrors('No updates were made!');
				}
			}
			else
			{
				if(auth()->user()->questions()->where('security_challenge_id', $request->security_challenge)->exists())
				{
					return redirect()->route('account.edit', $id)->withErrors('Security challenge is already in used!');
				}
			}
			
			$security_question->security_challenge_id = $request->security_challenge;
			$security_question->answer = $request->answer;
			$security_question->save();
		} 
		elseif($request->has('old_password')) // Update Password
		{
			$rules = [
				'old_password' => 'required',
				'new_password' => 'required|confirmed|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[@*&`~!$#%]).*$/',
			];
	
			$message = [
				'regex' => 'Password must contain: </br>
							a minimum of 1 lower case letter [a-z] and </br>
							a minimum of 1 upper case letter [A-Z] and </br>
							a minimum of 1 numeric character [0-9] and </br>
							a minimum of 1 special character: @!$#%*'
			];
	
			$this->validate($request, $rules, $message);

			$credentials = ['username' => auth()->user()->username, 'password' => $request->old_password];
			if(!Auth::validate($credentials))
			{
				return redirect()->route('account.edit', $id)->withErrors('Old password is incorrect!');
			}

			$input['password'] = bcrypt($request->new_password);
			User::whereId($id)->update($input);
			
	        $action = 'Changed Password';
	        ActivityLogger::store($request->segment(1), $action);
		}

		return redirect()->route('account.edit', $id)->with('success', 'Account successfully updated!');
	}

	public function download(Request $request)
	{
        $action = 'Download ' . $request->type . ' File ';
        ActivityLogger::store($request->segment(1), $action);
	}
}
