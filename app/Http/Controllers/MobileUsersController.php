<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ActivityLogger;
use Auth;

class MobileUsersController extends Controller
{
    public function auth(Request $request)
    {
        ActivityLogger::store($request->segment(1), 'Login');
        return Auth::user(); 
    }
}
