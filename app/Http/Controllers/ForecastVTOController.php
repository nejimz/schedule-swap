<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Center;
use App\ForecastVTO;
use App\ForecastVTORequest;
use App\ForecastVTORequestItem;
use App\SwapAgentReqeust;
use App\SwapAgentInformation;
use \Carbon\Carbon;
use DB;
use Auth;
use Validation;
use Schedule;
use VOT;
use VTO;
use ActivityLogger;

class ForecastVTOController extends Controller
{
	public function index()
	{
		return view('vto.index');
	}

	public function store(Request $request)
	{
		$items = $request->items;
		$prev_items = ($request->has('prev_items'))? $request->prev_items : [];

		if ($request->has('items')) {
			$skip = VTO::check_skip($items);
			$skip_first_1hour = VTO::skip_first_1hour($items);
			$clean_items = VTO::clean_items($prev_items, $items);
			if ($request->has('expr_items')) {
				$expr_items = $request->expr_items;
				$items = VOT::clean_items($expr_items, $items);
				$clean_items = VOT::clean_items($prev_items, $items);
				$request->request->add(['skip' => $skip, 'items' => $items]);
			}
			$only_even_number = count($clean_items)%2;
			$request->request->add(['skip_first_1hour' => $skip_first_1hour, 'skip' => $skip, 'minimum' => 0, 'only_even_number' => $only_even_number]); 
		}else{		
            $json['errors']['buddy_schedule'][0] = 'No slots has been selected.';
			return response()->json((object) $json, 422);
		}

		$request->validate([
			'confirmation' => 'accepted',
			#'items' => 'required|array|min:2|max:16',
			'skip' => 'max:1',
			'skip_first_1hour' => 'accepted',
			'only_even_number' => 'lte:minimum'
		],[
			'confirmation' => 'Confirmation',
			/*'items.array' => 'Please select a Schedule.',
			'items.required' => 'Please select a Schedule.',
			'items.min' => '1 hour is required.',
			'items.max' => 'You may not have more than 8 hours.',*/
			'skip.max' => 'Only one gap on plotted timeoff is allowed!',
			'skip_first_1hour.accepted' => 'First one hour is required!',
			'only_even_number.lte' => 'Please plot in an hourly interval.'
		]);

		$user = Auth::user();
		$date = $request->vto_shift_date;

		$exists = VOT::exists($date, $user->id);

		if($exists) {
            $json['errors']['buddy_schedule'][0] = 'Cannot avail Time-Off. You`ve already applied an Overtime.';
			return response()->json((object) $json, 422);
		}

		$json = [];
		$now = Carbon::now();

		foreach($clean_items as $item) {
			$vto = ForecastVTO::whereId($item)->orderBy('created_at', 'DESC')->first();
			$expired_at = Carbon::parse($vto->expired_at);
			$vto_item = ForecastVTORequestItem::leftJoin('forecast_vto_request', 'forecast_vto_request_item.forecast_vto_request_id', '=', 'forecast_vto_request.id')->where('user_id', $user->id)->whereForecastVtoId($item)->toSql();

			if($vto_item > 0)
			{
                $json['errors']['buddy_schedule'][0] = 'You`ve already applied for ' . $vto->schedule . '.';
				return response()->json((object) $json, 422);
			}
			elseif($now->gte($expired_at))
			{
                $json['errors']['buddy_schedule'][0] = $vto->schedule . ' already expired.';
				return response()->json((object) $json, 422);
			}
			elseif($vto->item_exists == 0 && $vto->remaining_slots == 0)
			{
                $json['errors']['buddy_schedule'][0] = 'No slot/s available for ' . $vto->schedule . '.';
				return response()->json((object) $json, 422);
			}
		}

		$row = ForecastVTORequest::create([ 'user_id'=>$user->id ]);

		foreach ($request->items as $item)
		{
			$vto = ForecastVTO::whereId($item)->first();
			if($vto->item_exists == 0)
			{
				ForecastVTORequestItem::create([
					'forecast_vto_request_id'=>$row->id,
					'forecast_vto_id'=>$item
				]);
			}
		}

        $action = 'VTO Request (' . $row->id . ') - Shift Swap';
        ActivityLogger::store($request->segment(1), $action);

        return response()->json(['success'=>'Voluntary Timeoff successfully plotted!', 'shift_date'=>$request->vto_shift_date, 'schedule'=>$request->vto_schedule]);
	}

	/*

	*/

	public function list($date, $schedule)
	{
		$list = [];
		$user = Auth::user();
		$now = Carbon::now();
		$schedule_off = $schedule;
		$schedule = explode('-', $schedule);
		$information = SwapAgentInformation::whereUserId($user->id)->first();
		$rows = ForecastVTO::whereIn('center_id', [$user->center_id, 9])->whereShiftDate($date)
				->where(function($query) use ($information){
					$query->whereOrganization($information->skill_clean);
					if ($information->is_tier_3) {
						// $skill = strtoupper($information->skill);
						// if(strpos( $skill, "ERD" )) {
						//   $skill = str_replace("ERD ".auth()->user()->center->code, "SUPERD", $skill);
						// }else{
						//   $skill = str_replace("SUP ".auth()->user()->center->code, "SUPERD", $skill);
						// }
						$skill = $information->skill_clean;
						$skill = $skill = str_replace(" ERD", "", strtoupper($skill)); // Remove ERD;
						$skill = $skill = str_replace(" SUP", "", strtoupper($skill)); // Remove SUP;
						
						$query = $query->orWhere('organization', 'LIKE', '%'.$skill.'%')
										->where('organization', 'LIKE' , '%SUPERD%');
					}
				})->orderBy('time_start', 'ASC')->get();
		$schedules = [];

		if(count($schedule) > 1) {
			$start = Carbon::parse($date . ' ' . $schedule[0]);
			$end = Carbon::parse($date . ' ' . $schedule[1]);

			if($start->gte($end)) {
				$end = Carbon::parse($date . ' 24:00');
			}

		} else {
			$start = Carbon::parse($date . ' 00:00:00');
			$end = Carbon::parse($date . ' 23:59:59');
		}

        $vto_items = ForecastVTORequestItem::whereHas('forecast', function($query) use ($date){
                    $query->where('shift_date', $date);
                })->whereHas('request', function($query) use ($user){
                    $query->where('user_id', $user->id);
                })->get(['forecast_vto_id'])->toArray();
		$vto_items = array_flatten($vto_items);
		$last_hour_end_time = count($schedules) - 1;
		#dump($schedule);
		foreach ($rows as $row) {
			$status = 1;
			$start_time = Carbon::parse($date . ' ' . $row->time_start);
			$end_time = Carbon::parse($date . ' ' . $row->time_end);
			$expired_at = Carbon::parse($row->expired_at);
			
			#if($row->time_end == '00:00:00') {
			if(in_array($row->time_end, ['00:00:00', '01:00:00', '02:00:00', '03:00:00', '04:00:00'])) {
				$end_time->addDay();
			}

			if(in_array($row->id, $vto_items)) {
				$status = 2;
			} else {
				if($schedule_off != 'Off' && $start_time->between($start, $end) && $end_time->between($start, $end)) {
					$status = 0;
				}
			}

			if(!VTO::check_superd($row->organization, $information->skill)) {					
				$status = 1;
			}

			if($now->gte($expired_at)) {
				$status = 3;
			}
			
			$list[$row->id] = [
				'id' => $row->id,
				'status' => $status,
				'schedule' => $row->schedule,
				'slots' => $row->remaining_slots
			];
		}
		#dump($schedules);dump($rows->toArray());dd($list);

        return response()->json($list);
	}

	public function calendar(Request $request)
	{
		$calendar_dates = '';
		$date = Carbon::parse($request->calendar_month);
		$tempDate = Carbon::createFromDate($date->year, $date->month, 1);
		$skip = $tempDate->dayOfWeek;

		$user = Auth::user();
		$information = SwapAgentInformation::whereUserId($user->id)->first();
		#dd($information->skill_clean);
		for($i = 0; $i < $skip; $i++) {
			$tempDate->subDay();
		}
		
		do {
			for($i = 0; $i < 7; $i++) {
				$onclick = '';
				$highlight_active_temp = '';
				$highlight_active = '';
				$title = '';
				#$schedule = $this->schedule($user->id, $tempDate->toDateString());
				$schedule = Schedule::schedule_shift_date($user->id, $tempDate->toDateString());

				if(count($schedule) > 0) {
					$shift_date = ($schedule['new_shift_date'] == '')? $schedule['shift_date'] : $schedule['new_shift_date'];
					$shift_schedule = ($schedule['new_schedule'] == '')? $schedule['schedule'] : $schedule['new_schedule'];
					$vtos = ForecastVTO::whereIn('center_id', [$user->center_id, 9])->whereShiftDate($tempDate->toDateString())
							->whereOrganization($information->skill_clean)->orderBy('time_start', 'ASC')->first();

					if($vtos) {
						$highlight_active_temp = 'is-active';
						$highlight_active = 'is-active';
						$title = ' Slot/s';
						$onclick = "onclick=\"vto_request('$shift_date', '$shift_schedule')\"";
					} elseif ($information->is_tier_3) {
						// $skill = strtoupper($information->skill);
			            // if(strpos( $skill, "ERD" )) {
			            //   $skill = str_replace("ERD ".auth()->user()->center->code, "SUPERD", $skill);
			            // }else{
			            //   $skill = str_replace("SUP ".auth()->user()->center->code, "SUPERD", $skill);
						// }
						
						$skill = $information->skill_clean;
						$skill = $skill = str_replace(" ERD", "", strtoupper($skill)); // Remove ERD;
						$skill = $skill = str_replace(" SUP", "", strtoupper($skill)); // Remove SUP;

			            $superd = ForecastVTO::whereIn('center_id', [$user->center_id, 9])
								->whereShiftDate($tempDate->toDateString())
								->where('organization', 'LIKE', '%SUPERD%')
								->where('organization', 'LIKE', '%'.$skill.'%')
								->orderBy('time_start', 'ASC')->first();

						// $superd = ForecastVTO::whereIn('center_id', [$user->center_id, 9])->whereShiftDate($tempDate->toDateString())
						// 		->where('organization', 'LIKE', '%SUPERD%')
						// 		->where(function($query) {
						// 			$query->where('organization', 'LIKE', '%ERD%')
						// 				  ->orWhere('organization', 'LIKE', '%SUP%');
						// 		})->orderBy('time_start', 'ASC')->first();
						// dump($superd);
						if ($superd) {
							if(VTO::check_superd($superd->organization, $information->skill))
							{
								$highlight_active = 'is-active';
								$title = ' Slot/s';
								$onclick = "onclick=\"vto_request('$shift_date', '$shift_schedule')\"";
							}
						}
					}
				}

				if($tempDate->month == $date->month) {
					$calendar_dates .= '<div class="calendar-date"><button class="date-item ' . $highlight_active . '" title="' . $title . '" ' . $onclick . '>'. $tempDate->day . '</button></div>';
				} else {
					$calendar_dates .= '<div class="calendar-date is-disabled""><button class="date-item ' . $highlight_active_temp . '">' . $tempDate->day . '</button></div>';
				}
				
				$tempDate->addDay();
			}
		}
		while($tempDate->month == $date->month);
		return $calendar_dates;
	}

}
