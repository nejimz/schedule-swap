<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SwapAgentSchedule;
use App\Center;
use \Carbon\Carbon;
use Auth;

class ShiftSwapSchedulesController extends Controller
{
	public function index()
	{
		$rows = Center::orderBy('name', 'ASC')->get();
		$week_endings = $this->schedule_week_endings(1);
		#dd($week_endings);
		$data = compact('rows', 'week_endings');
    	return view('shift-swap.schedule.index', $data);
	}



	public function center_schedule($center_id, $week_ending, Request $request)
	{
		$user = Auth::user();
		if($user->center_id != $center_id) {
			if(!$user->isRoot()) {
				abort(404);
			}
		}

		$search = '';
		$week_ending_count = $this->week_ending_count($center_id, $week_ending);
		$center = Center::whereId($center_id)->orderBy('name', 'ASC')->first();
		$query = SwapAgentSchedule::with(['user', 'user.information']);

		if($request->has('search') && $request->search != '') {
			$search = trim($request->search);
			$query->whereHas('user.information', function($query) use ($search){
				$query->where('avaya', $search);
			});
		}

		$rows = $query->whereCenterId($center_id)->whereWeekEnding($week_ending)->orderBy('user_id', 'DESC')->orderBy('shift_date', 'DESC')->paginate(49);

		$data = compact('center', 'rows', 'search', 'week_ending', 'week_ending_count');
    	return view('shift-swap.schedule.schedule', $data);
	}

	public function center_week_ending($center_id)
	{
		$week_endings = $this->schedule_week_endings($center_id);
        return response()->json(['week_endings'=>$week_endings]);
	}

	public function week_ending_count($center_id, $date)
	{
		$count = SwapAgentSchedule::whereCenterId($center_id)->whereWeekEnding($date)->count();
        return $count;
	}

	public function schedule_week_endings($center_id)
	{
		$n = 0;
		$week_endings = [];
		$row = SwapAgentSchedule::orderBy('shift_date', 'ASC')->first();
		$start_date = Carbon::parse($row->week_ending);
		$end_date = Carbon::now();
		$end_date->endOfWeek();
		$end_date->addDays(7);

		while($end_date > $start_date) {
			$count = 0;
			$restriction_count = 0;
			$week_endings[$n]['date'] = $end_date->toDateString();
			$week_endings[$n]['count'] = $count;
			$week_endings[$n]['restriction'] = $restriction_count;
			$end_date->subDays(7);
			$n++;
		}

		return $week_endings;
	}
}
