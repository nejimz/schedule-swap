<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\SwapType;
use App\SwapAgentReqeust;
use App\SwapAgentInformation;
use App\SwapAgentSchedule;
use \Carbon\Carbon;
use DB;
use Auth;
use Validation;
use Response;
use ActivityLogger;

class ShiftSwapReportsController extends Controller
{
	public function index(Request $request)
	{
        $user_id = Auth::user()->id;
        $now = Carbon::now();
    	$date_now = $now->toDateString();
        $swap_types = SwapType::orderBy('id', 'ASC')->get();

    	$data = compact('date_now', 'swap_types');

    	return view('shift-swap.reports.index', $data);
	}

	public function show(Request $request)
	{
		$input = $request->only('swap_type', 'date_confirmed');
		$request->validate([ 'swap_type' => 'required', 'date_confirmed' => 'required' ]);

		$user = Auth::user();
		$center_id = $user->center_id;
		$swap_type = $request->swap_type;
		$date_confirmed = Carbon::parse($request->date_confirmed)->toDateString();

		$rows = SwapAgentReqeust::whereCenterId($center_id)->whereSwapType($swap_type)->whereStatus(1)
				->whereRaw("DATE(updated_at) = '$date_confirmed'")
				->orderBy('created_at', 'ASC')->get();
		$view = '';
        $date_now = Carbon::now()->toDateTimeString();
		$file = SwapType::whereAbbr($swap_type)->first();

		if($swap_type == 'sd')
		{
			$view = 'shift-swap.reports.single-day.show';
		}
		elseif($swap_type == 'srd')
		{
			$view = 'shift-swap.reports.single-rest-day.show';
		}
		elseif($swap_type == '2rd')
		{
			$view = 'shift-swap.reports.2-rest-day.show';
		}
		elseif($swap_type == 'shrd')
		{
			$view = 'shift-swap.reports.shift-rest-day.show';
		}

        $action = 'Download ' . $file->name . ' Shift Swap for ' . $date_confirmed;
        ActivityLogger::store($request->segment(1), $action);

		$data = compact('rows');

    	return response()->view($view, $data, 200)
    			->header('Content-Disposition', 'attachment; filename="' . $file->name . ' Swap (' . $date_now . ').xls"')
    			->header('Content-Type', 'application/vnd.ms-excel');
	}
}
