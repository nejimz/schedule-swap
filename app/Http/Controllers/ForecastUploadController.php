<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Center;
use App\ForecastVOT;
use App\ForecastVOTRequest;
use App\ForecastVOTRequestItem;
use App\ForecastVTO;
use App\ForecastVTORequest;
use App\ForecastVTTRequestItem;
use \Carbon\Carbon;
use Auth;

class ForecastUploadController extends Controller
{
    public function index()
	{
		$centers = Center::orderBy('name', 'ASC')->get();

		$data = compact('centers');

		return view('vforecast.upload', $data);
	}

	public function store(Request $request)
	{
      	$number_of_mns_expired_at = 30;
		$user_id = Auth::user()->id;
		$type = $request->type;
		$center_id = $request->center_id;
        $rows = json_decode($request->row, TRUE);
	    #dd($rows);
        foreach($rows as $row) {
            $organization = trim($row[0]);
            $slots = trim($row[1]);
            $shift_date = Carbon::parse($row[2]);
            $time_start = Carbon::parse($shift_date->toDateString() . ' ' . trim($row[3]));
            $time_end = Carbon::parse($shift_date->toDateString() . ' ' . trim($row[4]));
	        $expired_at = Carbon::parse($shift_date->toDateString() . ' ' . $time_start->toTimeString());
	        $expired_at->subMinutes($number_of_mns_expired_at);

	        if ($type == 'vot') {
	        	$forecast = ForecastVOT::create([
	        		'center_id' => $center_id,
	        		'organization' => $organization,
	        		'slots' => $slots,
	        		'shift_date' => $shift_date->toDateString(),
	        		'time_start' => $time_start->toTimeString(),
	        		'time_end' => $time_end->toTimeString(),
	        		'user_id' => $user_id,
	        		'expired_at' => $expired_at->toDateTimeString(),
	        	]);
	        } else {
	        	$forecast = ForecastVTO::create([
	        		'center_id' => $center_id,
	        		'organization' => $organization,
	        		'slots' => $slots,
	        		'shift_date' => $shift_date->toDateString(),
	        		'time_start' => $time_start->toTimeString(),
	        		'time_end' => $time_end->toTimeString(),
	        		'user_id' => $user_id,
	        		'expired_at' => $expired_at->toDateTimeString(),
	        	]);
	        }

	        /*dump($shift_date->toDateString());
	        dump($time_start->toTimeString());
	        dump($time_end->toTimeString());
	        dd($expired_at->toDateTimeString());*/

        }
    	return response()->json(['success'=>true]);
	}
}
