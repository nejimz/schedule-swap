<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\SwapType;
use App\SwapAgentReqeust;
use App\SwapAgentInformation;
use App\SwapAgentSchedule;
use App\MailRecipients;
use App\User;
use App\ActivityLog;
use \Carbon\Carbon;
use VOT;
use VTO;
use DB;
use Auth;
use Validation;
use Mail;
use Schedule;
use ShiftSwap;
use ActivityLogger;
#use MaileRecipientsHelper;

class ShiftSwapController extends Controller
{
    public function index()
    {
        $user_id = Auth::user()->id;
        $now = Carbon::now();
    	$datetime_now = $now->toDateTimeString();
        $swap_types = SwapType::orderBy('id', 'ASC')->get();
        
    	$week_endings = SwapAgentSchedule::where('user_id', $user_id)->where('shift_date', '>=', $datetime_now)
                        ->groupBy('week_ending')->orderBy('week_ending', 'ASC')->get(['week_ending']);

    	$data = compact('week_endings', 'swap_types');

    	return view('shift-swap.index', $data);
    }

    public function store(Requests\ShiftSwapStorePost $request)
    {
        $date_now = Carbon::now();
        $user = Auth::user();
        $my_user_id = $user->id;
        $json = ['errors'=>[]];
        $earliest_date = '';
        $my = SwapAgentInformation::whereUserId($my_user_id)->orderBy('id', 'DESC')->first();
        $buddy = SwapAgentInformation::whereAvaya($request->buddy_avaya)->orderBy('id', 'DESC')->first();

        // 2 Rest Day Swap
        if($request->swap_type == '2rd') {
            $data = $request->only('swap_type', 'week_ending');
            // Not same work pattern
            if($my->wp != $buddy->wp) {
                $json['errors']['buddy_schedule'][0] = 'Sorry, 2 Rest Day Swap not allowed. Not the same Work Pattern.';
                return response()->json((object) $json, 422); 
            }
            //  check first RD if no longer available
            #$rest_days = $this->agents_rest_days($request->week_ending, $my_user_id, $buddy->user_id);
            $rest_days = Schedule::agents_rest_days($request->week_ending, $my_user_id, $buddy->user_id);
            $first_rest_day = array_flatten(array_first($rest_days));
            $first_rest_day = $first_rest_day[0];
            $first_rest_day = Carbon::parse($first_rest_day . ' 00:00:00');
            if($date_now->gte($first_rest_day)) {
                $json['errors']['buddy_schedule'][0] = 'Sorry, 2 Rest Day Swap not allowed. Other Rest Day is no longer available.';
                return response()->json((object) $json, 422); 
            }
            // Check Agents Rest Days, if have restrictions
            $rest_days_restrictions = Schedule::agents_4_rest_day_with_shift_dates_has_restrictions($request->week_ending, $my_user_id, $buddy->user_id);
            if(count($rest_days_restrictions) > 0) {
                $message = '';
                foreach ($rest_days_restrictions as $row)
                {
                    $name_tag = 'Agent ' . $row->name . ' has';
                    if($row->user_id == $my_user_id)
                    {
                        $name_tag = 'You have';
                    }
                    $message .= ' ' . $name_tag . ' ' . $row->restrictions . ' on ' . $row->shift_date;
                }
                $json['errors']['buddy_schedule'][0] = 'Sorry, 2 Rest Day Swap not allowed. ' . $message;
                return response()->json((object) $json, 422); 
            }

            $earliest_date = $first_rest_day;
            $employee_shift_date = $request->employee_shift_date;
            $buddy_shift_date = $request->buddy_shift_date;
        }
        else
        {
            $data = $request->only('swap_type', 'buddy_schedule');

            $my_shift = explode('_', $request->my_shift_date);
            $buddy_shift = explode('_', $request->buddy_shift_date);
            $my_shift_schedule = explode('-', $my_shift[1]);
            $buddy_shift_schedule = explode('-', $buddy_shift[1]);
            
            if ($my_shift[1] == 'Off') {
                $my_shift_schedule_start = '00:00';
                $my_shift_schedule_end = '00:00';
            } else {
                $my_shift_schedule_start = Carbon::parse($my_shift_schedule[0])->format('H:i');
                $my_shift_schedule_end = Carbon::parse($my_shift_schedule[1])->format('H:i');
            }
            if ($buddy_shift[1] == 'Off') {
                $buddy_shift_schedule_start = '00:00';
                $buddy_shift_schedule_end = '00:00';
            } else {
                $buddy_shift_schedule_start = Carbon::parse($buddy_shift_schedule[0])->format('H:i');
                $buddy_shift_schedule_end = Carbon::parse($buddy_shift_schedule[1])->format('H:i');
            }

            $employee_shift_date = Carbon::parse($my_shift[0]);
            $buddy_shift_date = Carbon::parse($buddy_shift[0]);

            $employee_shift_date_week_ending = Carbon::parse($my_shift[0])->endOfWeek()->toDateString();
            $buddy_shift_date_week_ending = Carbon::parse($buddy_shift[0])->endOfWeek()->toDateString();

            $my_shift_start = Carbon::parse($my_shift[0] . ' ' . $my_shift_schedule_start);
            $my_shift_end = Carbon::parse($my_shift[0] . ' ' . $my_shift_schedule_end);

            $buddy_shift_start = Carbon::parse($buddy_shift[0] . ' ' . $buddy_shift_schedule_start);
            $buddy_shift_end = Carbon::parse($buddy_shift[0] . ' ' . $buddy_shift_schedule_end);

            #if ($my_shift_end->format('H:i') == '00:00') {
            if(in_array($my_shift_end->hour, [0, 1, 2, 3, 4])) {
                $my_shift_end->addDay();
            }
            #if ($buddy_shift_end->format('H:i') == '00:00') {
            if(in_array($buddy_shift_end->hour, [0, 1, 2, 3, 4])) {
                $buddy_shift_end->addDay();
            }

            $my_duration = $my_shift_end->diffInHours($my_shift_start);
            $buddy_duration = $buddy_shift_end->diffInHours($buddy_shift_start);

            $data['employee_shift_date'] = $employee_shift_date->toDateString();
            $data['employee_schedule'] = $my_shift[1];
            $data['buddy_shift_date'] = $buddy_shift_date->toDateString();
            $data['week_ending'] = $employee_shift_date_week_ending;

            if($employee_shift_date_week_ending != $buddy_shift_date_week_ending) {
                $json['errors']['buddy_schedule'][0] = 'Sorry, Swap not allowed. Your Week Ending are not the same.';
                return response()->json((object) $json, 422); 
            // Single Day Swap
            } elseif($request->swap_type == 'sd' && $data['employee_shift_date'] != $data['buddy_shift_date']) {
                $json['errors']['buddy_schedule'][0] = 'Sorry, swap not allowed. Shift Date are not the same.';
                return response()->json((object) $json, 422);
            } elseif($request->swap_type == 'sd' && $data['employee_schedule'] == $data['buddy_schedule']) {
                $json['errors']['buddy_schedule'][0] = 'Sorry, swap not allowed. You have the same Work Pattern.';
                return response()->json((object) $json, 422);
            } elseif($request->swap_type == 'sd' && $my_duration != $buddy_duration) {
                $json['errors']['buddy_schedule'][0] = 'Sorry, swap not allowed. Shift schedule does not matched.';
                return response()->json((object) $json, 422);
            // Single Rest Day Swap
            // } elseif($request->swap_type == 'srd' && $data['employee_schedule'] != $data['buddy_schedule']) {
            //     $json['errors']['buddy_schedule'][0] = 'Sorry, Swap not allowed. Your Work Pattern are not the same.';
            //     return response()->json((object) $json, 422); 
            } elseif($request->swap_type == 'srd' && $data['employee_shift_date'] == $data['buddy_shift_date']) {
                $json['errors']['buddy_schedule'][0] = 'Sorry, swap not allowed. You have the same Shift Date and Schedule.';
                return response()->json((object) $json, 422); 
            } elseif($request->swap_type == 'srd' && Schedule::srd_other_shift($my_user_id, $data['buddy_shift_date'])) {
                $json['errors']['buddy_schedule'][0] = 'Sorry, swap not allowed. Your schedule on ' . $data['employee_shift_date'] . ' and ' . $data['buddy_shift_date'] . ' is Off.';
                return response()->json((object) $json, 422); 
            } elseif($request->swap_type == 'srd' && Schedule::srd_other_shift($buddy->user_id, $data['employee_shift_date'])) {
                $json['errors']['buddy_schedule'][0] = 'Sorry, swap not allowed. Your buddy`s schedule on ' . $data['employee_shift_date'] . ' and ' . $data['buddy_shift_date'] . ' is Off.';
                return response()->json((object) $json, 422); 
            }
            // dd($buddy_duration);
            /* elseif($request->swap_type == 'srd' && !in_array($user->center_id, [7]) && $my->wp != $buddy->wp) {
                $json['errors']['buddy_schedule'][0] = 'Sorry, swap not allowed. Work Pattern are not the same.';
                return response()->json((object) $json, 422); 
            }

            // Shift Rest Day Swap
            /*if($request->swap_type == 'shrd') {
                $shrd_my_shift_date = SwapAgentSchedule::whereUserId($my_user_id)->whereIn('shift_date', [$employee_shift_date, $buddy_shift_date])->orderBy('shift_date', 'ASC')->get(['shift_date', 'schedule'])->toArray();
                $shrd_buddy_shift_date = SwapAgentSchedule::whereUserId($buddy->user_id)->whereIn('shift_date', [$employee_shift_date, $buddy_shift_date])->orderBy('shift_date', 'ASC')->get(['shift_date', 'schedule'])->toArray();

                if($shrd_my_shift_date[0]['schedule'] == $shrd_buddy_shift_date[0]['schedule'])
                {
                    $json['errors']['buddy_schedule'][0] = 'Sorry, swap not allowed. You have the same Work Pattern';
                    return response()->json((object) $json, 422);
                }
                elseif($shrd_my_shift_date[1]['schedule'] == $shrd_buddy_shift_date[1]['schedule'])
                {
                    $json['errors']['buddy_schedule'][0] = 'Sorry, swap not allowed. You have the same Work Pattern';
                    return response()->json((object) $json, 422);
                }
            }*/

            // Check Shift Dates Restrictions
            $shift_date_restrictions = Schedule::shift_date_restrictions($data['week_ending'], $my_user_id, $buddy->user_id, $data['employee_shift_date'], $data['buddy_shift_date']);
            if($shift_date_restrictions != '') {
                $json['errors']['buddy_schedule'][0] = $shift_date_restrictions;
                return response()->json((object) $json, 422); 
            }

            $earliest_date = Schedule::earliest_date($my_shift[0], $my_shift_schedule[0], $buddy_shift[0], $buddy_shift_schedule[0]);
            
            $earliest_date = Carbon::parse($earliest_date);
            $employee_shift_date = $data['employee_shift_date'];
            $buddy_shift_date = $data['buddy_shift_date'];
        }

        $has_swap_request = ShiftSwap::agent_has_swap_request($request->swap_type, $data['week_ending'], $employee_shift_date, $buddy_shift_date, $my_user_id, $buddy->user_id);

        if($has_swap_request != '') {
            $json['errors']['buddy_schedule'][0] = $has_swap_request;
            return response()->json((object) $json, 422); 
        }

        $data['center_id'] = $user->center_id;
        $data['user_id'] = $my_user_id;
        $data['buddy_user_id'] = $buddy->user_id;

        $earliest_date->subHours(6);
        $data['expiration_date'] = $earliest_date->toDateTimeString();
        $earliest_date->subHours(6);
        $data['before_expiration_date'] = $earliest_date->toDateTimeString();
        #dd($data);
        $swap_request_id = SwapAgentReqeust::create($data)->id;
        $swap_request = SwapAgentReqeust::whereId($swap_request_id)->first();

        $action = strtoupper($swap_request->swap_type_name) . ' Request (' . $swap_request->id . ') - Shift Swap';
        ActivityLogger::store($request->segment(1), $action);
        
        return response()->json(['success'=>true]);
    }


    /*

    Confirmation

    */

    public function request_confirmation(Request $request)
    {
        $user = Auth::user();
        $now = Carbon::now();
        $data = ['status_exists'=>0];
        $id = $request->id;
        $status = $request->accepted;
        $row = SwapAgentReqeust::whereId($id)->first();
        $employee_shift_date = (is_null($row->employee_shift_date))? null : $row->employee_shift_date->toDateString();
        $buddy_shift_date = (is_null($row->buddy_shift_date))? null : $row->employee_shift_date->toDateString();

        if(!is_null($row->status)) {
            $data['status_exists'] = 1;
            $data['status'] = $row->status;
            return response()->json($data);
        } elseif($now->gte($row->expiration_date)) {
            $json['errors']['buddy_schedule'][0] = 'Swap Request already expired!';
            return response()->json((object) $json, 422); 
        }

        if ($user->id == $row->user_id) {
            $has_swap_request = ShiftSwap::agent_has_swap_request($row->swap_type, $row->week_ending->toDateString(), $employee_shift_date, $buddy_shift_date, $row->user_id, $row->buddy_user_id);
        } else {
            $has_swap_request = ShiftSwap::agent_has_swap_request($row->swap_type, $row->week_ending->toDateString(), $employee_shift_date, $buddy_shift_date, $row->buddy_user_id, $row->user_id);
        }

        if($has_swap_request != '') {
            $json['errors']['buddy_schedule'][0] = $has_swap_request;
            return response()->json((object) $json, 422); 
        }

        SwapAgentReqeust::whereId($id)->update([ 'status' => $status ]);
        $row = SwapAgentReqeust::whereId($id)->first();
        $recipients = MailRecipients::whereCenterId($row->center_id)->whereModule('shift swap')->get();

        $action = strtoupper($row->status_type) . ' Request (' . $row->id . ') - Shift Swap';
        ActivityLogger::store($request->segment(1), $action);

        // Only approved and recipients has email notification
        if(count($recipients) > 0 && $status == 1) {
            $subject = $row->swap_type_name . ' Shift Swap';
            $this->mailer($recipients, $row, $subject);
            #$module = 'shift swap';
            #MaileRecipientsHelper::mailer($row, $subject, $module);
        }

        $data['username'] = User::find($row->user_id)->username;
        $data['status'] = $status;
        return response()->json($data);
    }

    public function mailer($recipients, $row, $subject)
    {
        $data = [];
        $from = [];
        $to = [];
        $cc = [];
        $bcc = [];
        $n = 0;

        foreach($recipients as $recipient) {
            if($recipient->type == 'from') { $from[$n] = $recipient->mail; }
            if($recipient->type == 'to') { $to[$n] = $recipient->mail; }
            if($recipient->type == 'cc') { $cc[$n] = $recipient->mail; }
            if($recipient->type == 'bcc') { $bcc[$n] = $recipient->mail; }
            $n++;
        }
        $data = compact('row');
        Mail::send('email.shift-swap.' . $row->swap_mail_template, $data, function ($m) use ($from, $to, $cc, $subject) {
            $m->from($from);
            $m->to($to);
            $m->cc($cc);
            $m->bcc($cc);
            $m->subject($subject);
        });
    }

    // For Api
    public function swap_type()
    {
        return SwapType::orderBy('id', 'ASC')->get();
    }
}