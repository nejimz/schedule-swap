<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Schedule;
use App\SwapAgentInformation;
use App\SwapAgentSchedule;
use App\SwapAgentReqeust;
use App\ForecastVOTRequestItem;
use App\ForecastVTORequestItem;
use \Carbon\Carbon;
use DB;
use Auth;

class ShiftSwapInformationController extends Controller 
{
    public function search_json(Request $request)
    {
        $data = [];
        $auth = Auth::user();
        $avaya = $request->search_avaya;

        $my = SwapAgentInformation::whereUserId($auth->id)->orderBy('id', 'DESC')->first();

        if(is_null($my)) {
            $json['errors']['message'][0] = 'No agent information.';
            return response()->json((object) $json, 422); 
        }

        $buddy = SwapAgentInformation::whereCenterId($my->center_id)->where('avaya', $avaya)->orderBy('id', 'DESC')->first();
        #dd($buddy);
        if(!is_null($buddy)) {
            if($my->avaya == $buddy->avaya) {
                $json['errors']['message'][0] = 'Swapping with yourself is not allowed.';
                return response()->json((object) $json, 422); 
            } elseif($my->skill != $buddy->skill) {
                if($my->brand == $buddy->brand && $my->is_tier3 && $buddy->is_tier3) {

                } else {
                    $json['errors']['message'][0] = 'You and Agent ' . $buddy->avaya . ' were not on the same Organization.';
                    return response()->json((object) $json, 422); 
                }
            } elseif($my->tier != $buddy->tier) {
                $json['errors']['message'][0] = 'You and Agent ' . $buddy->avaya . ' were not on the same Tier.';
                return response()->json((object) $json, 422); 
            }
            /*elseif($my->brand != $buddy->brand && (!$my->is_tier3 || !$buddy->is_tier3))
            {
                $json['errors']['message'][0] = 'You and Agent ' . $buddy->avaya . ' were not on the same Organization.';
                return response()->json((object) $json, 422); 
            }*/
            /*elseif($my->skill != $buddy->skill)
            {
                $json['errors']['message'][0] = 'You and Agent ' . $buddy->avaya . ' were not on the same Organization.';
                return response()->json((object) $json, 422); 
            }*/
            /*if(!$my->is_tier3 && !$buddy->is_tier3)
            {
                $json['errors']['message'][0] = 'You and Agent ' . $buddy->avaya . ' were not on the same Organization.';
                return response()->json((object) $json, 422); 
            }*/

            $data['avaya'] = $buddy->avaya;
            $data['skill'] = $buddy->skill;
            $data['tier'] = $buddy->tier;
            $data['wp'] = $buddy->wp;
            $data['name'] = $buddy->user->name;

            $data['user_id'] = $buddy->user->id;
            $data['username'] = $buddy->user->username;

            return response()->json($data);
        } else {
            $json['errors']['message'][0] = 'No agent information.';
            return response()->json((object) $json, 422); 
        }

    }

    public function details_json($id)
    {
        $user_id = $id;
        $now = Carbon::now();
    	$datetime_now = $now->toDateTimeString();
        #$schedule = new Schedule;

    	$information = SwapAgentInformation::whereUserId($user_id)->first(['avaya', 'tier', 'skill', 'wp']);
        $schedules = Schedule::schedules($user_id);
        #$schedules = $schedule->schedules($user_id);
        $my_request = $this->shift_swap_request('request', $user_id);
    	$received_request = $this->shift_swap_request('received_request', $user_id);

        if(is_null($information)) {
            $information = [];
        }
        
    	$data = [ 	'information'=>$information, 'schedules'=>$schedules, 
    				'my_request'=>$my_request, 	 'received_request'=>$received_request ];

    	return response()->json($data);
    }

    public function api_details_json($id)
    {
        $user_id = $id;
        $now = Carbon::now();
    	$datetime_now = $now->toDateTimeString();
        #$schedule = new Schedule;

    	$information = SwapAgentInformation::whereUserId($user_id)->first(['avaya', 'tier', 'skill', 'wp']);
        $schedules = Schedule::api_schedules($user_id);
        #$schedules = $schedule->schedules($user_id);
        $my_request = $this->shift_swap_request('request', $user_id);
    	$received_request = $this->shift_swap_request('received_request', $user_id);

        if(is_null($information)) {
            $information = [];
        }
        
    	$data = [ 	'information'=>$information, 'schedules'=>$schedules, 
    				'my_request'=>$my_request, 	 'received_request'=>$received_request ];

    	return response()->json($data);
    }

    public function shift_swap_request($type, $user_id)
    {
        $n = 0;
        $data = [];
        $date_now = Carbon::now();

        $rows = SwapAgentReqeust::whereUserId($user_id)->orderBy('created_at', 'DESC')->take(5)->get();
        // Check if My Request or Received Request
        if($type == 'received_request') {
            $rows = SwapAgentReqeust::whereBuddyUserId($user_id)->orderBy('created_at', 'DESC')->take(5)->get();
        }

        foreach ($rows as $row) {
            $data[$n] = [
                'tracking_number' => $row->id,
                'created_at' => Carbon::parse($row->created_at)->toDateTimeString(),
                'expired_at' => Carbon::parse($row->expiration_date)->toDateTimeString(),
                'status' => $row->status,
                'user_id' => $row->user->id,
                'buddy_id' => $row->buddy_user->id,
                'buddy_name' => $row->buddy_user->name,
                'buddy_username' => $row->buddy_user->username,
                'swap_type' => $row->swap_type,
                'swap_type_name' => $row->swap_type_name
            ];

            // Check if Expired
            if(is_null($row->status) && $date_now->gte($row->expiration_date)) {
                $data[$n]['status'] = 2;
            }

            // Check if My Request or Received Request
            if($type == 'received_request') {
                $data[$n]['buddy_name'] = $row->user->name;
            }

            if($row->swap_type == 'sd' || $row->swap_type == 'srd') {
                // Empty Week Ending on Single Day
                $data[$n]['week_ending'] = '';
                $data[$n]['employee_shift_date'] = $row->employee_shift_date->toDateString();
                $data[$n]['employee_schedule'] = $row->employee_schedule;
                $data[$n]['buddy_shift_date'] = $row->buddy_shift_date->toDateString();
                $data[$n]['buddy_schedule'] = $row->buddy_schedule;
            } elseif($row->swap_type == 'shrd') {
                $my_shift_date = [];
                $buddy_shift_date = [];

                $schedules = SwapAgentSchedule::whereIn('user_id', [$row->user_id, $row->buddy_user_id])
                            ->whereIn('shift_date', [$row->employee_shift_date->toDateString(), $row->buddy_shift_date->toDateString()])
                            ->orderBy('user_id', 'ASC')->orderBy('shift_date', 'ASC')->get();

                foreach ($schedules as $schedule) {
                    if($row->buddy_user_id == $schedule->user_id) {
                        $my_shift_date[$schedule->id] = $schedule->shift_date . ' ' . $schedule->schedule;
                    } else {
                        $buddy_shift_date[$schedule->id] = $schedule->shift_date . ' ' . $schedule->schedule;
                    }
                }

                $data[$n]['week_ending'] = '';
                $data[$n]['employee_shift_date'] = $my_shift_date;
                $data[$n]['buddy_shift_date'] = $buddy_shift_date;
                $data[$n]['employee_schedule'] = '';
                $data[$n]['buddy_schedule'] = '';
            } elseif($row->swap_type == '2rd') {
                $my_rd_shift_date = [];
                $buddy_rd_shift_date = [];

                $schedules = SwapAgentSchedule::whereIn('user_id', [$row->user_id, $row->buddy_user_id])
                             ->whereWeekEnding($row->week_ending)->whereSchedule('Off')
                             ->orderBy('shift_date', 'ASC')->get();

                $my_rd_shift_date_api = [];
                $buddy_rd_shift_date_api = [];
                foreach ($schedules as $schedule) {
                    if($row->buddy_user_id == $schedule->user_id){
                        $my_rd_shift_date_api[] = [ "id" => $schedule->id, "shift_date" => $schedule->shift_date ];
                        $my_rd_shift_date[$schedule->id] = $schedule->shift_date;
                    } else {
                        $buddy_rd_shift_date_api[] = [ "id" => $schedule->id, "shift_date" => $schedule->shift_date ];
                        $buddy_rd_shift_date[$schedule->id] = $schedule->shift_date;
                    }
                }

                $data[$n]['week_ending'] = $row->week_ending;
                $data[$n]['employee_shift_date'] = $my_rd_shift_date;
                $data[$n]['buddy_shift_date'] = $buddy_rd_shift_date;

                // For Mobile API
                $data[$n]['employee_shift_date_api'] = $my_rd_shift_date_api;
                $data[$n]['buddy_shift_date_api'] = $buddy_rd_shift_date_api;
                // Empty Schedule on 2 RD
                $data[$n]['employee_schedule'] = '';
                $data[$n]['buddy_schedule'] = '';
            }

            $n++;
        }

        return $data;
    }

    public function shift_swap_record($id)
    {
        $n = 0;
        $data = [];
        $date_now = Carbon::now();

        $row = SwapAgentReqeust::whereId($id)->first();

        $data[$n] = [
            'tracking_number' => $row->id,
            'created_at' => Carbon::parse($row->created_at)->toDateTimeString(),
            'status' => $row->status,
            'buddy_name' => $row->buddy_user->name,
            'swap_type' => $row->swap_type,
            'swap_type_name' => $row->swap_type_name
        ];

        // Check if Expired
        if(is_null($row->status) && $date_now->gte($row->expiration_date)) {
            $data[$n]['status'] = 2;
        }

        if($row->swap_type == 'sd' || $row->swap_type == 'srd') {
            // Empty Week Ending on Single Day
            $data[$n]['week_ending'] = '';
            $data[$n]['employee_shift_date'] = $row->employee_shift_date->toDateString();
            $data[$n]['employee_schedule'] = $row->employee_schedule;
            $data[$n]['buddy_shift_date'] = $row->buddy_shift_date->toDateString();
            $data[$n]['buddy_schedule'] = $row->buddy_schedule;
        } elseif($row->swap_type == 'shrd') {
            $my_shift_date = [];
            $buddy_shift_date = [];

            $schedules = SwapAgentSchedule::whereIn('user_id', [$row->user_id, $row->buddy_user_id])
                        ->whereIn('shift_date', [$row->employee_shift_date->toDateString(), $row->buddy_shift_date->toDateString()])
                        ->orderBy('user_id', 'ASC')->orderBy('shift_date', 'ASC')->get();

            foreach ($schedules as $schedule) {
                if($row->buddy_user_id == $schedule->user_id) {
                    $my_shift_date[$schedule->id] = $schedule->shift_date . ' ' . $schedule->schedule;
                } else {
                    $buddy_shift_date[$schedule->id] = $schedule->shift_date . ' ' . $schedule->schedule;
                }
            }

            $data[$n]['week_ending'] = '';
            $data[$n]['employee_shift_date'] = $my_shift_date;
            $data[$n]['buddy_shift_date'] = $buddy_shift_date;
            $data[$n]['employee_schedule'] = '';
            $data[$n]['buddy_schedule'] = '';
        } elseif($row->swap_type == '2rd') {
            $my_rd_shift_date = [];
            $buddy_rd_shift_date = [];

            $schedules = SwapAgentSchedule::whereIn('user_id', [$row->user_id, $row->buddy_user_id])
                         ->whereWeekEnding($row->week_ending)->whereSchedule('Off')
                         ->orderBy('shift_date', 'ASC')->get();

            foreach ($schedules as $schedule)
            {
                if($row->buddy_user_id == $schedule->user_id) {
                    $my_rd_shift_date[$schedule->id] = $schedule->shift_date;
                } else {
                    $buddy_rd_shift_date[$schedule->id] = $schedule->shift_date;
                }
            }

            $data[$n]['week_ending'] = $row->week_ending;
            $data[$n]['employee_shift_date'] = $my_rd_shift_date;
            $data[$n]['buddy_shift_date'] = $buddy_rd_shift_date;
            // Empty Schedule on 2 RD
            $data[$n]['employee_schedule'] = '';
            $data[$n]['buddy_schedule'] = '';
        }

        return $data;
    }

    public function ss_we_and_my_sd($id, Request $request)
    {
        $schedule = '';
        $user_id = $id;
        $buddy_avaya = $request->buddy_avaya;
        $swap_type = $request->swap_type;
        $now = Carbon::now();
    	$datetime_now = $now->toDateTimeString();
        $buddy_information = SwapAgentInformation::whereAvaya($buddy_avaya)->orderBy('id', 'DESC')->first();

    	$week_endings = SwapAgentSchedule::whereUserId($user_id)->whereNull('restrictions')
                    ->whereRaw('expiration_date >= NOW()')
    				->groupBy('week_ending')
    				->orderBy('week_ending', 'ASC')
    				->get(['week_ending']);

        $shift_dates_query = SwapAgentSchedule::whereUserId($user_id)->whereNull('restrictions')->whereRaw('expiration_date >= NOW()');
        $buddy_shift_dates_query = SwapAgentSchedule::whereUserId($buddy_information->user_id)->whereNull('restrictions')
                    ->whereRaw('expiration_date >= NOW()');

        if($swap_type == 'sd') {
            $shift_dates_query->where('schedule', '<>', 'Off');
            $buddy_shift_dates_query->where('schedule', '<>', 'Off');
        } elseif($swap_type == 'srd' || $swap_type == 'shrd') {
            $shift_dates_query->where('schedule', 'Off');
            $buddy_shift_dates_query->where('schedule', 'Off');
        }

        $shift_dates = $shift_dates_query->orderBy('shift_date', 'ASC')->get(['shift_date', 'schedule']);
        $buddy_shift_dates = $buddy_shift_dates_query->orderBy('shift_date', 'ASC')->get(['shift_date', 'schedule']);

    	$data = [ 'week_endings'=>$week_endings, 'shift_dates'=>$shift_dates, 'buddy_shift_dates'=>$buddy_shift_dates ];

    	return response()->json($data);
    }
}