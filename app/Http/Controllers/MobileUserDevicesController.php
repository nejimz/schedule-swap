<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MobileUserDevicesController extends Controller
{
    public function register(Request $request)
	{
		$check_device = new \App\MobileUserDevices;
		$devices = $check_device->where('device_name', '=', $request->device_name)
			->where('device_name', '=', $request->device_name)
			->orderBy('id','desc')
			->first();
		// return $devices;
		if(!is_null($devices))
		{
			// Update Token
			$devices->user_id = $request->user_id;
			$devices->save();
			return $json['errors']['message'] = "Token has been updated";
		}
		else
		{
			// Register Device
			$data = [];
			$data['user_id'] = $request->user_id;
			$data['device_name'] = $request->device_name;
			$data['registration_token'] = $request->register_token;

			\App\MobileUserDevices::create($data);
			return $json['errors']['message'] = "Token has been registered";
		}
	}
}
