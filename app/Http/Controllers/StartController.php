<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SecurityChallenge;
use Auth;

class StartController extends Controller
{
    public function start()
    {
        return redirect()->route('start.reset');
    }

    public function reset()
    {
        if(Auth::user()->created_at == Auth::user()->updated_at)
        {
             return view('starter.reset');
        }
        return redirect()->route('start.security');
    }

    public function submitReset(Request $request)
    {    
        $rules = [
            'password' => 'required|confirmed|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[@*&`~!$#%]).*$/',
        ];

        $message = [
            'regex' => 'Password must contain: </br>
                        a minimum of 1 lower case letter [a-z] and </br>
                        a minimum of 1 upper case letter [A-Z] and </br>
                        a minimum of 1 numeric character [0-9] and </br>
                        a minimum of 1 special character: @!$#%*'
        ];

        $this->validate($request, $rules, $message);


        $user = Auth::user();
        $user->password = bcrypt($request->password);
        $user->save();

        return redirect()->route('start.security');
    }

    public function security()
    {
        if(Auth::user()->created_at == Auth::user()->updated_at)
        {
             return view('starter.reset');
        }

        if(is_null(Auth::user()->email) || Auth::user()->questions()->count() < 2)
        {
             $challenges = SecurityChallenge::all();
             $data = compact('challenges');
             return view('starter.security', $data);
        }
        return redirect()->route('home');
    }

    public function submitSecurity(Request $request)
    {
        $rules = [
            'email' => 'required|email|unique:users,email',
            'question_1' => 'required|different:question_2',
            'answer_1' => 'required',
            'question_2' => 'required|different:question_1',
            'answer_2' => 'required',
        ];

        $message = [
            'question_1.required' => 'Pleaese select a question for Question #1.',
            'question_1.different' => 'Question#1 must be different from Question#2.',
            'answer_1.required' => 'An answer to question #1 is required.',
            'question_2.required' => 'Pleaese select a question for Question #2.',
            'question_2.different' => 'Question#2 must be different from Question#1.',
            'answer_2.required' => 'An answer to question @2 is required.',
        ];

        $this->validate($request, $rules, $message);

        // Save User Email address
        $user = Auth::user();
        $user->email = $request->email;
        $user->save();

        // Save Question #1
        $security_question_1 = SecurityChallenge::find($request->question_1);
        $user->questions()->attach($security_question_1, ['answer' => $request->answer_1]);

        // Save Question #2
        $security_question_2 = SecurityChallenge::find($request->question_2);
        $user->questions()->attach($security_question_2, ['answer' => $request->answer_2]);

        return redirect()->route('mail.welcome');
    }
}
