<?php

namespace App\Http\Middleware;

use Closure;
use Route;
use Illuminate\Support\Facades\Auth;

class Newbie
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $route = $request->route();
        // $subdomain = $route->parameter('center');
        // $request->request->add(['center'=> $subdomain]);
        // $request->attributes->add(['center'=> $subdomain]);

        if(!in_array('start', Route::getCurrentRoute()->middleware()))
        {
            if(Auth::user()->created_at == Auth::user()->updated_at)
            {
                return redirect('/portal/start');
            }

            if(is_null(Auth::user()->email) || Auth::user()->questions()->count() < 2)
            {
                return redirect('/portal/start/security');
            }
        }

        return $next($request);
    }
}
