<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    public function handle($request, Closure $next)
    {
        if(Auth::user()->isAdmin())
        {
            return $next($request);
        }
        elseif(Auth::user()->isRoot())
        {
            return $next($request);
        }

        abort(404);
    }
}
