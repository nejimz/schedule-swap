<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use App\Center;

class CenterCheck
{
    public function handle($request, Closure $next)
    {
        $row = Center::whereCode($request->center)->first();

        if(is_null($row))
        {
            abort(404);
        }
        elseif(Auth::check())
        {
            return redirect()->route('home');
        }

        return $next($request);
    }
}
