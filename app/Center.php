<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Center extends Model
{
    public $table = "centers";
    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
    	'name', 'code', 'created_at', 'updated_at' 
	];

    public function mails()
    {
        return $this->hasMany('App\MailRecipients');
    }

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
