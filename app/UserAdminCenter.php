<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAdminCenter extends Model
{
    public $table = "user_admin_centers";
    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
    	'center_id', 'user_id', 'created_at', 'updated_at' 
	];

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
