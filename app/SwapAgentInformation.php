<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SwapAgentInformation extends Model
{
    use SoftDeletes;
    
    public $table = "swap_agent_informations";
    public $timestamps = true;

    protected $fillable = [
    	'user_id', 'center_id', 'avaya', 'tier', 'skill', 'wp', 
    	'created_at', 'updated_at', 'deleted_at'
	];

	public function user()
	{
    	return $this->belongsTo('App\User', 'user_id', 'id');
	}

    public function getBrandAttribute()
    {
        $value = str_replace('SUP ', '', strtoupper($this->skill));
        $value = str_replace('ERD ', '', strtoupper($value));
        return trim($value);
        //$value = substr($this->skill, 0, -4);
        //return strtolower(trim($value));
    }

    public function getSkillCleanAttribute()
    {
        $value = substr($this->skill, 0, -4);
        return trim($value);
    }

    public function getIsTier3Attribute()
    {
        // if($this->tier == 3) {
        //     return true;
        // }
        // return false;
        $value = str_contains($this->skill, ['ERD', 'SUP']);
        $skill = explode(' ', strtoupper($this->skill));
        if(in_array('ERD', $skill) || in_array('SUP', $skill)) {
            return true;
        }
        return false;
    }

    public function getIsErdAttribute()
    {
        #$value = str_contains($this->skill, ['ERD', 'SUP']);
        $skill = explode(' ', strtoupper($this->skill));
        if(in_array('ERD', $skill)) {
            return true;
        }
        return false;
    }

    public function getSkillTierAttribute()
    {
        $value = substr($this->skill, -7, 3);
        return strtolower(trim($value));
    }

    public function getShiftDurationAttribute()
    {
        $wp = $this->wp;
        $wp = explode('x', $wp);
        $wp_base = explode(' ', trim($wp[0]));
        $shift_duration = substr($wp_base[1], 0, 1);
        return $shift_duration;
    }
}
