<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MobileUserDevices extends Model
{
	use SoftDeletes;
    public $table = "mobile_users_devices";

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'user_id', 'device_name', 'registration_token', 'created_at', 'updated_at'
	];
}
