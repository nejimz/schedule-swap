<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Lab404\Impersonate\Models\Impersonate;

class User extends Authenticatable 
{
    use HasApiTokens, Notifiable, SoftDeletes, Impersonate;

    public $table = "users";

    protected $fillable = [
        'employee_number', 'first_name', 'last_name', 
        'username', 'email', 'password', 'access', 
        'role', 'center_id',  'active', 
        'created_at', 'updated_at'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['name', 'center'];

    protected $dates = ['deleted_at'];

    public function findForPassport($username) {
        return $this->where('username', $username)->first();
    }

    public function center()
    {
        return $this->belongsTo('App\Center', 'center_id', 'id');
    }

    public function information()
    {
        return $this->hasOne('App\SwapAgentInformation', 'user_id', 'id')->withTrashed();
    }

    public function getNameAttribute()
    {
        $name = $this->last_name . ", " . $this->first_name;
        return $name;
    }

    public function getCenterAttribute()
    {
        return \App\Center::select('id', 'name', 'code')->find($this->center_id);
    }

    // Security Questions
    public function questions()
    {
        return $this->belongsToMany('App\SecurityChallenge', 'security_questions', 'user_id', 'security_challenge_id')
                    ->withPivot('id', 'answer')
                    ->withTimestamps();
    }

    public function isTest()
    {
        $center_list = [1, 2, 3, 9, 7, 4, 6, 5];
        $user_id_list = [];

        $id = $this->role;
        $center_id = $this->center_id;
        $role = $this->role;

        if($this->role == 'root')
        {
            return true;
        }
        elseif(in_array($center_id, $center_list))
        {
            return true;
        }
        
        return false;
    }

    public function isAgent()
    {
        return ($this->role == 'agent' || $this->role == 'root') ? true : false;
    }

    public function isAdmin()
    {
        return ($this->role == 'admin' || $this->role == 'root') ? true : false;
    }

    public function isRoot()
    {
        return ($this->role == 'root') ? true : false;
    }

    public function isEnterprise()
    {
        if($this->center_id == 9)
        {
            return true;
        }
        elseif($this->role == 'root')
        {
            return true;
        }

        return false;
    }
}
