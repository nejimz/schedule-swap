<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailRecipients extends Model
{
    public $table = "mail_recipients";
    public $timestamps = true;

    protected $fillable = [
    	'center_id', 'module', 'type', 'mail', 'created_at', 'updated_at'
	];

    public function center()
    {
        return $this->belongsTo('App\Center');
    }
}