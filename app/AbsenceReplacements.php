<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AbsenceReplacements extends Model
{
    public $table = 'absence_replacements';
    public $timestamps = true;

    protected $dates = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
    	'user_id', 'shift_date', 'schedule_base', 'hours', 'schedule', 'created_at', 'updated_at' 
	];

    public function absence()
    {
        return $this->belongsTo('App\Absence', 'absence_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function information()
    {
        return $this->hasOne('App\SwapAgentInformation', 'user_id', 'user_id');
    }

    public function getStatusNameAttribute()
    {
        $status = $this->status;

        if(is_null($status))
        {
            return 'pending';
        }
        elseif($status == 0)
        {
            return 'denied';
        }
        elseif($status == 1)
        {
            return 'approved';
        }
    }
}
