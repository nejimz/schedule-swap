<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSwapTypeTable extends Migration
{
    public function up()
    {
        Schema::create('swap_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('abbr');
            $table->string('name');
            $table->timestamps();
        });

        App\SwapType::create([ 'abbr' => 'sd',  'name' => 'Single Day' ]);
        App\SwapType::create([ 'abbr' => 'srd', 'name' => 'Single Rest Day' ]);
        App\SwapType::create([ 'abbr' => '2rd', 'name' => '2 Rest Day' ]);
        App\SwapType::create([ 'abbr' => 'shrd','name' => 'Shift - Rest Day' ]);
    }

    public function down()
    {
        Schema::dropIfExists('swap_type');
    }
}