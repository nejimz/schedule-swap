<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSwapAgentReqeuestsTableRemoveEmployeeNumberAndChangeToUserId extends Migration
{
    public function up()
    {
        if (Schema::hasTable('swap_agent_reqeusts'))
        {
            Schema::table('swap_agent_reqeusts', function (Blueprint $table) {
                $table->dropColumn('employee_number');
                $table->dropColumn('buddy_employee_number');

                $table->unsignedInteger('user_id')->index()->after('week_ending');
                $table->foreign('user_id')->references('id')->on('users');

                $table->unsignedInteger('buddy_user_id')->index()->after('employee_schedule');
                $table->foreign('buddy_user_id')->references('id')->on('users');
            });
        }
    }
}
