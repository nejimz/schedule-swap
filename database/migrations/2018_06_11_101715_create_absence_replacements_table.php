<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbsenceReplacementsTable extends Migration
{
    public function up()
    {
        Schema::create('absence_replacements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->index();
            $table->date('shift_date');
            $table->string('schedule_base', 20);
            $table->decimal('hours', 8, 2);
            $table->string('schedule', 50);
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::dropIfExists('absence_replacements');
    }
}
