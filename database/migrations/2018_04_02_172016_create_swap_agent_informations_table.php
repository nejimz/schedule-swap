<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSwapAgentInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('swap_agent_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_number', 50);
            $table->integer('center_id');
            $table->string('avaya', 50);
            $table->string('tier', 50);
            $table->string('skill', 50);
            $table->string('wp', 50);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('swap_agent_informations');
    }
}
