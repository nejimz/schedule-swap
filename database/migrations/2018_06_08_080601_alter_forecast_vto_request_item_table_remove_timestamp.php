<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForecastVtoRequestItemTableRemoveTimestamp extends Migration
{
    public function up()
    {
        if (Schema::hasTable('forecast_vto_request_item'))
        {
            Schema::table('forecast_vto_request_item', function (Blueprint $table) {
                $table->dropColumn(['created_at', 'updated_at']);
            });
        }
    }
}
