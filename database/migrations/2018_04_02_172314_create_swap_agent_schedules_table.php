<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSwapAgentSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('swap_agent_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_number', 50);
            $table->integer('center_id');
            $table->date('shift_date');
            $table->string('schedule', 50);
            $table->string('restrictions')->nullable();
            $table->date('week_ending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('swap_agent_schedules');
    }
}
