<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForecastVtoTableAddExpiredAt extends Migration
{
    public function up()
    {
        if (Schema::hasTable('forecast_vto'))
        {
            Schema::table('forecast_vto', function (Blueprint $table) {
                $table->datetime('expired_at')->nullable()->after('user_id');
            });
        }
    }
}
