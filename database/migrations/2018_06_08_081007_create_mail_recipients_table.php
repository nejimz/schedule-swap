<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailRecipientsTable extends Migration
{
    public function up()
    {
        Schema::create('mail_recipients', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('center_id')->index();
            $table->string('module', 100);// shift swap, vto, vot
            $table->string('type', 10);// to, cc, bcc
            $table->string('mail');// email address
            $table->timestamps();

            $table->foreign('center_id')->references('id')->on('centers');
        });
    }

    public function down()
    {
        Schema::dropIfExists('mail_recipients');
    }
}
