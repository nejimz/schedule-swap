<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAdminCentersTable extends Migration
{
    public function up()
    {
        Schema::create('user_admin_centers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('center_id')->index();
            $table->foreign('center_id')->references('id')->on('centers');
            $table->unsignedInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_admin_centers');
    }
}
