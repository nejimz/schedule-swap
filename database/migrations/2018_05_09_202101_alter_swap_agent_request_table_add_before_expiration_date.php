<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSwapAgentRequestTableAddBeforeExpirationDate extends Migration
{
    public function up()
    {
        if (Schema::hasTable('swap_agent_reqeusts'))
        {
            Schema::table('swap_agent_reqeusts', function (Blueprint $table) {
                $table->datetime('before_expiration_date')->after('status');
            });
        }
    }
}
