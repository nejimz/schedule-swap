<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityLogsTable extends Migration
{
    public function up()
    {
        Schema::create('activity_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('center_id')->index();
            $table->unsignedInteger('user_id')->index();
            $table->string('platform');
            $table->string('action');
            $table->timestamps();

            $table->foreign('center_id')->references('id')->on('centers');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::dropIfExists('activity_logs');
    }
}
