<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForecastVtoRequestItemTable extends Migration
{
    public function up()
    {
        Schema::create('forecast_vto_request_item', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('forecast_vto_request_id')->index();
            $table->unsignedInteger('forecast_vto_id')->index();
            $table->timestamps();

            $table->foreign('forecast_vto_request_id')->references('id')->on('forecast_vto_request');
            $table->foreign('forecast_vto_id')->references('id')->on('forecast_vto');
        });
    }

    public function down()
    {
        Schema::dropIfExists('forecast_vto_request_item');
    }
}
