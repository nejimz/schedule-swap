<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSwapAgentInformationsTableRenameEmployeeNumber extends Migration
{
    public function up()
    {
        if (Schema::hasTable('swap_agent_informations'))
        {
            Schema::table('swap_agent_informations', function (Blueprint $table) {
                $table->dropColumn('employee_number');

                $table->unsignedInteger('user_id')->index();
                $table->foreign('user_id')->references('id')->on('users');
            });
        }
    }
}