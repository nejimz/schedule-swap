<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForecastVotRequestItemTable extends Migration
{
    public function up()
    {
        Schema::create('forecast_vot_request_item', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('forecast_vot_request_id')->index();
            $table->unsignedInteger('forecast_vot_id')->index();

            $table->foreign('forecast_vot_request_id')->references('id')->on('forecast_vot_request');
            $table->foreign('forecast_vot_id')->references('id')->on('forecast_vot');
        });
    }

    public function down()
    {
        Schema::dropIfExists('forecast_vot_request_item');
    }
}
