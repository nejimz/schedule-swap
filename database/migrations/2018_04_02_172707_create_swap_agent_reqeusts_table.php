<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSwapAgentReqeustsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('swap_agent_reqeusts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('swap_type', 50);
            $table->integer('center_id');
            $table->date('week_ending');
            $table->string('employee_number', 50);
            $table->date('employee_shift_date')->nullable();
            $table->string('employee_schedule', 50)->nullable();
            $table->string('buddy_employee_number', 50);
            $table->date('buddy_shift_date')->nullable();
            $table->string('buddy_schedule', 50)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('swap_agent_reqeusts');
    }
}
