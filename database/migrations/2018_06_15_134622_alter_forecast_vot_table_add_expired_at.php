<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForecastVotTableAddExpiredAt extends Migration
{
    public function up()
    {
        if (Schema::hasTable('forecast_vot'))
        {
            Schema::table('forecast_vot', function (Blueprint $table) {
                $table->datetime('expired_at')->nullable()->after('user_id');
            });
        }
    }
}
