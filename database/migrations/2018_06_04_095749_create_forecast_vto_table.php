<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForecastVtoTable extends Migration
{
    public function up()
    {
        Schema::create('forecast_vto', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('center_id')->index();
            $table->string('organization');
            $table->integer('slots');
            $table->date('shift_date');
            $table->time('time_start');
            $table->time('time_end');
            $table->unsignedInteger('user_id')->index();
            $table->timestamps();

            $table->foreign('center_id')->references('id')->on('centers');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::dropIfExists('forecast_vto');
    }
}