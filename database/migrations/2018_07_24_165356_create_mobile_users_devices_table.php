<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobileUsersDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile_users_devices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('device_name', 255);
            $table->string('registration_token', 255);
            $table->timestamps();
            $table->softDeletes();
            $table->index(['id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile_users_devices');
    }
}
