<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSwapAgentScheduleTableAddExpirationDate extends Migration
{
    public function up()
    {
        if (Schema::hasTable('swap_agent_schedules'))
        {
            Schema::table('swap_agent_schedules', function (Blueprint $table) {
                $table->datetime('expiration_date')->after('week_ending');
            });
        }
    }
}
