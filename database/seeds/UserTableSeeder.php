<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*
    	| Root Users
    	*/
		$root_users = [
			[
				'employee_number' => '0112-04994',
				'username' => 'dgquijote',
				'password' => bcrypt('p@ssw0rd'),
				'first_name' => 'Duke Anthony',
				'last_name' => 'Quijote',
				'role' => 'root',
				'timezone' => '',
				'center_id' => 1
			],
			[
				'employee_number' => '0112-04430',
				'username' => 'jaasong',
				'password' => bcrypt('p@ssw0rd'),
				'first_name' => 'Jimmy',
				'last_name' => 'Asong',
				'role' => 'root',
				'timezone' => '',
				'center_id' => 1
			],
			[
				'employee_number' => '0115-14401',
				'username' => 'jctillaflor',
				'password' => bcrypt('p@ssw0rd'),
				'first_name' => 'Jenelyn',
				'last_name' => 'Tillaflor',
				'role' => 'root',
				'timezone' => '',
				'center_id' => 1
			],
			[
				'employee_number' => '0112-05038',
				'username' => 'nbzaragoza',
				'password' => bcrypt('p@ssw0rd'),
				'first_name' => 'Nathaniel',
				'last_name' => 'Zaragoza',
				'role' => 'root',
				'timezone' => '',
				'center_id' => 1
			],			
			[
				'employee_number' => '0116-17495',
				'username' => 'mmsapalo',
				'password' => bcrypt('p@ssw0rd'),
				'first_name' => 'Monika Antonette',
				'last_name' => 'Sapalo',
				'role' => 'root',
				'timezone' => '',
				'center_id' => 1
			],
		];
		$this->seed($root_users);
       
    }

    public static function seed($lists)
	{
    	foreach($lists as $list)
    	{
    		$user = new User();
			$user->employee_number = $list['employee_number'];
			$user->username = $list['username'];
	        $user->password= $list['password'];
			$user->email = isset($list['email']) ? $list['email'] : NULL;
			$user->first_name = $list['first_name'];
			$user->last_name = $list['last_name'];
			$user->role = $list['role'];
			$user->timezone =$list['timezone'];
			$user->center_id = $list['center_id'];
	        $user->save();
    	}
    }
}
