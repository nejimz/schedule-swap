<?php

use Illuminate\Database\Seeder;
use App\SecurityChallenge;

class SecurityChallengeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$challenges = [
                    [ 'question' => 'What is the name of your favorite pet?' ],
					[ 'question' => 'What street did you grow up on?' ],
					[ 'question' => 'What is the name of your first grade teacher?' ],
					[ 'question' => 'What is your favorite movie?' ],
					[ 'question' => 'What is your mother`s maiden name?' ]
                ];

        $this->seed($challenges);
    }

    public static function seed($lists)
	{
    	foreach($lists as $list)
    	{
    		$user = new SecurityChallenge();
			$user->question = $list['question'];
	        $user->save();
    	}
    }
}
