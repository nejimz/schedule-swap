<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(CenterTableSeeder::class);
        $this->call(SecurityChallengeTableSeeder::class);
        $this->call(UserTableSeeder::class);
    }
}
