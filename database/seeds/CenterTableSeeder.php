<?php

use Illuminate\Database\Seeder;
use App\Center;
class CenterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$centers = [
                    [ 'name' => 'Bacolod', 'code' => 'BAC' ],
					[ 'name' => 'Cebu', 'code' => 'CEB'],
					[ 'name' => 'Dumaguete', 'code' => 'DMG' ],
					[ 'name' => 'Guatemala', 'code' => 'GUA' ],
					[ 'name' => 'Honduras', 'code' => 'HON' ],
					[ 'name' => 'Guyana', 'code' => 'GEO' ],
					[ 'name' => 'Barranquilla', 'code' => 'BAQ' ],
                    [ 'name' => 'Tijuana', 'code' => 'TIJ' ],
                    [ 'name' => 'Enterprise', 'code' => 'ENT' ]
                ];

        $this->seed($centers);
    }

    public static function seed($lists)
	{
    	foreach($lists as $list)
    	{
    		$user = new Center();
			$user->name = $list['name'];
            $user->code = $list['code'];
	        $user->save();
    	}
    }
}
